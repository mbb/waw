import os
import re
import sys

def raw_bowtie2_index(results_dir, bowtie2_index):
    out = dict()
    from os import listdir
    from os.path import isfile, join
    onlyfiles = [join(bowtie2_index, f) for f in listdir(bowtie2_index) if isfile(join(bowtie2_index, f))]
    out["bowtie2_index"] = onlyfiles

    return(out)

#print(raw_bowtie2_index(sys.argv[1],sys.argv[2])