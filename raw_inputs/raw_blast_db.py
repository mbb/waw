import os
import re
import sys

def raw_blast_db(results_dir, blastdb_dir):
    out = dict()
    from os import listdir
    from os.path import isfile, join

    included_extensions = ['phr','pin', 'psq', 'nhr', 'nin', 'nsq']
    onlyfiles = [join(blastdb_dir,fn) for fn in os.listdir(blastdb_dir)
              if any(fn.endswith(ext) for ext in included_extensions)]

    out["blast_db"] = os.path.splitext(onlyfiles[0])[0] #the key must be the same as the type of input
    return(out)

#print(raw_blast_db(sys.argv[1],sys.argv[2])