import os
import re
import sys

def raw_accel_align_index(results_dir, accel_align_index):
    out = dict()
    from os import listdir
    from os.path import isfile, join
    onlyfiles = [join(accel_align_index, f) for f in listdir(accel_align_index) if isfile(join(accel_align_index, f))]
    out["accel_align_index"] = onlyfiles

    return(out)

#print(raw_accel_align_index(sys.argv[1],sys.argv[2]) )