import os
import re
import sys

def raw_hisat2_index(results_dir,  hisat2_index):
    out = dict()
    from os import listdir
    from os.path import isfile, join
    onlyfiles = [join( hisat2_index, f) for f in listdir(hisat2_index) if isfile(join(hisat2_index, f))]
    out["hisat2_index"] = onlyfiles

    return(out)

#print(raw_hisat2_index(sys.argv[1],sys.argv[2])