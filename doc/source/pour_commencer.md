# Pour commencer

## Prérequis

### Pour utiliser le framework

* python3.6 ou plus récent (voir <https://www.python.org/>)
* oyaml 1.0 ou plus récent (utilisé pour conserver l'ordre dans les fichiers yaml après manipulations pour plus de lisibilité) :<br/>`pip install oyaml==1.0`
* cerberus 1.3.2 ou plus récent (utilisé pour vérifier la validité des fichiers yaml selon des schémas définis) :<br/>`pip install cerberus==1.3.2`

### Optionnel

#### Pour lancer un workflow

* Docker engine pour construire le conteneur (<https://docs.docker.com/engine/install/>)

#### Pour compiler la documentation

* Sphinx 3.2.1 ou plus récent (utilisé pour produire cette documentation) :<br/>`pip install sphinx==3.2.1`
* recommonmark 0.6.0 ou plus récent (permet d'utiliser du markdown avec Sphinx) :<br/>`pip install recommonmark==0.6.0`
* sphinx_rtd_theme 0.5.0 ou plus récent (thème de la documentation) :<br/>`pip install sphinx_rtd_theme==0.5.0`

## Installation

### WAW

Pour installer ce framework il suffit de cloner le dépôt gitlab.

``` console
    $ git clone https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw.git
```

### SAG

Pour pouvoir générer les applications graphiques R Shiny permettant de paramétrer et exécuter les workflows il faut installer le projet SAG (Shiny App Generator) et ses dépendances (voir <https://gitlab.mbb.univ-montp2.fr/jlopez/sag/>)

``` console
    $ git clone https://gitlab.mbb.univ-montp2.fr/jlopez/sag.git
```

Une fois tout ceci installé on peut commencer à générer des workflows à partir des briques outil présentes, ou créer de nouvelles briques.
