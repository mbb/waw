# Ajouter un workflow

## Fichier de description yaml

Pour ajouter un workflow dans WAW il faut tout d'abord rédiger le fichier de description au format yaml. Pour cela on peut se baser sur le template de base `workflow_template.yaml` disponible dans le dossier `workflows` de WAW.

### Infos générales

* `name` : Nom du workflow (sans espaces)
* `docker_name` : Nom à utiliser pour le conteneur docker ("en minuscule")
* `description` : Description du workflow
* `version` : Version du workflow
* `author` : Auteur du workflow
* `input` : Entrée de base du workflow (choix parmi les options dans le dossier raw_inputs)

exemple :

``` yaml
{
  name: "My_Workflow",
  docker_name: "my_workflow",
  description: "My workflow for ...",
  version: "1.0.0",
  author: "Me",
  input: "raw_reads",
  ...
```

### Étapes

Le champ `steps` permet de spécifier les différentes étapes d'un workflow. Pour chaque étape on spécifie :

* `title` : le titre de l'étape (pour l'interface graphique)
* `name` : le nom de l'étape (sans accents, sans espaces et sans tirets)
* `tools` : le ou les outils pour réaliser cette étape parmi les outils dans le dossier tools (l'outil "null" permettant de rendre l'étape optionnelle)
* `default` : l'outil par défaut pour réaliser cette étape

exemple :

``` yaml
steps:
  [
    { title: "Quality check", name: "quality_check", tools: ["fastqc"], default: "fastqc" },
    { title: "Trimming", name: "trim", tools: ["trimmomatic"], default: "trimmomatic" },
    { title: "Quality check post trimming", name: "quality_check_post_trim", tools: ["fastqc"], default: "fastqc" },
  ...
```

### Inputs des étapes

Le champ `steps_in` est celui qui fait le lien entre les différentes étapes du workflow. Il est structuré de la façon suivante.

``` yaml
{ ...
  { step_name: "", tool_name: "", rule_name: "",
    params: [
      { input_name: "", origin_step: "", origin_command: "", origin_name: "" },
    ]
  }
```

Pour une étape, un outil, et une commande donnée, on va lister les inputs de la commande et les relier à une autre étape, un outil et une commande. Si on prends les 3 étapes décrites précédement (en se limitant à des reads single end) on a :

``` yaml
{ ...
  { step_name: "quality_check", tool_name: "fastqc", rule_name: "fastqc_SE",
    params: [
      { input_name: "read", origin_step: "", origin_command: "raw_reads", origin_name: "read" },
    ]
  },
  { step_name: "trim", tool_name: "trimmomatic", rule_name: "trimmomatic_SE",
    params: [
      { input_name: "read", origin_step: "", origin_command: "raw_reads", origin_name: "read" },
    ]
  },
  { step_name: "quality_check_post_trim", tool_name: "fastqc", rule_name: "fastqc_SE",
    params: [
      { input_name: "read", origin_step: "trim", origin_command: "trimmomatic_SE", origin_name: "read_clean" },
    ]
  },
```

Les deux premières étapes prennent en entrée les `raw_reads` (l'input principal du workflow).

Pour l'étape `quality_check`, l'outil `fastqc` et la commande `fastqc_SE` on a un seul input qui s'appelle `read`. Cet input est fourni par la commande `raw_reads` qui n'est pas rattachée à une étape et a un output nommé `read`. On retrouve la même chose pour l'étape `trim`.

Pour l'étape `quality_check_post_trim` , l'outil `fastqc` et la commande `fastqc_SE` on a ici aussi un seul input qui s'appelle `read`. Cet input est cette fois issu de l'étape `trim`, de la commande `trimmomatic_SE` qui a un output nommé `read_clean`.

``` Note:: Les cas où une étape est optionnelle ou possède plusieurs choix d'outils ne sont pas encore gérés par le framework pour la génération automatique. Il est tout de même possible de les faire fonctionner en ajustant manuellement la partie snakefile du workflow.
```

### Paramètres égaux

Dans certains cas des paramètres de différents outils vont nécessairement avoir la même valeur. On peut alors retirer certains paramètres superflus de l'interface. Pour cela la section `params_equals` permet de donner une liste de couples de paramètres égaux.

exemple :

``` yaml
params_equals: [
  { param_A: kmer_counting__jellyfish_count_kmer_len, param_B: kmer_analysis__genomescope_kmer_len },
  ...
```

Ici retire le paramètre `genomescope_kmer_len` de l'étape `kmer_analysis` d'un workflow pour ne garder que le paramètre `jellyfish_count_kmer_len` de l'étape `kmer_counting` qui vaut la même valeur.

## Workflow Snakefile

Le fichier snakefile d'un workflow peut être généré à l'aide du script `generate_workflow_snakefile.py`. Le script doit être lancé depuis le dossier WAW et il suffit de lui passer le nom du workflow. A partir du fichier `workflow.yaml` décrit précédement on va générer les fonctions d'input des règles et la fonction d'output des étapes du workflow.

Avec le workflow en exemple ci dessus on obtient :

``` python
# File generated with generate_workflow_snakefile.py

{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

# Tools inputs functions
def quality_check__fastqc_SE_inputs():
  inputs = dict()
  inputs["read"] = raw_reads["read"]
  return inputs

def trim__trimmomatic_SE_inputs():
  inputs = dict()
  inputs["read"] = raw_reads["read"]
  return inputs

def quality_check_post_trim__fastqc_SE_inputs():
  inputs = dict()
  inputs["read"] = rules.trim__trimmomatic_SE.output.read_clean
  return inputs


{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
  outputs = list()
  if (step == "quality_check" and config['SeOrPe'] == 'SE' ):
    outputs = expand(rules.quality_check__fastqc_SE.output , sample=SAMPLES)

  if (step == "trim" and config['SeOrPe'] == 'SE' ):
    outputs = expand(rules.trim__trimmomatic_SE.output , sample=SAMPLES)

  if (step == "quality_check_post_trim" and config['SeOrPe'] == 'SE' ):
    outputs = expand(rules.quality_check_post_trim__fastqc_SE.output , sample=SAMPLES)

  if (step == "all"):
    outputs = list(rules.multiqc.output)

  return outputs

#########
# Rules #
#########

{import rules}
{import global_rules}
```

### Partie Inputs

Dans cette partie sont définies les fonctions d'input des outils du workflow. On utilise l'objet `rules` définit par Snakemake pour.

Reprenons la fonction `quality_check_post_trim__fastqc_SE_inputs()` dans le cas ou l'étape de streaming possède deux outils disponibles. Il suffit de tester l'outil choisi à l'étape `trim` pour savoir quel input utiliser.

exemple :

``` python
def quality_check_post_trim__fastqc_SE_inputs():
  inputs = dict()
  if config["trim"] == "trimmomatic":
    inputs["read"] = rules.trim__trimmomatic_SE.output.read_clean
  if config["trim"] == "cutadapt":
    inputs["read"] = rules.trim__cutadapt_SE.output.read_trimmed
  return inputs
```

### Partie outputs

Dans cette partie on trouve la fonction `step_outputs` qui permet d'obtenir les outputs d'une étape.

Dans le cas ou l'étape de streaming possède deux outils disponibles il faut ici aussi ajouter un test sur l'outil utilisé à l'étape `trim` pour fournir les bonnes sorties.

exemple :

``` python
def step_outputs(step):
  outputs = list()
  if (step == "quality_check" and config['SeOrPe'] == 'SE' ):
    outputs = expand(rules.quality_check__fastqc_SE.output , sample=SAMPLES)

  if (step == "trim" and config['SeOrPe'] == 'SE' ):
    if config["trim"] == "trimmomatic":
      outputs = expand(rules.trim__trimmomatic_SE.output , sample=SAMPLES)
    if config["trim"] == "cutadapt":
      outputs = expand(rules.trim__cutadapt_SE.output , sample=SAMPLES)

  if (step == "quality_check_post_trim" and config['SeOrPe'] == 'SE' ):
    outputs = expand(rules.quality_check_post_trim__fastqc_SE.output , sample=SAMPLES)

  if (step == "all"):
    outputs = list(rules.multiqc.output)

  return outputs
```
