# Documentation de WAW

```eval_rst
.. toctree::
   :hidden:

   pour_commencer
   Concepts_de_base/index
   Fonctionnement/index
   ajout_outil
   ajout_outil_exemple
   ajout_workflow
   generer_workflow
   utiliser_workflow
   subwaw
```

WAW (Workflow Application Wrapper) est un framework de génération automatique de workflows. Ce framework permet de générer un workflow ainsi que son environnement d'exécution et de l'utiliser avec ou sans interface graphique sur différentes infrastructures (machine personnelle, un serveur de calcul, un cluster, un cloud, etc).

Ce framework intègre environ 80 briques d'applications bioinformatiques assemblables pour former une grande variété de workflows d'analyse de données.

Ce framework repose sur plusieurs technologies existantes comme [Snakemake](https://snakemake.readthedocs.io/en/stable/), [Docker](https://www.docker.com/), [R Shiny](https://shiny.rstudio.com/) et [MultiQC](https://multiqc.info/).

## Fonctionnement de WAW

![Schéma de fonctionnement de WAW](../images/a_to_z_waw.png)

Nous partons de fichiers détaillant d'une part les outils d'analyses avec leurs paramètres, leurs entrées et sorties, leur méthode d'installation, etc (voir partie [Brique outil](./Concepts_de_base/index.html#les-briques-outils)), et d'autre part la description des workflows avec l'enchaînement des étapes et le cheminement des données d'une étape à l'autre (voir partie [Description de workflow](./Concepts_de_base/index.html#les-workflows)).

Nous avons ensuite développé différents scripts pour générer l'environnement permettant l'utilisation du workflow : la recette du workflow en Snakemake, une recette de conteneur Docker ainsi qu'une application Shiny permettant de paramétrer et exécuter le workflow. Le conteneur est alors autonome et contient les versions figées des outils ainsi que le workflow Snakemake et l'application Shiny.

## Résultat

### Un ensemble de fichiers

Le framework permet de générer ensemble de fichiers comprenant plusieurs dossiers :

* Un dossier files avec les fichiers liés au workflow (Snakefile, fichier de params, scripts, ...)

![arborescence workflow (files)](../images/arbo_workflow_1.png)

* Un dossier sagApp pour l'application shiny

![arborescence workflow (sagApp)](../images/arbo_workflow_2.png)

Dans cet ensemble de fichiers générés on trouve aussi des scripts permettant de déployer le workflow sur une machine (machine perso, cluster, instance dans un cloud). A l'aide de ces scripts une image Docker est construite avec toutes les dépendances requises.
La procédure de déploiement permet également de lancer un conteneur qui va héberger une application shiny pour le workflow ainsi que la possibilité d'y accéder en ligne de commande.

![arborescence workflow (deploy)](../images/arbo_workflow_3.png)

### Interface graphique

Le workflow est utilisable à l'aide d'une interface graphique. Cette interface est générée elle aussi à partir des fichiers de description des outils et des workflows. Elle permet de paramétrer le workflow avec les données en entrée, le dossier de sortie et les paramètres des différents outils.

<center><h4>Visuel de l'interface graphique</h4></center>

![Visuel de l'interface graphique](../images/shiny_app.png)

### Rapport final

Après avoir exécuté le workflow on obtient un rapport html généré par MultiQC reprenant les informations générales du workflow (paramètres utilisés, liste des fichiers de sortie, représentation graphique du workflow, version des outils, ...) et des résumés des résultats des différentes étapes sous différentes formes (graphes, tableaux, ...).

<center><h4>Rapport MultiQC</h4></center>

![Rapport MultiQC](../images/report_mqc.png)
