# Utiliser un workflow

Pour utiliser un workflow généré par le framework il suffit d'avoir le dossier généré et docker installé sur sa machine. On peut utiliser les scripts de déploiement.

## Script de déploiement local

Le script de déploiement local va permettre de construire (ou télécharger) le conteneur et de le lancer avec les paramètres adéquats sur votre machine.
Il prend en paramètre le dossier où se trouvent les données et le dossier où écrire les résultats (les dossiers sont créés si inexistants).

Le dernier paramètre est optionnel et permet de spécifier si l'on utilise la version disponible sur le dockerhub (pour les workflows du dossier workflows de WAW).

``` console
  $ bash deployLocalHost.sh dataDir resultsDir '[dockerHub|local]'
```

``` Note:: Dans le cas d'un workflow non présent où pour être certain d'utiliser la dernière version du workflow il faut utiliser le paramètre "local".
```

Le script se déroule et va afficher l'adresse web pour accéder à l'interface du workflow ainsi que la commande pour lancer une session bash dans le conteneur.

## Déploiement manuel

Le déploiement manuel se passe en 2 étapes :

1. Construction de l'image
2. Lancer un conteneur

### Construction de l'image

Pour construire l'image il suffit de se placer dans le dossier du workflow à la racine (où se trouve le Dockerfile). La commande `docker build` a besoin d'un nom pour l'image et du chemin vers le dossier contenant le dockerfile (ici le dossier courant `.`)

``` console
  $ docker build -t 'workflow_name' .
```

``` Note:: Cette construction peut être longue selon le nombre d'outils à installer dans le conteneur
```

### Lancer un conteneur

Une fois l'image construite il reste à lancer un conteneur. Pour cela on utilise la commande `docker run`.

``` console
  $ docker run -p 127.0.0.1:80:3838 -v /path/to/results/dir/:/Results -v /path/to/data/dir/:/Data workflow_name
```

Le paramètre `-p` permet de faire le lien entre le port ouvert dans le conteneur (ici 3838) et un port de la machine ou s'exécute le conteneur (ici on choisit le port 80) et l'adresse 127.0.0.1 qui correspond au localhost.

Le paramètre `-v` permet de faire le lien entre des dossiers sur la machine hôte et le conteneur afin d'y accéder depuis le conteneur. Un workflow a besoin d'avoir accès à 2 dossiers (`/Results` et `/Data`). Il suffit donc de lier le dossier ou se trouvent vos données au dossier `/Data` et de lier le dossier dans lequel vous voulez avoir les résultats des analyses avec `/Results`

Une fois la commande lancée vous avez accès à l'interface graphique dans un navigateur web à l'adresse indiquée (ici 127.0.0.1:80).
