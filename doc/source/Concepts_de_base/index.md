# Briques du framework

Le framework se base sur deux concepts clés :

* les [workflows](#les-workflows)
* les [outils](#les-briques-outils) (tools).

Les workflows sont composé d'un ou plusieurs outils. Les workflows et les outils sont composés de plusieurs fichiers décrivant leur structure et leur fonctionnement.

## Les workflows

Un workflow est composée de plusieurs fichiers :

* Un fichier de description YAML (*workflow_name.yaml*)
* Un fichier Snakemake (*workflow.snakefile*)

et dans certains cas :

* Un dossier data (qui contient des données liées à l'utilisation du workflow, par exemple une base de données)

A l'aide de certains scripts, ces fichiers seront parssés et permettrons de produire un fichier Snakemake, une application Shiny, un Dockerfile (pour générer une image Docker) ainsi que d'autres fichiers utilitaires.

### Description du workflow

Le fichier de description au format YAML contient de multiples informations sur le workflow.

#### Informations générales

* `name` : Nom du workflow
* `docker_name` : Nom de l'image Docker
* `description` : Description du workflow
* `version` : Version du workflow
* `author` : Auteur du workflow

Ces informations permettent le référencement du workflow. Parmi ces informations on trouve le `docker_name` qui est une version sans majuscule du nom du workflow qui pourra être donnée à l'image du workflow (docker exige qu'il n'y ait pas de majuscule dans le nom d'un conteneur).

exemple :

``` yaml
{
  name: "Genome_Profile",
  docker_name: "genomeprofiler",
  description: "Infer the global properties of sa genome from unassembled sequenced data",
  version: "0.0.1",
  author: "MBB",
  ...
```

#### Input

Le champ `input` du fichier de description est optionnel. Il correspond à l'entrée standard du workflow (des reads, des bams, des long reads, ...). La valeur doit correspondre à un des type d'inputs présent dans le dossier `raw_inputs` de WAW. Ce champ permet plusieurs choses.

1. Importer dans l'interface les controles permettant de selectionner le dossier dans lequels se trouvent les données d'entrée du workflow et potentiellement d'autres options (exemple: reads single end / paired end).
2. Lancer une fonction qui va permettre de détecter les fichiers, d'en extraire le chemin, l'extension et potentiellement un 'sample_name' qui va permettre de généraliser ces fichiers à l'aide d'un wildcard. (Voir partie TODO)

exemple :

``` yaml
input: "raw_reads"
```

#### Steps

Le champ `steps` définit les étapes du workflow ainsi que les outils disponibles pour réaliser ces étapes. En effet il est possible de proposer plusieurs outils lors d'une étape (le choix se fera dans l'interface ou dans le fichier de paramètre du workflow). Il est aussi possible de rendre une étape optionnelle en ajoutant l'outil `"null"`.

``` Attention:: A l'heure actuelle, la génération automatique de workflow fonctionne uniquement avec des étapes non optionnelle et ne comportant qu'un seul outil. Dans le cas contraire quelques adaptations manuelle sont nécessaire (voir partie Ajouter un workflow).
```

exemple :

``` yaml
steps:
  [
    { title: Preprocessing, name: preprocessing, tools: [fastp,"null"], default: "null" },
    { title: K-mer counting, name: kmer_counting, tools: [jellyfish_count], default: jellyfish_count },
    { title: K-mer histogram, name: kmer_histogram, tools: [jellyfish_histo], default: jellyfish_histo },
    { title: K-mer analysis, name: kmer_analysis, tools: [genomescope], default: genomescope },
    ...
```

#### Options

Le champ `options` permet de spécifier des options globales au workflow qui seront présentes dans l'interface. Par défaut il n'y a qu'une seule option qui est le chemin du dossier de résultat de l'analyse.

exemple :

``` yaml
options:
  [
    {
      name: "results_dir",
      type: "output_dir",
      value: "/Results",
      label: "Results directory: ",
      volumes:  [Results: "/Results"]
    },
    ...
```

#### Entrées des étapes

Le champ `steps_in` permet de décrire les liens entre les étapes du workflow. Il sert à générer les fonctions d'input des différents outils du workflow. Pour chaque étape on spécifie l'outil et le nom de la commande (le `rule_name`). Pour chacun des inputs de la commande on donne son origine (étape d'origine, commande d'origine et nom d'origine).

Dans l'exemple qui suit on voit que pour l'étape preprocessing, pour l'outil fastp, pour la règle fastp_PE on a 2 inputs (`read` et `read2`). Ils proviennent tous les deux des inputs du workflow (voir partie [Input](../Concepts_de_base/workflow.html#input) ci dessus). Ils n'ont donc pas d'étape d'origine mais seulement une commande (`raw_reads`) et les noms `read` et `read2` qui sont les outputs de `raw_reads`.

exemple :

``` yaml
{
steps_in: [
  { step_name: preprocessing, tool_name: fastp, rule_name: fastp_PE,
    params: [
      {input_name: read, origin_step: "", origin_command: raw_reads, origin_name: read },
      {input_name: read2, origin_step: "", origin_command: raw_reads, origin_name: read2 }
    ]
  },
  ...
```

En dehors de ce cas particulier, prenons le cas de l'étape kmer_analysis. Cette étape est réalisé par l'outil `genomescope` et sa commande du même nom. Cette commande a un input nommé `kmer_histo`. Cet input est fourni à cette commande par l'outil `jellyfish_histo` à l'étape `kmer_histogram` qui a un output nommé `kmer_histo`.

``` yaml
{ step_name: kmer_analysis, tool_name: genomescope, rule_name: genomescope,
  params: [
    {input_name: kmer_histo, origin_step: "kmer_histogram", origin_command: jellyfish_histo, origin_name: kmer_histo },
  ]
...
```

``` Attention:: A l'heure actuelle, cette description ne permet pas de décrire les cas où l'on a plusieurs outils pour une étape et les cas où une étape est optionnelle. Il faudra donc une intervention manuelle dans le Snakefile pour gérer ces cas (voir partie Ajouter un workflow).
```

#### Paramètres égaux

Dans plusieurs cas lors de la création d'un workflow il arrive que plusieurs outils aient des paramètres qui vont prendre une même valeur (exemple: un même génome de référence, une même taille de k-mer, ...). Dans ce cas la on se retrouve avec ce paramètre plusieurs fois dans l'interface du workflow ce qui rend la saisie fastidieuse. On peut alors préciser dans le chams `params_equals` les paramètres qui auront la même valeur. Ici le paramètre `genomescope_kmer_len` de l'étape `kmer_analysis` prendra la valeur du paramètre `jellyfish_count_kmer_len` de l'étape `kmer_counting`.

exemple :

``` yaml
params_equals: [
  { param_A: kmer_counting__jellyfish_count_kmer_len, param_B: kmer_analysis__genomescope_kmer_len },
  ...
```

Dans certains cas on peut aussi avoir besoin de supprimer le paramètre. Par exemple dans les cas ou le paramètre ne sera pas passé par l'utilisateur mais par une étape précédente (exemple: un outil d'assemblage qui fournis l'entrée d'un autre outil au lieu d'un fichier entré par l'utilisateur).

exemple :

``` yaml
params_equals: [
  { remove: mon_etape__parametre_a_retirer },
  ...
```

#### Cas particuliers

##### Data

Dans certains cas, pour permettre la reproductibilité on inclus dans le dépôt git certains fichiers liés au workflow comme par exemple une base de données. Dans ce cas on indique dans le yaml le nom du fichier ou du dossier de données. Ces données sont stockées dans un dossier data du dossier du workflow.

exemple : Dans un workflow d'assemblage de virus on fournit une base de données blast de virus.

``` yaml
data: [
    {
      name: blast_db_virus,
      type: directory
    }
...
```

#### Description complète

``` yaml
{
  name: Genome_Profile,
  docker_name: genomeprofiler,
  description: "Infer the global properties of a genome from unassembled sequenced data",
  version: "0.0.1",
  author: "MBB",
  input: raw_reads,
  steps:
    [
      { title: Preprocessing, name: preprocessing, tools: [fastp,"null"], default: "null" },
      { title: K-mer counting, name: kmer_counting, tools: [jellyfish_count], default: jellyfish_count },
      { title: K-mer histogram, name: kmer_histogram, tools: [jellyfish_histo], default: jellyfish_histo },
      { title: K-mer analysis, name: kmer_analysis, tools: [genomescope], default: genomescope },
    ],
  options:
    [
      {
        name: "results_dir",
        type: "output_dir",
        value: "/Results",
        label: "Results directory: ",
        volumes:  [Results: "/Results"]
      },
    ],
    steps_in: [
      { step_name: preprocessing, tool_name: fastp, rule_name: fastp_PE, 
        params: [
          {input_name: read, origin_step: "", origin_command: raw_reads, origin_name: read },
          {input_name: read2, origin_step: "", origin_command: raw_reads, origin_name: read2 }
        ]
      },
      { step_name: preprocessing, tool_name: fastp, rule_name: fastp_SE, 
        params: [
          {input_name: read, origin_step: "", origin_command: raw_reads, origin_name: read },
        ]
      },
      { step_name: kmer_counting, tool_name: jellyfish_count, rule_name: jellyfish_count,
        params: [
          {input_name: read, origin_step: "", origin_command: raw_reads, origin_name: read },
          {input_name: read2, origin_step: "", origin_command: raw_reads, origin_name: read2 },
          {input_name: read, origin_step: "preprocessing", origin_command: fastp_PE, origin_name: read_preprocessed },
          {input_name: read2, origin_step: "preprocessing", origin_command: fastp_PE, origin_name: read2_preprocessed },
        ]
      },
      { step_name: kmer_histogram, tool_name: jellyfish_histo, rule_name: jellyfish_histo, 
        params: [
          {input_name: kmer_counts, origin_step: "kmer_counting", origin_command: jellyfish_count, origin_name: kmer_counts },
        ]
      },
      { step_name: kmer_analysis, tool_name: genomescope, rule_name: genomescope, 
        params: [
          {input_name: kmer_histo, origin_step: "kmer_histogram", origin_command: jellyfish_histo, origin_name: kmer_histo },
        ]
      },
    ],
    params_equals: [
      { param_A: kmer_counting__jellyfish_count_kmer_len, param_B: kmer_analysis__genomescope_kmer_len },
    ]
}
```

### Snakemake du workflow

Le snakemake du workflow est la base du workflow. Il va rassembler les règles de chacun des outils du workflow, les fonctions d'input de chacune de ces règles et des règles génériques présentes pour tous les workflows (exemple: `prepare_report` et `multiqc`).

``` Note:: Le fichier Snakemake peut être généré à partir du fichier yaml à l'aide du script *generate_workflow_snakefile.py* présent à la racine du dépôt. Cela permet de compléter les parties 'input' et 'output'.
```

``` Attention:: A l'heure actuelle, dans le cas où l'on a plusieurs outils pour une même étape ou une étape optionnelle les parties 'input' et 'output' du Snakefile sont à ajuster manuellement.
```

#### Imports globaux

La première partie du Snakefile est l'import de différentes librairies et l'import des différentes parties du fichier de config dans des variables globales. Cette partie est définie dans un fichier `global_imports.py` qui sera importé pendant la génération du workflow en remplaçant `{import global_imports}` par le contenu du fichier (voir partie [Génération du Snakefile](../Fonctionnement/snakefile_generate)).

``` python
{import global_imports}

# est remplacé par

import os
import re
import snakemake.utils
import csv

#############
# Wildcards #
#############

STEPS = config["steps"]
PREPARE_REPORT_OUTPUTS = config["prepare_report_outputs"]
PREPARE_REPORT_SCRIPTS = config["prepare_report_scripts"]
OUTPUTS = config["outputs"]
PARAMS_INFO = config["params_info"]
MULTIQC = config["multiqc"]
config = config["params"]
```

#### Inputs

Dans la partie inputs on définit les fonctions d'inputs des différents outils. Tout d'abord on définit l'input principal du workflow (ici des reads). Cela permet d'appeler la fonction `raw_reads` qui va retourner plusieurs informations. A partir du dossier où se trouvent les fichiers la fonction va parcourir les fichiers et extraire un nom de sample depuis le nom de fichier. La fonction va retourner la liste des sample ainsi que l'adresse des fichiers avec le wildcard sample (voir partie TODO).

exemple :

``` python
##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']
...
```

Pour chaque outil on définit ensuite la (ou les) fonction(s) d'inputs. Ici dans notre exemple l'outil fastp a deux fonctions qui correspondent à la version reads single end (SE) et reads paired end (PE). Elles prennent en inputs un read et un read2 pour la fonction PE. Ces inputs viennent du résultat de la fonction raw_reads vu précédemment.

``` python
...
# Tools inputs functions

def preprocessing__fastp_SE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    return inputs

def preprocessing__fastp_PE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    inputs["read2"] = raw_reads['read2']
    return inputs
...
```

Dans certains cas les inputs d'un outils peuvent dépendre d'un ou plusieurs paramètres. Dans ce cas, les inputs de `jellyfish_count` dépendent tout d'abord du fait que l'étape `preprocessing` est optionnelle. Si l'étape `preprocessing` est réalisée avec fastp on donne les résultats de fastp en inputs, sinon on donne le resultat de la fonction `raw_reads`. Il faut ensuite différencier les cas SE et PE à l'aide du paramètre `SeOrPe` pour donner les bonnes entrée.

Une autre spécificité ici est que nous voulons donner tous nos samples à `jellyfish_count` pour qu'il les traite simultanément et non pas sample par sample. Il nous faut alors utiliser la fonction `expand` qui va permetre de remplace le wildcard sample par les valeurs des différents samples détectés par `raw_reads` (voir partie TODO).

``` python
def kmer_counting__jellyfish_count_inputs():
    inputs = dict()
    if (config["preprocessing"] == "fastp"):
        if (config["SeOrPe"] == "PE"):
            inputs["read"] = expand(rules.preprocessing__fastp_PE.output.R1,sample=SAMPLES)
            inputs["read2"] = expand(rules.preprocessing__fastp_PE.output.R2,sample=SAMPLES)
        else:
            inputs["read"] = expand(rules.preprocessing__fastp_SE.output.read,sample=SAMPLES)
    else:
        inputs["read"] = expand(raw_reads["read"],sample=SAMPLES)
        if (config["SeOrPe"] == "PE"):
            inputs["read2"] = expand(raw_reads["read2"],sample=SAMPLES)
    return inputs
...
```

#### Outputs

Dans la partie outputs on se contente de définir les sorties des différentes étapes. Dans la fonction `step_outputs` pour chaque étape on donne les sorties de l'outil utilisé. Pour cela on utilise l'objet `rules` mis à disposition par Snakemake. Avec cet objet on a facilement accès aux différents règles du Snakefile ainsi qu'à leurs outputs.

exemple :

``` python
###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()

    if (step == "preprocessing"):
        if (config[step] == "fastp"):
            if config["SeOrPe"] == "SE":
                outputs = rules.preprocessing__fastp_SE.output
            elif config["SeOrPe"] == "PE":
                outputs = rules.preprocessing__fastp_PE.output

    if (step == "kmer_counting"):
        if (config[step] == "jellyfish_count"):
            outputs = rules.kmer_counting__jellyfish_count.output

    if (step == "kmer_histogram"):
        if (config[step] == "jellyfish_histo"):
            outputs = rules.kmer_histogram__jellyfish_histo.output

    if (step == "kmer_analysis"):
        if (config[step] == "genomescope"):
            outputs = rules.kmer_analysis__genomescope.output

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

```

#### Rules

La partie rules est entièrement générée à partir des règles des outils et des règles globales identiques à tous les workflows. Les règles sont importées lors de la génération (voir partie [Génération du Snakefile](../Fonctionnement/snakefile_generate))

``` python
#########
# Rules #
#########

{import rules}

{import global_rules}
```

#### Snakefile complet (avant génération)

``` python
{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

# Tools inputs functions

def preprocessing__fastp_SE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    return inputs

def preprocessing__fastp_PE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    inputs["read2"] = raw_reads['read2']
    return inputs

def kmer_counting__jellyfish_count_inputs():
    inputs = dict()
    if (config["preprocessing"] == "fastp"):
        if (config["SeOrPe"] == "PE"):
            inputs["read"] = expand(rules.preprocessing__fastp_PE.output.R1,sample=SAMPLES)
            inputs["read2"] = expand(rules.preprocessing__fastp_PE.output.R2,sample=SAMPLES)
        else:
            inputs["read"] = expand(rules.preprocessing__fastp_SE.output.read,sample=SAMPLES)
    else:
        inputs["read"] = expand(raw_reads["read"],sample=SAMPLES)
        if (config["SeOrPe"] == "PE"):
            inputs["read2"] = expand(raw_reads["read2"],sample=SAMPLES)
    return inputs

def kmer_histogram__jellyfish_histo_inputs():
    inputs = dict()
    if (config["kmer_counting"] == "jellyfish_count"):
        inputs["kmer_counts"] = rules.kmer_counting__jellyfish_count.output.kmer_counts
    return inputs

def kmer_analysis__genomescope_inputs():
    inputs = dict()
    if (config["kmer_analysis"] == "genomescope"):
        inputs["kmer_histo"] = rules.kmer_histogram__jellyfish_histo.output.kmer_histo
    return inputs

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()

    if (step == "kmer_counting"):
        if (config[step] == "jellyfish_count"):
            outputs = rules.kmer_counting__jellyfish_count.output

    if (step == "kmer_histogram"):
        if (config[step] == "jellyfish_histo"):
            outputs = rules.kmer_histogram__jellyfish_histo.output

    if (step == "kmer_analysis"):
        if (config[step] == "genomescope"):
            outputs = rules.kmer_analysis__genomescope.output

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}
```

## Les briques outils

Une brique outil est composée de plusieurs fichiers :

* Un fichier de description YAML (*tool_name.yaml*)
* Un fichier Snakemake (*tool_name.rules.snakefile*)

et dans certains cas :

* Le script de l'outil (si l'outil est par exemple une librairie, on crée alors un script pour l'utiliser, par exemple: edgeR, dada2, ...)
* Un script prepare report (pour créer ou transformer des sorties à insérer dans le rapport)
* Un dossier data (qui contient des données liées à l'utilisation de l'outil, par exemple une base de données)
* Un dossier source (dans certains cas les sources pour l'installation de l'outil)

### Description de l'outil

Le fichier de description au format YAML contient de multiples informations sur l'outil.

#### Infos générales

* Identifiant, nom, description
* Version
* Site internet, documentation, dépôt de code (git, bitbucket, ...)
* Publication
* Identifiant MultiQC (si l'outil est géré par MultiQC)

exemple :

``` yaml
{
  id: fastp,
  name: fastp,
  article: 10.1093/bioinformatics/bty560,
  website: "https://github.com/OpenGene/fastp",
  git: "https://github.com/OpenGene/fastp",
  description: "A tool designed to provide fast all-in-one preprocessing for FastQ files.",
  version: "0.20.0",
  documentation: "https://github.com/OpenGene/fastp",
  multiqc: "fastp",
  ...
```

Ces infos sont utilisés dans l'interface graphique pour aider l'utilisateur d'un workflow. L'information `multiqc` permet de lancer le bon module multiqc et de configurer l'ordre des sections dans le rapport final.

#### Commandes

Chaque commande va correspondre à une règle Snakemake, elles sont décrites avec les informations suivantes :

* Le nom
* Le dossier de sortie
* La catégorie
* La commande (comment lancer l'exécutable)
* Les inputs (nom, type, description, ...)
* Les outputs (nom, type, description, ...)
* Les options ou paramètres

exemple :

``` yaml
commands:
  [
    {
      name: fastp_PE,
      cname: "Fastp PE",
      category: "quality",
      command: fastp,
      output_dir: fastp_PE,
      inputs: [
        { name: read, type: "reads" },
        { name: read2, type: "reads" }
      ],
      outputs:  [
        { name: report_html, type: "html", file: "fastp_report_{sample}.html", description: "Rapport HTML du préprocessing effectué" },
        { name: report_json, type: "json", file: "fastp_report_{sample}.json", description: "Rapport JSON du préprocessing effectué" },
        { name: read_preprocessed, type: "reads", file: "{sample}_R1.fq.gz", description: "Reads R1 préprocessés" },
        { name: read2_preprocessed, type: "reads", file: "{sample}_R2.fq.gz", description: "Reads R2 préprocessés" },
      ],
      options:
        [
          {
            name: fastp_threads,
            prefix: --thread,
            type: numeric,
            value: 4,
            min: 1,
            max: NA,
            step: 1,
            label: "Number of threads to use",
          },
          {
            name: fastp_complexity_threshold,
            prefix: --complexity_threshold,
            type: numeric,
            value: 30,
            min: 0,
            max: 100,
            step: 1,
            label: "The threshold for low complexity filter (0~100)",
          },
          ...
```

Les inputs et outputs vont être utilisés pour lier les outils entre eux pour former le workflow. Les options seront affichées dans l'interface et liées aux paramètres d'exécution de l'outil à l'aide du fichier de paramètres.

#### Installation et dépendances

On retrouve aussi la méthode d'installation de l'outil et de ses dépendences. Cela permettra de générer automatiquement un ficher de recette Docker afin d'installer tous les outils nécessaires pour un workflow donné.

exemple :

``` yaml
install: {
  fastp: [
    "wget https://github.com/OpenGene/fastp/archive/v0.20.0.tar.gz",
    "tar -xvzf v0.20.0.tar.gz",
    "cd fastp-0.20.0",
    "make",
    "mv fastp /opt/biotools/bin/fastp",
    "cd ..",
    "rm -r fastp-0.20.0 v0.20.0.tar.gz "
  ]
  ...
```

#### Citations

On note aussi les articles à citer pour utiliser l'outil (et ses dépendances). Ces références seront agrégées dans le rapport final de l'analyse.

exemple :

``` yaml
citations:  {
  fastp: [
    "Shifu Chen, Yanqing Zhou, Yaru Chen, Jia Gu, fastp: an ultra-fast all-in-one FASTQ preprocessor, Bioinformatics, Volume 34, Issue 17, 01 September 2018, Pages i884-i890, https://doi.org/10.1093/bioinformatics/bty560"
  ]
  ...
```

#### Cas particuliers

##### Script

Lorsque l'outil est une librairie par exemple on a recours à un script pour l'utiliser. Dans ce cas là on met la clé `script: 'tool_name.script.py'` dans le fichier yaml et on place le script dans le dossier de l'outil (exemple: edgeR, dada2, ...).

##### Prepare report

Lorsque l'outil n'a pas de module MultiQC ou quand on veut rajouter une sortie (tableau, graphique, ...) on peut ajouter un script "prepare report" qui sera lancé en fin de workflow et qui à partir des sorties de l'outil sera chargé de produire de nouvelles sorties. On note dans le fichier de descritption le script ainsi que les fichiers qu'il va générer.

exemple :

``` yaml
prepare_report_script:  tool_name.prepare.report.R,
prepare_report_outputs: [
  Summary_tab_mqc.csv,
  Graph_mqc.png
  ...
```

##### Data

Dans certains cas, pour permettre la reproductibilité on inclus dans le dépôt git certains fichiers utilisés par l'outil comme par exemple une base de données. Dans ce cas on indique dans le yaml le nom du fichier ou du dossier de données. Ces données sont stockées dans un dossier data du dossier de l'outil.

exemple :

``` yaml
data: [
    {
      name: data_name,
      type: directory
    }
    ...
```

##### Sources

Dans le cas ou l'on possède le code source de l'outil, on peut l'inclure dans un dossier sources dans le dossier de l'outil et il sera copié dans le conteneur pour permettre son installation.

### Règles Snakemake

Le fichier de règles Snakemake reprend les différentes commandes du fichier de description yaml. Découvrons le petit à petit.

#### Rule name

On retrouve le nom de la commande (ex: `fastp_PE`) dans le nom de la règle Snakemake : `rule <step_name>__fastp_PE` préfixé de `<step_name>__` (<step_name> suivi de 2 underscores). Ce préfixe permet de rendre la règle réutilisable à différentes étapes du workflow, en effet il sera remplacé par le nom de l'étape qui va utiliser cette règle au moment de la génération du workflow.

#### Input

L'input d'une régle est une fonction qui sera défini lors de la génération du workflow et dépendra des étapes précédentes. La fonction est nommée avec le nom de la règle (`<step_name>__fastp_PE`) suivi de `_inputs`. On y ajoute `**` qui est l'opérateur d'unpacking en python. La fonction étant définie pour retourner un dictionnaire l'unpacking va permettre de nommer les inputs avec les clés du dictionnaire et d'y faire correspondre la valeur.

exemple :

``` python
    input:
        **<step_name>__fastp_PE_inputs()

    # Dans le cas ou cette fonction retourne le dictionnaire suivant

    {"read": "/Data/fastq/{sample}_R1.fq", "read2": "/Data/fastq/{sample}_R2.fq"}

    # L'opérateur ** rendra le code précédent équivalent à

    input:
        read = "/Data/fastq/{sample}_R1.fq",
        read2 = "/Data/fastq/{sample}_R2.fq"
```

#### Output

L'output d'une règle est composé des différents fichiers de sortie de l'outil. Il n'a pas vocation à être exhaustif mais doit comporter les fichiers déclarés dans le fichier de descritpion yaml. Ils correspondent aux fichiers qui vont potentiellement être passés en input des autres étapes d'un workflow.

On retrouvre deux types d'output:

* Avec wildcard
* Sans wildcard

Le wildcard est une mécanique de Snakemake permettant de généraliser une adresse avec un paramètre (voir TODO pour l'explication).

Ici dans notre exemple on retrouve le wildcard `{sample}`.

``` python
output:
    report_html = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"] + "/fastp_report_{sample}.html",
    report_json = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"] + "/fastp_report_{sample}.json",
    read_preprocessed = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"] + "/{sample}_R1.fq.gz",
    read2_preprocessed = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"] + "/{sample}_R2.fq.gz"
```

#### Params

La partie params regroupe les paramètres définis dans le fichier de description. Certains paramètres y sont transformés pour être passé de la bonne manière dans la partie shell. Par exemple `correction = "--correction " if config["<step_name>__fastp_correction_PE"] == True else ""`. Ici on transforme le paramètre booléen en l'absence ou la présence du `--correction`.

exemple :

``` python
params:
    command = config["<step_name>__fastp_PE_command"],
    output_dir = config["<step_name>__fastp_PE_output_dir"],
    complexity_threshold = config["<step_name>__fastp_complexity_threshold"],
    report_title = config["<step_name>__fastp_report_title"],
    adapter_sequence = config["<step_name>__fastp_adapter_sequence"],
    adapter_sequence_R2 = config["<step_name>__fastp_adapter_sequence_R2_PE"],
    P = config["<step_name>__fastp_P"],
    correction = "--correction " if config["<step_name>__fastp_correction_PE"] == True else "",
    low_complexity_filter = "--low_complexity_filter " if config["<step_name>__fastp_low_complexity_filter"] == True else "",
    overrepresentation_analysis = "--overrepresentation_analysis " if config["<step_name>__fastp_overrepresentation_analysis"] == True else "",
```

#### Log

Le fichier de log est un fichier qui permet de garder une trace de l'exécution de la règle. Dans le cas de l'exécution d'un workflow si une règle échoue à cause d'une erreur Snakemake conserve le log mais retire les fichiers output qui pourraient être erronnés ou incomplets.

exemple :

``` python
log:
    config["results_dir"] + "/logs/" + config["<step_name>__fastp_PE_output_dir"] + "/{sample}_fastp_log.txt"
```

#### Threads

La partie threads correspond au nombre de threads qui seront attribués à une règle. Ce nombre provient souvent d'un paramètre renseigné par l'utilisateur mais peut être fixe dans certains cas, ou absent ce qui signifie que la règle n'utilisera qu'un seul thread.

exemple :

``` python
threads:
    config["<step_name>__fastp_threads"]
```

#### Shell

La partie shell correspond à l'execution de la règle. On construit ici la ligne de commande qui va permettre de lancer l'outil. Dans cette partie on reprend les autres éléments de la règle à l'intérieur d'accolades (`{input.read}`, `{output.read_preprocessed}`, `{params.correction}`,`{log}`,`{threads}`, voire `{wildcards.sample}` au besoin). On écrit la commande entre guillemets et on peut sauter des lignes pour plus de lisibilité (mais attention à ne pas oublier d'espaces en fin de ligne).

exemple:

``` python
shell:
    "{params.command} "
    "-i {input.read} "
    "-I {input.read2} "
    "-o {output.read_preprocessed} "
    "-O {output.read2_preprocessed} "
    "-w {threads} "
    "{params.correction} "
    "{params.low_complexity_filter} "
    "--complexity_threshold {params.complexity_threshold} "
    "--html {output.report_html} "
    "--json {output.report_json} "
    "--report_title {params.report_title} "
    "--adapter_sequence '{params.adapter_sequence}' "
    "--adapter_sequence_r2 '{params.adapter_sequence_R2}' "
    "{params.overrepresentation_analysis} "
    "-P {params.P} "
    "|& tee {log}"
```

Cette partie shell peut être remplacé par une partie `run:` dans laquelle on peut mettre directement du code python ou par une partie `script:` si notre outil est un script (script R, python ou bash)

#### Règle au complet

``` python
rule <step_name>__fastp_PE:
    input:
        **<step_name>__fastp_PE_inputs()
    output:
        report_html = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"] + "/fastp_report_{sample}.html",
        report_json = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"] + "/fastp_report_{sample}.json",
        read_preprocessed = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"] + "/{sample}_R1.fq.gz",
        read2_preprocessed = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"] + "/{sample}_R2.fq.gz"
    params:
        command = config["<step_name>__fastp_PE_command"],
        complexity_threshold = config["<step_name>__fastp_complexity_threshold"],
        report_title = config["<step_name>__fastp_report_title"],
        adapter_sequence = config["<step_name>__fastp_adapter_sequence"],
        adapter_sequence_R2 = config["<step_name>__fastp_adapter_sequence_R2_PE"],
        P = config["<step_name>__fastp_P"],
        output_dir = config["<step_name>__fastp_PE_output_dir"],
        correction = "--correction " if config["<step_name>__fastp_correction_PE"] == True else "",
        low_complexity_filter = "--low_complexity_filter " if config["<step_name>__fastp_low_complexity_filter"] == True else "",
        overrepresentation_analysis = "--overrepresentation_analysis " if config["<step_name>__fastp_overrepresentation_analysis"] == True else "",
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__fastp_PE_output_dir"] + "/{sample}_fastp_log.txt"
    threads:
        config["<step_name>__fastp_threads"]
    shell:
        "{params.command} "
        "-i {input.read} "
        "-I {input.read2} "
        "-o {output.read_preprocessed} "
        "-O {output.read2_preprocessed} "
        "-w {threads} "
        "{params.correction} "
        "{params.low_complexity_filter} "
        "--complexity_threshold {params.complexity_threshold} "
        "--html {output.report_html} "
        "--json {output.report_json} "
        "--report_title {params.report_title} "
        "--adapter_sequence '{params.adapter_sequence}' "
        "--adapter_sequence_r2 '{params.adapter_sequence_R2}' "
        "{params.overrepresentation_analysis} "
        "-P {params.P} "
        "|& tee {log}"
```

Le fichier de règles Snakemake peut être en partie généré à partir du fichier yaml à l'aide du script *generate_tool_snakefile.py* présent à la racine du dépôt (voir partie [tool.rule.snakefile](../Ajout_outil/tool.html#tool-rule-snakefile)). Cela permet de préremplir les parties input, output, params, log, et threads ainsi que le début de la partie shell. Il reste ensuite à ajuster et à compléter la partie shell.
