import oyaml as yaml
import shutil
import cerberus

def read_yaml(filepath):
    try:
        with open(filepath, 'rb') as file:
            data = yaml.load(file, Loader=yaml.FullLoader)
            #data = yaml.load(file)
            return data
    except IOError as e:
        print("Error in file opening:", e)
    except yaml.YAMLError as exc:
        print("Error in yaml loading:", exc)

def write_yaml(filepath,data):
    noalias_dumper = yaml.dumper.SafeDumper
    noalias_dumper.ignore_aliases = lambda self, data: True
    try:
        with open(filepath, 'w') as file:
            yaml.dump(data, file, default_flow_style=False, Dumper=noalias_dumper)
    except IOError as e:
        print("Error in file opening:", e)

def copy_dir(src,dst):
    try:
        shutil.copytree(src,dst)
    except FileExistsError:
        shutil.rmtree(dst, ignore_errors=True)
        shutil.copytree(src,dst)

def validate_yaml(document_yaml, schema_yaml):
    document = read_yaml(document_yaml)
    schema = read_yaml(schema_yaml)
    v = cerberus.Validator(schema)
    
    res = v.validate(document, schema)

    if (not res):
        exit(v.errors)
    else:
        return res