Ce workflow peut être lancé de manière autonome sur :
  * sur HPC
  * des machines de type BigMem
  * sur un Cloud type IFB <https://biosphere.france-bioinformatique.fr/>
  * sur le cluster MBB (non détaillée)

* En pouvant les modifier en y apporter de légères adaptations


* Dans ce qui suit nous allons voir comment déployer un workflow sur une machine autonome, de lancer ce workflow en mode Web, de le modifier puis de le lancer en mode ligne de commande.

   

  * Remarquer la présence des fichiers suivants :

    * Dockerfile : fichier de recette pour la construction de l'image docker. Cette recette contient l'environnement nécéssaire pour que le worflow tourne (logiciel bioinfo du workflow).

    * Singularity.def : fichier de recette pour la construction de l'image singularity (nécessite les droits root ou l'option --fakeroot). Cette recette contient l'environnement nécéssaire pour que le worflow tourne (logiciel bioinfo du workflow).

    * deployBigMem.sh : permet de déployer un conteneur en mode web sur une machine de type bigmem

    * deployIFB.sh : permet de deployer en mode web sur le cloud IFB (<https://biosphere.france-bioinformatique.fr/cloudweb/login/)>

    * deployLocalHost.sh : script permettant de déployer l'image docker et le workflow sur la machine.

    * waw_workflow.qsub : script de soumission d'un workflow sur le cluster MBB (nécessite une la création de l'image singularity au préalable)

    * RunCmdLine.sh : permet de déployer et executer un workflow en ligne de commande (Nous verrons cela dans une prochaine partie)

    * waw_workflow.sbatch : script bash permettant de soumettre un job sur cluster de calcul slurm. Le script est adapté au MBB workflow et nécéssite des arguments pour être éxécuté.

    * slurm-config.yml : fichier profile décrivant la configuration et les ressources pour l'exécution des différentes étapes du workflow (ressources global au workflow + ressource par défault pour différentes étapes + ressources spécifique à certaines étapes).

    * install.sh : script shell permettant d'installer une partie des requirements sur le cluster de calcul (à faire une seule fois si nécessaire!).

    * {workflow_name}.json : fichier json permettant de regénérer le workflow dans l'interface web subwaw grâce au bouton import json.
    
    * {workflow_name}.png : image du pipeline qui a été générée dans l'interface web subwaw.
    
    * files/envs/ : répertoire contenant les fichiers d'environnement conda utilisés dans les installations de logiciels bioinfo lors de l'exécution des workflows.
    
    * files/scripts/ : répertoire contenant les scripts (python, R, ...) exécutés dans certaines étapes du workflow.
    
    * files/citations.yaml : fichier de métadonnées contenant la liste des articles scientifique utilisés dans le workflow.
    
    * files/versions.yaml : fichier de métadonnées contenant la liste des versions d'outils utilisés dans le workflow.
    
    * files/generate_multiqc_config.py : script utilisé en dernière étape du workflow pour la génération d'un rapport multiqc.
    
    * files/tools.py : script permettant de gérer certaines fonctions nécessaire au bon fonctionnement du workflow (Snakefile).
    
    * files/raw_*.py : script permettant de gérer les inputs nécessaire au bon fonctionnement du workflow (Snakefile).
    
    * files/params.total.yml : fichier contenant le noms des paramètres du workflow, ainsi que les paramètres par défaut. Les paramètres ne doivent pas être modifiés manuellement par l'utilisateur, une application shiny lors du déploiement est prévu à cet usage afin de facilité le paramétrage du workflow.

    * files/Snakefile : script snakemake permettant l'execution et la gestion du workflow (gestions des tâches à éxécuter -> ligne de commande, des ressources, des environnements...)

## Déploiement sur cluster de calcul (HPC):

Ceci peut être fait uniquement si singularity est installé sur le cluster de calcul (HPC) en question. Sinon impossible de déployer votre workflow sur le cluster.

  * En local :
    * Télécharger dans subwaw l'archive.zip (Download)
    * Dézipper l'archive.zip
    * Générer le fichier de paramètre du workflow (input, output, paramètres...)
      * Si l'application MBBworkflow qui a permis de générer le workflow (.zip) est sur ce PC
        * Transférer les données dans le répertoire Data/ qui a été mis en paramètre lors de l'installation du MBBworflow (/scratch/Data)
        * Dans subwaw cliquer sur Run
        * Remplir les paramètres des différentes étapes dans le shiny
        * Cliquer sur Download Config file
        * Renommer le fichier téléchargé en params.total.yml
        * Transférer le fichier dans l'archive.zip à la place du fichier archive/files/params.total.yml
      * Si l'archive.zip a été envoyé par mail et que donc MBBworkflow n'est pas installé en local
        * Créer un répertoire scratch/Data/MonProjet/ ***mkdir -p scratch/Data/MonProjet/***
        * Transférer les données d'input du pipeline dans ce répertoire scratch/Data/MonProjet/
        * Créer un répertoire scratch/Results/MonProjet/ ***mkdir -p scratch/Results/MonProjet/***
        * Installer l'application shiny spécifique au workflow ***bash deployLocalHost.sh scratch/Data/ scratch/Results/ local***
        * Ouvrir le lien http renvoyer en fin d'installation dans un navigateur
        * Remplir les paramètres des différentes étapes dans le shiny
        * Cliquer sur Download Config file
        * Renommer le fichier téléchargé en params.total.yml
        * Transférer le fichier dans l'archive.zip à la place du fichier archive/files/params.total.yml
    * Construire l'image singularity (besoin que singularity soit installé https://docs.sylabs.io/guides/3.0/user-guide/installation.html).
      * Aller dans le répertoire de l'archive (archive/) où est le fichier singularity.def
      * Lancer le build de l'image : ***singularity build --fakeroot Singularity.sif Songularity.def***
  * Sur le cluster :
      * Transférer les fichiers d'entrée du workflow (mis en paramètre dans l'application shiny) dans un repertoire du cluster (i.e. /home/login/MonProjet/Data/). Ces fichiers doivent garder la même organisation qu'en local.
      * Transférer le répertoire de l'archive dezippé (l'archive avec le fichier params.total.yml et Singularity.sif qui ont étéait construit en local) dans un répertoire du cluster (i.e. /home/login/MonProjet/)
      * Installation de l'environnement.
        * 1/ Si conda n'est pas installé sur le cluster
          * Aller dans le répertoire de l'archive
          * Exécuter le fichier install.sh ***bash install.sh***
        * Si conda est installé sur le cluster
          * 2/ Si snakemake n'est pas installé
            * Installé dans un environnement conda snakemake ***mamba create -n MBB_HPC_env -c conda-forge -c bioconda -y snakemake=7.25.0=hdfd78af_1 cerberus=1.3.4=pyhd8ed1ab_0 oyaml=1.0=pyhd8ed1ab_0***
          * 3/ Si snakemake est installé
            * Load du module snakemake ***module load snakemake***
            * Installation de cerberus et oyaml ***pip install cerberus oyaml***
      * Lancer le workflow
        * Aller dans le répertoire de l'archive
        * Lire le help du script waw_workflow.sbatch ***bash waw_workflow.sbatch -h***
        * Exécuter le script waw_workflow.sbatch. Si l'installation de l'environnement a été fait selon le 1/ ou 2/ mettre -i True. Si fait selon le 3/ suivre ce qui est écrit dans le help avec -i False.
        ***sbatch waw_workflow.sbatch -d /home/login/MonProjet/ -r /home/login/MonProjet/Results -m /home/login/MonProjet/Archive/ -s /home/login/MonProjet/Archive/Singularity.sif -i True|False -j 10***




## Déploiement en mode application web :    

  * Lancer ***bash deployLocalHost.sh*** pour voir les paramètres dont il a besoin :

    * dataDir : dossier de la machine Bigmem hôte contenant les données

    * resultsDir : dossier de la machine hôte qui va contenir les résultats de l'analyse

    * le dernier paramètre (optionnel) indique la source de l'image Docker à utiliser
      * dockerHub : l'image sera téléchargée depuis le dépôt Docker Hub (cette option n'est valide que pour les workflows développés par MBB)
      * local : l'image sera construite en local à partir des fichiers sources issus du dépôt Git (il faut choisir cette option pour les workflows non disponibles sur gitHub)

    * Assurez vous que les données soient disponibles dans un dossier ex. :  /home/$USER/datasets/rnaseq/
    
      les fichiers de reads doivent être de la forme :     <sample_name><pair><extension>
      
      pair = _R1 _R2 ou _1 _2 ou vide en single end
      extension libre (fastq, fastq.gz, fq, fq.gz, ...)

      ex paired end : sample1_R1.fastq.gz sample1_R2.fastq.gz
      ex single end : sample1.fastq.gz

    * Créer un dossier pour les résultas ex. : *** mkdir -p /home/$USER/result1 ***

    * lancer :
        *** deployLocalHost.sh /home/$USER/datasets/rnaseq/ /home/$USER/result1 local ***

    * Voir plus bas pour la correspondance entre chemins sur le système hôte et chemins du conteneur

    * Consulter la sortie écran pour voir comment :
      * Accéder au workflow par navigateur web
      * Accéder en *ssh* à l'intérieur du système du conteneur
      * Noter quel est l'identifiant de votre conteneur !!!

    * Pour arrêter le conteneur :
      * *** docker ps *** pour lister les conteneurs
      * *** docker kill ID *** 

  * Modifications du workflow

    ### A/ Ajouter un paramètre à un outil

    Les règles des différentes étapes du workflow sont assemblées dans le fichier  files/Snakefile. Elles sont écrites selon la syntaxe du gestionnaire de workflow Snakemake (<https://github.com/deto/Snakemake_Tutorial)> (<https://www.youtube.com/watch?v=UOKxta3061g&feature=youtu.be)>
    * Ajout du paramètre --gcBias pour corriger les biais de GC des reads à l'outil de quantification SALMON :
      * Ouvrir le fichier Snakemake et aller au rule salmon_quant_PE
      * Repérer la partie shell qui indique comment sera lancé l'outil SALMON
      * Insérer le paramètre --gcBias

        * Relancer le conteneur avec l'option 'local' pour reconstruire l'image avec vos modifs

          ***deployLocalHost.sh /home/$USER/datasets/rnaseq/ /home/$USER/result1 local***

    ### B/ Changer la version d'un outil

    Les procédures d'installation des différente outils nécessaires au bon fonctionnement du workflow sont rassemblées dans un fichier de recette nommé Dockerfile.
    * Ouvrir ce fichier et repérer la partie concernant l'installation de kallisto
    * Liste des versions de kallisto : <https://github.com/pachterlab/kallisto/releases>
    * Modifier le numéro de version pour une version de kallisto plus récente
    * Relancer le conteneur avec l'option 'local' pour reconstruire l'image avec vos modifs

      ***deployLocalHost.sh /home/$USER/datasets/rnaseq/ /home/$USER/result1 local***

    ### C/ Ajouter une étape

    * Deux possibilités :
      * recharger le fichier .json sur l'interface subwaw (http://web.mbb.univ-montp2.fr/subwaw/workflowmanager.php) puis insérer l'étape souhaitée puis télécharger la nouvelle version du workflow
      * Faire une demande via le système de tickets : <https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html>
      
##  Utilisation en mode ligne de commande

Pour ré-utiliser un workflow sur différents fichiers ou avec différents paramètres, il est bon de pouvoir le lancer en ligne de commande.

Il faut pour cela avoir un fichier texte contenant tous les paramètres du workflow.
Ce fichier peur être :
  * Récupéré depuis l'interface Web d'un déploiement comme en [Déploiement en mode application web] puis modifié à vos besoins
  * Récupéré depuis le dossier de résultats d'une analyse effectuée avec ce workflow
  * Directement à partir du modèle par défaut disponible dans files/params.total.yml

* Modifier un ou plusieurs paramètres parmi les suivants :
  * results_dir:
  * sample_dir:
  * group_file:
  * kallisto_index_input:
  ou
  * salmon_index_input:
  * edger_annotations:

* Enregistrer vos modifs dans maconfig.yaml dans par ex. /home/$USER/results1/version2/ et sera visible dans le conteneur sous /Result/maconfig.yaml

  * lancer depuis une console la ligne de commande (ici le paramètre 10 pour utiliser 10 coeurs) :

    ***bash RunCmdLine.sh /home/$USER/datasets/rnaseq/ /home/$USER/results1/version2/ /Results/maconfig.yaml 10***

  * Suivre la progression du workflow

  * A la fin vérifier le contenu de /home/$USER/results1/version2/



##  Correspondance entre dossiers de votre machine et dossiers du conteneur

ex 1 deploiement : ***bash deployBigMem.sh /home/votrelogin/data1/ /home/votrelogin/results1/***

A l'intérieur du conteneur :

* /home/votrelogin/data1/ -> /Data/
* /home/votrelogin/results1/ -> /Results/

ex 2 deploiement : ***bash deployBigMem.sh /share/bio/datasets/rnaseq/ /home/votrelogin/results1/version1/***

A l'interieur du conteneur :

* /share/bio/datasets/rnaseq/ -> /Data/
* /share/bio/datasets/rnaseq/fastqs/ -> /Data/fastqs/
* /share/bio/datasets/rnaseq/reference/ -> /Data/reference/
* /share/bio/datasets/rnaseq/conditions/groups.tsv -> /Results/conditions/groups.tsv
* /home/votrelogin/results1/version1/ -> /Results/

## 6/ Liens utiles

* commandes docker : <https://devhints.io/docker>
* commandes bash : <https://devhints.io/bash>
* système de tickets MBB : <https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html>
* système de réservation de Bigmem : <https://mbb.univ-montp2.fr/grr/login.php>
* cloud IFB : <https://biosphere.france-bioinformatique.fr/>
* cluster mbb : ssh login@cluster-mbb.mbb.univ-montp2.fr
* depôts Git des MBBworkflows : <https://gitlab.mbb.univ-montp2.fr/mmassaviol/wapps>
* dépôt Git du framework de conception MBBworkflows : <https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw>
* les conteneurs docker des MBBworkflows : <https://hub.docker.com/search?q=mbbteam&type=image>
