unzippedWF=$1
data=$2
results=$3
condabin=$4

#ex when running inside an unzipped WF 
#bash RunShinySlurm.sh ./ ../data/ ../results/ ../condabin/

jobid=$(sbatch --parsable ShinySlurm.sh $unzippedWF $data $results $condabin  )
workdir=$(squeue --format=%Z --noheader -j $jobid)
logfile=$workdir/shiny-$jobid.log
errfile=$workdir/shiny-$jobid.err
echo -n "Waiting for notebook to start  "

# wait for the logfile to appear and be not-empty
sp="/-\|"
while [ ! -s $logfile ] 
do
    printf "\b${sp:i++%${#sp}:1}"
    sleep 1;
done

. $logfile
cat $logfile
