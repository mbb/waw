
# 1- Description


Ce readme a pour but de décrire le déploiement d'un workflow MBB sur un cluster de calcul slurm (HPC). Le déploiement se fera à partir d'une archive zip obtenu depuis le bouton Download dans subwaw (appelé ici {workflow_name}.zip dans ce document ou {workflow_name} une fois dézippé).
Le contenu ci après s'appuie sur l'archive zip obtenu depuis le bouton Download dans subwaw (appelé ici {workflow_name}.zip dans ce document ou {workflow_name} une fois dézippé).

# 2- Description des fichiers de l'archive nécessaire pour le déploiement en local


L'archive {workflow_name}.zip contient plusieurs fichiers/repertoires nécessairent au déploiement du workflow en local :

    * Singularity.def : fichier de recette pour la construction de l'image singularity (nécessite les droits root ou l'option --fakeroot). Cette recette contient l'environnement nécéssaire pour que le worflow tourne (logiciel bioinfo du workflow).

    * waw_workflow.sbatch : script bash permettant de soumettre un job sur cluster de calcul slurm. Le script est adapté au MBB workflow et nécéssite des arguments pour être éxécuté.

    * slurm-config.yml : fichier profile décrivant la configuration et les ressources pour l'exécution des différentes étapes du workflow (ressources global au workflow + ressource par défault pour différentes étapes + ressources spécifique à certaines étapes).

    * install.sh : script shell permettant d'installer une partie des requirements sur le cluster de calcul. (à faire une seule fois si nécessaire!)
    
    * files/envs/ : répertoire contenant les fichiers d'environnement conda utilisés dans les installations de logiciels bioinfo lors de l'exécution des workflows.

    * files/scripts/ : répertoire contenant les scripts (python, R, ...) exécutés dans certaines étapes du workflow.

    * files/versions.yaml : fichier de métadonnées contenant la liste des versions d'outils utilisés dans le workflow.

    * params.total.yml : fichier contenant le noms des paramètres du workflow, ainsi que les paramètres par défaut. Les paramètres peuvent être modifiés manuellement par l'utilisateur, ou via une application shiny en suivant les consignes du déploiement sur machine locale (voir Readme_local.md).

    * Snakefile : script snakemake permettant l'execution et la gestion du workflow (gestions des tâches à éxécuter -> ligne de commande, des ressources, des environnements...)


# 3- Requirements


Le déploiement de workflow MBB sur HPC nécessite certains requirements :

    * Singularity doit être installé sur le cluster de calcul (HPC) et sur la machine en local. <https://docs.sylabs.io/guides/3.0/user-guide/installation.html>

    * Répertoire sur le cluster contenant les données d'entrée (représenté par les briques vertes dans subwaw ou {workflow_name}.png) (i.e $HOME/MyProjectName/Data/)

    * Répertoire vide sur le cluster où l'on souhaite écrire les résultats (i.e. $HOME/MyProjectName/Results/)


# 4- Installation


Une fois que les requirements sont installés, pour déployer le workflow contenu dans {workflow_name}.zip sur un cluster il faut :

* A- Dans le cas de l'utilisation de l'application shiny pour configurer les paramètres du workflow :

  * En local :
    * Se déplacer dans le répertoire qui contient l'archive zip du workflow (i.e. $HOME/MyProjectName/) ***cd $HOME/MyProjectName/***
    * Dézipper l'archive du workflow ***unzip {workflow_name}.zip***
    * Se déplacer dans le répertoire de l'archive dézzipée ***cd {workflow_name}/***
    * Construire l'image singularity ***sudo singularity build Singularity.sif Songularity.def*** ou ***singularity build --fakeroot Singularity.sif Singularity.def***
    * Générer le fichier de paramètre du workflow (input, output, paramètres...) via l'application shiny. Pour cela, suivre le Readme_local.md. A l'exception qu'un fois les paramètres modifiaient dans l'application shiny, il faut cliquer sur "Download config file", puis renommer le fichier téléchargé en params.total.yml, puis le transférer dans le répertoire {workflow_name}/files à la place du fichier params.totatl.yml déjà existant.

  * Sur le cluster :
      * Transférer les fichiers d'entrée du workflow (mis en paramètre dans l'application shiny) dans un repertoire du cluster (i.e. /home/login/MonProjet/Data/). Ces fichiers doivent garder la même organisation qu'en local.
      * Transférer le répertoire de l'archive dezippé (l'archive avec le fichier params.total.yml et Singularity.sif qui ont étéait construit en local) dans un répertoire du cluster (i.e. /home/login/MonProjet/)
      * Installer l'environnement du workflow.
        * 1/ Si conda n'est pas installé sur le cluster
          * Aller dans le répertoire de l'archive
          * Exécuter le fichier install.sh ***bash install.sh***
        * Si conda est installé sur le cluster
          * 2/ Si snakemake n'est pas installé
            * Installé dans un environnement conda snakemake ***mamba create -n MBB_HPC_env -c conda-forge -c bioconda -y snakemake=7.25.0=hdfd78af_1 cerberus=1.3.4=pyhd8ed1ab_0 oyaml=1.0=pyhd8ed1ab_0***
          * 3/ Si snakemake est installé
            * Load du module snakemake ***module load snakemake***
            * Installation de cerberus et oyaml ***pip install cerberus oyaml***


* B- Dans le cas d'une configuration manuelle du fichier params.total.yml :


  * En local :
    * Se déplacer dans le répertoire qui contient l'archive zip du workflow (i.e. $HOME/MyProjectName/) ***cd $HOME/MyProjectName/***
    * Dézipper l'archive du workflow ***unzip {workflow_name}.zip***
    * Se déplacer dans le répertoire de l'archive dézzipée ***cd {workflow_name}/***
    * Construire l'image singularity ***sudo singularity build Singularity.sif Songularity.def*** ou ***singularity build --fakeroot Singularity.sif Singularity.def***

  * Sur le cluster :
      * Transférer les fichiers d'entrée (input) du workflow dans un repertoire du cluster (i.e. /home/login/MonProjet/Data/). L'ensemble des fichiers doivent être contenu dans un même répertoire (cependnat il peuvent être dans des sous répertoire)
      * Transférer le répertoire de l'archive dezippé (l'archive {workflow_name} avec le fichier Singularity.sif qui a été construit en local) dans un répertoire du cluster (i.e. /home/login/MonProjet/).
      * Installer l'environnement du worflow.
        * 1/ Si conda n'est pas installé sur le cluster
          * Aller dans le répertoire de l'archive
          * Exécuter le fichier install.sh ***bash install.sh***
        * Si conda est installé sur le cluster
          * 2/ Si snakemake n'est pas installé
            * Installé dans un environnement conda snakemake ***mamba create -n MBB_HPC_env -c conda-forge -c bioconda -y snakemake=7.25.0=hdfd78af_1 cerberus=1.3.4=pyhd8ed1ab_0 oyaml=1.0=pyhd8ed1ab_0***
          * 3/ Si snakemake est installé
            * Load du module snakemake ***module load snakemake***
            * Installation de cerberus et oyaml ***pip install cerberus oyaml***
      * Configurer manuellement le fichier params.total.yml. Seul les paramètres avec un "#commentaire description du paramètre" sont à modifier, il faut modifier la partie après les ":".


# 5- Utilisation

* Pour executer le workflow lorsqu'on a paramétrer le fichier params.total.yaml avec l'application shiny (cas A-) il faut :

    * Se connecter au cluster de calcul.
    * Se déplacer dans le répertoire de l'archive zip du workflow (i.e. $HOME/MyProjectName/) ***cd $HOME/MyProjectName/{workflow_name}/***
    * Lire le help du script waw_workflow.sbatch ***bash waw_workflow.sbatch -h***
    * Exécuter le script waw_workflow.sbatch. Si l'installation de l'environnement a été fait selon le 1/ ou 2/ mettre -i True. Si fait selon le 3/ suivre ce qui est écrit dans le help avec -i False.
    ***sbatch waw_workflow.sbatch -d /home/login/MonProjet/ -r /home/login/MonProjet/Results -m /home/login/MonProjet/Archive/ -s /home/login/MonProjet/Archive/Singularity.sif -i True|False -j 10***
    * Une fois l'analyse terminé, il est possible d'aller voir directement les résultats et fichiers générés dans le répertoire Results (mis en paramètre dans params.total.yaml). Afin de mieux comprendre les outputs dans le rapport multiqc_report.html, l'onglet "Workflow output" donne un descriptif des fichiers générés.
    

* Pour executer le workflow lorsqu'on a paramétrer le fichier params.total.yaml manuellement (cas B-) il faut :

(A rédiger quand le fichier sbatch sera créé pour ce cas de figure là.)
    * Se connecter au cluster de calcul.
    * Se déplacer dans le répertoire de l'archive zip du workflow (i.e. $HOME/MyProjectName/) ***cd $HOME/MyProjectName/{workflow_name}/***
    * Lire le help du script waw_workflow.sbatch ***bash waw_workflow.sbatch -h***
    * Exécuter le script waw_workflow.sbatch. Si l'installation de l'environnement a été fait selon le 1/ ou 2/ mettre -i True. Si fait selon le 3/ suivre ce qui est écrit dans le help avec -i False.
    ***sbatch waw_workflow.sbatch -d /home/login/MonProjet/ -r /home/login/MonProjet/Results -m /home/login/MonProjet/Archive/ -s /home/login/MonProjet/Archive/Singularity.sif -i True|False -j 10***
    * Une fois l'analyse terminé, il est possible d'aller voir directement les résultats et fichiers générés dans le répertoire Results (mis en paramètre dans params.total.yaml). Afin de mieux comprendre les outputs dans le rapport multiqc_report.html, l'onglet "Workflow output" donne un descriptif des fichiers générés.

