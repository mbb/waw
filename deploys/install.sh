#!/bin/bash

# sudo apt-get update

# ##### Docker install #####
# #sudo apt-get install -y docker.io
# sudo apt-get update
# sudo apt-get install \
#     ca-certificates \
#     curl \
#     gnupg \
#     lsb-release
# sudo mkdir -p /etc/apt/keyrings
# curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

# echo \
#   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
#   $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# sudo apt-get update

# sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin

# ##### nginx install #####
# sudo apt-get install -y nginx

# ###### Singularity install #######
# sudo apt-get install -y build-essential libssl-dev uuid-dev libgpgme11-dev \
#      squashfs-tools libseccomp-dev wget pkg-config git cryptsetup debootstrap

# #Install Go https://golang.org/ and extract to /usr/local/go
# wget https://dl.google.com/go/go1.13.linux-amd64.tar.gz

# sudo tar --directory=/usr/local -xzvf go1.13.linux-amd64.tar.gz
# export PATH=/usr/local/go/bin:$PATH

# #Change version if needed
# wget https://github.com/singularityware/singularity/releases/download/v3.8.3/singularity-3.8.3.tar.gz
# tar -xzvf singularity-3.8.3.tar.gz

# cd singularity-3.8.3
# ./mconfig
# cd builddir
# make
# sudo make install
 
#You can test your installation
# singularity run library://godlovedc/funny/lolcow


# conda install
UNAME_M="$(uname -m)"
CONDA_VERSION=py310_23.3.1-0

if [ "${UNAME_M}" = "x86_64" ]; then
  MINICONDA_URL="https://repo.anaconda.com/miniconda/Miniconda3-${CONDA_VERSION}-Linux-x86_64.sh";
elif [ "${UNAME_M}" = "s390x" ]; then
  MINICONDA_URL="https://repo.anaconda.com/miniconda/Miniconda3-${CONDA_VERSION}-Linux-s390x.sh";
elif [ "${UNAME_M}" = "aarch64" ]; then
  MINICONDA_URL="https://repo.anaconda.com/miniconda/Miniconda3-${CONDA_VERSION}-Linux-aarch64.sh";
elif [ "${UNAME_M}" = "ppc64le" ]; then
  MINICONDA_URL="https://repo.anaconda.com/miniconda/Miniconda3-${CONDA_VERSION}-Linux-ppc64le.sh"; 
fi

wget "${MINICONDA_URL}" -O miniconda.sh -q


mkdir -p ${HOME}/Tools

bash miniconda.sh -b -p ${HOME}/Tools/conda

rm miniconda.sh

echo ". ${HOME}/Tools/conda/etc/profile.d/conda.sh" >> ~/.bashrc

echo "conda activate base" >> ~/.bashrc

find ${HOME}/Tools/conda/ -follow -type f -name '*.a' -delete
find ${HOME}/Tools/conda/ -follow -type f -name '*.js.map' -delete

${HOME}/Tools/conda/bin/conda clean -afy

source ~/.bashrc

export PATH=$PATH:${HOME}/Tools/conda/bin

conda install -y -c conda-forge -c bioconda mamba=1.4.2=py310h51d5547_0

mamba create -n MBB_HPC_env -c conda-forge -c bioconda -y snakemake=7.25.0=hdfd78af_1 \
                                                          cerberus=1.3.4=pyhd8ed1ab_0 \
                                                          oyaml=1.0=pyhd8ed1ab_0

