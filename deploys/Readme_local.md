
# 1- Description


Ce readme a pour but de décrire le déploiement d'un workflow MBB sur une machine local. Cette machine doit avoir un système d'exploitation linux, pour laquelle vous avez les droits root (sudoers), et doit disposer d'un accès ssh et d'au moins X de RAM et Y CPU.
Le contenu ci après s'appuie sur l'archive zip obtenu depuis le bouton Download dans subwaw (appelé ici {workflow_name}.zip dans ce document ou {workflow_name} une fois dézippé).


# 2- Description des fichiers de l'archive nécessaire pour le déploiement en local


L'archive {workflow_name}.zip contient plusieurs fichiers/repertoires nécessairent au déploiement du workflow en local :

    * deployLocalHost.sh : script permettant de déployer l'image docker et le workflow sur la machine.

    * Dockerfile : fichier de recette pour la construction de l'image docker. Cette recette contient l'environnement nécéssaire pour que le worflow tourne (logiciel bioinfo du workflow).

    * files/envs/ : répertoire contenant les fichiers d'environnement conda utilisés dans les installations de logiciels bioinfo lors de l'exécution des workflows.

    * files/scripts/ : répertoire contenant les scripts (python, R, ...) exécutés dans certaines étapes du workflow.

    * files/versions.yaml : fichier de métadonnées contenant la liste des versions d'outils utilisés dans le workflow.

    * files/params.total.yml : fichier contenant le noms des paramètres du workflow, ainsi que les paramètres par défaut. Les paramètres ne doivent pas être modifiés manuellement par l'utilisateur, une application shiny lors du déploiement est prévu à cet usage afin de facilité le paramétrage du workflow.

    * files/Snakefile : script snakemake permettant l'execution et la gestion du workflow (gestions des tâches à éxécuter -> ligne de commande, des ressources, des environnements...).


# 3- Requirements


Le déploiement de workflow MBB nécessite certains requirements :

    * Environnement linux. (possible depuis subsystem for linux (WSL) dans windows)

    * Docker installé <https://docs.docker.com/engine/install/>

    * Répertoire contenant les données d'entrée (représenté par les briques vertes dans subwaw ou {workflow_name}.png) (i.e $HOME/MyProjectName/Data/)
    
    * Répertoire vide ou l'on souhaite écrire les résultats (i.e. $HOME/MyProjectName/Results/)


# 4- Installation


Une fois que les requirements sont installés, pour déployer le workflow contenu dans {workflow_name}.zip en local sur son ordinateur il faut :

    * Se déplacer dans le répertoire qui contient l'archive zip du workflow (i.e. $HOME/MyProjectName/) ***cd $HOME/MyProjectName/***

    * Dézipper l'archive du workflow ***unzip {workflow_name}.zip***

    * Se déplacer dans le répertoire de l'archive dézzipée ***cd {workflow_name}/***

    * Déployer l'environnement en éxécutant le script deployLocalHost.sh avec pour argument le répertoire contenant les données d'entrée, le répertoire où l'on souhaite écrire les résultats et l'argument "local". ***bash deployLocalHost.sh $HOME/MyProjectName/Data/ $HOME/MyProjectName/Results/ local***

    * L'exécution de ce script, une fois finie, va renvoyer un url (à entrer dans votre navigateur) donnant accès à l'application shiny permettant de modifier les paramètres et d'exécuter le workflow. (accessible aussi à l'url http://127.0.0.1:3838 si le navigateur est sur la même machine que là où a été lancé le deploy).

# 5- Utilisation

Le workflow est utilisable à l'aide d'une interface graphique shiny. Cette interface apparait lorsqu'on se connecte à l'url renvoyé par deployLocalHost.sh. Cette interface permet de paramétrer le workflow avec les données en entrée, le dossier de sortie et les paramètres des différents outils. L'application shiny s'utilise ainsi :

    * L’interface de saisie des paramètres se présente sous la forme d’un formulaire qui comporte un onglet pour chaque étape du workflow dans le volet de gauche. Il suffit de remplir les champs correspondant au formulaire, chaque champs possédant déjà un paramètre par défaut (à l'exception des fichiers et répertoire qui sont forcément dépendant de chaque utilisateur et ordianteur).

    * Si ce workflow a vocation a être utilisé à plusieurs reprises sur différents jeux de données ou avec différentes options, il est fortement recommandé d'écrire les résultats (Global parameter > Results directory) dans un répertoire différent pour chaque analyse (sinon risque d'écrasement par réécriture des résultats précédemment obtenus). Pour cela dans "Global parameter > Results directory", cliquez sur "Results", puis "Create new folder", entrez un nom de répertoire, puis appuyez sur le "+", enfin sélectionnez le répertoire créait dans la partie basse de la fenêtre.

    * Suite à la saisie de l'ensemble des paramètres, il est possible de faire un pré-check de si le workflow est correctement construit. Pour cela il faut aller dans l'onglet "Check worflow validity". Si un graphique représentant le process des étapes s'affiche, c'est qu'il ne semble pas y avoir de problème dans ce pré-check. En dessous du graphique il est également possible de voir les lignes de commande que va éxécuter snakemake.

    * Suite à la saisie de l'ensemble des paramètres il est également possible de sauvegarder les valeurs choisies en paramètre en cliquant sur "Download config file".

    * Pour exécuter le workflow il suffit de cliquer sur le bouton "Run pipeline". A noter qu'il est possible de commencer à partir d'un étape spécifique grâce au volet déroulant "Start again from a step". Ceci peut être particulièrement utile lorsqu'on a lancé une première fois le workflow, et que les résultats ne sont pas tout à fait satisfaisant à cause d'un paramètre choisi lors de la première exécution. Alors, on pourrait souhaiter changer un paramètre pour générer de nouveaux résultats. Afin de gagner du temps, grâce à ce volet déroulant on peut commencer spécifiquement le pipeline à partir de l'étape du paramètre en question.

    * Lorsque l'analyse est en cours il est possible de voir les lignes de commande qu'exécute le workflow snakemake dans l'onglet "Running workflow output".

    * Un fois l'analyse terminée dans l'onglet "Running worflow output" on voit apparaitre "Workflow finished with SUCCESS". Ainsi on peut aller voir le rapport dans l'onglet "Final report" qui est généré grâce à l'outil multiqc.

    * Il est également possible d'aller voir directement les résultats et fichiers générés dans le répertoire Results (mis en option lors du deploy) + répertoire mis en paramètre dans l'onglet "Global parameter" (i.e. si dans l'onglet "global parameter" le répertoire Results est nommé Analyse_1_param_x alors les résultats seront dans $HOME/MyProjectName/Results/Analyse_1_param_x/). Un descriptif des fichiers générés est disponible dans le rapport onglet "Workflow outputs".

