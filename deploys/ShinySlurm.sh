#!/bin/sh
#SBATCH --job-name=shiny
#SBATCH -N 1
#SBATCH -n 1
# #SBATCH --ntasks-per-core=1
#SBATCH --cpus-per-task=6
#SBATCH --partition=small
##SBATCH --time=00:45:00
##SBATCH --exclusive
#SBATCH --output=shiny-%j.log
#SBATCH --error=shiny-%j.err

unzippedWF=$1
data=$2
results=$3
condabin=$4

echo "# Running on: $SLURM_NODELIST"
#source activate base

# set a random port for the notebook, in case multiple APPS are
# on the same compute node.
APPPORT=`shuf -i 18000-18500 -n 1`

# set a random port for tunneling, in case multiple connections are happening
# on the same login node.
TUNNELPORT=`shuf -i 18501-19000 -n 1`

# Set up a reverse SSH tunnel from the compute node back to the submitting host (login01 or login02)
# This is the machine we will connect to with SSH forward tunneling from our client.

echo "# $SLURM_SUBMIT_HOST"
externe=$(basename $SLURM_SUBMIT_HOST .cluster.local)

echo "# $externe"

ssh -R$TUNNELPORT:localhost:$APPPORT $SLURM_SUBMIT_HOST -N -f

#this is the command to run on user computer and open http://127.0.0.1:8888
echo "FWDSSH='ssh -L8888:localhost:$TUNNELPORT $(whoami)@${externe}.mbb.cnrs.fr  -N'"

# ShinySingularity.sif must be build : 
# singularity  build --fakeroot ShinySingularity.sif ShinySingularity.def
# then run
#singularity  run --bind $unzippedWF/files:/workflow,$data:/Data,$results:/Results,$condabin:/var/www/html/condabin,$unzippedWF/sagApp:/sagApp $unzippedWF/ShinySingularity.sif  $APPPORT

# or this way for non sudoers
singularity pull $unzippedWF/ShinySingularity.sif docker://mbbteam/mbb_workflows_base

singularity  run --bind $unzippedWF/files:/workflow,$data:/Data,$results:/Results,$condabin:/var/www/html/condabin,$unzippedWF/sagApp:/sagApp $unzippedWF/ShinySingularity.sif  R --slave --no-restore -e "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=$APPPORT , host='0.0.0.0')"
