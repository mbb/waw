def get_vcffiles(vcfslist): 
    for vcf_file in vcfslist:
        #check if tabix index is present
        if not os.path.isfile(vcf_file+".tbi"):
           command = "tabix -p vcf " + vcf_file
           os.system(command)
    return " ".join(vcfslist)

def post_traitement(vcfslist, sites, output_dir):
    names = list()
    for vcf_file in vcfslist:
        names.append(os.path.basename(vcf_file))
    post_traitement = "awk '"
    post_traitement += "BEGIN {print \"pos;" + ";".join(names) + "\"}"
    post_traitement += "{ n = split($5, t, \"\") ;"
    post_traitement += "x=$1\":\"$2;"
    post_traitement += "for (i = 1; i <= n; i++)  x=x\";\"t[i];"
    post_traitement += "print x"
    post_traitement += "}' " + sites + " > " + output_dir+"/snps.txt; "
    return post_traitement

# THIS WILL BE CALLED MULTIPLE TIMES with only one file at the time !!!!
# Solve this before allowing use !

rule <step_name>__compare_vcfs_isec:
    input:
        **<step_name>__compare_vcfs_isec_inputs(),
    output:
        sites = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_isec_output_dir"]+"/{sample}/sites.txt",
        snps = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_isec_output_dir"]+"/{sample}/snps.txt",
        common = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_isec_output_dir"]+"/{sample}/common.txt",
        common_vcf = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_isec_output_dir"]+"/{sample}/output_common_only.vcf.gz",
    params:
        command = config["<step_name>__compare_vcfs_isec_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_isec_output_dir"]+"/{sample}",
        nb_vcfs = len(<step_name>__compare_vcfs_isec_inputs()["vcfs"]),
        minQUAL = config["<step_name>__compare_vcfs_isec_QUAL"],
        snponly = ' & TYPE="snp" ' if (config["<step_name>__compare_vcfs_isec_snponly"]) else '',
        vcf_files = get_vcffiles(<step_name>__compare_vcfs_isec_inputs()["vcfs"]),
        post_traitement = post_traitement(<step_name>__compare_vcfs_isec_inputs()["vcfs"], config["results_dir"]+"/"+config["<step_name>__compare_vcfs_isec_output_dir"]+"/sites.txt", config["results_dir"]+"/"+config["<step_name>__compare_vcfs_isec_output_dir"]),
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"        
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__compare_vcfs_isec_output_dir"] + "/{sample}/compare_vcfs_isec_log.txt"
    shell:
        "{params.command} " +
        "-n+1 " +
        " {params.vcf_files} " +
        "-i 'QUAL>={params.minQUAL} {params.snponly}'  " +
        "-o {output.sites} " +
        "-c none " +
        "|& tee {log}; " +
        "{params.post_traitement} " +
        "N={params.nb_vcfs} && nbOnes=$(perl -e \"print '1' x $N;\") ; " +
        "grep $nbOnes {output.sites} > {output.common}; " +
        "vcfFiles='{params.vcf_files}' && listFiles=( $vcfFiles ) ; " +
        "vcftools --positions {output.common} " +
        "--gzvcf  ${{listFiles[0]}} + " 
        "--recode --stdout | bgzip -c > {output.common_vcf}"
        "Rscript {snake_dir}/scripts/<step_name>__compare_vcfs_isec.prepare.report.R {conf_path} |& tee {log};"

