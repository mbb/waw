rule <step_name>__vcftools_allelic_variants_count_sfs:
    input:
        **<step_name>__vcftools_allelic_variants_count_sfs_inputs(),
    output: 
        frq = config["results_dir"] + "/" + config["<step_name>__vcftools_allelic_variants_count_sfs_output_dir"] + "/vcf_count.frq.count",
        sfs = config["results_dir"] + "/" + config["<step_name>__vcftools_allelic_variants_count_sfs_output_dir"] + "/vcf_count.sfs.count",
    params:
        prefix = config["results_dir"] + "/" + config["<step_name>__vcftools_allelic_variants_count_sfs_output_dir"] + "/vcf_count",
        snp = "--remove-indels" if config["<step_name>__vcftools_allelic_variants_count_sfs_snps_only"] else "",
        biallelic = "--min-alleles 2 --max-alleles 2" if config["<step_name>__vcftools_allelic_variants_count_sfs_biallelic_only"] else "",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__vcftools_allelic_variants_count_sfs_output_dir"] + "/vcf_count_log.txt"
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"
    shell:
        """
        if [ "{input.vcf}" == "*.gz" ];then
            vcftools --gzvcf {input.vcf} --counts {params.snp} {params.biallelic} --out {params.prefix}
        else
            vcftools --vcf {input.vcf} --counts {params.snp} {params.biallelic} --out {params.prefix}
        fi
        awk -F "\\t" '{{if(NR!=1){{allele["A"]=0;allele["C"]=0;allele["G"]=0;allele["T"]=0;for(a=5;a<=NF;a++){{split($a,c,":");if(c[1] in allele){{allele[c[1]]=c[2]}}}};print $1":"$2"\\t"allele["A"]","allele["C"]","allele["G"]","allele["T"]}}}}' {output.frq} > {output.sfs}
        """
