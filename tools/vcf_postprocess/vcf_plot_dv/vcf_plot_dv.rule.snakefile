rule <step_name>__vcf_plot_dv:
    input:
        **<step_name>__vcf_plot_dv_inputs()
    output:
        vcf_plots_mqc = config["results_dir"]+"/"+config["<step_name>__vcf_plot_dv_output_dir"]+"/vcf_plots_mqc_list.tsv",
    params:
        results_dir = config["results_dir"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__vcf_plot_dv_output_dir"] + "/vcf_plot_dv_log.txt"
    conda:
        "envs/deepvariant.yaml"    
    run:
        cmd = ""
        for vcf_file in input.vcfs:
            output_dir = os.path.dirname(vcf_file)
            prefix = os.path.basename(os.path.dirname(vcf_file))
            cmd += "/bin/bash -c \"source /opt/deepvariant/deepvariant_env/bin/activate && "
            cmd += "/opt/deepvariant/bazel-bin/deepvariant/vcf_stats_report "
            cmd += " --input_vcf " + vcf_file
            cmd += " --outfile_base " + output_dir + "/" + prefix # result in the original vcf dir and prefixed by the vcf dirname
            cmd += " |& tee -a {log}\"; "
            # add to tsv for mqc plugin
            cmd += "echo '" + prefix + "\t" + output_dir.replace(params.results_dir,".") + "/" + prefix + ".visual_report.html' >> {output.vcf_plots_mqc}; "

        shell(cmd)