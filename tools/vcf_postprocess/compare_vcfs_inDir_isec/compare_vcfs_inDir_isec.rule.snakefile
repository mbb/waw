def get_vcffiles(vcfsinDir):
        from os.path import isfile, join

        included_extensions = ['vcf','vcf.gz']
        onlyfiles = [join(vcfsinDir,fn) for fn in os.listdir(vcfsinDir)
              if any(fn.endswith(ext) for ext in included_extensions)]

        for vcf_file in onlyfiles:
            #check if tabix index is present
            if not os.path.isfile(vcf_file+".tbi"):
               command = "tabix -p vcf " + vcf_file
               os.system(command)

        return " ".join(onlyfiles)

def count_vcffiles(vcfsinDir):
        from os.path import isfile, join

        included_extensions = ['vcf','vcf.gz']
        onlyfiles = [join(vcfsinDir,fn) for fn in os.listdir(vcfsinDir)
              if any(fn.endswith(ext) for ext in included_extensions)]
        return len(onlyfiles)

def post_traitement(vcfsinDir, sites, output_dir):
        from os.path import isfile, join
        
        included_extensions = ['vcf','vcf.gz']
        onlyfiles = [join(vcfsinDir,fn) for fn in os.listdir(vcfsinDir)
              if any(fn.endswith(ext) for ext in included_extensions)]

        names = list()
        for vcf_file in onlyfiles:
            names.append(os.path.basename(vcf_file))

        post_traitement = "awk '"
        post_traitement += "BEGIN {print \"pos;" + ";".join(names) + "\"}"
        post_traitement += "{ n = split($5, t, \"\") ;"
        post_traitement += "x=$1\":\"$2;"
        post_traitement += "for (i = 1; i <= n; i++)  x=x\";\"t[i];"
        post_traitement += "print x"
        post_traitement += "}' " + sites + " > " + output_dir+"/snps.txt; "

        return post_traitement

rule <step_name>__compare_vcfs_inDir_isec:
    input:
        **<step_name>__compare_vcfs_inDir_isec_inputs(),
    output:
        sites = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_inDir_isec_output_dir"]+"/sites.txt",
        common = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_inDir_isec_output_dir"]+"/common.txt",
        common_vcf = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_inDir_isec_output_dir"]+"/common_only.vcf.gz",
    params:
        command = config["<step_name>__compare_vcfs_inDir_isec_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_inDir_isec_output_dir"],
        minQUAL = config["<step_name>__compare_vcfs_inDir_QUAL"],
        snponly = ' & TYPE="snp" ' if (config["<step_name>__compare_vcfs_inDir_snponly"]) else '',
        vcf_files = get_vcffiles(<step_name>__compare_vcfs_inDir_isec_inputs()["vcfs"][0]),
        nb_vcfs = count_vcffiles(<step_name>__compare_vcfs_inDir_isec_inputs()["vcfs"][0]),
        post_traitement = post_traitement(<step_name>__compare_vcfs_inDir_isec_inputs()["vcfs"][0], config["results_dir"]+"/"+config["<step_name>__compare_vcfs_inDir_isec_output_dir"]+"/sites.txt", config["results_dir"]+"/"+config["<step_name>__compare_vcfs_inDir_isec_output_dir"]),
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__compare_vcfs_inDir_isec_output_dir"] + "/compare_vcfs_inDir_isec_log.txt"
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"    
    shell:
            "{params.command} " +
            "-n+1 " +
            " {params.vcf_files} " +
            "-i 'QUAL>={params.minQUAL} {params.snponly}'  " +
            "-o {output.sites} " +
            "-c none " +
            "|& tee {log}; " +
            "{params.post_traitement} " +
            "N={params.nb_vcfs} && nbOnes=$(perl -e \"print '1' x $N;\") ; " +
            "grep $nbOnes {output.sites} > {output.common}; " +
            "vcfFiles='{params.vcf_files}' && listFiles=( $vcfFiles ) ; " +
            "vcftools --positions {output.common} " +
            "--gzvcf  ${{listFiles[0]}}  " +
            "--recode --stdout | bgzip -c > {output.common_vcf}; "
            "Rscript {snake_dir}/scripts/<step_name>__compare_vcfs_inDir_isec.prepare.report.R {conf_path} |& tee {log};"
        
