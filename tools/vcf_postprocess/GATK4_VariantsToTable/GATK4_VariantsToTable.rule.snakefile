rule <step_name>__GATK4_VariantsToTable:
    input:
        **<step_name>__GATK4_VariantsToTable_inputs(),
    output:
        table = config["results_dir"] + "/" + config["<step_name>__GATK4_VariantsToTable_output_dir"] + "/Variants.tsv",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__GATK4_VariantsToTable_output_dir"],
        command = config["<step_name>__GATK4_VariantsToTable_command"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__GATK4_VariantsToTable_output_dir"] + "/GATK4_VariantsToTable_log.txt"
    threads: 1
    conda:
        "envs/gatk4.yaml"
    shell: 
        """
        gatk VariantsToTable -V {input.vcf} -R {input.genome_fasta} -O {output.table} -F CHROM -F POS -F QUAL -F QD -F DP -F MQ -F MQRankSum -F FS -F ReadPosRankSum -F SOR
        """

