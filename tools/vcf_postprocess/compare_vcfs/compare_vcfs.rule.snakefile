def get_files_to_compare(vcfsinDir):
        from os.path import isfile, join

        included_extensions = ['vcf','vcf.gz']
        onlyfiles = [join(vcfsinDir,fn) for fn in os.listdir(vcfsinDir)
              if any(fn.endswith(ext) for ext in included_extensions)]

        tmp_tocompare= list()
        for vcf_file in onlyfiles:
            tmpfile = "/tmp/" + os.path.basename(vcf_file)
            shutil.copyfile(vcf_file, tmpfile)
            tmp_tocompare.append(tmpfile)
        return   " ".join(tmp_tocompare)

def png_cmd(vcfsinDir):
    to_png = ""
    nb_vcfs=0    
    for vcf_file in os.listdir(vcfsinDir):
        nb_vcfs = nb_vcfs+1

    if nb_vcfs < 4:
        to_png += "pdftoppm -png venn" +str(nb_vcfs)+ ".positions.pdf > Venn_positions_mqc.png; "
        to_png += "pdftoppm -png venn" +str(nb_vcfs)+ ".snps.pdf > Venn_snps_mqc.png; "
    else:
        to_png += "pdftoppm -png venn" +str(nb_vcfs)+ ".pdf > Venn_mqc.png; "
    return to_png

def prepare(vcfsinDir,output_dir, truth_vcf, truth_vcf_file, bench_to_truth ):
        from os.path import isfile, join

        included_extensions = ['vcf','vcf.gz']
        onlyfiles = [join(vcfsinDir,fn) for fn in os.listdir(vcfsinDir)
              if any(fn.endswith(ext) for ext in included_extensions)]

        tmp_tocompare= list()
        for vcf_file in onlyfiles:
            tmpfile = "/tmp/" + os.path.basename(vcf_file)
            tmp_tocompare.append(tmpfile)            
        
        to_truth = ""
        stats_truth = dict()
        if truth_vcf:
            for vcf in tmp_tocompare:
                a = vcf.split('.')[0].split('/')
                dir = a[len(a)-1]
                to_truth += "mkdir -p "+output_dir+"/" + dir + " && cd $_ &&"
                to_truth += "vcftoolz compare --truth " + truth_vcf_file + " " + vcf + " > "+output_dir+"/" + dir + "/stats.txt; "
                stats_truth[dir] = output_dir+"/" + dir + "/stats.txt"
        
        summary_bench_truth = "echo \"# id: compare_to_truth\" > bench_to_truth; "
        summary_bench_truth += "echo \"# section_name: 'Compare VCFs to truth'\" >> bench_to_truth; "
        #summary_bench_truth += "echo \"# description: ''\" > bench_to_truth; "
        summary_bench_truth += "echo \"# format: 'tsv'\" >> bench_to_truth; "
        summary_bench_truth += "echo \"# plot_type: 'table'\" >> bench_to_truth; "
        summary_bench_truth += "echo -n 'Variant caller\tPrecision\tRecall\tF1 score\tTrue positive calls\tFalse negative calls\tFalse positive calls\n' >> bench_to_truth; "
        for variant_caller in stats_truth.keys():
            summary_bench_truth += "echo -n '" + variant_caller + "\t' >> bench_to_truth; "
            summary_bench_truth += "grep 'Precision' " + stats_truth[variant_caller] + " | awk '{{printf $1\"\t\"}}' >> bench_to_truth; "
            summary_bench_truth += "grep 'Recall' " + stats_truth[variant_caller] + " | awk '{{printf $1\"\t\"}}' >> bench_to_truth; "
            summary_bench_truth += "grep 'F1 score' " + stats_truth[variant_caller] + " | awk '{{printf $1\"\t\"}}' >> bench_to_truth; "
            summary_bench_truth += "grep 'True positive calls' " + stats_truth[variant_caller] + " | awk '{{printf $1\"\t\"}}' >> bench_to_truth; "
            summary_bench_truth += "grep 'False negative calls' -m 1 " + stats_truth[variant_caller] + " | awk '{{printf $1\"\t\"}}' >> bench_to_truth; "
            summary_bench_truth += "grep 'False positive calls' " + stats_truth[variant_caller] + " | awk '{{print $1}}' >> bench_to_truth; "
        out = dict()    
        out["to_truth"]      = to_truth
        out["summary_bench_truth"] = summary_bench_truth
        return out


rule <step_name>__compare_vcfs:
    input:
        **<step_name>__compare_vcfs_inputs(),
        truth_vcf_file = config["<step_name>__compare_vcfs_truth_file"] if config["<step_name>__compare_vcfs_truth"] else []
    output:
        stats = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_output_dir"]+"/stats.txt",
        bench_to_truth = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_output_dir"]+"/VCF_compare_to_truth_mqc.tsv" if config["<step_name>__compare_vcfs_truth"] else [],
    params:
        command = config["<step_name>__compare_vcfs_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_output_dir"],
        nb_vcfs =  len(<step_name>__compare_vcfs_inputs()["vcfsinDir"]),
        truth_vcf = config["<step_name>__compare_vcfs_truth"],
        tmp_tocompare = get_files_to_compare(<step_name>__compare_vcfs_inputs()["vcfsinDir"][0]),
        to_png = png_cmd(<step_name>__compare_vcfs_inputs()["vcfsinDir"][0]),
        to_truth = prepare(<step_name>__compare_vcfs_inputs()["vcfsinDir"][0],config["results_dir"]+"/"+config["<step_name>__compare_vcfs_output_dir"], config["<step_name>__compare_vcfs_truth"], config["results_dir"]+"/"+config["<step_name>__compare_vcfs_output_dir"]+"/VCF_compare_to_truth_mqc.tsv")["to_truth"] if config["<step_name>__compare_vcfs_truth"] else "" ,
        summary_bench_truth = prepare(<step_name>__compare_vcfs_inputs()["vcfsinDir"][0],config["results_dir"]+"/"+config["<step_name>__compare_vcfs_output_dir"], config["<step_name>__compare_vcfs_truth"], config["results_dir"]+"/"+config["<step_name>__compare_vcfs_output_dir"]+"/VCF_compare_to_truth_mqc.tsv")["summary_bench_truth"] if config["<step_name>__compare_vcfs_truth"] else "" 
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__compare_vcfs_output_dir"]+ "/compare_vcfs_log.txt"
    conda:
        "envs/vcftoolz.yaml"    
    shell:
            "cd {params.output_dir}; "
            "{params.command} " +
            "{params.tmp_tocompare} "+
            "> {output.stats} " +
            "2> >(tee {log} >&2); " +
            "{params.to_png} "+
            "{params.to_truth} "+
            "{params.summary_bench_truth}  "
            "rm -f {params.tmp_tocompare}"
        
