rule <step_name>__BEDTools_vcf_flanking_sequence:
    input:
        **<step_name>__BEDTools_vcf_flanking_sequence_inputs(),
    output: 
        bed = config["results_dir"] + "/" + config["<step_name>__BEDTools_vcf_flanking_sequence_output_dir"] + "/vcf_flanking.bed",
        fasta = config["results_dir"] + "/" + config["<step_name>__BEDTools_vcf_flanking_sequence_output_dir"] + "/vcf_flanking.fa",
    params: 
        size = config["<step_name>__BEDTools_vcf_flanking_size"],
        dir = config["results_dir"] + "/" + config["<step_name>__BEDTools_vcf_flanking_sequence_output_dir"]
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__BEDTools_vcf_flanking_sequence_output_dir"] + "/vcf_flanking_log.txt"
    threads:
        config["<step_name>__BEDTools_vcf_flanking_sequence_threads"],
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"    
    shell:
        """
        # Format genome
        samtools faidx {input.genome_fasta} -o {params.dir}/reference.fa.fai
        cut -f 1,2 {params.dir}/reference.fa.fai > {params.dir}/reference.genome
        # Create bed for each variant
        bcftools query -f '%CHROM\\t%POS0\\t%END\\t%CHROM:%POS:%REF\\n' {input.vcf} > {params.dir}/variant.bed
        # Create bed and fasta with flanking sequence
        bedtools slop -b {params.size} -i {params.dir}/variant.bed -g {params.dir}/reference.genome > {params.dir}/flanking_tmp.bed
        awk -F "\\t" -v s={params.size} '{{n=split($4,a,":");if((a[n-1]-s)<=0){{p=a[n-1]}}else{{p=s+1}};joined="";for (i=1;i<=n;i++){{if(i==1){{joined=a[i]}}else{{joined=joined":"a[i]}}}};print $1"\\t"$2"\\t"$3"\\t"joined":"p}}' {params.dir}/flanking_tmp.bed > {output.bed}
        rm {params.dir}/flanking_tmp.bed
        bedtools getfasta -fi {input.genome_fasta} -bed {output.bed} -nameOnly > {output.fasta}
        """
