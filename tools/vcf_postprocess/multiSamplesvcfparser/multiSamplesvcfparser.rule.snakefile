rule <step_name>__multiSamplesvcfparser:
    input:
        **<step_name>__multiSamplesvcfparser_inputs()
    output:
        vcf_stats = config["results_dir"]+"/"+config["<step_name>__multiSamplesvcfparser_output_dir"]+"/<step_name>_vcfstats.txt",
    params:
        output_dir = config["results_dir"]+"/"+config["<step_name>__multiSamplesvcfparser_output_dir"]+"/",
        command = config["<step_name>__multiSamplesvcfparser_command"],
    threads: config["<step_name>__multiSamplesvcfparser_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__multiSamplesvcfparser_output_dir"] + "/<step_name>_multiSamplesvcfparser_log.txt"
    conda:
        "envs/vcfparser.yaml"
    shell:
        "{params.command} "
        "{input.vcf} "
        "{output.vcf_stats} "
        "|& tee -a {log} ;"
        "fic={output.vcf_stats} ;" 
        "samplePos=1 ;"
        "scriptsDir=`dirname $(echo {params.command} | sed 's/bash //')`; "
        "Rscript -e \"library(doParallel); df = read.table('$fic', nrows=1, header=TRUE, sep='\\t', stringsAsFactors=FALSE); nsamples = (dim(df)[2] - 6)/5;cl <- makeCluster({threads}); registerDoParallel(cl); foreach (samplePos=1:nsamples)  %dopar% {{ source(paste0('$scriptsDir','/Draw_fonction_MultiSamples.R'));png(file=paste0('{params.output_dir}','/sample', samplePos,'vcfPlot_mqc.png'), width = 1200, height = 1000); print(Draw('$fic', samplePos, NULL)); dev.off(); }}\" "