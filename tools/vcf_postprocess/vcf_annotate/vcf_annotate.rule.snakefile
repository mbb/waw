rule <step_name>__vcf_annotate:
    input:
        **<step_name>__vcf_annotate_inputs(),
    output:
        vcf_annot = config["results_dir"] + "/" + config["<step_name>__vcf_annotate_output_dir"] + "/variant_annotate.vcf.gz",
    params:
        outdir = config["results_dir"] + "/" + config["<step_name>__vcf_annotate_output_dir"],
        header = config["<step_name>__vcf_annotate_header"],
        column = config["<step_name>__vcf_annotate_column"],
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__vcf_annotate_output_dir"] + '/variant_annotate_log.txt'
    threads:
        config["<step_name>__vcf_annotate_threads"]
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"
    shell:
        """
        # Format column and header option
        echo '{params.header}' > {params.outdir}/header_vcf.txt
        column="CHROM,POS,INFO/{params.column}"
        bgzip < {input.tsv} > {params.outdir}/variant.tsv.gz
        tabix -s1 -b2 -e2 {params.outdir}/variant.tsv.gz
        # Annotate vcf
        bcftools annotate -a {params.outdir}/variant.tsv.gz -c $column -h {params.outdir}/header_vcf.txt -Oz -o {output.vcf_annot} --threads {threads} {input.vcf}
        """