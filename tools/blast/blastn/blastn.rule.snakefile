def <step_name>__db():
    from os.path import isfile, join
    db_name = ""
    db_path = ""

    #this condition have to be tested
    if "<step_name>__blastn_blastdb_dir" in config.keys():
        #files = os.listdir(config["<step_name>__blastn_blastdb_dir"])
        blastdb_dir = config["<step_name>__blastn_blastdb_dir"]
        included_extensions = ['phr','pin', 'psq', 'nhr', 'nin', 'nsq']
        files = [join(blastdb_dir,fn) for fn in os.listdir(blastdb_dir)
                if any(fn.endswith(ext) for ext in included_extensions)]
        db_name = os.path.basename(os.path.splitext(files[0])[0])
        db_path = os.path.dirname(config["<step_name>__blastn_blastdb_dir"]+"/")
    # if from makeblastdb step or raw_blastdb
    else:
        file = <step_name>__blastn_inputs()["blastdb_dir"]
        included_extensions = ['phr','pin', 'psq', 'nhr', 'nin', 'nsq']
        files=[fn for fn in file if any(fn.endswith(ext) for ext in included_extensions)]
        db_name = os.path.basename(os.path.splitext(files[0])[0])
        db_path = os.path.dirname(files[0])

    return { "db_name": db_name, "db_path": db_path }

rule <step_name>__blastn:
    input:
        **<step_name>__blastn_inputs(),
    output:
        blastout = config["results_dir"] + "/" + config["<step_name>__blastn_output_dir"] + "/blastout_" + <step_name>__db()["db_name"] + ".tsv",
        selected_contigs = config["results_dir"] + "/" + config["<step_name>__blastn_output_dir"] + "/selected_contigs.fasta",
    params:
        command = config["<step_name>__blastn_command"],
        evalue = config["<step_name>__blastn_evalue"],
        max_target_sequences =  config["<step_name>__blastn_max_target_sequences"],
        max_hsps =  config["<step_name>__blastn_max_hsps"],
        min_len = config["<step_name>__blastn_min_len"],
        #db = lambda w,input: os.path.splitext(input.blastdb)[0]
        db = <step_name>__db()["db_path"] + "/" + <step_name>__db()["db_name"]
    threads:
        config["<step_name>__blastn_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__blastn_output_dir"] + '/blastn_log.txt'
    conda:
       "envs/blast.yaml"    
    shell:
        # output header
        "echo -e 'qseqid\tsseqid\tpident\tlength\tmismach\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore\tqlen' > {output.blastout}; "
        # command
        "{params.command} "
        "-query {input.query} "
        "-db {params.db} "
        "-evalue {params.evalue} "
        "-max_target_seqs {params.max_target_sequences} "
        "-max_hsps {params.max_hsps} "
        "-outfmt '6 std qlen' "
        "-num_threads {threads} 2> {log} | "
        "awk \"{{if (\$4 >= {params.min_len}) print }}\""
        ">> {output.blastout} ;"

        "if [ $(wc -l < {output.blastout} ) -ge 2 ]; then "
            "head -n +2 {output.blastout} | cut -f 1 > /tmp/qids.txt "
            "&& grep --no-group-separator -f /tmp/qids.txt -A 1 {input.query} > {output.selected_contigs};"
        "else "
        "touch {output.selected_contigs};"
        "fi"
