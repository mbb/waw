def <step_name>__db():
    from os.path import isfile, join
    db_name = ""
    db_path = ""

    #this condition have to be tested
    if "<step_name>__sfs_outgroup_blastdb_dir" in config.keys():
        #files = os.listdir(config["<step_name>__sfs_outgroup_blastdb_dir"])
        blastdb_dir = config["<step_name>__sfs_outgroup_blastdb_dir"]
        included_extensions = ['phr','pin', 'psq', 'nhr', 'nin', 'nsq']
        files = [join(blastdb_dir,fn) for fn in os.listdir(blastdb_dir)
                if any(fn.endswith(ext) for ext in included_extensions)]
        db_name = os.path.basename(os.path.splitext(files[0])[0])
        db_path = os.path.dirname(config["<step_name>__sfs_outgroup_blastdb_dir"]+"/")
    # if from makeblastdb step or raw_blastdb
    else:
        file = <step_name>__sfs_outgroup_inputs()["blastdb_dir"]
        included_extensions = ['phr','pin', 'psq', 'nhr', 'nin', 'nsq']
        files=[fn for fn in file if any(fn.endswith(ext) for ext in included_extensions)]
        db_name = os.path.basename(os.path.splitext(files[0])[0])
        db_path = os.path.dirname(files[0])

    return { "db_name": db_name, "db_path": db_path }

rule <step_name>__sfs_outgroup:
    input:
        **<step_name>__sfs_outgroup_inputs(),
    output:
        blastout = config["results_dir"] + "/" + config["<step_name>__sfs_outgroup_output_dir"] + "/blastout_filtered_" + <step_name>__db()["db_name"] + ".tsv",
        sfs_outgroup = config["results_dir"] + "/" + config["<step_name>__sfs_outgroup_output_dir"] + "/outgroup_" + <step_name>__db()["db_name"] + ".sfs.count",
    params:
        command = config["<step_name>__sfs_outgroup_command"],
        blastout = config["results_dir"] + "/" + config["<step_name>__sfs_outgroup_output_dir"] + "/blastout_" + <step_name>__db()["db_name"] + ".tsv",
        max_target_sequences =  config["<step_name>__blast_max_target_sequences"],
        max_hsps =  config["<step_name>__blast_max_hsps"],
        #db = lambda w,input: os.path.splitext(input.blastdb)[0]
        db = <step_name>__db()["db_path"] + "/" + <step_name>__db()["db_name"]
    threads:
        config["<step_name>__sfs_outgroup_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__sfs_outgroup_output_dir"] + '/sfs_outgroup_log.txt'
    conda:
       "envs/blast.yaml"
    shell:
        """
        ### Blast step to find homologous region in outgroup
        # Define output format for blast
        param_out="qseqid\\tpident\\tqstart\\tqend\\tqseq\\tsstart\\tsend\\tsseq\\tbtop\\tbitscore\\tevalue"
        echo -e "#"$param_out > {output.blastout}
        fmt=$(echo -e "6 "$param_out |sed -E 's/\\t/ /g')
        # Run blast with vcf flanking sequence as query and fasta genome outgroup as subject
        {params.command} -query {input.query} -db {params.db} -max_target_seqs {params.max_target_sequences} -max_hsps {params.max_hsps} -outfmt \"$fmt\" -num_threads {threads} 2> {log} >> {params.blastout}
        # Extract only best hit foreach blast results
        sort -k1,1 -k10,10nr -k2,2nr  {params.blastout} | sort -u -k1,1 --merge | sort -V -k 1,1 >> {output.blastout}

        ### Snom step to find hologous nucleotid in outgroup
        # Add right to exec snom script because no right when copy in /workflow/scripts/
        chmod a+x {snake_dir}/scripts/sfs_outgroup_snom
        # Run snom : from blast output find homologous of specific base. Then, use som input (blast output) and snom output to create est-sfs format output. It contains qseqid, est-sfs-format and pident. 
        {snake_dir}/scripts/sfs_outgroup_snom {output.blastout} | cat <(echo -e "ref\\talt") - | paste {output.blastout} - | awk -F "\\t" '{{if(NR==1){{for(i=1;i<=NF;i++){{if($i=="#qseqid"){{qseqid=i}};if($i=="pident"){{pident=i}};if($i=="alt"){{alt=i}}}}}}else{{split($qseqid,n,":");allele["A"]=0;allele["C"]=0;allele["G"]=0;allele["T"]=0;if($alt in allele){{allele[$alt]=1}};if($alt=="="){{allele[n[3]]=1}};print n[1]":"n[2]"\\t"allele["A"]","allele["C"]","allele["G"]","allele["T"]"\\t"$pident}}}}' > {output.sfs_outgroup}
        """

