rule <step_name>__makeblastdb:
    input:
        **<step_name>__makeblastdb_inputs(),
    output: 
        database = (
            config["results_dir"] + "/" + config["<step_name>__makeblastdb_output_dir"]+ "/" + config["<step_name>__makeblastdb_dbname"].replace(" ","_") + ".nhr",
            config["results_dir"] + "/" + config["<step_name>__makeblastdb_output_dir"]+ "/" + config["<step_name>__makeblastdb_dbname"].replace(" ","_") + ".nin",
            config["results_dir"] + "/" + config["<step_name>__makeblastdb_output_dir"]+ "/" + config["<step_name>__makeblastdb_dbname"].replace(" ","_") + ".nsq",
        )
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__makeblastdb_output_dir"]+ "/",
        command = config["<step_name>__makeblastdb_command"],
        dbtype = config["<step_name>__makeblastdb_dbtype"],
        dbname = config["<step_name>__makeblastdb_dbname"],
        filename = config["results_dir"] + "/" + config["<step_name>__makeblastdb_output_dir"]+ "/" + config["<step_name>__makeblastdb_dbname"].replace(" ","_"),
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__makeblastdb_output_dir"] + "/makeblastdb_log.txt"
    conda:
       "envs/blast.yaml"         
    shell:
        "{params.command} "
        "-in {input.genome_fasta} "
        "-dbtype {params.dbtype} "
        "-title {params.dbname} "
        "-out {params.filename} "
        "|& tee {log}"
