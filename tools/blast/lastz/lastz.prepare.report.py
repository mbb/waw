import oyaml as yaml
import sys
import os
from collections import OrderedDict

config = dict()
with open(sys.argv[1], 'r') as paramfile:
    config = yaml.load(paramfile, Loader=yaml.FullLoader)

config = config["params"]

def get_sample(logfile):
    if "_lastz_sam_stats.tsv" in logfile:
        return os.path.basename(logfile).replace("_lastz_sam_stats.tsv","")
    else:
        return "unknown"

out ="""# description: 'LASTZ mapping statistics'
# section_name: 'LASTZ stats'
# plot_type: 'table'
"""

#out += '\t'.join(['sample','totalpairs','discardpairs','%discard','combopairs','inniepairs','outiepairs','uncombopairs','%combo']) + '\n'
header = ""
stats = ""
for file in os.listdir(os.path.join(config["results_dir"],config["<step_name>__lastz_output_dir"],"stats")):
    stat = ""
    sample = get_sample(file)
    with open(os.path.join(config["results_dir"],config["<step_name>__lastz_output_dir"],"stats",file), 'r') as logfile:
        header = logfile.readline()
        stat = logfile.readline()
        stats += sample + '\t' + stat

header = "SAMPLE\t" + header
out += header + stats

with open(os.path.join(config["results_dir"],config["<step_name>__lastz_output_dir"],"lastz_stats_mqc.csv"), "w") as outfile:
    outfile.write(out)
