rule <step_name>__tlex3:
    input:
        **<step_name>__tlex3_inputs()
    output:
        Tresults =  config["results_dir"] + "/" + config["<step_name>__tlex3_output_dir"] + "/tlex_output/Tresults"
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__tlex3_output_dir"],
        command = config["<step_name>__tlex3_command"],
        pairends = "yes" if config["SeOrPe"] == "PE" else "no",
        species = "-s " + config["<step_name>__tlex3_species"] if config["<step_name>__tlex3_species"] != "" else "",
        te_list = config["<step_name>__tlex3_te_list"],
        te_annotations = config["<step_name>__tlex3_te_annotations"],
        genome_fasta = config["<step_name>__tlex3_genome_fasta"],

    log:
        config["results_dir"]+"/logs/" + config["<step_name>__tlex3_output_dir"] + "/tlex3_log.txt"
    shell:
        "cd {params.output_dir}; "
        "{params.command} "
        "-T {params.te_list} "
        "-M {params.te_annotations} "
        "-G {params.genome_fasta} "
        "-R {inputs.read_dir} "
        "-pairends {params.pairends} "
        "{params.species} "
        "|& tee {log}"