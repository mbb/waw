rule <step_name>__misa:
    input:
        **<step_name>__misa_inputs()
    output:
        annotations = config["results_dir"] + "/" + config["<step_name>__misa_output_dir"] + "/result_primers_misa.gff",
        stats = config["results_dir"] + "/" + config["<step_name>__misa_output_dir"] + "/result_primers_misa_mqc.txt",
        SsrReferenceFile = config["results_dir"] + "/" + config["<step_name>__misa_output_dir"] + "/SsrReferenceFile.fasta",
        primer3in = config["results_dir"] + "/" + config["<step_name>__misa_output_dir"] + "/SsrReferenceFile.fasta.p3in",
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__misa_output_dir"] + "/misa.log"
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__misa_output_dir"],
        search_params = config["<step_name>__misa_search_params"],
        compound_SSR  = config["<step_name>__misa_compound_SSR"],
        flank_SSR = config["<step_name>__misa_flank_SSR"],
        minsize_SSR = config["<step_name>__misa_minsize_SSR"],
        maxsize_SSR = config["<step_name>__misa_maxsize_SSR"],
        command = config["<step_name>__misa_command"],
    conda:
        "envs/misa.yaml"   
    shell:
        """
        cd  {params.output_dir} && cp {params.command} . ;
         
        echo 'definition\(unit_size,min_repeats\): {params.search_params}' > misa.ini 
        echo 'interruptions\(max_difference_between_2_SSRs\): {params.compound_SSR}' >> misa.ini 
        echo 'GFF: true' >> misa.ini 
        
        perl misa.pl {input.fasta} 2>/dev/null 1> {log};
        
        for i in *.gff; 
        do 
         grep -v 'compound' $i | awk '{{ if ($5-$4 >= {params.minsize_SSR} && $5-$4 <= {params.maxsize_SSR} && $4 > {params.flank_SSR}) {{ match($9, /\(([ATGC]*)\)([0-9]*)/,a); print $1 "\\t" $4-{params.flank_SSR} "\\t" $5+{params.flank_SSR} "\\t" a[1]a[2] }} }}' >> {output.annotations}; 
        done  
        bedtools getfasta -fi {input.fasta} -bed {output.annotations}  -fo {output.SsrReferenceFile} -name+;
        mv {input.fasta}.statistics statistics.txt;

        echo "# plot_type: 'table'" > {output.stats}
        echo "# pconfig:" >> {output.stats}
        echo "#     format: '{{:,.0f}}'" >> {output.stats}
        echo "#     scale: false" >> {output.stats}
        awk '/RESULTS OF/{{flag=1; next}} /Distri/{{flag=0}} flag' statistics.txt | grep -v "\=\=" | grep -v "^$" | sed 's/ \{{2,\}}/\t/g'  >> {output.stats}
        awk '/Distri/{{flag=1; next}} /Frequency of identified SSR motifs/{{flag=0}} flag' statistics.txt | grep -v "\-\-" | grep -v "^$" >> {output.stats};

        awk -v flank={params.flank_SSR} '
         BEGIN{{chr="000"}}
         {{
            if (index($0, ">") == 1) 
            {{
                split($0, a, ":")
                match(a[1], /[ATGCatgc]+/) 
                motif = substr(a[1], RSTART, RLENGTH)
                match(a[1], /[0-9]+/)

                nbrepet = substr(a[1], RSTART, RLENGTH)
                
                start = flank 
                len = nbrepet * length(motif)
                if (a[3] != chr) {{chr = a[3]; cpt=1}}
                else cpt = cpt+1
                chrpos = a[4]
            }}
            else 
            {{
                seq = $0
                print "SEQUENCE_ID=" chr "_" chrpos "_" motif "_" nbrepet  > "{output.primer3in}"
                print "SEQUENCE_TEMPLATE="seq > "{output.primer3in}"
                print "PRIMER_PRODUCT_SIZE_RANGE=100-280" > "{output.primer3in}"
                print "SEQUENCE_TARGET=" start "," len > "{output.primer3in}"
                print "PRIMER_MAX_END_STABILITY=250"   > "{output.primer3in}"
                print "PRIMER_NUM_RETURN=2"   > "{output.primer3in}"
                print "=" > "{output.primer3in}"
            }}    
        }}' {output.SsrReferenceFile}
        """
