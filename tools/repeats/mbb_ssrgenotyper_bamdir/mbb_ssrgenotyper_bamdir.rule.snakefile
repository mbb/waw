rule <step_name>__mbb_ssrgenotyper_bamdir:
    input:
        **<step_name>__mbb_ssrgenotyper_bamdir_inputs()
    output:
        stats = config["results_dir"] + "/" + config["<step_name>__mbb_ssrgenotyper_bamdir_output_dir"] + "/ssrgenotyper.statistics",
        SSRFile = config["results_dir"] + "/" + config["<step_name>__mbb_ssrgenotyper_bamdir_output_dir"] + "/SsrFile_mqc.tsv",
        genepop = config["results_dir"] + "/" + config["<step_name>__mbb_ssrgenotyper_bamdir_output_dir"] + "/SsrFile.gen",
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__mbb_ssrgenotyper_bamdir_output_dir"] + "/mbb_ssrgenotyper_bamdir.log"
    params:
        output_dir    = config["results_dir"] + "/" + config["<step_name>__mbb_ssrgenotyper_bamdir_output_dir"],
        command       = config["<step_name>__mbb_ssrgenotyper_bamdir_command"],
        #genepop      = "--Genepop" if ( config["<step_name>__mbb_ssrgenotyper_bamdir_Genepop"] == True) else "",
        support       = config["<step_name>__mbb_ssrgenotyper_bamdir_Support"],
        minorAllele   = config["<step_name>__mbb_ssrgenotyper_bamdir_MinorAlleleHet"],
        refunitsmin   =  config["<step_name>__mbb_ssrgenotyper_bamdir_RefUnitsMin"],
        motifSize     = config["<step_name>__mbb_ssrgenotyper_bamdir_MotifSize"],
        qualityfilter = config["<step_name>__mbb_ssrgenotyper_bamdir_QualityFilter"],
        filterloci    = config["<step_name>__mbb_ssrgenotyper_bamdir_FilterDataLoci"],
        filtersam     = config["<step_name>__mbb_ssrgenotyper_bamdir_filterDataSam"],
        popunitsmin   = config["<step_name>__mbb_ssrgenotyper_bamdir_PopUnitsMin"],
        spurious      = config["<step_name>__mbb_ssrgenotyper_bamdir_spuriousAlleleRemoval"],
        mismatch      = config["<step_name>__mbb_ssrgenotyper_bamdir_mismatch"],
    conda:
        "envs/mbb_ssrgenotyper.yaml"   
    shell:
        """
        # Here the bam files are from a mapping on all genome
        # we have to filter only reads mapping on regions given in input.coords input file
        # the resulting sub-sam files must have Reference sequence NAME identical to those in input.fasta
        # coords and fasta are supposed to come from misa tool !
        echo -n > {params.output_dir}/liste_sams.txt
        echo -n > {params.output_dir}/shortnames_sams.txt
        for i in {input.bams}/*.bam
        do
          if [ -f "$i.bai" ]; then
              echo "$FILE exists."
          else 
              echo "indexing $i";
              samtools index $i
          fi

          
          echo -n > $i.sam;
          while read ref start end motif
          do    
              region=$ref":"${{start}}"-"${{end}} #region spanning the ssr 
              # filter reads mapping on this region and set there name as in the fasta file
              samtools view $i $region | awk -v newref=$motif"::"$region 'BEGIN{{OFS="\\t"}}{{$3=newref; print}}' >> $i.sam
          done < {input.coords}
              
          #create a list of sam files
          echo $i.sam >>{params.output_dir}/liste_sams.txt
          echo $i.sam $(basename $i.sam .sam) >>{params.output_dir}/shortnames_sams.txt
              
        done

        {params.command} {input.fasta} {params.output_dir}/liste_sams.txt {params.output_dir}/SsrFile --Genepop \
        --Support {params.support} \
        --MinorAlleleHet {params.minorAllele} \
        --RefUnitsMin {params.refunitsmin} \
        --MotifSize {params.motifSize} \
        --FilterDataLoci {params.filterloci} \
        --filterDataSam {params.filtersam} \
        --PopUnitsMin {params.popunitsmin} \
        --spuriousAlleleRemoval {params.spurious} \
        --mismatch {params.mismatch} \
        --QualityFilter {params.qualityfilter} |& tee {log};

        for i in {input.bams}/*.bam.sam
        do
         rm $i
        done
        
        mv {params.output_dir}/SsrFile.ssrstat {output.stats};

        sed -i 's/,/\|/g' {params.output_dir}/SsrFile.ssr;
        cat {params.output_dir}/SsrFile.ssr > {output.SSRFile};
        sed -i '1 i\# plot_type: "table"' {output.SSRFile};
        sed -i '2 i\# pconfig:'  {output.SSRFile};
        sed -i '3 i\#     format: "{{:,.3f}}"'  {output.SSRFile};
        sed -i '4 i\#     scale: false' {output.SSRFile};

        while read long short
        do
           sed -i -e "s|${{long}}|${{short}}|"  {output.SSRFile}
        done < {params.output_dir}/shortnames_sams.txt

        #do the same for genepop output
        if [ -f {params.output_dir}/SsrFile.pop.txt ];
        then
          while read long short
          do
            sed -i -e "s|${{long}}|${{short}}|"  {params.output_dir}/SsrFile.pop.txt
            done < {params.output_dir}/shortnames_sams.txt
          mv {params.output_dir}/SsrFile.pop.txt {params.output_dir}/SsrFile.gen
        fi

        rm {params.output_dir}/SsrFile.ssr;

        """
