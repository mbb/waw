def to_exclude():
    exclude = "";
    if config["<step_name>__micro_primers_exclude_c"] == True:
       exclude = exclude + "c";
    if config["<step_name>__micro_primers_exclude_cstar"] == True:  
       exclude = exclude+",c*" if exclude != "" else  "c*"
    if config["<step_name>__micro_primers_exclude_p1"] == True:  
       exclude = exclude+",p1" if exclude != "" else  "p1" 
    if config["<step_name>__micro_primers_exclude_p2"] == True:  
       exclude = exclude+",p2" if exclude != "" else  "p2" 
    if config["<step_name>__micro_primers_exclude_p3"] == True:  
       exclude = exclude+",p3" if exclude != "" else  "p3" 
    if config["<step_name>__micro_primers_exclude_p4"] == True:  
       exclude = exclude+",p4" if exclude != "" else  "p4" 
    if config["<step_name>__micro_primers_exclude_p5"] == True:  
       exclude = exclude+",p5" if exclude != "" else "p5"                 
    if config["<step_name>__micro_primers_exclude_p6"] == True:  
       exclude = exclude+",p6" if exclude != "" else "p6"

    exclude = "-exc " + exclude if exclude != "" else ""   

    return exclude

rule <step_name>__micro_primers:
        input:
            **<step_name>__micro_primers_inputs(),
        output: 
            primers = config["results_dir"] + "/" + config["<step_name>__micro_primers_output_dir"] + "/{sample}_result_primers_mqc.tsv",
        params: 
            command = config["<step_name>__micro_primers_command"],
            output_dir = config["results_dir"] + "/" + config["<step_name>__micro_primers_output_dir"]+ "/",
            exclude = to_exclude(),
            special = "--special" if config["<step_name>__micro_primers_special"] == True else "",
            filter  = "--p3filter" if config["<step_name>__micro_primers_p3f"] == True else "",
            skip_cutadapt =  "--skip_cutadapt" if config["<step_name>__micro_primers_skip_cutadapt"] == True else "",
            minflank = config["<step_name>__micro_primers_minflank"],
            mincount = config["<step_name>__micro_primers_mincount"],
            mindiff  = config["<step_name>__micro_primers_mindiff"],
        conda:
           "envs/micro_primers.yaml"                
        log: 
            config["results_dir"] + "/logs/" + config["<step_name>__micro_primers_output_dir"] + "/{sample}_micro_primers_log.txt"
        shell:
            "{params.command} "
            "-r1 {input.read} "
            "-r2 {input.read2} "
            "-o {output.primers} "
            "{params.exclude} "
            "-flank {params.minflank} "
            "-cnt {params.mincount} "
            "-diff {params.mindiff} "
            "{params.skip_cutadapt} "
            "{params.special} "
            "&& mv {output.primers}.txt {output.primers} "
            "|& tee {log}; "
