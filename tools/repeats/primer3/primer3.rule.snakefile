rule <step_name>__primer3:
        input:
            **<step_name>__primer3_inputs(),
        output: 
            stats = config["results_dir"] + "/" + config["<step_name>__primer3_output_dir"] + "/summary_primers_top100_mqc.tsv",
            primer3res = config["results_dir"] + "/" + config["<step_name>__primer3_output_dir"] + "/primer3.p3out",
            blastout = config["results_dir"] + "/" + config["<step_name>__primer3_output_dir"] + "/blast_primer3.tsv",
            previewblast = config["results_dir"] + "/" + config["<step_name>__primer3_output_dir"] +  "/blast_primer3_top100_mqc.tsv",
        params: 
            command = config["<step_name>__primer3_command"],
            output_dir = config["results_dir"] + "/" + config["<step_name>__primer3_output_dir"]+ "/",
            prod_size_range = config["<step_name>__PRIMER_PRODUCT_SIZE_RANGE"],
            max_end_stab = config["<step_name>__PRIMER_MAX_END_STABILITY"],
            min_size = config["<step_name>__PRIMER_MIN_SIZE"],
            max_size = config["<step_name>__PRIMER_MAX_SIZE"],
            min_TM = config["<step_name>__PRIMER_MIN_TM"],
            max_TM = config["<step_name>__PRIMER_MAX_TM"],
            min_GC = config["<step_name>__PRIMER_MIN_GC"],
        conda:
           "envs/primer3.yaml"                
        log: 
            config["results_dir"] + "/logs/" + config["<step_name>__primer3_output_dir"] + "/primer3_log.txt"
        threads:
            config["<step_name>__PRIMER_threads"]    
        shell:
            """
            cp {input.primerparam} {input.primerparam}_mod;
            sed -i 's/PRIMER_PRODUCT_SIZE_RANGE=.*/PRIMER_PRODUCT_SIZE_RANGE={params.prod_size_range}/' {input.primerparam}_mod;
            sed -i 's/PRIMER_MAX_END_STABILITY=.*/PRIMER_MAX_END_STABILITY={params.max_end_stab}/' {input.primerparam}_mod;
            optSize=20;
            if [ {params.min_size} > $optSize ] 
            then
              optSize={params.min_size}
            fi
            sed -i "s/^=/PRIMER_MIN_SIZE={params.min_size}\\nPRIMER_MAX_SIZE={params.max_size}\\nPRIMER_MIN_TM={params.min_TM}\\nPRIMER_MAX_TM={params.max_TM}\\nPRIMER_MIN_GC={params.min_GC}\\nPRIMER_OPT_SIZE=$optSize\\n=/" {input.primerparam}_mod;

            {params.command} {input.primerparam}_mod --output {output.primer3res} |& tee {log}; 

            grep 'SEQUENCE_ID\|PRIMER_LEFT_0_SEQUENCE\|PRIMER_LEFT_0_TM\|PRIMER_RIGHT_0_SEQUENCE\|PRIMER_RIGHT_0_TM\|PRIMER_LEFT_1_SEQUENCE\|PRIMER_LEFT_1_TM\|PRIMER_RIGHT_1_SEQUENCE\|PRIMER_RIGHT_1_TM' {output.primer3res} | 
            awk '
            BEGIN {{OFS="\\t"; seqid = "none";}}
            {{
                if ( index($0,"SEQUENCE_ID") == 1) {{
                    split($0, a, "=");  
                    if (seqid != "none") {{ 
                        print seqid, Lseq0, L0Tm, Rseq0, R0Tm, Lseq1, L1Tm, Rseq1, R1Tm
                        seqid = a[2];
                        Lseq0="NULL"; L0Tm="NULL"; Rseq0="NULL"; R0Tm="NULL"; Lseq1="NULL";L1Tm="NULL"; Rseq1="NULL";R1Tm="NULL";
                    }}
                    else 
                    {{
                        seqid = a[2];
                        Lseq0="NULL"; L0Tm="NULL"; Rseq0="NULL"; R0Tm="NULL"; Lseq1="NULL";L1Tm="NULL"; Rseq1="NULL";R1Tm="NULL";
                    }}
                }}
                else {{
                    if (index($0,"PRIMER_LEFT_0_SEQUENCE")==1) {{split($0, a, "="); Lseq0=a[2]}}
                    if (index($0,"PRIMER_LEFT_0_TM")==1) {{split($0, a, "="); L0Tm=a[2]}}
                    if (index($0,"PRIMER_RIGHT_0_SEQUENCE")==1) {{split($0, a, "="); Rseq0=a[2]}}
                    if (index($0,"PRIMER_RIGHT_0_TM")==1) {{split($0, a, "="); R0Tm=a[2]}}
                    if (index($0,"PRIMER_LEFT_1_SEQUENCE")==1) {{split($0, a, "="); Lseq1=a[2]}}
                    if (index($0,"PRIMER_LEFT_1_TM")==1) {{split($0, a, "="); L1Tm=a[2]}}
                    if (index($0,"PRIMER_RIGHT_1_SEQUENCE")==1) {{split($0, a, "="); Rseq1=a[2]}}
                    if (index($0,"PRIMER_RIGHT_1_TM")==1) {{split($0, a, "="); R1Tm=a[2]}}
                }}    

            }}
            END{{print seqid, Lseq0, L0Tm, Rseq0, R0Tm, Lseq1, L1Tm, Rseq1, R1Tm }}' > {output.stats} ;

            sed -ni '1,100p' {output.stats};
            sed -i '1 i\# plot_type: "table"' {output.stats};
            sed -i '2 i\# pconfig:'  {output.stats};
            sed -i '3 i\#     format: "{{:,.3f}}"'  {output.stats};
            sed -i '4 i\#     scale: false' {output.stats};
            sed -i '5 i\chr_start_end_motif_repet\\tprimer0_left\\tTmL0\\tprimer0_right\\tTmR0\\tprimer1_left\\tTmL1\\tprimer1_right\\tTmR1' {output.stats};

            #Check PCR primer specificity by BLASTing : https://sequenceserver.com/blog/check-primer-specificity-with-blast/
            
            #concatenate the right and left primers together, separated by a few “NNN” nucleotides
            #here keep only 2 primers per target
            grep -E "SEQUENCE_ID|PRIMER_(LEFT|RIGHT)_(0|1)_SEQUENCE" {output.primer3res} | \

            awk '
                BEGIN {{OFS="\t"; seqid = "none";c["A"] = "T"; c["C"] = "G"; c["G"] = "C"; c["T"] = "A" ;}}
                function revcomp(arg) {{ o = ""; for(i = length(arg); i > 0; i--)  {{o = o c[substr(arg, i, 1)]}}    return(o);}}
                {{
                    if ( index($0,"SEQUENCE_ID") == 1) {{
                        split($0, a, "=");  
                        if (seqid != "none") {{
                            if (Lseq0!="NULL" && Rseq0!="NULL") print ">"seqid"_prm0\\n"Lseq0 "NNNNNNNNN" revcomp(Rseq0)
                            if (Lseq1!="NULL" && Rseq1!="NULL") print ">"seqid"_prm1\\n"Lseq1 "NNNNNNNNN" revcomp(Rseq1)
                            seqid = a[2];
                            Lseq0="NULL";  Rseq0="NULL";  Lseq1="NULL"; Rseq1="NULL";
                        }}
                        else 
                        {{
                            seqid = a[2];
                            Lseq0="NULL";  Rseq0="NULL";  Lseq1="NULL"; Rseq1="NULL";
                        }}
                    }}
                    else {{
                        if (index($0,"PRIMER_LEFT_0_SEQUENCE")==1) {{split($0, a, "="); Lseq0=a[2]}}
                        if (index($0,"PRIMER_RIGHT_0_SEQUENCE")==1) {{split($0, a, "="); Rseq0=a[2]}}
                        if (index($0,"PRIMER_LEFT_1_SEQUENCE")==1) {{split($0, a, "="); Lseq1=a[2]}}
                        if (index($0,"PRIMER_RIGHT_1_SEQUENCE")==1) {{split($0, a, "="); Rseq1=a[2]}}
                    }}    

                }}
                END{{
                    if (Lseq0!="NULL" && Rseq0!="NULL") print ">"seqid"_prm0\\n"Lseq0 "NNNNNNNNN" revcomp(Rseq0)
                    if (Lseq1!="NULL" && Rseq1!="NULL") print ">"seqid"_prm1\\n"Lseq1 "NNNNNNNNN" revcomp(Rseq1)
                    }}'  > {params.output_dir}/flank_primers_stitch.fa
            
            makeblastdb -in {input.refgenome} -dbtype nucl ;
            echo -e 'qseqid\tsseqid\tpident\tlength\tmismach\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore\tqlen' > {output.blastout}; 

            blastn -outfmt '6 std qlen' -num_threads {threads} -evalue 0.01 -task blastn-short -dust no -soft_masking false -penalty -3 -reward 1 -gapopen 5 -gapextend 2 -db {input.refgenome} -query {params.output_dir}/flank_primers_stitch.fa >> {output.blastout};
 
            head -n 100  {output.blastout} > {output.previewblast};
            sed -i '1 i\# plot_type: "table"' {output.previewblast};
            sed -i '2 i\# pconfig:'  {output.previewblast};
            sed -i '3 i\#     format: "{{:,.3f}}"'  {output.previewblast};
            sed -i '4 i\#     scale: false' {output.previewblast};
            """  
