rule <step_name>__ssrgenotyper:
    input:
        **<step_name>__ssrgenotyper_inputs()
    output:
        stats = config["results_dir"] + "/" + config["<step_name>__ssrgenotyper_output_dir"] + "/ssrgenotyper.statistics",
        SSRFile = config["results_dir"] + "/" + config["<step_name>__ssrgenotyper_output_dir"] + "/SsrFile.ssr_mqc.tsv",
        genepop = config["results_dir"] + "/" + config["<step_name>__ssrgenotyper_output_dir"] + "/SsrFile.gen",
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__ssrgenotyper_output_dir"] + "/ssrgenotyper.log"
    params:
        output_dir    = config["results_dir"] + "/" + config["<step_name>__ssrgenotyper_output_dir"],
        command       = config["<step_name>__ssrgenotyper_command"],
        #genepop       = "--Genepop" if ( config["<step_name>__ssrgenotyper_Genepop"] == True) else "",
        support       = config["<step_name>__ssrgenotyper_Support"],
        minorAllele   = config["<step_name>__ssrgenotyper_MinorAlleleHet"],
        refunitsmin   =  config["<step_name>__ssrgenotyper_RefUnitsMin"],
        qualityfilter = config["<step_name>__ssrgenotyper_QualityFilter"],
        filterloci    = config["<step_name>__ssrgenotyper_FilterDataLoci"],
        filtersam     = config["<step_name>__ssrgenotyper_filterDataSam"],
        popunitsmin   = config["<step_name>__ssrgenotyper_PopUnitsMin"],
        spurious      = config["<step_name>__ssrgenotyper_spuriousAlleleRemoval"],
        mismatch      = config["<step_name>__ssrgenotyper_mismatch"],
    conda:
        "envs/ssrgenotyper.yaml"   
    shell:
        """
        #create sam files
        echo -n > {params.output_dir}/liste_sams.txt
        echo -n > {params.output_dir}/shortnames_sams.txt
        for i in {input.bams}
        do
          samtools view $i > $i.sam
          #create a list of sam files
          echo $i.sam >>{params.output_dir}/liste_sams.txt
          echo $i.sam $(basename $i.sam .sam) >>{params.output_dir}/shortnames_sams.txt
        done

        {params.command} {input.fasta} {params.output_dir}/liste_sams.txt {params.output_dir}/SsrFile --Genepop \
        --Support {params.support} \
        --MinorAlleleHet {params.minorAllele} \
        --RefUnitsMin {params.refunitsmin} \
        --FilterDataLoci {params.filterloci} \
        --filterDataSam {params.filtersam} \
        --PopUnitsMin {params.popunitsmin} \
        --spuriousAlleleRemoval {params.spurious} \
        --mismatch {params.mismatch} \
        --QualityFilter {params.qualityfilter} |& tee {log};

        for i in {input.bams}
        do
         rm $i.sam
        done
        
        mv {params.output_dir}/SsrFile.ssrstat {output.stats};

        sed -i 's/,/\|/g' {params.output_dir}/SsrFile.ssr;
        cat {params.output_dir}/SsrFile.ssr > {output.SSRFile};
        sed -i '1 i\# plot_type: "table"' {output.SSRFile};
        sed -i '2 i\# pconfig:'  {output.SSRFile};
        sed -i '3 i\#     format: "{{:,.3f}}"'  {output.SSRFile};
        sed -i '4 i\#     scale: false' {output.SSRFile};

        while read long short
        do
           sed -i -e "s|${{long}}|${{short}}|"  {output.SSRFile}
        done < {params.output_dir}/shortnames_sams.txt

        #do the same for genepop output
        if [ -f {params.output_dir}/SsrFile.pop.txt ];
        then
          while read long short
          do
            sed -i -e "s|${{long}}|${{short}}|"  {params.output_dir}/SsrFile.pop.txt
            done < {params.output_dir}/shortnames_sams.txt
          mv {params.output_dir}/SsrFile.pop.txt {params.output_dir}/SsrFile.gen
        fi

        rm {params.output_dir}/SsrFile.ssr;

        """
