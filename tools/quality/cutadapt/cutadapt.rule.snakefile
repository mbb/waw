#def generate_output(logs):
#    for log in logs:
#        sample = log.split("/")[-1].strip("_cutadapt_log.txt")
#        command = """processed=$(grep -m 1 "processed" """ + log + """ | awk \'BEGIN {{FS=":"}}{{split($2,a," ");gsub(",","",a[1]);print a[1]}}\') ;"""
#        command += """filtred=$(grep -m 1 "passing filter" """ + log + """ | awk \'BEGIN {{FS=":"}}{{split($2,a," ");gsub(",","",a[1]);print a[1]}}\') ;"""
#        command += 'echo -e "'+sample+'\t$processed\t$filtred" '
#    return command

if config["SeOrPe"] == "PE":

    rule <step_name>__cutadapt_PE:
        input:
            **<step_name>__cutadapt_PE_inputs()
        output:
            read_trimmed = config["results_dir"]+"/"+config["<step_name>__cutadapt_PE_output_dir"]+"/{sample}_trimmed_R1.fq.gz",
            read2_trimmed = config["results_dir"]+"/"+config["<step_name>__cutadapt_PE_output_dir"]+"/{sample}_trimmed_R2.fq.gz",
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__cutadapt_PE_output_dir"] + '/{sample}_cutadapt_log.txt'
        params:
            command = config["<step_name>__cutadapt_PE_command"],
            qc_score = config["<step_name>__cutadapt_qc_score"],
            qc_min = config["<step_name>__cutadapt_qc_min"],
            a = "-g" + config["<step_name>__cutadapt_a"] if (config["<step_name>__cutadapt_a"] != "") else "",
            A = "-G" + config["<step_name>__cutadapt_A"] if (config["<step_name>__cutadapt_A"] != "") else "",
            max_N = config["<step_name>__cutadapt_max_N"]
        threads:
            config["<step_name>__cutadapt_threads"]
        conda:
            "envs/cutadapt.yaml"
        shell:
            "{params.command} "
            "--cores {threads} "
            "{params.qc_score} "
            "{params.a} "
            "{params.A} "
            "-q {params.qc_min} "
            "-m 10 "
            "-o {output.read_trimmed} "
            "-p {output.read2_trimmed} "
            "--max-n {params.max_N} "
            "{input.read} "
            "{input.read2} "
            "|& tee {log}"


elif config["SeOrPe"] == "SE":

    rule <step_name>__cutadapt_SE:
        input:
            **<step_name>__cutadapt_SE_inputs()
        output:
            read_trimmed = config["results_dir"]+"/"+config["<step_name>__cutadapt_SE_output_dir"]+"/{sample}_trimmed.fq.gz",
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__cutadapt_SE_output_dir"] + '/{sample}_cutadapt_log.txt'
        params:
            command = config["<step_name>__cutadapt_SE_command"],
            qc_score = config["<step_name>__cutadapt_qc_score"],
            qc_min = config["<step_name>__cutadapt_qc_min"],
            a = "-g" + config["<step_name>__cutadapt_a"] if (config["<step_name>__cutadapt_a"] != "") else "",
            max_N = config["<step_name>__cutadapt_max_N"]
        threads:
            config["<step_name>__cutadapt_threads"]
        conda:
            "envs/cutadapt.yaml"
        shell:
            "{params.command} "
            "--cores {threads} "
            "{params.qc_score} "
            "-q {params.qc_min} "
            "-o {output.read_trimmed} "
            "--max-n {params.max_N} "
            "-m 10 "
            "{input} "
            "|& tee {log}"


#rule <step_name>__cutadapt_stats:
#    input:
#        logs = expand(config["results_dir"]+'/logs/' + config["<step_name>__cutadapt_" + config["SeOrPe"] + "_output_dir"] + '/{sample}_cutadapt_log.txt',sample=SAMPLES)
#    output:
#        stats = config["results_dir"]+"/cutadapt_stats/cutadapt_stats.tsv",
#    params:
#        command =   generate_output(expand(config["results_dir"]+'/logs/' + config["<step_name>__cutadapt_" + config["SeOrPe"] + "_output_dir"] + '/{sample}_cutadapt_log.txt',sample=SAMPLES))  
#    conda:
#        "envs/cutadapt.env.yaml"
#    shell:
#        "echo -e 'sample\traw\tcutadapt_trimmed' > {output}" +
#        "{params.command} >>  {output} "
