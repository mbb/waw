if config["SeOrPe"] == "PE":

    rule <step_name>__fastp_PE:
        input:
            **<step_name>__fastp_PE_inputs()
        output:
            report_html = config["results_dir"]+"/"+config["<step_name>__fastp_PE_output_dir"]+"/fastp_report_{sample}.html",
            report_json = config["results_dir"]+"/"+config["<step_name>__fastp_PE_output_dir"]+"/fastp_report_{sample}fastp.json",
            read_preprocessed = config["results_dir"]+"/"+config["<step_name>__fastp_PE_output_dir"]+"/{sample}_R1.fq.gz",
            read2_preprocessed = config["results_dir"]+"/"+config["<step_name>__fastp_PE_output_dir"]+"/{sample}_R2.fq.gz"
        params:
            command = config["<step_name>__fastp_PE_command"],
            complexity_threshold = config["<step_name>__fastp_complexity_threshold"],
            report_title = config["<step_name>__fastp_report_title"]+" : {sample}",
            detect_adapter_for_pe = "--detect_adapter_for_pe" if config["<step_name>__fastp_adapter_detection_PE"] == True else "",
            adapter_sequence = "--adapter_sequence "+config["<step_name>__fastp_adapter_sequence"] if config["<step_name>__fastp_adapter_sequence"] != "" else "",
            adapter_sequence_R2 = "--adapter_sequence_r2 " + config["<step_name>__fastp_adapter_sequence_R2_PE"] if config["<step_name>__fastp_adapter_sequence_R2_PE"] != "" else "",
            P = "-P " + str(config["<step_name>__fastp_P_PE"]) if config["<step_name>__fastp_overrepresentation_analysis"] == True else "",
            output_dir = config["<step_name>__fastp_PE_output_dir"],
            correction = "--correction " if config["<step_name>__fastp_correction_PE"] == True else "",
            low_complexity_filter = "--low_complexity_filter " if config["<step_name>__fastp_low_complexity_filter"] == True else "",
            overrepresentation_analysis = "--overrepresentation_analysis " if config["<step_name>__fastp_overrepresentation_analysis"] == True else "",
            dedup_PE = "--dedup" if config["<step_name>__fastp_dedup_PE"] == True else "",
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__fastp_PE_output_dir"] + "/{sample}_fastp_log.txt"
        threads:
            config["<step_name>__fastp_threads"]
        conda:
            "envs/fastp.yaml"
        shell:
            "{params.command} "
            "-i {input.read} "
            "-I {input.read2} "
            "-o {output.read_preprocessed} "
            "-O {output.read2_preprocessed} "
            "-w {threads} "
            "{params.correction} "
            "{params.low_complexity_filter} "
            "--complexity_threshold {params.complexity_threshold} "
            "--html {output.report_html} "
            "--json {output.report_json} "
            "--report_title '{params.report_title}' "
            "{params.detect_adapter_for_pe} "
            "{params.adapter_sequence} "
            "{params.adapter_sequence_R2} "
            "{params.overrepresentation_analysis} "
            "{params.P} "
            "{params.dedup_PE} "
            "|& tee {log}"


elif config["SeOrPe"] == "SE":

    rule <step_name>__fastp_SE:
        input:
            **<step_name>__fastp_SE_inputs()
        output:
            report_html = config["results_dir"]+"/"+config["<step_name>__fastp_SE_output_dir"]+"/fastp_report_{sample}.html",
            report_json = config["results_dir"]+"/"+config["<step_name>__fastp_SE_output_dir"]+"/fastp_report_{sample}fastp.json",
            read_preprocessed = config["results_dir"]+"/"+config["<step_name>__fastp_SE_output_dir"]+"/{sample}.fq.gz",
        params:
            command = config["<step_name>__fastp_SE_command"],
            complexity_threshold = config["<step_name>__fastp_complexity_threshold"],
            report_title = config["<step_name>__fastp_report_title"],
            adapter_sequence = config["<step_name>__fastp_adapter_sequence"],
            P = config["<step_name>__fastp_P_SE"],
            output_dir = config["<step_name>__fastp_SE_output_dir"],
            low_complexity_filter = "--low_complexity_filter " if config["<step_name>__fastp_low_complexity_filter"] == True else "",
            overrepresentation_analysis = "--overrepresentation_analysis " if config["<step_name>__fastp_overrepresentation_analysis"] == True else "",
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__fastp_SE_output_dir"] + "/{sample}_fastp_log.txt"
        threads:
            config["<step_name>__fastp_threads"]
        conda:
            "envs/fastp.yaml"            
        shell:
            "{params.command} "
            "-i {input.read} "
            "-o {output.read_preprocessed} "
            "-w {threads} "
            "{params.low_complexity_filter} "
            "--complexity_threshold {params.complexity_threshold} "
            "--html {output.report_html} "
            "--json {output.report_json} "
            "--report_title {params.report_title} "
            "--adapter_sequence '{params.adapter_sequence}' "
            "{params.overrepresentation_analysis} "
            "-P {params.P} "
            "|& tee {log}"
