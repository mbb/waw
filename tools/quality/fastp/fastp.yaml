{
  id: fastp,
  name: fastp,
  article: 10.1093/bioinformatics/bty560,
  website: "https://github.com/OpenGene/fastp",
  git: "https://github.com/OpenGene/fastp",
  description: "A tool designed to provide fast all-in-one preprocessing for FastQ files.",
  version: "0.23.4",
  documentation: "https://github.com/OpenGene/fastp",
  multiqc: "fastp",
  commands:
    [
      {
        name: fastp_PE,
        cname: "Fastp PE",
        command: fastp,
        category: "quality",
        output_dir: fastp_PE,
        inputs: [{ name: read, type: "reads_PE" }, { name: read2, type: "reads_PE" }],
        outputs:  [
          { name: report_html, type: "html", file: "fastp_report_{sample}.html", description: "Report HTML of preprocessing done" },
          { name: report_json, type: "json", file: "fastp_report_{sample}fastp.json", description: "Report JSON of preprocessing done" },
          { name: read_preprocessed, type: "reads_PE", file: "{sample}_R1.fq.gz", description: "Reads R1 preprocess" },
          { name: read2_preprocessed, type: "reads_PE", file: "{sample}_R2.fq.gz", description: "Reads R2 preprocess" },
        ],
        options:
          [
            {
              name: fastp_threads,
              prefix: --thread,
              type: numeric,
              value: 4,
              min: 1,
              max: NA,
              step: 1,
              label: "Number of threads to use",
            },
            {
              name: fastp_complexity_threshold,
              prefix: --complexity_threshold,
              type: numeric,
              value: 30,
              min: 1,
              max: NA,
              step: 1,
              label: "The threshold for low complexity filter (0~100)",
            },
            {
              name: fastp_report_title,
              prefix: --report_title,
              type: text,
              value: "fastp report",
              label: "fastp report title",
            },
            {
              name: "fastp_adapter_detection_PE",
              type: "checkbox",
              value: TRUE,
              label: "adapter sequence auto-detection is disabled by default since the adapters can be trimmed by overlap analysis. However, you can specify --detect_adapter_for_pe to enable it",
            },
            {
              name: fastp_adapter_sequence,
              prefix: --adapter_sequence,
              type: text,
              value: "",
              label: "The adapter for read1. For SE data, if not specified, the adapter will be auto-detected. For PE data, this is used if R1/R2 are found not overlapped."
            },
            {
              name: fastp_adapter_sequence_R2_PE,
              prefix: --adapter_sequence_r2,
              type: text,
              value: "",
              label: "the adapter for read2 (PE data only). This is used if R1/R2 are found not overlapped. If not specified, it will be the same as <adapter_sequence>"
            },
            {
              name: fastp_P_PE,
              prefix: -P,
              type: numeric,
              value: 20,
              min: 1,
              max: NA,
              step: 1,
              label: "For example, if you set -P 100, only 1/100 reads will be used for counting, and if you set -P 1, all reads will be used and slower.",
            },
            {
              name: "fastp_correction_PE",
              type: "checkbox",
              value: TRUE,
              label: "Enable base correction in overlapped regions",
            },
            {
              name: "fastp_low_complexity_filter",
              type: "checkbox",
              value: TRUE,
              label: "Enable low complexity filter. The complexity is defined as the percentage of base that is different from its next base (base[i] != base[i+1]).",
            },
            {
              name: "fastp_overrepresentation_analysis",
              type: "checkbox",
              value: TRUE,
              label: "enable overrepresented sequence analysis.",
            },
            {
              name: "fastp_dedup_PE",
              prefix: --dedup,
              type: "checkbox",
              value: TRUE,
              label: "deduplication for FASTQ data",
            },
          ],
      },
      {
        name: fastp_SE,
        cname: "Fastp SE",
        command: fastp,
        category: "quality",
        output_dir: fastp_SE,
        inputs: [{ name: read, type: "reads_SE" }],
        outputs:  [
          { name: report_html, type: "html", file: "fastp_report_{sample}.html", description: "Report HTML of preprocessing done" },
          { name: report_json, type: "json", file: "fastp_report_{sample}fastp.json", description: "Report JSON of preprocessing done" },
          { name: read_preprocessed, type: "reads_SE", file: "{sample}.fq.gz", description: "Reads preprocess" },
        ],
        options:
          [
            {
              name: fastp_threads,
              prefix: --thread,
              type: numeric,
              value: 4,
              min: 1,
              max: NA,
              step: 1,
              label: "Number of threads to use",
            },
            {
              name: fastp_complexity_threshold,
              prefix: --complexity_threshold,
              type: numeric,
              value: 30,
              min: 1,
              max: NA,
              step: 1,
              label: "The threshold for low complexity filter (0~100)",
            },
            {
              name: fastp_report_title,
              prefix: --report_title,
              type: text,
              value: "fastp report",
              label: "fastp report title",
            },
            {
              name: fastp_adapter_sequence,
              prefix: --adapter_sequence,
              type: text,
              value: "",
              label: "The adapter for read1. For SE data, if not specified, the adapter will be auto-detected. For PE data, this is used if R1/R2 are found not overlapped."
            },
            {
              name: fastp_P_SE,
              prefix: -P,
              type: numeric,
              value: 20,
              min: 1,
              max: NA,
              step: 1,
              label: "For example, if you set -P 100, only 1/100 reads will be used for counting, and if you set -P 1, all reads will be used and slower.",
            },
            {
              name: "fastp_low_complexity_filter",
              type: "checkbox",
              value: TRUE,
              label: "Enable low complexity filter. The complexity is defined as the percentage of base that is different from its next base (base[i] != base[i+1]).",
            },
            {
              name: "fastp_overrepresentation_analysis",
              type: "checkbox",
              value: TRUE,
              label: "enable overrepresented sequence analysis.",
            },
          ],
      },
    ],
  install: {
    # fastp: [
    #   "wget https://github.com/OpenGene/fastp/archive/v0.23.0.tar.gz",
    #   "tar -xvzf v0.23.0.tar.gz",
    #   "cd fastp-0.23.0",
    #   "make",
    #   "mv fastp /opt/biotools/bin/fastp",
    #   "cd ..",
    #   "rm -r fastp-0.23.0 v0.23.0.tar.gz "
    # ]
  },
  citations:  {
    fastp: [
      "Shifu Chen, Yanqing Zhou, Yaru Chen, Jia Gu, fastp: an ultra-fast all-in-one FASTQ preprocessor, Bioinformatics, Volume 34, Issue 17, 01 September 2018, Pages i884-i890, https://doi.org/10.1093/bioinformatics/bty560"
    ]
  }
}
