rule <step_name>__quast:
    input:
        **<step_name>__quast_inputs(),
    output: 
        report = config["results_dir"] + "/" + config["<step_name>__quast_output_dir"] + "/report.tsv",
    params:
        command = config["<step_name>__quast_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__quast_output_dir"]+ "/",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__quast_output_dir"] + "/quast_log.txt"
    threads: 
        config["<step_name>__quast_threads"]
    conda:
       "envs/quast.yaml"    
    shell: 
        "{params.command} "
        "{input.assembly} "
        "-o {params.output_dir} "
        "--threads {threads}"
