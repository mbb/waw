rule <step_name>__genomescope:
    input:
        **<step_name>__genomescope_inputs()
    output:
        linear_plot = config["results_dir"] + "/" + config["<step_name>__genomescope_output_dir"] + "/linear_plot_mqc.png",
        log_plot = config["results_dir"] + "/" + config["<step_name>__genomescope_output_dir"] + "/log_plot_mqc.png",
        transformed_linear_plot = config["results_dir"] + "/" + config["<step_name>__genomescope_output_dir"] + "/transformed_linear_plot_mqc.png",
        transformed_log_plot = config["results_dir"] + "/" + config["<step_name>__genomescope_output_dir"] + "/transformed_log_plot_mqc.png",       
        Summary = config["results_dir"] + "/" + config["<step_name>__genomescope_output_dir"] + "/GenomeScope_Summary_mqc.csv"
    params:
        command = config["<step_name>__genomescope_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__genomescope_output_dir"],
        kmer_len = config["<step_name>__genomescope_kmer_len"],
        ploidy = config["<step_name>__genomescope_ploidy"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__genomescope_output_dir"] + '/genomescope_log.txt'
    conda:
        "envs/genomescope.yaml"    
    shell:
        "{params.command} "
        "-i {input.kmer_histo} "
        "-k {params.kmer_len} "
        "-p {params.ploidy} "
        "-o {params.output_dir} "
        "|& tee {log};"
        # prepare mqc custom content
        "mv {params.output_dir}/linear_plot.png {params.output_dir}/linear_plot_mqc.png && "
        "mv {params.output_dir}/log_plot.png {params.output_dir}/log_plot_mqc.png && "
        "mv {params.output_dir}/transformed_linear_plot.png {params.output_dir}/transformed_linear_plot_mqc.png && "
        "mv {params.output_dir}/transformed_log_plot.png {params.output_dir}/transformed_log_plot_mqc.png && "
        "tail -n +7 {params.output_dir}/summary.txt | sed 's/ \{{2,\}}/\t/g' | sed 's/\t$//g' > {params.output_dir}/GenomeScope_Summary_mqc.csv"


