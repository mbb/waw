rule <step_name>__rnaQUAST:
    input:
        **<step_name>__rnaQUAST_inputs(),
    output: 
        report = config["results_dir"] + "/" + config["<step_name>__rnaQUAST_output_dir"] + "/report_mqc.tsv",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__rnaQUAST_output_dir"]+ "/",
        script = config["<step_name>__rnaQUAST_script"],
        ss = "-ss" if config["<step_name>__rnaQUAST_Strand_Specific_Orientation"] else "",
        reference = "-r "+config["<step_name>__rnaQUAST_Reference"] if config["<step_name>__rnaQUAST_Reference"]!="" else "",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__rnaQUAST_output_dir"] + "/rnaquast_log.txt"
    threads:
        config["<step_name>__rnaQUAST_threads"]
    conda:
       "envs/rnaquast.yaml"    
    shell:
        """
        tar -xvf {snake_dir}/{params.script} -C $CONDA_PREFIX"/bin"
        rnaQUAST.py -c {input.assembly} -1 {input.r1} -2 {input.r2} -o {params.output_dir} -t {threads} {params.ss} {params.reference}
        cp {params.output_dir}/short_report.tsv {output.report}
        awk -F '[[:space:]][[:space:]]+' '{{if($1~/Average length of assembled transcripts/){{print "Average Transcript Length\t"$2}};if($1~/Transcript N50/){{print "N50 Transcript\t"$2}}}}' {params.output_dir}/*_output/basic_metrics.txt >> {output.report}
        cp {params.output_dir}/*_output/transcript_length.png {params.output_dir}/transcript_length_mqc.png
        """

