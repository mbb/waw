if config["SeOrPe"] == "SE":

    rule <step_name>__mitoz_filter_SE:
        input:
            **<step_name>__mitoz_filter_SE_inputs()
        output:
            reads_filtered = config["results_dir"]+ "/" + config["<step_name>__mitoz_filter_SE_output_dir"] + '/{sample}.fq.gz',
        log: config["results_dir"]+'/logs/' + config["<step_name>__mitoz_filter_SE_output_dir"] + '/{sample}_mitoz_filter_log.txt'
        threads: config["<step_name>__mitoz_filter_threads"]
        conda: 
            "envs/mitoz.yaml"        
        params:
            command = config["<step_name>__mitoz_filter_SE_command"],
            output_dir = config["<step_name>__mitoz_filter_SE_output_dir"],
            prefix = "mitoz_filter_SE",
            output_file = "{sample}.fq.gz",
            clade = config["<step_name>__mitoz_filter_clade"],
            donedir = config["results_dir"]+ "/" +config["<step_name>__mitoz_filter_PE_output_dir"] + "/{sample}.result/done",
            reads_filtered = config["results_dir"]+ "/" +config["<step_name>__mitoz_filter_SE_output_dir"] + "/mitoz_filter_SE.tmp/mitoz_filter_SE/{sample}.clean.fq.gz",
        shell:
            "mkdir -p {params.output_dir}/{wildcards.sample}.result && "
            "cd {params.output_dir}/{wildcards.sample}.result; "
            "{params.command} "
            "--fq1 {input.read} "
            "--fastq_read_length 5000 "
            "--thread_number {threads} "
            "--outprefix {params.prefix} "
            "|& tee {log}; "
            "mv {params.reads_filtered} {output.reads_filtered}; "
            "mv {wildcards.sample}.QC.json {wildcards.sample}.QC.fastp.json; "
            # remove .done dir to allow rerun
            "rm -r {params.donedir} "


elif config["SeOrPe"] == "PE":

    rule <step_name>__mitoz_filter_PE:
        input:
            **<step_name>__mitoz_filter_PE_inputs()
        output:
            read_filtered = config["results_dir"]+ "/" +config["<step_name>__mitoz_filter_PE_output_dir"] + '/{sample}_R1.fq.gz',
            read2_filtered = config["results_dir"]+ "/" + config["<step_name>__mitoz_filter_PE_output_dir"] +'/{sample}_R2.fq.gz',
        log: config["results_dir"]+'/logs/' + config["<step_name>__mitoz_filter_PE_output_dir"] + '/{sample}_mitoz_filter_log.txt'
        threads: config["<step_name>__mitoz_filter_threads"]
        params:
            command = config["<step_name>__mitoz_filter_PE_command"],
            output_dir = config["<step_name>__mitoz_filter_PE_output_dir"],
            prefix = "{sample}",
            clade = config["<step_name>__mitoz_filter_clade"],
            donedir = config["results_dir"]+ "/" +config["<step_name>__mitoz_filter_PE_output_dir"] + "/{sample}.result/done",
            read_filtered = config["results_dir"]+ "/" +config["<step_name>__mitoz_filter_PE_output_dir"] + "/{sample}.result/{sample}.clean_R1.fq.gz",
            read2_filtered = config["results_dir"]+ "/" +config["<step_name>__mitoz_filter_PE_output_dir"] + "/{sample}.result/{sample}.clean_R2.fq.gz",
        conda: 
            "envs/mitoz.yaml"
        shell:
            "mkdir -p {params.output_dir}/{wildcards.sample}.result && "
            "cd {params.output_dir}/{wildcards.sample}.result; "
            "{params.command} "
            "--fq1 {input.read} "
            "--fq2 {input.read2} "
            "--fastq_read_length  5000 "
            "--thread_number {threads} "
            "--outprefix {params.prefix} "
            "|& tee {log}; "
            "mv {params.read_filtered} {output.read_filtered}; "
            "mv {params.read2_filtered} {output.read2_filtered}; "
            "mv {wildcards.sample}.QC.json {wildcards.sample}.QC.fastp.json; "            
            # remove .done dir to allow rerun
            "rm -r {params.donedir} "
