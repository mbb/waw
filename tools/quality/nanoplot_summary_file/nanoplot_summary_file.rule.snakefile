rule <step_name>__nanoplot_summary_file:
	input:
		**<step_name>__nanoplot_summary_file_inputs(),
	output: 
		nanoplot_stats = config["results_dir"] + "/" + config["<step_name>__nanoplot_summary_file_output_dir"] + "/NanoStats.txt",
	params: 
		output_dir = config["results_dir"] + "/" + config["<step_name>__nanoplot_summary_file_output_dir"]+ "/",
		command = config["<step_name>__nanoplot_summary_file_command"],
		maxlength = config["<step_name>__nanoplot_summary_file_maxlength"],
		minlength = config["<step_name>__nanoplot_summary_file_minlength"],
		minqual = config["<step_name>__nanoplot_summary_file_minqual"],
	log: 
		config["results_dir"] + "/logs/" + config["<step_name>__nanoplot_summary_file_output_dir"] + "/nanoplot_summary_file_log.txt"
	threads: 
		config["<step_name>__nanoplot_summary_file_threads"]
	conda:
	    "envs/nanoplot.yaml"
	shell: 
		"{params.command} "
		"-t {threads} "
		"--outdir {params.output_dir} "
		"--summary {input.summary} "
		"--maxlength {params.maxlength} "
		"--minlength {params.minlength} "
		"--minqual {params.minqual} "
		"|& tee {log}; "
		"for i in $(ls {params.output_dir}/*.png); do  name=$(basename $i .png); mv $i {params.output_dir}/${{name}}_mqc.png ; done"