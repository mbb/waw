rule <step_name>__samtools_stats:
    input:
        **<step_name>__samtools_stats_inputs()
    output:
        stats = config["results_dir"]+'/' + config["<step_name>__samtools_stats_output_dir"] + '/{sample}.txt',
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__samtools_stats_output_dir"] + '/{sample}_samtools_stats_log.txt'
    threads:
        config["<step_name>__samtools_stats_threads"]
    params:
        command = config["<step_name>__samtools_stats_command"],
        output_dir = config["results_dir"]+'/'+config["<step_name>__samtools_stats_output_dir"]
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"
    shell:
        "{params.command} {input.bam} -@ {threads} 2> {log} > {output.stats}"