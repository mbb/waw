rule <step_name>__pca_genepop:
    input:
        **<step_name>__pca_genepop_inputs(),
    output: 
        #eigenvectors = config["results_dir"] + "/" + config["<step_name>__pca_genepop_output_dir"] + "/" + <step_name>__pca_genepop_gds_name + "_eigenvects.txt",
        #eigenvalues = config["results_dir"] + "/" + config["<step_name>__pca_genepop_output_dir"] + "/" + <step_name>__pca_genepop_gds_name + "_eigenvals.txt",
        #varprop = config["results_dir"] + "/" + config["<step_name>__pca_genepop_output_dir"] + "/pca_varprop_mqc.txt",
        summary_png = config["results_dir"] + "/" + config["<step_name>__pca_genepop_output_dir"] + "/summary_plot_mqc.png",
        pca_png = config["results_dir"] + "/" + config["<step_name>__pca_genepop_output_dir"] + "/pca_topaxes_plot_mqc.png",
        axe1axe2_png = config["results_dir"] + "/" + config["<step_name>__pca_genepop_output_dir"] + "/pca_axe1axe2_plot_mqc.png",
        hwtest = config["results_dir"] + "/" + config["<step_name>__pca_genepop_output_dir"] + "/hwtest_mqc.tsv",
        fstat = config["results_dir"] + "/" + config["<step_name>__pca_genepop_output_dir"] + "/fstat_mqc.tsv",
        pairfstat = config["results_dir"] + "/" + config["<step_name>__pca_genepop_output_dir"] + "/pairfstat_mqc.tsv",
        pca3d = config["results_dir"] + "/" + config["<step_name>__pca_genepop_output_dir"] + "/pca_3d_mqc.html",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__pca_genepop_output_dir"]+ "/",
        #rmdscript=config["<step_name>__pca_genepop_script"] ,
    threads:
        config["<step_name>__pca_genepop_threads"]           
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__pca_genepop_output_dir"] + "/pca_genepop_log.txt"
    conda:
        "envs/pca_genepop.yaml"    
    shell:
        r"""
        export HOME="/root";
        Rscript - <<'        EOF'
        library(pegas); library(adegenet);library(canvasXpress); 
        X = read.genepop("{input.genepop}", ncode=3);
        XnoMiss = tab(X, NA.method = "mean");
        pca1 <- dudi.pca(XnoMiss, scale = FALSE, scannf = FALSE, nf = 4);
        png("{output.pca_png}", height=600, width=900);
        barplot(pca1$eig[1:7], main = "PCA eigenvalues", col = heat.colors(50));
        dev.off();
        png("{output.axe1axe2_png}", height=600, width=900);
        colorplot(pca1$li, pca1$li, transp=TRUE, cex=3, xlab="PC 1", ylab="PC 2");
        title("PCA of dataset {input.genepop} : axes 1-2");
        abline(v=0,h=0,col="grey", lty=2);
        dev.off(); 
        if ( "{input.popmap_file}" != "" && file.exists("{input.popmap_file}") )
        {{ 
            pop_map= read.table("{input.popmap_file}", header=F, sep="\t",stringsAsFactors = FALSE) ; 
            colnames(pop_map)[c(1,2)]=c("sample.id", "popcode") ; 
            sample.id <- rownames(X$tab); 
            regex = paste(pop_map$sample.id, collapse="|"); 
            pops = pop_map$popcode[grep(regex, sample.id)]; 
            X$pop = as.factor(pops); 
            y = pca1$li; rownames(y) = sample.id; 
            z = as.data.frame(paste("_",pops, sep="")); rownames(z) = sample.id;
            colnames(z)="pops"; 
            pca3D = canvasXpress(data = y, varAnnot  = z, graphType ="Scatter3D", colorBy = "pops", ellipseBy = "pops", xAxis = list("Axis1"), yAxis = list("Axis2"), zAxis = list("Axis3"),theme = "CanvasXpress",title = "PCA plot", width = 1200, height = 1000, axisTickScaleFontFactor = 0.5, axisTitleScaleFontFactor = 0.5); 
             html_page <- cxHtmlPage(pca3D); 
             writeLines(html_page, "{output.pca3d}"); 
            #tab <- data.frame(sample.id, popcode = pops, stringsAsFactors = FALSE); 
            #Allpops = unique(tab$popcode); 
            nbpops = nlevels(X$pop); 
            resum = summary(X); 
            png("{output.summary_png}", height=600, width=900); 
            par(mfrow=c(2,2)); 
            plot(resum$n.by.pop, resum$pop.n.all, xlab="sample size", ylab="Number of alleles",main="Alleles numbers and sample sizes", type="n"); 
            text(resum$n.by.pop,resum$pop.n.all,lab=names(resum$n.by.pop)); 
            barplot(resum$loc.n.all, ylab="Number of alleles", main="Number of alleles per locus"); 
            barplot(resum$Hexp-resum$Hobs, main="Heterozygosity: expected-observed", ylab="Hexp - Hobs"); 
            barplot(resum$n.by.pop, main="Sample sizes per population", ylab="Number of genotypes",las=3); 
            dev.off(); 
            hw <- hw.test(X, B=0); 
            write.table(hw, file="{output.hwtest}",  append = FALSE, quote = F, sep = "\t", row.names = T, col.names = NA); 
            ftab <- Fst(as.loci(X));
            write.table(ftab, file="{output.fstat}",  append = FALSE, quote = F, sep = "\t", row.names = T, col.names = NA); 
            write.table(as.matrix(as.dist(hierfstat::genet.dist(X,method="WC84"), upper=T)), file="{output.pairfstat}", append = FALSE, quote = F, sep = "\t", row.names = T, col.names = NA); 
         }}
        EOF

        sed -i '1 i\# plot_type: "table"' {output.hwtest};
        sed -i '2 i\# pconfig:'  {output.hwtest};
        sed -i '3 i\#     format: "{{:,.3f}}"'  {output.hwtest};
        sed -i '4 i\#     scale: false' {output.hwtest};

        sed -i '1 i\# plot_type: "table"' {output.fstat};
        sed -i '2 i\# pconfig:'  {output.fstat};
        sed -i '3 i\#     format: "{{:,.3f}}"'  {output.fstat};
        sed -i '4 i\#     scale: false' {output.fstat};

        sed -i '1 i\# plot_type: "table"' {output.pairfstat};
        sed -i '2 i\# pconfig:'  {output.pairfstat};
        sed -i '3 i\#     format: "{{:,.3f}}"'  {output.pairfstat};
        sed -i '4 i\#     scale: false' {output.pairfstat};

        """
