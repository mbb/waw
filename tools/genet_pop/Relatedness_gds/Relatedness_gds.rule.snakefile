<step_name>__Relatedness_gds_gds_name = os.path.basename(<step_name>__Relatedness_gds_inputs()["gds"]).split(".")[0]

rule <step_name>__Relatedness_gds:
    input:
        **<step_name>__Relatedness_gds_inputs(),
    output: 
        k0 = config["results_dir"] + "/" + config["<step_name>__Relatedness_gds_output_dir"] + "/" + <step_name>__Relatedness_gds_gds_name + "_k0.txt",
        k1 = config["results_dir"] + "/" + config["<step_name>__Relatedness_gds_output_dir"] + "/" + <step_name>__Relatedness_gds_gds_name + "_k1.txt",
        k0vsk1_png = config["results_dir"] + "/" + config["<step_name>__Relatedness_gds_output_dir"] + "/K0vsK1_plot_mqc.png",
        k1_heatmap_png = config["results_dir"] + "/" + config["<step_name>__Relatedness_gds_output_dir"] + "/K1_heatmap_mqc.png",
    params: 
        method = config["<step_name>__Relatedness_gds_method"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__Relatedness_gds_output_dir"]+ "/",
        command = config["<step_name>__Relatedness_gds_command"],
    threads:
            config["<step_name>__Relatedness_gds_threads"]           
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__Relatedness_gds_output_dir"] + "/Relatedness_gds_log.txt"
    conda:
        "envs/pca_gds.yaml"
    script:
        config["<step_name>__Relatedness_gds_script"]    
    
