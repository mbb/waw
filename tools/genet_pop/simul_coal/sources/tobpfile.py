#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script to create a bpfile from the data (take the contigs number and their lengths) or from simulations.

# Import library
import os
import sys
import getopt
from numpy import random
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
# Functions


def usage():
    """ Function for help """
    print("\n##########################################"

          "\n\n# This script create a bpfile from a fasta file which contains contigs.\n\n"

          "# -h --help : print usage fonction.\n"
          "# -c --comment : line of comment at the top of bpfile.\n"
          "# -l --samplesize1 : sample size 1 (if bpfile is from data or if u want to choose it if bpfile is not from data).\n"
          "# -L --samplesize2 : sample size 2.\n"
          "# -N --Nref : size of reference population.\n"
          "# -M --mu : mutation rate per individual per generation.\n\n"
          "# -s --nsim : nber of multilocus simulations in 'ms'.\n"

          "# if bpfile from data\n"
          "# -D --Data : if bpfile from data \n"
          "# -f --fasta : path to the data pseudofasta file with contigs.\n\n"

          "# if bpfile not from data\n"
          "# -n --ncontig : number of contigs (if bpfile not from data).\n"
          "# -m --meanlcontig : mean of contigs' length (if bpfile not from data).\n"
          "# -d --sdlcontig : standard deviation of contigs' length (if bpfile not from data).\n\n"

          "###########################################\n")
    return()


def takearg(argv):
    """ Function which record arguments from the command line."""

    data = False
    path2contigfile = None
    nref = None
    mu = None
    nbrsim = None
    nbrcontig = None
    meanlengthcontig = None
    sdlengthcontig = None
    samplesize1 = None
    samplesize2 = None
    comment = None

    if len(argv) < 2:
        print("You should give, at least, the name of the fasta file !")
        usage()
        sys.exit(1)
    try:
        opts, args = getopt.getopt(argv[1:], "hDf:N:M:s:n:m:d:l:L:c:", ["--help", "--Data", "--fasta_file=", "--Nref=", "--Mu=",
                                                                        "--nsim=", "--ncontig=", "--meanlcontig=", "--sdlcontig=", "--samplesize1=", "--samplesize2=", "--comment="])
    except getopt.GetoptError as err:
        # Affiche l'aide et quitte le programme
        print(err)  # Va afficher l'erreur en anglais
        usage()  # Fonction à écrire rappelant la syntaxe de la commande
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-D", "--Data"):
            data = True
        elif opt in ("-f", "--fasta"):
            path2contigfile = arg
        elif opt in ("-N", "--Nref"):
            nref = int(arg)
        elif opt in ("-M", "--mu"):
            mu = float(arg)
        elif opt in ("-s", "--nsim"):
            nbrsim = int(arg)
        elif opt in ("-n, --ncontig"):
            nbrcontig = int(arg)
        elif opt in ("-m, --meanlcontig"):
            meanlengthcontig = int(arg)
        elif opt in ("-d, --sdlcontig"):
            sdlengthcontig = int(arg)
        elif opt in ("-l", "--samplesize1"):
            samplesize1 = int(arg)
        elif opt in ("-L", "--samplesize2"):
            samplesize2 = int(arg)
        elif opt in ("-c", "--comment"):
            comment = arg
        else:
            print("Option {} inconnue".format(opt))
            sys.exit(2)

    return(data, path2contigfile, nref, mu, nbrsim, nbrcontig, meanlengthcontig, sdlengthcontig, samplesize1, samplesize2, comment)


def extract_deb(ch1, ch2):
    """ Function which delete a part of a ch1 from ch2 index. """

    index_ch2 = ch1.find(ch2)
    ch1 = ch1[:(index_ch2)]

    return(ch1)

##################################################################


# Initialization
data, path2contigfile, nref, mu, nbrsim, nbrcontig, meanlengthcontig, sdlengthcontig, samplesize1, samplesize2, comment = takearg(
    sys.argv)
listsamplesize1 = []
listsamplesize2 = []
theta = []
rec = []
#nref = 100000
#mu = 0.00000002763


# If a fasta file is provided
if data:
    nbrcontig = 0
    size = []
    sizeN = []
    contigsdone = []
    for seq_record in SeqIO.parse(path2contigfile, "fasta"):
        nom_contig = str(extract_deb(seq_record.id, '|'))
        nom_contig = nom_contig.rstrip('\n\r')
        nom_contig = nom_contig + "|"
        if not nom_contig in contigsdone:
            contigsdone.append(nom_contig)
            size.append(len(seq_record.seq))
            listsamplesize1.append(samplesize1)
            listsamplesize2.append(samplesize2)

            # Sequence length without 'N' and '-'
            liste_Nt = []
            for i in range(0, size[nbrcontig]):
                id_Nt = seq_record.seq[i]
                if (str(id_Nt) != "-") and (str(id_Nt) != "N"):
                    liste_Nt.append(str(repr(id_Nt)))
            sizeN.append(len(liste_Nt))

            theta.append(4*nref*mu*sizeN[-1])
            rec.append(theta[-1]/2)
            nbrcontig += 1


# If simulations
else:
    sizeN = random.normal(meanlengthcontig, sdlengthcontig, nbrcontig)

    for i in range(0, nbrcontig):
        listsamplesize1.append(samplesize1)
        listsamplesize2.append(samplesize2)
        theta.append(4*nref*mu*sizeN[i])
        rec.append(theta[i]/2)


# Write in the bpfile
print(nbrcontig)
outfile = open("bpfile", "w")
outfile.write("#" + comment)
outfile.write("\n" + " ".join(map(str, map(int, map(round, sizeN)))))
outfile.write("\n" + " ".join(map(str, listsamplesize1)))
outfile.write("\n" + " ".join(map(str, listsamplesize2)))
outfile.write("\n" + " ".join(map(str, theta)))
outfile.write("\n" + " ".join(map(str, rec)))


# Write in the spinput file
outfile = open("spinput.txt", "w")
outfile.write(str(nbrcontig))
for i in range(0, nbrcontig):
    outfile.write("\n" + str(listsamplesize1[i]))
    outfile.write("\n" + str(listsamplesize2[i]))
    outfile.write("\n" + str(sizeN[i]))
outfile.write("\n" + str(nbrsim))
outfile.write("\n" + "msout")

outfile.close()
