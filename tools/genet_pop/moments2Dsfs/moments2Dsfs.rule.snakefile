
rule <step_name>__moments2Dsfs:
    input:
        **<step_name>__moments2Dsfs_inputs(),
    output: 
        sfsplotDone = config["results_dir"] + "/" + config["<step_name>__moments2Dsfs_output_dir"] + "/sfsplot.txt", 
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__moments2Dsfs_output_dir"]+ "/",
        projections =  config["<step_name>__moments2Dsfs_projections"] ,
        polarized =  config["<step_name>__moments2Dsfs_polarized"] 
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__moments2Dsfs_output_dir"] + "/moments2Dsfs_log.txt"
    conda:
        "envs/moments.yaml"    
    shell: """
python3 <<-HEREDOC
from numpy import array
import moments
import matplotlib.pylab as plt
popinfo_file = open('{input.popmap_file}')
# pop_dict has key, value pairs of "SAMPLE_NAME" : "POP_NAME"
popinfo_dict = set()
for line in popinfo_file:
        if line.startswith("#"):
            continue
        cols = line.rstrip().split("\t")
        sample = cols[0]
        pop = cols[1]
        popinfo_dict.add(pop)

popinfo_file.close()

for pop1 in range(len(list(popinfo_dict))):
    for pop2 in range(pop1+1,len(list(popinfo_dict))):
        print(pop1, list(popinfo_dict)[pop1], list(popinfo_dict)[pop2] )
        data_dict = moments.Misc.make_data_dict_vcf('{input.vcfFile}', '{input.popmap_file}')
        fs = moments.Spectrum.from_data_dict(data_dict, [list(popinfo_dict)[pop1],list(popinfo_dict)[pop2]],[{params.projections},{params.projections}], polarized=False)
        moments.Plotting.plot_single_2d_sfs(fs, show=False)
        plt.savefig('{params.output_dir}'+"-"+list(popinfo_dict)[pop1]+"-"+list(popinfo_dict)[pop2] +"_mqc.png")

HEREDOC

touch {output.sfsplotDone} 
"""

