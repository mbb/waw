rule <step_name>__fastStructure_gds:
    input:
        **<step_name>__fastStructure_gds_inputs(),
    output:
        chooseK = config["results_dir"]+"/"+config["<step_name>__fastStructure_gds_output_dir"]+"/chooseK.txt",
        #distructPlot = config["results_dir"]+"/"+config["<step_name>__fastStructure_gds_output_dir"]+"/distruct_plot_mqc.svg",
    params:
        command = config["<step_name>__fastStructure_gds_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__fastStructure_gds_output_dir"]+"/",
        Kmin = config["<step_name>__fastStructure_gds_Kmin"],
        Kmax = config["<step_name>__fastStructure_gds_Kmax"],
        prior = config["<step_name>__fastStructure_gds_prior"],
        cv = config["<step_name>__fastStructure_gds_cv"],
        tol = config["<step_name>__fastStructure_gds_tol"],
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__fastStructure_gds_output_dir"] + "/fastStructure_gds_log.txt"
    shell:
          """
            #create  .bim and .fam file nedded by fastStructure_gds
            Rscript -e 'library(SeqArray); seqGDS2BED("{input.gds}", out.fn="{params.output_dir}/plink",verbose=F) '

            export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
              
            for k in `seq  {params.Kmin} {params.Kmax}`
            do
                if [[ $k < {params.Kmax} ]]; then
                 structure.py -K $k --tol=1e-6 --prior=simple --cv 0 --input={params.output_dir}/plink --output={params.output_dir}/plink-fastStr &
                else
                 structure.py -K $k --tol=1e-6 --prior=simple --cv 0 --input={params.output_dir}/plink --output={params.output_dir}/plink-fastStr
                fi
            done

             chooseK.py --input={params.output_dir}/plink-fastStr > {output.chooseK}

            """