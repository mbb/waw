import oyaml as yaml
import sys
import os
from collections import OrderedDict

config = dict()
with open(sys.argv[1], 'r') as paramfile:
    config = yaml.load(paramfile, Loader=yaml.FullLoader)

config = config["params"]

out ="""# description: 'dadi results'
# section_name: 'dadi_results'
# plot_type: 'html'
<pre>
"""

with open(os.path.join(config["results_dir"],config["<step_name>__dadi_output_dir"],"dadi-full.txt"), "r") as dadi_full:
    out += ''.join(dadi_full.readlines())

out += "</pre>"

with open(os.path.join(config["results_dir"],config["<step_name>__dadi_output_dir"],"dadi-full_mqc.txt"), "w") as outfile:
    outfile.write(out)