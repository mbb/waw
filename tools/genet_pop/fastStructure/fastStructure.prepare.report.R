library(yaml)
library(gridExtra)
library(RColorBrewer)

library(pophelper)

args = commandArgs(trailingOnly=TRUE)
parameters = read_yaml(args[1])$params
chemin= paste0(parameters$results_dir,"/",parameters$<step_name>__fastStructure_output_dir)

files <- Sys.glob(paste0(paste0(chemin),'/plink-fastStr*.meanQ') )
qlist <- readQ(files)
        
kelly <- c("#BE0032","#F3C300","#875692","#F38400","#A1CAF1","#C2B280","#848482","#008856","#E68FAC","#0067A5","#F99379","#604E97","#F6A600","#B3446C","#DCD300","#882D17","#8DB600", "#654522","#E25822","#2B3D26")
curr.sample.id = read.table(paste0(chemin, "/plink.fam"), header=FALSE, sep= " ")[,1]
qlist <- lapply(qlist,"rownames<-", curr.sample.id)
haut = round(40 / length(qlist))
nopops =T

# TODO check if we have more than one plot nb K to test > 1


  pop_color1= rep("grey", length(curr.sample.id))
  pop_code1 = rep("pop", length(curr.sample.id))
  names(pop_code1) = curr.sample.id
  popc="grey"
  names(popc)="pop"

  popMapfile = parameters$popmap_file
  if (popMapfile != "") {        
      ficpopmap = popMapfile
  
      popmap = read.table(ficpopmap, header=F, sep='\t', stringsAsFactors=F)
      if  (length(curr.sample.id) == length(popmap[,1]) & length(setdiff(curr.sample.id, popmap[,1])) == 0)
      {
        rownames(popmap) = popmap[,1]
        pop_code1 = popmap[popmap[,1] %in% curr.sample.id, 2]
        names(pop_code1)  = curr.sample.id
        ncolors = length(unique(pop_code1))
        popc <- colorRampPalette(brewer.pal(12, "Set3"))(ncolors) #brewer.pal(n = ncolors, name = palette)
        names(popc)       = unique(pop_code1)
        pop_color1        = popc[pop_code1]
        names(pop_color1) = curr.sample.id  
        nopops =  F
      }  else {
       nopops = T      
     }
  }

  
popDeco=list(pop_color1=pop_color1, popc = popc, pop_code1= pop_code1)
minK = parameters$<step_name>__fastStructure_Kmin
maxK = parameters$<step_name>__fastStructure_Kmax
titre = paste0("fastStructure with prior=",parameters$<step_name>__fastStructure_prior," k: ",minK, " - ", maxK  )

if (popMapfile != "") {
  popGroups = as.data.frame(popDeco$pop_code1)  
  if (length(popDeco$popc) > 1) {
    nopops = F 
    colnames(popGroups) = 'Pops'
    p = plotQ(qlist,returnplot=T, exportplot=F,basesize=11, grplab=popGroups,grplabsize=3,linesize=0.8,pointsize=3, showindlab=T,useindlab=T, ordergrp=T, indlabangle=90,indlabvjust=1, height=haut, width=120, indlabsize=3, divsize=2, spbgcol="grey", imgoutput="join", panelspacer=0.4, splab=paste0("K=",sapply(qlist,ncol)),splabsize=20 , clustercol=kelly,barbordersize=0.2 , barbordercolour='white', showtitle=T, titlesize=6,titlelab= titre)
    
  } 
  else 
  {
    nopops = T
  }
}
if (popMapfile == "" | nopops) 
  p= plotQ(qlist,returnplot=T,exportplot=F,basesize=11,linesize=0.8,pointsize=3,  height=haut, width=140,indlabsize=, divsize=2, spbgcol="grey", imgoutput="join", panelspacer=0.4, splab=paste0("K=",sapply(qlist,ncol)),splabsize=20 , indlabangle=90, indlabvjust=1, showindlab=T,useindlab=T, clustercol=kelly,barbordersize=0.2 , barbordercolour='white', showtitle=T, titlesize=6, titlelab= titre)
  

png(paste0(chemin,'/distruct_plot_mqc.png'), width = 1800, height = 800)
 gridExtra::grid.arrange(p$plot[[1]])
dev.off()

#meanQ
for (k in minK:maxK)
{
   fic = paste0(chemin,'/plink-fastStr.',k,'.meanQ')
    if  ( file.exists(fic) )  {
        df = read.table(fic, header=F)
        rownames(df)  = curr.sample.id
        colnames(df) =  paste0("cluster", 1:k)
        if (length(popDeco$popc) > 1 )  df = cbind(pop=popDeco$pop_code1, df, stringsAsFactors=F)
        mqc_file = paste0(chemin,"/posterior_mean_of_admixture_proportions-K",k,"_mqc.tsv") 
        cat("
# id: posterior_mean_of_admixture_proportions_k",k,"\n",
"# section_name: 'Posterior mean of admixture proportions k=",k,"'\n",
"# description: 'Posterior mean of admixture proportions.'
# pconfig: 
#    format: '{:,.6f}'
# plot_type: 'table'\nsample",sep="", file=mqc_file)

        write.table(format(df, digits=6, scientific=F), mqc_file, quote =F, sep = "\t", col.names=NA, append=TRUE)
    }
}   

#https://github.com/ewels/MultiQC_TestData/blob/master/data/custom_content/embedded_config/table_headers_mqc.txt