rule <step_name>__fastStructure:
    input:
        **<step_name>__fastStructure_inputs(),
    output:
        chooseK = config["results_dir"]+"/"+config["<step_name>__fastStructure_output_dir"]+"/chooseK.txt",
        #distructPlot = config["results_dir"]+"/"+config["<step_name>__fastStructure_output_dir"]+"/distruct_plot_mqc.svg",
    params:
        command = config["<step_name>__fastStructure_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__fastStructure_output_dir"]+"/",
        Kmin = config["<step_name>__fastStructure_Kmin"],
        Kmax = config["<step_name>__fastStructure_Kmax"],
        prior = config["<step_name>__fastStructure_prior"],
        cv = config["<step_name>__fastStructure_cv"],
        tol = config["<step_name>__fastStructure_tol"],
        #params_file = workflow.overwrite_configfiles,
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__fastStructure_output_dir"] + "/fastStructure_log.txt"
    conda:
        "envs/faststructure.yaml"
    shell:
          """
            #here avoid to use plink to generate bim, bed and fam directly from vcf file (fro plink max number of contigs in vcf is small) 
            vcftools --gzvcf {input.vcf} --out {params.output_dir}/plink --plink
            #create  .bim and .fam file nedded by fastStructure
            plink --file {params.output_dir}/plink --make-bed --double-id --allow-extra-chr -autosome-num 95 --out {params.output_dir}/plink #95 is the max 

            #export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

            for k in `seq  {params.Kmin} {params.Kmax}`
            do
                if [[ $k < {params.Kmax} ]]; then
                 fastStructure -K $k --tol=1e-6 --prior=simple --cv 0 --input={params.output_dir}/plink --output={params.output_dir}/plink-fastStr &
                else
                 fastStructure -K $k --tol=1e-6 --prior=simple --cv 0 --input={params.output_dir}/plink --output={params.output_dir}/plink-fastStr
                fi
            done

            # Need to moove output from plink to avoid an erro from fastChooseK.py script
            mkdir -p {params.output_dir}/plink
            mv {params.output_dir}/plink.* {params.output_dir}/plink/
            choose=$(find $CONDA_PREFIX -name fastChooseK.py)
            python $choose {params.output_dir}/ {params.output_dir}/
            mv {params.output_dir}/plink/* {params.output_dir}/
            rmdir {params.output_dir}/plink/

            Rscript {snake_dir}/scripts/<step_name>__fastStructure.prepare.report.R {conf_path} |& tee {log}

            """