---
title: "`r params$gds` "
output:    
  html_document:
    toc: false
    toc_depth: 2
params: 
    gds: "test.vcf.gds"
    popMap: ""
    palette: "Set3"
    expectedK: 4

---

```{r setup,  echo=FALSE, warning=FALSE, message = FALSE}
library(knitr)
library(rgl)

knit_hooks$set(webgl = hook_webgl)

#Rscript -e 'rmarkdown::render("autre.rmd", clean=FALSE, params = list(gds="/dir/populations.snps.vcf.gds", popMap="/dir/popmap.txt"), output_format = "html_document" , output_file = "PCA-3D_mqc.html")'

library(SNPRelate)
library(SeqArray)
library(RColorBrewer)

library(threejs)


knitr::opts_chunk$set(
  comment = '', fig.width = 8, fig.height = 8, echo =FALSE
)

Sys.setenv(gds=params$gds)
outimage = paste0(tempfile(),".png")
Sys.setenv(outimage=outimage )

if (params$popMap != "") popmap=read.table(params$popMap, header=F, sep='\t', stringsAsFactors=F)


evaluate=TRUE
```

```{r, PCA, out.width="100%", echo=FALSE, warning=FALSE}
genofile <-  seqOpen(params$gds)
PCA1 <- snpgdsPCA(genofile,  snp.id=NULL, maf=NaN, missing.rate=0.2, num.thread=8, verbose=FALSE, autosome.only=FALSE)
sample.id1 <- PCA1$sample.id

seqClose(genofile)

if (params$popMap == "" ){
  pop_color1= "black"
  pop_code1 = rep(1, length(sample.id1) ) 
  popc=data.frame("black"); rownames(popc)="undef"
} else {

pop_code1= popmap[which(popmap[,1] %in% sample.id1), 2]
#brewer return a min of 3 colors
ncolors = max(length(unique(pop_code1)), 3)
popc <- brewer.pal(n = ncolors, name = params$palette)
popc = popc[1:length(unique(pop_code1) ) ] 
names(popc) = unique(pop_code1)
pop_color1 = popc[pop_code1]
names(pop_color1) = sample.id1
}


# make a data.frame
tableau = data.frame(sample.id = sample.id1,
                   pop = factor(pop_code1),
                   Axe1 = PCA1$eigenvect[,1],    # the first eigenvector
                   Axe2 = PCA1$eigenvect[,2],    # the second eigenvector
                   Axe3 = PCA1$eigenvect[,3],    # the 3 eigenvector
                   Axe4 = PCA1$eigenvect[,4],    # the 4 eigenvector
                   Axe5 = PCA1$eigenvect[,5],    
                   Axe5 = PCA1$eigenvect[,6], 
                   Axe5 = PCA1$eigenvect[,7], 
                   Axe5 = PCA1$eigenvect[,8], 
                   Axe5 = PCA1$eigenvect[,9], 
                   Axe5 = PCA1$eigenvect[,10], 
                   couleur = pop_color1,
                   stringsAsFactors = FALSE) 
```

```{r, webgl=TRUE, , warning=FALSE}
x <- tableau[,3]
y <- tableau[,4]
z <- tableau[,5]

axisLabels = paste0('Axe',c("1" , "2", "3") )
couleurs=tableau$couleur

scatterplot3js(x,y,z, height = '1000px', width='1000px', axisLabels = axisLabels, cex.lab=0.2, size=0.5, labels = paste(tableau[,1],tableau[,2], sep="-"), color=couleurs)
```