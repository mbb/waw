<step_name>__pca_gds_gds_name = os.path.basename(<step_name>__pca_gds_inputs()["gds"]).split(".")[0]

rule <step_name>__pca_gds:
    input:
        **<step_name>__pca_gds_inputs(),
    output: 
        eigenvectors = config["results_dir"] + "/" + config["<step_name>__pca_gds_output_dir"] + "/" + <step_name>__pca_gds_gds_name + "_eigenvects.txt",
        eigenvalues = config["results_dir"] + "/" + config["<step_name>__pca_gds_output_dir"] + "/" + <step_name>__pca_gds_gds_name + "_eigenvals.txt",
        varprop = config["results_dir"] + "/" + config["<step_name>__pca_gds_output_dir"] + "/pca_varprop_mqc.txt",
        pca_png = config["results_dir"] + "/" + config["<step_name>__pca_gds_output_dir"] + "/pca_topaxes_plot_mqc.png",
        axe1axe2_png = config["results_dir"] + "/" + config["<step_name>__pca_gds_output_dir"] + "/pca_axe1axe2_plot_mqc.png",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__pca_gds_output_dir"]+ "/",
        pca_gds_minmaf = config["<step_name>__pca_gds_minmaf"],
        pca_gds_missing = config["<step_name>__pca_gds_missing"],
    threads:
        config["<step_name>__pca_gds_threads"]
    conda:
        "envs/pca_gds.yaml"
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__pca_gds_output_dir"] + "/pca_gds_log.txt"
    script:
        config["<step_name>__pca_gds_script"]    
    
