
rule <step_name>__moments2Dinfer:
    input:
        **<step_name>__moments2Dinfer_inputs(),
    output: 
        moments2DinferDone = config["results_dir"] + "/" + config["<step_name>__moments2Dinfer_output_dir"] + "/moments2DinferDone.txt", 
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__moments2Dinfer_output_dir"]+ "/",
        projections =  config["<step_name>__moments2Dinfer_projections"] ,
        polarized =  config["<step_name>__moments2Dinfer_polarized"],
        model     =  config["<step_name>__moments2Dinfer_model"],
        pop1index   = config["<step_name>__moments2Dinfer_firstpop"],
        pop2index   = config["<step_name>__moments2Dinfer_secondpop"],
        optimizer   = config["<step_name>__moments2Dinfer_optimizer"],
        MaxIter     = config["<step_name>__moments2Dinfer_MaxIter"],
        mutrate     = config["<step_name>__moments2Dinfer_mutation"],
        gtime       = config["<step_name>__moments2Dinfer_gtime"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__moments2Dinfer_output_dir"] + "/moments2Dinfer_log.txt"
    conda:
        "envs/moments.yaml"    
    shell: """
python3 <<-HEREDOC
from numpy import array
import moments
import matplotlib.pylab as plt
import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/workflow/scripts')
import moments2Dinfer_moments_models_2pop as moments_models_2pop
popinfo_file = open('{input.popmap_file}')
# pop_dict has key, value pairs of "SAMPLE_NAME" : "POP_NAME"
popinfo_dict = set()
for line in popinfo_file:
        if line.startswith("#"):
            continue
        cols = line.rstrip().split("\t")
        sample = cols[0]
        pop = cols[1]
        popinfo_dict.add(pop)

popinfo_file.close()

filename="/workflow/scripts/moments2Dinfer_models_and_params.txt"
p=[]
lower=[]
upper = []
with open(filename) as file:
    for line in file:
        x=line.split(";")
        model = x[0]
        if (model == "{params.model}"):
            p.append(float(x[4].split(" = ")[1]))
            upper.append(float(x[2].split(" = ")[1]))
            lower.append(float(x[3].split(" = ")[1]))

data_dict = moments.Misc.make_data_dict_vcf('{input.vcfFile}', '{input.popmap_file}')

pop1=list(popinfo_dict)[{params.pop1index}]
pop2=list(popinfo_dict)[{params.pop2index}]
print(pop1, pop2  )
fs = moments.Spectrum.from_data_dict(data_dict, [pop1,pop2],[{params.projections},{params.projections}], polarized=False)
#moments.Plotting.plot_single_2d_sfs(fs, show=False)
ns = fs.sample_sizes
func=moments_models_2pop.{params.model}
p0 = p
upper_bound = upper
lower_bound = lower
p0 = moments.Misc.perturb_params(p0, fold=1, upper_bound=upper_bound, lower_bound=lower_bound)
if "{params.optimizer}" == "optimize_dual_anneal":
    import moments2Dinfer_moments_inference_dualanneal as moments_inference_dualanneal
    popt = moments_inference_dualanneal.{params.optimizer}(
        p0,
        fs,
        func,
        lower_bound=lower_bound,
        upper_bound=upper_bound,
        verbose=len(p0),
        maxiter={params.MaxIter},#maximum global search iterations - in each iteration, it explores twice the number of parameters
        ##### Set optimization values
        accept=1, #parameter for acceptance distribution (lower values makes the probability of acceptance smaller)
        visit=1.01, #parameter for visiting distribution (higher values makes the algorithm jump to a more distant region)
        Tini=50, #initial temperature
        no_local_search=False, #If set to True, a Generalized Simulated Annealing will be performed with no local search strategy applied
        local_method='L-BFGS-B', #local search method
        local_maxiter=20, #maximum local search iterations
        full_output=False #, output_file=logfile
    )
else:
    popt = moments.Inference.{params.optimizer}(
        p0,
        fs,
        func,
        lower_bound=lower_bound,
        upper_bound=upper_bound,
        verbose=len(p0),
        maxiter={params.MaxIter},
    )    
# Calculate the best-fit model AFS.
model = func(popt, ns)
# Likelihood of the data given the model AFS.
ll_model = moments.Inference.ll_multinom(model, fs)
print("Maximum log composite likelihood: {{0}}".format(ll_model))

# The optimal value of theta given the model.
theta = moments.Inference.optimal_sfs_scaling(model, fs)
print("Optimal value of theta: {{0}}".format(theta))

# Plot a comparison of the resulting fs with the data.
moments.Plotting.plot_2d_comp_multinom(
    model, fs, vmin=1, resid_range=3, pop_ids=("YRI", "CEU"), show=False
)

# Save the figure
plt.savefig('{params.output_dir}'+"-"+pop1+"-"+pop2+"-{params.model}-{params.optimizer}_mqc.png", dpi=500)
plt.close()

mu    = {params.mutrate}/100
gtime = {params.gtime}
nref  = theta/(4*mu)
plot_mod = moments.ModelPlot.generate_model(func, popt, ns)
moments.ModelPlot.plot_model(plot_mod, pop_labels=[pop1, pop2], nref=nref, draw_scale=False, gen_time=gtime, gen_time_units="KYear", reverse_timeline=True, save_file='{params.output_dir}'+'Model'+pop1+"-"+pop2+"-{params.model}-{params.optimizer}_mqc.png", fig_title=pop1+" "+pop2+" {params.model}Model: mu="+str(mu)+ " generation time:"+ str(gtime))

HEREDOC

touch {output.moments2DinferDone} 
"""

