<step_name>__Fst_gds_gds_name = os.path.basename(<step_name>__Fst_gds_inputs()["gds"]).split(".")[0]

rule <step_name>__Fst_gds:
    input:
        **<step_name>__Fst_gds_inputs(),
    output: 
        fst = config["results_dir"] + "/" + config["<step_name>__Fst_gds_output_dir"] + "/" + <step_name>__Fst_gds_gds_name + "_fst.txt",
        meanfst = config["results_dir"] + "/" + config["<step_name>__Fst_gds_output_dir"] + "/" + <step_name>__Fst_gds_gds_name + "_meanfst.txt",
        NJ_png = config["results_dir"] + "/" + config["<step_name>__Fst_gds_output_dir"] + "/Fst_NJ_plot_mqc.png",
        Fst_heatmap_png = config["results_dir"] + "/" + config["<step_name>__Fst_gds_output_dir"] + "/Fst_heatmap_mqc.png",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__Fst_gds_output_dir"]+ "/",
        command = config["<step_name>__Fst_gds_command"],
    threads:
            config["<step_name>__Fst_gds_threads"]           
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__Fst_gds_output_dir"] + "/Fst_gds_log.txt"
    conda:
        "envs/ggtree.yaml"
    script:
        config["<step_name>__Fst_gds_script"]    
    
