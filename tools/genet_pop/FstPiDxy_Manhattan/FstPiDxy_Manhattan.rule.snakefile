
rule <step_name>__FstPiDxy_Manhattan:
    input:
        **<step_name>__FstPiDxy_Manhattan_inputs(),
    output: 
        manhattan_done = config["results_dir"] + "/" + config["<step_name>__FstPiDxy_Manhattan_output_dir"] + "/manhattan_done.txt",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__FstPiDxy_Manhattan_output_dir"]+ "/",
        quantil =  config["<step_name>__FstPiDxy_Manhattan_quantil"] ,
        windSize =  config["<step_name>__FstPiDxy_Manhattan_windSize"]
    threads: config["<step_name>__FstPiDxy_Manhattan_threads"]
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__FstPiDxy_Manhattan_output_dir"] + "/FstPiDxy_Manhattan_log.txt"
    conda:
        "envs/faststructure.yaml"
    script:
        config["<step_name>__FstPiDxy_Manhattan_script"]
       