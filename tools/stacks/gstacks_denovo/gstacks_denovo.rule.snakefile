rule <step_name>__gstacks_denovo:
    input:
        **<step_name>__gstacks_denovo_inputs(),
    output:
        catalog_calls = config["results_dir"]+"/"+config["<step_name>__gstacks_denovo_output_dir"]+"/catalog.calls",
        catalog_fa = config["results_dir"]+"/"+config["<step_name>__gstacks_denovo_output_dir"]+"/catalog.fa.gz",
        stats = config["results_dir"]+"/"+config["<step_name>__gstacks_denovo_output_dir"]+"/gstacks.log.distribs"
    params:
        command = config["<step_name>__gstacks_denovo_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__gstacks_denovo_output_dir"],
        bam_dir = os.path.dirname(<step_name>__gstacks_denovo_inputs()["tsv2bam_done"]),
        model = config["<step_name>__gstacks_denovo_model"],
        var_alpha = config["<step_name>__gstacks_denovo_var_alpha"],
        gt_alpha = config["<step_name>__gstacks_denovo_gt_alpha"],
        min_mapq = config["<step_name>__gstacks_denovo_min_mapq"],
        max_clipped = config["<step_name>__gstacks_denovo_max_clipped"],
        params_file = workflow.overwrite_configfiles,
    threads:
        config["<step_name>__gstacks_denovo_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__gstacks_denovo_output_dir"] + "/gstacks_denovo_log.txt"
    conda:
       "envs/stacks.yaml"        
    shell: 
        """
        gstacks -t {threads} -P {params.bam_dir} -M {input.popmap} --model {params.model} \
        --var-alpha {params.var_alpha} --gt-alpha {params.gt_alpha} --min-mapq {params.min_mapq} \
        --max-clipped {params.max_clipped} -O {params.output_dir} |& tee {log};

        Rscript {snake_dir}/scripts/<step_name>__gstacks_denovo.prepare.report.R {params.params_file} |& tee {log};
        """
       