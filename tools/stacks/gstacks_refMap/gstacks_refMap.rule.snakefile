rule <step_name>__gstacks_refMap:
    input:
        **<step_name>__gstacks_refMap_inputs(),
    output:
        catalog_calls = config["results_dir"]+"/"+config["<step_name>__gstacks_refMap_output_dir"]+"/catalog.calls",
        catalog_fa = config["results_dir"]+"/"+config["<step_name>__gstacks_refMap_output_dir"]+"/catalog.fa.gz",
        stats = config["results_dir"]+"/"+config["<step_name>__gstacks_refMap_output_dir"]+"/gstacks.log.distribs"
    params:
        command = config["<step_name>__gstacks_refMap_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__gstacks_refMap_output_dir"],
        bam_dir = os.path.dirname(<step_name>__gstacks_refMap_inputs()["bams"][0] if type(<step_name>__gstacks_refMap_inputs()["bams"]) is list else <step_name>__gstacks_refMap_inputs()["bams"]), #lambda w, input: os.path.dirname(input.bams[0] if type(input.bams) is list else input.bams),
        model = config["<step_name>__gstacks_refMap_model"],
        var_alpha = config["<step_name>__gstacks_refMap_var_alpha"],
        gt_alpha = config["<step_name>__gstacks_refMap_gt_alpha"],
        min_mapq = config["<step_name>__gstacks_refMap_min_mapq"],
        max_clipped = config["<step_name>__gstacks_refMap_max_clipped"],
        max_insert = config["<step_name>__gstacks_refMap_max_insert"],
        rm_pcr_dupli = "--rm-pcr-duplicates" if (config["<step_name>__gstacks_refMap_rm_pcr_duplicates"] == True)  else "",
        params_file = workflow.overwrite_configfiles,
    threads:
        config["<step_name>__gstacks_refMap_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__gstacks_refMap_output_dir"] + "/gstacks_refMap_log.txt"
    conda:
       "envs/stacks.yaml"        
    shell: 
        """
        nonemptySamples='';
        emptycount=0
        for i in `ls {params.bam_dir}/*.bam`; do [ -s $i ] || emptycount=$((emptycount + 1)) ;  done;
        if [[ ${{emptycount}} -gt 0 ]];
        then 
                # here the bam file must include RG and SM
                for i in `ls {params.bam_dir}/*.bam`; 
                do 
                 if [[ -s $i ]]; 
                 then 
                  countRG=`samtools view -H $i | grep -c @RG` 
                  if [[ ${{countRG}} -eq 0 ]];
                  then
                    sample=`basename $i .bam`;
                    samtools addreplacerg -r ID:$sample -r SM:$sample -o tmp.bam $i
                    mv tmp.bam $i
                  fi
                  nonemptySamples=${{nonemptySamples}}" -B "$i ;  
                 fi 
                done;
                gstacks -t {threads} $nonemptySamples  --model {params.model}  \
                --var-alpha {params.var_alpha} --gt-alpha {params.gt_alpha} --min-mapq {params.min_mapq} --max-insert-len {params.max_insert} \
                --max-clipped {params.max_clipped} {params.rm_pcr_dupli} -O {params.output_dir}  |& tee {log}
        else
                gstacks -t {threads} -I {params.bam_dir} -M {input.popmap} --model {params.model} \
                --var-alpha {params.var_alpha} --gt-alpha {params.gt_alpha} --min-mapq {params.min_mapq} --max-insert-len {params.max_insert} \
                --max-clipped {params.max_clipped} {params.rm_pcr_dupli} -O {params.output_dir}  |& tee {log}
        fi
        Rscript {snake_dir}/scripts/<step_name>__gstacks_refMap.prepare.report.R {params.params_file} |& tee {log};
        """
       