if config["SeOrPe"] == "SE":
  rule <step_name>__tsv2bam_SE:
    input:
        **<step_name>__tsv2bam_SE_inputs()
    output:
        tsv2bam_done = touch(os.path.dirname(<step_name>__tsv2bam_SE_inputs()["sstacks_done"])+"/tsv2bam.done")
    params:
        command = config["<step_name>__tsv2bam_SE_command"],
        sstacks_dir = os.path.dirname(<step_name>__tsv2bam_SE_inputs()["sstacks_done"]) ,
    threads: 
        config["<step_name>__tsv2bam_threads"]
    conda:
        "envs/stacks.yaml" 
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__tsv2bam_SE_output_dir"] + "/tsv2bam_log.txt"
    shell:
        "{params.command} "
        "-t {threads} "
        "-M {input.popmap} "
        "-P {params.sstacks_dir} "
        "|& tee {log} "

elif config["SeOrPe"] == "PE":
  rule <step_name>__tsv2bam_PE:
    input:
        **<step_name>__tsv2bam_PE_inputs()
    output:
        tsv2bam_done = touch(os.path.dirname(<step_name>__tsv2bam_PE_inputs()["sstacks_done"])+"/tsv2bam.done")
    params:
        command = config["<step_name>__tsv2bam_PE_command"],
        sstacks_dir = os.path.dirname(<step_name>__tsv2bam_PE_inputs()["sstacks_done"]) ,
        pe_reads_dir = "--pe-reads-dir " + <step_name>__tsv2bam_PE_inputs()["read_dir"],
    threads: 
        config["<step_name>__tsv2bam_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__tsv2bam_PE_output_dir"] + "/tsv2bam_log.txt"
    conda:
        "envs/stacks.yaml"    
    shell:
        "{params.command} "
        "-t {threads} "
        "-M {input.popmap} "
        "-P {params.sstacks_dir} "
        "{params.pe_reads_dir} "
        "|& tee {log} "

