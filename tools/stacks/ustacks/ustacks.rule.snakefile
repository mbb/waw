
rule <step_name>__ustacks:
    input:
        **<step_name>__ustacks_inputs(),
    output:
        ustacks_done =  config["results_dir"]+"/"+config["<step_name>__ustacks_output_dir"]+"/ustacks_done"
    params:
        command = config["<step_name>__ustacks_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__ustacks_output_dir"],
        input_dir  = <step_name>__ustacks_inputs()["read_dir"], #os.path.dirname(<step_name>__ustacks_inputs()["read"][0]),
        m = config["<step_name>__ustacks_m"],
        M = config["<step_name>__ustacks_M"],
        N = config["<step_name>__ustacks_N"],
    threads:
        config["<step_name>__ustacks_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__ustacks_output_dir"] + "/ustacks_log.txt"
    conda:
        "envs/stacks.yaml"
    shell:
        """
        mkdir -p {params.output_dir}; #this is not automatically created because no other outputs
        id=1; 
        for sam in  ` ls {input.read_dir}/*.{{fastq,fq}}.gz 2>/dev/null` ;
        do 
         if [ -f ${{sam}} ]; then 
          {params.command} -f $sam -p {threads} -i $id -m {params.m} -M {params.M} -N {params.N} -o {params.output_dir} |& tee {log}; 
          let 'id+=1'; 
         fi 
        done ;
        touch {params.ustacks_done} 
        """
