rule <step_name>__sstacks:
    input:
        **<step_name>__sstacks_inputs()
    output:
        sstacks_done = touch(os.path.dirname(<step_name>__sstacks_inputs()["cstacks_done"])+"/sstacks.done"),
    params:
        command = config["<step_name>__sstacks_command"],
        cstacks_dir = os.path.dirname(<step_name>__sstacks_inputs()["cstacks_done"]),
        output_dir  = os.path.dirname(<step_name>__sstacks_inputs()["cstacks_done"]),
    threads: 
        config["<step_name>__sstacks_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__sstacks_output_dir"] + "/sstacks_log.txt"
    conda:
       "envs/stacks.yaml"    
    shell:
        "mkdir -p {params.output_dir}; " # not needed as it's the same as cstacks ?because no other outputs
        "{params.command} "
        "-p {threads} "
        "-M {input.popmap} "
        "-P {params.cstacks_dir} "
        "|& tee {log}"
        