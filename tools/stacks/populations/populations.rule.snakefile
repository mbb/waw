rule <step_name>__populations:
    input:
        **<step_name>__populations_inputs(),
    output:
        log = config["results_dir"]+"/"+config["<step_name>__populations_output_dir"]+"/populations.log",
        vcf = config["results_dir"]+"/"+config["<step_name>__populations_output_dir"]+"/populations.snps.vcf.gz",
    params:
        sample_phylip = config["results_dir"]+"/"+config["<step_name>__populations_output_dir"]+"/samples_phylip/samples.fixed.phylip",
        command = config["<step_name>__populations_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__populations_output_dir"],
        gstacks_dir = os.path.dirname(<step_name>__populations_inputs()["catalog_calls"]),
        popmap = <step_name>__populations_inputs()["popmap"],
        r = config["<step_name>__populations_r"],
        max_obs_het = config["<step_name>__populations_max_obs_het"],
        min_maf = config["<step_name>__populations_min_maf"],
        p = config["<step_name>__populations_p"],
        remove_ctg_info = config["<step_name>__populations_remove_ctg_info"],
        out_phylip = config["<step_name>__populations_sample_phylip"],
        params_file = workflow.overwrite_configfiles,
    threads:
        config["<step_name>__populations_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__populations_output_dir"] + "/populations_log.txt"
    conda:
       "envs/stacks.yaml"        
    shell:
        #see https://groups.google.com/g/stacks-users/c/-iChdFNJnuk/m/iF2LdlGLAgAJ for ordered-export
        "{params.command} "
        "-t {threads} "
        "-P {params.gstacks_dir} "
        "-M {params.popmap} "
        "-O {params.output_dir} "
        "-r {params.r} "
        "--max-obs-het {params.max_obs_het} "
        "--min-maf {params.min_maf} "
        "-p {params.p} "
        "--vcf "
        "--ordered-export " 
        "--fstats "
        "--genepop "
        "--structure "
        "--radpainter "
        "--plink "
        "--phylip "
        "--phylip-var "
        "|& tee {log} ;"
        "if [ '{params.remove_ctg_info}' == 'True' ];"
        "then "
        " bcftools sort {params.output_dir}/populations.snps.vcf | grep -v '^##contig'   | bgzip  > {params.output_dir}/populations.snps.vcf.gz;"
        "else "
        " bcftools sort {params.output_dir}/populations.snps.vcf | bgzip  > {params.output_dir}/populations.snps.vcf.gz;"
        "fi;"
        " tabix -p vcf {params.output_dir}/populations.snps.vcf.gz ;"
        " rm -f  {params.output_dir}/populations.snps.vcf;"


        #generate a phylip file including sequences for each sample as described here https://groups.google.com/g/stacks-users/c/z9zRQGrEIcE/m/OFa8bsJiBAAJ

        #generate a whitelist from populations previous run
        "grep -v '^#' {params.output_dir}/populations.sumstats.tsv | cut -f 1 | sort |uniq > {params.output_dir}/whitelist.txt ;"
        # create a dummy popMap where each sample is a putative population of its own
        "if [ '{params.out_phylip}' == 'True' ];"
        "then "
        " awk '{{print $1\"\\t\"$1}}' {params.popmap} > {params.output_dir}/dummyPopMap.txt ;"
        
        #save pop based phylip already generated
        #"mv {params.output_dir}/populations.fixed.phylip {params.output_dir}/populations.fixed.phylip_save ;"

        #Rerun populations to generate a sample based phylip file
        " {params.command} -t {threads} -P {params.gstacks_dir} -M {params.output_dir}/dummyPopMap.txt -O {params.output_dir}/samples_phylip --phylip ;"
        " grep -v '^#' {params.output_dir}/samples_phylip/populations.fixed.phylip > {params.output_dir}/samples_phylip/samples.fixed.phylip;"
        "fi;"

        #"mv {params.output_dir}/populations.fixed.phylip_save {params.output_dir}/populations.fixed.phylip;"
        #"Rscript {snake_dir}/scripts/<step_name>__populations.prepare.report.R {params.params_file} |& tee {log};"
        "Rscript {snake_dir}/scripts/<step_name>__populations.prepare.report.R {conf_path} |& tee {log};"

