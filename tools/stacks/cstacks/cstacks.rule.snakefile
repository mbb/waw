rule <step_name>__cstacks:
    input:
        **<step_name>__cstacks_inputs(),
    output:
        cstacks_done = touch(os.path.dirname(<step_name>__cstacks_inputs()["ustacks_done"])+"/cstacks.done"),
    params:
        command = config["<step_name>__cstacks_command"],
        n = config["<step_name>__cstacks_n"],
        input_dir = os.path.dirname(<step_name>__cstacks_inputs()["ustacks_done"]),
        output_dir = os.path.dirname(<step_name>__cstacks_inputs()["ustacks_done"]),
    threads: 
        config["<step_name>__cstacks_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__cstacks_output_dir"] + "/cstacks_log.txt"
    conda:
       "envs/stacks.yaml"        
    shell:
        "mkdir -p {params.output_dir}; " # here the same as ustacks (input_dir)  ? needed because no output file
        "{params.command} "
        "-p {threads} "
        "-n {params.n} "
        "-P {params.input_dir} "
        "-M {input.popmap} "
        "|& tee {log}; "