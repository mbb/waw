def get_individus():
        if "<step_name>__process_radtags_SE_inputs" in globals():
            with open(<step_name>__process_radtags_SE_inputs()["tags"], mode="r") as infile:
                reader = csv.reader(infile,delimiter='\t')
                return [row[-1] for row in reader]
        else:
            with open(<step_name>__process_radtags_PE_inputs()["tags"], mode="r") as infile:
                    reader = csv.reader(infile,delimiter='\t')
                    return [row[-1] for row in reader]      

individus = get_individus()
#!!!!! this will override SAMPLES obtained from raw_reads ...
SAMPLES = individus

if config["SeOrPe"] == "SE":

    rule <step_name>__process_radtags_SE:
        input:
            **<step_name>__process_radtags_SE_inputs(),
            #barcode_file = config["<step_name>__process_radtags_barcode_file"]
        output:
            reads_demultiplexed=expand(config["results_dir"]+"/"+config["<step_name>__process_radtags_SE_output_dir"]+"/{id}.fq.gz",id=individus),# fastq par individu pour un sample
            read_dir = directory(config["results_dir"]+"/"+config["<step_name>__process_radtags_SE_output_dir"]) #need snakemake > 5.9.1 to avoid ChildIOException !!
        params:
            command = config["<step_name>__process_radtags_SE_command"],
            output_dir = config["results_dir"]+"/"+config["<step_name>__process_radtags_SE_output_dir"]+"/",
            barcode_type = config["<step_name>__process_radtags_barcode_type"],
            enzyme_SE = config["<step_name>__process_radtags_enzyme_SE"],
        threads:
            config["<step_name>__process_radtags_threads"]  
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__process_radtags_SE_output_dir"] + "/process_radtags_log.txt"
        conda:
            "envs/stacks.yaml"    
        shell:
            "{params.command} "
            "-p {input.reads_dir} "
            "-b {input.tags} "
            "-o {params.output_dir} "
            "{params.barcode_type} "
            "-e {params.enzyme_SE} "    
            "--clean "
            #"--quality "
            "--rescue "
            "--threads {threads} "
            "|& tee {log}"



elif config["SeOrPe"] == "PE":

    rule <step_name>__process_radtags_PE:
        input:
            **<step_name>__process_radtags_PE_inputs(),
            #barcode_file = config["<step_name>__process_radtags_barcode_file"]
        output:
            reads_forward_demultiplexed=expand(config["results_dir"]+"/"+config["<step_name>__process_radtags_PE_output_dir"]+"/{id}.1.fq.gz",id=individus), # fastq par individu pour un sample
            reads_reverse_demultiplexed=expand(config["results_dir"]+"/"+config["<step_name>__process_radtags_PE_output_dir"]+"/{id}.2.fq.gz",id=individus), # fastq par individu pour un sample
            read_dir = directory(config["results_dir"]+"/"+config["<step_name>__process_radtags_PE_output_dir"])
        params:
            command = config["<step_name>__process_radtags_PE_command"],
            output_dir = config["results_dir"]+"/"+config["<step_name>__process_radtags_PE_output_dir"],
            barcode_type = config["<step_name>__process_radtags_barcode_type"],
            enzyme_1_PE = "--renz_1 " + config["<step_name>__process_radtags_enzyme_1_PE"],
            enzyme_2_PE = "" if (config["<step_name>__process_radtags_enzyme_2_PE"] == "") else "--renz_2 " + config["<step_name>__process_radtags_enzyme_2_PE"],
        threads:
            config["<step_name>__process_radtags_threads"]  
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__process_radtags_PE_output_dir"] + "/process_radtags_log.txt"
        conda:
            "envs/stacks.yaml"              
        shell:
            "{params.command} "
            "-p {input.reads_dir} "
            "-P "
            "-b {input.tags} "
            "-o {params.output_dir} "
            "{params.barcode_type} "
            "{params.enzyme_1_PE} "
            "{params.enzyme_2_PE} "
            "--clean "
            #"--quality "
            "--rescue "
            "--threads {threads} "
            "|& tee {log}"

