def get_individus():
        if "<step_name>__process_shortreadstags_SE_inputs" in globals():
            with open(<step_name>__process_shortreadstags_SE_inputs()["tags"], mode="r") as infile:
                reader = csv.reader(infile,delimiter='\t')
                return [row[-1] for row in reader]
        else:
            with open(<step_name>__process_shortreadstags_PE_inputs()["tags"], mode="r") as infile:
                    reader = csv.reader(infile,delimiter='\t')
                    return [row[-1] for row in reader]                

individus = get_individus()
#!!!!! this will override SAMPLES obtained from raw_reads ...
SAMPLES = individus

if config["SeOrPe"] == "SE":

    rule <step_name>__process_shortreadstags_SE:
        input:
            **<step_name>__process_shortreadstags_SE_inputs(),
            #barcode_file = config["<step_name>__process_shortreadstags_barcode_file"]
        output:
            reads_demultiplexed=expand(config["results_dir"]+"/"+config["<step_name>__process_shortreadstags_SE_output_dir"]+"/{id}.fq.gz",id=individus),# fastq par individu pour un sample
            read_dir = directory(config["results_dir"]+"/"+config["<step_name>__process_shortreadstags_SE_output_dir"]) #need snakemake > 5.9.1 to avoid ChildIOException !!
        params:
            command = config["<step_name>__process_shortreadstags_SE_command"],
            output_dir = config["results_dir"]+"/"+config["<step_name>__process_shortreadstags_SE_output_dir"]+"/",
            barcode_type = config["<step_name>__process_shortreadstags_barcode_type"],
        threads:
            config["<step_name>__process_shortreadstags_threads"]      
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__process_shortreadstags_SE_output_dir"] + "/process_shortreadstags_log.txt"
        conda:
            "envs/stacks.yaml"
        shell:
            "{params.command} "
            "-p {input.reads_dir} "
            "-b {input.tags} "
            "-o {params.output_dir} "
            "{params.barcode_type} "
            "--clean "
            #"--quality "
            "-r "
            "--threads {threads} "
            "|& tee {log}"



elif config["SeOrPe"] == "PE":

    rule <step_name>__process_shortreadstags_PE:
        input:
            **<step_name>__process_shortreadstags_PE_inputs(),
            #barcode_file = config["<step_name>__process_shortreadstags_barcode_file"]
        output:
            reads_forward_demultiplexed=expand(config["results_dir"]+"/"+config["<step_name>__process_shortreadstags_PE_output_dir"]+"/{id}.1.fq.gz",id=individus), # fastq par individu pour un sample
            reads_reverse_demultiplexed=expand(config["results_dir"]+"/"+config["<step_name>__process_shortreadstags_PE_output_dir"]+"/{id}.2.fq.gz",id=individus), # fastq par individu pour un sample
            read_dir = directory(config["results_dir"]+"/"+config["<step_name>__process_shortreadstags_PE_output_dir"])
        params:
            command = config["<step_name>__process_shortreadstags_PE_command"],
            output_dir = config["results_dir"]+"/"+config["<step_name>__process_shortreadstags_PE_output_dir"],
            barcode_type = config["<step_name>__process_shortreadstags_barcode_type"],
        threads:
            config["<step_name>__process_shortreadstags_threads"]
        conda:
            "envs/stacks.yaml"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__process_shortreadstags_PE_output_dir"] + "/process_shortreadstags_log.txt"
        shell:
            "{params.command} "
            "-p {input.reads_dir} "
            "-P "
            "-b {input.tags} "
            "-o {params.output_dir} "
            "{params.barcode_type} "
            "--clean "
            #"--quality "
            "-r "
            "--threads {threads} "
            "|& tee {log}"

