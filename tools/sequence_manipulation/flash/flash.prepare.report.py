import oyaml as yaml
import sys
import re
import os
from collections import OrderedDict

config = dict()
with open(sys.argv[1], 'r') as paramfile:
    config = yaml.load(paramfile, Loader=yaml.FullLoader)

config = config["params"]

def get_field(field, slog, fl=False):
    """parse sample log for field
       set fl=True to return a float
       otherwise, returns int
    """
    field += r'\:\s+([\d\.]+)'
    match = re.search(field, slog)
    if match:
        if fl:
            return float(match.group(1))
        return int(match.group(1))
    return 0

def get_sample(logfile):
    if "_flash_log.txt" in logfile:
        return os.path.basename(logfile).replace("_flash_log.txt","")
    else:
        return "unknown"

out ="""# description: 'FLASh read combination statistics'
# section_name: 'FLASh combinations'
# plot_type: 'table'
"""

out += '\t'.join(['sample','totalpairs','discardpairs','%discard','combopairs','inniepairs','outiepairs','uncombopairs','%combo']) + '\n'
for file in os.listdir(os.path.join(config["results_dir"],"logs",config["<step_name>__flash_output_dir"])):
    slog = ""
    res = OrderedDict()
    sample = get_sample(file)
    with open(os.path.join(config["results_dir"],"logs",config["<step_name>__flash_output_dir"],file), 'r') as logfile:
        slog = "".join(logfile.readlines())

    res['totalpairs'] = get_field('Total pairs', slog)
    res['discardpairs'] = get_field('Discarded pairs', slog)
    res['percdiscard'] = get_field('Percent Discarded', slog, fl=True)
    res['combopairs'] = get_field('Combined pairs', slog)
    res['inniepairs'] = get_field('Innie pairs', slog)
    res['outiepairs'] = get_field('Outie pairs', slog)
    res['uncombopairs'] = get_field('Uncombined pairs', slog)
    res['perccombo'] = get_field('Percent combined', slog, fl=True)

    out += sample + '\t' + '\t'.join(str(x) for x in res.values()) + '\n'

with open(os.path.join(config["results_dir"],config["<step_name>__flash_output_dir"],"flash_combinations_mqc.csv"), "w") as outfile:
    outfile.write(out)