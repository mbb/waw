if config["SeOrPe"] == "PE":

    rule <step_name>__bowtie2_PE:
        input:
            **<step_name>__bowtie2_PE_inputs()
        output:
            bam = config["results_dir"]+"/"+config["<step_name>__bowtie2_PE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__bowtie2_PE_output_dir"] + "/{sample}_bowtie2_log.txt"
        threads:
            config["<step_name>__bowtie2_threads"]
        params:
            command = config["<step_name>__bowtie2_PE_command"],
            #indexPrefix = config["<step_name>__bowtie2_index_output_dir"]+"/index",
            #here we have index files  : first discard index.rev.?.ebwt to get only index.?.ebwt so we have two remove the ext twice
            indexPrefix = lambda w, input: os.path.splitext(os.path.splitext([x for x in input.index if not 'rev' in x][0])[0])[0],
            minins = config["<step_name>__bowtie2_minins_PE"],
            maxins = config["<step_name>__bowtie2_maxins_PE"],
            orientation = config["<step_name>__bowtie2_orientation_PE"],
        conda:   
           "envs/bowtie2.yaml"
        shell:
            "# conda create env -n bowtie2 -f /workflow/envs/bowtie2.yaml ; \n"
            "# conda activate bowtie2; \n"
            "{params.command} "
            "--threads {threads} "
            "-x {params.indexPrefix} "
            "-I {params.minins} "
            "-X {params.maxins} "
            "{params.orientation} "
            "--rg-id ID:WAW --rg SM:{wildcards.sample} "
            #"-S "#output in sam format
            "-1 {input.read} "
            "-2 {input.read2} 2> {log} "
            "| samtools view -b 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"

elif config["SeOrPe"] == "SE":

    rule <step_name>__bowtie2_SE:
        input:
            **<step_name>__bowtie2_SE_inputs()
        output:
            bam = config["results_dir"]+"/"+config["<step_name>__bowtie2_SE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__bowtie2_SE_output_dir"] + "/{sample}_bowtie2_log.txt"
        threads:
            config["<step_name>__bowtie2_threads"]
        params:
            command = config["<step_name>__bowtie2_SE_command"],
            #indexPrefix = config["<step_name>__bowtie2_index_output_dir"]+"/index",
            indexPrefix = lambda w, input: os.path.splitext(os.path.splitext([x for x in input.index if not 'rev' in x][0])[0])[0],
        conda:  
           "envs/bowtie2.yaml"
        shell:
            "{params.command} "
            "--threads {threads} "
            "-x {params.indexPrefix} "
            "--rg-id ID:WAW --rg SM:{wildcards.sample} "
            #"-S "#output in sam format
            "{input.read} 2> {log} "
            "| samtools view -b 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"
