rule <step_name>__minimap2_reference_bam:
    input:
        **<step_name>__minimap2_reference_bam_inputs(),
    output: 
        reads_mapping = config["results_dir"] + "/" + config["<step_name>__minimap2_reference_bam_output_dir"] + "/reads.bam",
    params:
        command = config["<step_name>__minimap2_reference_bam_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__minimap2_reference_bam_output_dir"]+ "/",
        pacbio_oxfordNanopore = config["<step_name>__minimap2_reference_bam_pacbio_oxfordNanopore"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__minimap2_reference_bam_output_dir"] + "/minimap2_reference_bam_log.txt"
    threads: 
        config["<step_name>__minimap2_reference_bam_threads"]
    conda:
        "envs/minimap2.yaml"        
    shell: 
        "{params.command} map-{params.pacbio_oxfordNanopore} "
        "-t {threads} "
        "{input.fasta} "
        "{input.read} "
        "| samtools view -hb 2> {log} "
        "| samtools sort -@ {threads} > {output.reads_mapping} 2>> {log} &&"
        " samtools index -@ {threads} {output.reads_mapping} 2>> {log}"
        
