if config["SeOrPe"] == "PE":

    rule <step_name>__bwa_mem_PE:
        input:
            **<step_name>__bwa_mem_PE_inputs(),
        output:
            bam = config["results_dir"]+"/"+config["<step_name>__bwa_mem_PE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__bwa_mem_PE_output_dir"] + "/{sample}_bwa_mem_log.txt"
        threads:
            config["<step_name>__bwa_mem_threads"]
        params:
            command = config["<step_name>__bwa_mem_PE_command"],
            #indexPrefix =  config["<step_name>__bwa_index_output_dir"]+"/index",
            indexPrefix = lambda w, input: os.path.splitext(input.index[0])[0],
            quality0_multimapping = """| awk '{{pos = index("XA:Z",$0); if (pos>0) $5=0;print}}'""" if (config["<step_name>__bwa_mem_quality0_multimapping"] == True) else ""
        conda:
            "envs/bwa.yaml"   
        shell:
            "{params.command} "
            "-t {threads} "
            "{params.indexPrefix} "
            "-R '@RG\\tID:WAW\\tSM:{wildcards.sample}' "
            "{input.read} "
            "{input.read2} 2> {log} "
            "{params.quality0_multimapping} "
            "| samtools view -b 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"

elif config["SeOrPe"] == "SE":

    rule <step_name>__bwa_mem_SE:
        input:
            **<step_name>__bwa_mem_SE_inputs(),
        output:
            bam = config["results_dir"]+"/"+config["<step_name>__bwa_mem_SE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__bwa_mem_SE_output_dir"] + "/{sample}_bwa_mem_log.txt"
        threads:
            config["<step_name>__bwa_mem_threads"]
        params:
            command = config["<step_name>__bwa_mem_SE_command"],
            #indexPrefix =  config["<step_name>__bwa_index_output_dir"]+"/index",
            indexPrefix = lambda w, input: os.path.splitext(input.index[0])[0],
            quality0_multimapping = """| awk '{{pos = index("XA:Z",$0); if (pos>0) $5=0;print}}'""" if (config["<step_name>__bwa_mem_quality0_multimapping"] == True) else ""
        conda:
            "envs/bwa.yaml"  
        shell:
            "{params.command} "
            "-t {threads} "
            "{params.indexPrefix} "
            "-R '@RG\\tID:WAW\\tSM:{wildcards.sample}' "
            "{input.read} 2> {log} "
            "{params.quality0_multimapping} "
            "| samtools view -b 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"
