if config["SeOrPe"] == "PE":

    rule <step_name>__snap_PE:
        input:
            **<step_name>__snap_PE_inputs()
        output:
            bam = config["results_dir"]+"/"+config["<step_name>__snap_PE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__snap_PE_output_dir"] + "/{sample}_snap_log.txt"
        threads:
            config["<step_name>__snap_threads"]
        params:
            command = config["<step_name>__snap_PE_command"],
            indexDir = lambda w, input: os.path.dirname(input.index[0]),
            snap_seed_number = config["<step_name>__snap_seed_number"],
            snap_min_insert_size = config["<step_name>__snap_min_insert_size"],
            snap_max_insert_size = config["<step_name>__snap_max_insert_size"],
            snap_disable_alt_awareness = "-A-" if config["<step_name>__snap_disable_alt_awareness"] else "",
            snap_max_score_gap = config["<step_name>__snap_max_score_gap"]
        shell:
            "{params.command} "
            "{params.indexDir} "
            "{input.read} {input.read2} "
            "-t {threads} "
            "-n {params.snap_seed_number} "
            "-s {params.snap_min_insert_size} {params.snap_max_insert_size} "
            "{params.snap_disable_alt_awareness} "
            "-asg {params.snap_max_score_gap} "
            "-so "
            "-o {output.bam}   2> {log}; "


elif config["SeOrPe"] == "SE":

    rule <step_name>__snap_SE:
        input:
            **<step_name>__snap_SE_inputs()
        output:
            bam = config["results_dir"]+"/"+config["<step_name>__snap_SE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__snap_SE_output_dir"] + "/{sample}_snap_log.txt"
        threads:
            config["<step_name>__snap_threads"]
        params:
            command = config["<step_name>__snap_PE_command"],
            snap_seed_number = config["<step_name>__snap_seed_number"],
            snap_disable_alt_awareness = "-A-" if config["<step_name>__snap_disable_alt_awareness"] else "",
            snap_max_score_gap = config["<step_name>__snap_max_score_gap"],
            indexDir = lambda w, input: os.path.dirname( input.index[0]),
        conda:
            "envs/snap-aligner.yaml"
        shell:
            "{params.command} "                        
            "{params.indexDir} "
            "{input.read} "
            "-t {threads} "
            "-n {params.snap_seed_number} "
            "{params.snap_disable_alt_awareness} "
            "-asg {params.snap_max_score_gap} "            
            "-so "            
            "-o {output.bam}  2> {log}; "
