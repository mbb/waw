if config["SeOrPe"] == "PE":

    rule <step_name>__hisat2_PE:
        input:
            **<step_name>__hisat2_PE_inputs()
        output:
            bam = config["results_dir"]+"/"+config["<step_name>__hisat2_PE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__hisat2_PE_output_dir"] + "/{sample}_hisat2_log.txt"
        threads:
            config["<step_name>__hisat2_threads"]
        params:
            command = config["<step_name>__hisat2_PE_command"],
            #indexPrefix = config["<step_name>__hisat2_index_output_dir"]+"/index",
            #here we have index files  : from .1.ht2 to .8.ht2
            indexPrefix = lambda w, input: os.path.splitext(os.path.splitext([x for x in input.index ][0])[0])[0],
            minins = "-I " + config["<step_name>__hisat2_minins_PE"] if config["<step_name>__hisat2_reads_type"] == "genome" else "",
            maxins = "-X " + config["<step_name>__hisat2_maxins_PE"] if config["<step_name>__hisat2_reads_type"] == "genome" else "",
            orientation = config["<step_name>__hisat2_orientation_PE"],
        conda:
            "envs/hisat2.yaml" #if samtools in base image    
        shell:
            "{params.command} --new-summary "
            "--threads {threads} "
            "-x {params.indexPrefix} "
            "{params.minins} "
            "{params.maxins} "
            "{params.orientation} "
            "--rg-id ID:WAW --rg SM:{wildcards.sample} "
            #"-S "#output in sam format
            "-1 {input.read} "
            "-2 {input.read2} 2> {log} "
            "| samtools view -b 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"

elif config["SeOrPe"] == "SE":

    rule <step_name>__hisat2_SE:
        input:
            **<step_name>__hisat2_SE_inputs()
        output:
            bam = config["results_dir"]+"/"+config["<step_name>__hisat2_SE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__hisat2_SE_output_dir"] + "/{sample}_hisat2_log.txt"
        threads:
            config["<step_name>__hisat2_threads"]
        params:
            command = config["<step_name>__hisat2_SE_command"],
            #indexPrefix = config["<step_name>__hisat2_index_output_dir"]+"/index",
            indexPrefix = lambda w, input: os.path.splitext(os.path.splitext([x for x in input.index if not 'rev' in x][0])[0])[0],
        conda:
            "envs/hisat2.yaml"  #if samtools in base image  
        shell:
            "{params.command} "
            "--threads {threads} "
            "-x {params.indexPrefix} "
            "--rg-id ID:WAW --rg SM:{wildcards.sample} "
            #"-S "#output in sam format
            "-U {input.read} 2> {log} "
            "| samtools view -b 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"
