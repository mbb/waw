rule <step_name>__map_box_index:
    input:
        **<step_name>__map_box_index_inputs()
    output:
        config["results_dir"]+"/"+config["<step_name>__map_box_output_dir"]+"/index/index.amb",
        config["results_dir"]+"/"+config["<step_name>__map_box_output_dir"]+"/index/index.ann",
        config["results_dir"]+"/"+config["<step_name>__map_box_output_dir"]+"/index/index.bwt",
        config["results_dir"]+"/"+config["<step_name>__map_box_output_dir"]+"/index/index.pac",
        config["results_dir"]+"/"+config["<step_name>__map_box_output_dir"]+"/index/index.sa"
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__map_box_output_dir"] + "/index.log"
    params:
        algorithm="bwtsw",
        output_prefix = config["results_dir"]+"/"+config["<step_name>__map_box_output_dir"]+"/index/index"
    conda:
        "envs/bwa.yaml"
    shell:
        "bwa index "
        "-p {params.output_prefix} "
        "-a {params.algorithm} "
        "{input.fasta_refseq} |& tee {log}"

rule <step_name>__map_box_align:
    input:
        **<step_name>__map_box_align_inputs(),
        index = rules.<step_name>__map_box_index.output
    output:
        bam = config["results_dir"]+"/"+config["<step_name>__map_box_output_dir"]+"/{sample}.bam",
        stats = config["results_dir"]+"/"+config["<step_name>__map_box_output_dir"]+"/{sample}_bamstats.txt",
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__map_box_output_dir"] + "/{sample}_map_box_log.txt"
    threads:
        config["<step_name>__map_box_threads"]
    params:
        indexPrefix = config["results_dir"]+"/"+config["<step_name>__map_box_output_dir"]+"/index/index",
        pe = lambda w,input: input.read2 if (config["SeOrPe"] == "PE") else ""
    conda:
        "envs/bwa.yaml"
    shell:
        "bwa mem "
        "-t {threads} "
        "{params.indexPrefix} "
        "{input.read} "
        "{params.pe} 2> {log} "
        "| samtools view -b 2>> {log} "
        "| samtools sort --threads {threads} > {output.bam} 2>> {log} "
        "&& samtools stats {output.bam} -@ {threads} > {output.stats} 2>> {log}"
