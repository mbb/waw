if config["SeOrPe"] == "PE":

    rule <step_name>__accel_align_PE:
        input:
            **<step_name>__accel_align_PE_inputs()
        output:
            bam = config["results_dir"]+"/"+config["<step_name>__accel_align_PE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__accel_align_PE_output_dir"] + "/{sample}_accel_align_log.txt"
        threads:
            config["<step_name>__accel_align_threads"]
        params:
            command = config["<step_name>__accel_align_PE_command"],
            tmp_sam = config["results_dir"]+"/"+config["<step_name>__accel_align_PE_output_dir"]+"/{sample}.sam",
            #indexPrefix =  config["<step_name>__accel_align_index_output_dir"]+"/index",
            #/patho_index/indexName.fna.hash 
            indexPrefix = lambda w, input: os.path.splitext([x for x in input.index if 'hash' in x][0])[0],
            accel_align_seed_length = config["<step_name>__accel_align_seed_length"],
            accel_align_insert_size = config["<step_name>__accel_align_insert_size"],
            accel_align_softclipping = "-s" if config["<step_name>__accel_align_softclipping"] else "",
            accel_align_alignment_free = "-x" if config["<step_name>__accel_align_alignment_free"] else "",
        shell:
            "{params.command} "
            "-t {threads} "
            "-l {params.accel_align_seed_length} "
            "-p {params.accel_align_insert_size} "
            "{params.accel_align_softclipping} "
            "{params.accel_align_alignment_free} "
            "-o {params.tmp_sam}  "
            "{params.indexPrefix} "
            "{input.read} {input.read2} 2> {log}; "
            "samtools view -hb {params.tmp_sam} 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} && "
            "samtools index -@ {threads} {output.bam} 2>> {log}; "
            "rm -f {params.tmp_sam} "


elif config["SeOrPe"] == "SE":

    rule <step_name>__accel_align_SE:
        input:
            **<step_name>__accel_align_SE_inputs()
        output:
            bam = config["results_dir"]+"/"+config["<step_name>__accel_align_SE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__accel_align_SE_output_dir"] + "/{sample}_accel_align_log.txt"
        threads:
            config["<step_name>__accel_align_threads"]
        params:
            tmp_sam = config["results_dir"]+"/"+config["<step_name>__accel_align_PE_output_dir"]+"/{sample}.sam",
            command = config["<step_name>__accel_align_PE_command"],
            accel_align_seed_length = config["<step_name>__accel_align_seed_length"],
            accel_align_softclipping = "-s" if config["<step_name>__accel_align_softclipping"] else "",
            accel_align_alignment_free = "-x" if config["<step_name>__accel_align_alignment_free"] else "",
            #indexPrefix =  config["<step_name>__accel_align_index_output_dir"]+"/index",
            indexPrefix = lambda w, input: os.path.splitext([x for x in input.index if 'hash' in x][0])[0],
        shell:
            "{params.command} "
            "-t {threads} "
            "-l {params.accel_align_seed_length} "
            "{params.accel_align_softclipping} "
            "{params.accel_align_alignment_free} "
            "-o {params.tmp_sam} "
            "{params.indexPrefix} "
            "{input.read} 2> {log}; "
            "| samtools view -hb {params.tmp_sam} 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} && "
            "samtools index -@ {threads} {output.bam} 2>> {log}; "
            "rm -f {params.tmp_sam} "
