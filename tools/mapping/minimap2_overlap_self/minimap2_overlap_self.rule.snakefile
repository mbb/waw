rule <step_name>__minimap2_overlap_self:
    input:
        **<step_name>__minimap2_overlap_self_inputs(),
    output: 
        reads_overlaps = config["results_dir"] + "/" + config["<step_name>__minimap2_overlap_self_output_dir"] + "/reads.paf.gz",
    params:
        command = config["<step_name>__minimap2_overlap_self_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__minimap2_overlap_self_output_dir"]+ "/",
        pacbio_oxfordNanopore = config["<step_name>__minimap2_overlap_self_pacbio_oxfordNanopore"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__minimap2_overlap_self_output_dir"] + "/minimap2_overlap_self_log.txt"
    threads: 
        config["<step_name>__minimap2_overlap_self_threads"]
    conda:
        "envs/minimap2.yaml"        
    shell: 
        "{params.command} ava-{params.pacbio_oxfordNanopore} "
        "-t {threads} "
        "{input.read} "
        "{input.read} "
        "| gzip -1 > {output.reads_overlaps} "
        "|& tee {log}"
