rule <step_name>__FROGS_biom_to_tsv:
    input:
        **<step_name>__FROGS_biom_to_tsv_inputs(),
    output:
        abundance_file = config["results_dir"]+'/'+config["<step_name>__FROGS_biom_to_tsv_output_dir"]+'/Abundance_Table.tsv',
        frogs_file = config["results_dir"]+'/'+config["<step_name>__FROGS_biom_to_tsv_output_dir"]+'/FROGS_Table.tsv',
        multihits = config["results_dir"]+'/'+config["<step_name>__FROGS_biom_to_tsv_output_dir"]+'/multihits.tsv',
    log: 
        config["results_dir"]+'/logs/' + config["<step_name>__FROGS_biom_to_tsv_output_dir"] + '/FROGS_biom_to_tsv_log.txt',
    params:
        command = config["<step_name>__FROGS_biom_to_tsv_command"],
        outDir = config["results_dir"]+'/'+config["<step_name>__FROGS_biom_to_tsv_output_dir"],
    conda: 
        "envs/frogs.yaml"
    shell: 
        """
        biom2tsv=$(echo $CONDA_PREFIX"/libexec/biom2tsv.py")
        python $biom2tsv --input-file {input.biom} --output-file {output.abundance_file} --fields '@observation_name' '@sample_count'
        sed -i 's/^#observation_name//' {output.abundance_file}

        {params.command} -b {input.biom} -t {output.frogs_file} -m {output.multihits}
        if [ ! -f "{output.multihits}" ]; then
            touch {output.multihits}
        fi
        """
