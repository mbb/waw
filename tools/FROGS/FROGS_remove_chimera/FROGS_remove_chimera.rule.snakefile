rule <step_name>__FROGS_remove_chimera:
    input:
        **<step_name>__FROGS_remove_chimera_inputs(),
    output:
        abundance_file = config["results_dir"]+'/'+config["<step_name>__FROGS_remove_chimera_output_dir"]+'/out_abundance_biom.biom',
        non_chimera_fasta = config["results_dir"]+'/'+config["<step_name>__FROGS_remove_chimera_output_dir"]+'/non_chimera_fasta.fasta',
        summary = config["results_dir"]+'/'+config["<step_name>__FROGS_remove_chimera_output_dir"]+'/chimera_report.html',
    log: config["results_dir"]+'/logs/' + config["<step_name>__FROGS_remove_chimera_output_dir"] + '/FROGS_remove_chimera_log.txt',
    params:
        command = config["<step_name>__FROGS_remove_chimera_command"],
        outDir = config["results_dir"]+'/'+config["<step_name>__FROGS_remove_chimera_output_dir"],
        stepDir = config["<step_name>__FROGS_remove_chimera_output_dir"],
    threads: 
        config["<step_name>__FROGS_remove_chimera_threads"],
    conda: 
        "envs/frogs.yaml"
    shell:
        "{params.command} "
        "--input-fasta {input.sequence_file} "
        "--input-biom {input.abundance_biom} "
        "--nb-cpus {threads} "
        "--out-abundance {output.abundance_file} "
        "--non-chimera {output.non_chimera_fasta} "
        "--summary {output.summary}; "
        "echo \"1\t./{params.stepDir}/chimera_report.html\" > {params.outDir}/text.generic.tsv"
