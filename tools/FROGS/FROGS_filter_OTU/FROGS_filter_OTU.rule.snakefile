rule <step_name>__FROGS_filter_OTU:
    input:
        **<step_name>__FROGS_filter_OTU_inputs(),
    output:
        abundance_file = config["results_dir"]+'/'+config["<step_name>__FROGS_filter_OTU_output_dir"]+'/otuFilter_abundance.biom',
        filtered_sequences = config["results_dir"]+'/'+config["<step_name>__FROGS_filter_OTU_output_dir"]+'/otuFilter_sequences.fasta',
        excluded_file = config["results_dir"]+'/'+config["<step_name>__FROGS_filter_OTU_output_dir"]+'/excluded.tsv',
        summary = config["results_dir"]+'/'+config["<step_name>__FROGS_filter_OTU_output_dir"]+'/filtOTU_report.html',
    log: config["results_dir"]+'/logs/' + config["<step_name>__FROGS_filter_OTU_output_dir"] + '/FROGS_filter_OTU_log.txt',
    params:
        command = config["<step_name>__FROGS_filter_OTU_command"],
        outDir = config["results_dir"]+'/'+config["<step_name>__FROGS_filter_OTU_output_dir"],
        stepDir = config["<step_name>__FROGS_filter_OTU_output_dir"],
        min_sample_presence = "--min-sample-presence "+str(config["<step_name>__FROGS_filter_OTU_min_sample_presence"]) if config["<step_name>__FROGS_filter_OTU_min_sample_presence"]>0 else "",
        min_abundance_proportion = config["<step_name>__FROGS_filter_OTU_min_abundance_proportion"],
        nb_biggest_otu = "--nb-biggest-clusters "+str(config["<step_name>__FROGS_filter_OTU_nb_biggest_otu"]) if config["<step_name>__FROGS_filter_OTU_nb_biggest_otu"]>0 else "",
        contaminantSource = "--contaminant " if config["<step_name>__FROGS_filter_OTU_contaminantSource"] =="history" else "",
        contaminantFile = config["<step_name>__FROGS_filter_OTU_contaminantFile"],
        contaminantDB = config["<step_name>__FROGS_filter_OTU_contaminants_db"]+"/train_set.fasta",
        contaminantYaml = config["<step_name>__FROGS_filter_OTU_contaminants_db"]+"/info.yaml",
        contaminantPath = config["<step_name>__FROGS_filter_OTU_contaminants_db"],
    threads: 
        config["<step_name>__FROGS_filter_OTU_threads"],
    conda: 
        "envs/frogs.yaml"
    shell: 
        """
        contaminant=""
        if [ "{params.contaminantSource}" != "" ]; then
            if [ "{params.contaminantFile}" == "" ]; then
                contaminant="{params.contaminantSource}{params.contaminantDB}"
                if [ ! -f {params.contaminantDB} ]; then
                    taxURL=$(grep train_set: {params.contaminantYaml} | sed 's/train_set://' | sed 's/"//g')
                    cd {params.contaminantPath};
                    echo "Downloading contaminant database from:  $taxURL";
                    wget $taxURL -O {params.contaminantDB}.tar.gz -q ;
                    tar --strip-components=1 -xzf {params.contaminantDB}.tar.gz && rm -f {params.contaminantDB}.tar.gz;
                    for i in `ls *.fa*`
                    do 
                        newname=$(echo $i | sed 's/.*fasta/train_set.fasta/')
                        mv $i $newname
                    done
                fi
            else
                contaminant="{params.contaminantSource}{params.contaminantFile}"
            fi
        else
            contaminant=""
        fi

        cd {params.outDir}
        {params.command} --input-biom {input.biom} --input-fasta {input.sequence_file} --output-biom {output.abundance_file} --output-fasta {output.filtered_sequences} --excluded {output.excluded_file} --summary {output.summary} --nb-cpus {threads} {params.min_sample_presence} --min-abundance {params.min_abundance_proportion} {params.nb_biggest_otu} ${{contaminant}};
        

        echo \"1\t./{params.stepDir}/filtOTU_report.html\" > {params.outDir}/text.generic.tsv
        """