{
  id: FROGS_filter_OTU,
  name: FROGS filter OTU,
  article: 10.1093/bib/bbab318,
  website: "http://frogs.toulouse.inrae.fr/",
  git: "https://github.com/geraldinepascal/FROGS",
  description: "Filters OTUs on several criteria",
  version: "4.1.0",
  documentation: "http://frogs.toulouse.inrae.fr/html/commands_utax.html",
  multiqc: "generic_iframe",
  commands:
    [
      {
        name: FROGS_filter_OTU,
        command: "cluster_filters.py",
        category: "FROGS",
        output_dir: FROGS_filter_OTU,
        inputs: 
          [
            { name: sequence_file, type: "fasta_file", description: "the sequences in fasta format"},
            { name: biom, type: "biom", description: "The abundance of each OTU in each sample"},
            # { name: contaminant_file, type: "contigs", description: "A sequence fasta file containing the reference sequence of known contaminant"},
          ],
        outputs:
          [
            { name: "filtered_sequences", file: "otuFilter_sequences.fasta", type: "fasta_file", description: "The sequences after filtering"},
            { name: "abundance_file", file: "otuFilter_abundance.biom", type: "biom", description: "The abundance after filtering"},
            { name: "excluded_file", file: "excluded.tsv", type: "tsv", description: "The list of the OTUs deleted by filters"},
            { name: "summary", file: "filtOTU_report.html", type: "html", description: "The filters and the number of removed sequences"}
          ],
        options:
          [
            {
              name: FROGS_filter_OTU_threads,
              prefix: -p,
              type: numeric,
              value: 4,
              min: 1,
              max: NA,
              step: 1,
              label: "Number of threads to use",
            },
            {
              name: FROGS_filter_OTU_min_sample_presence,
              prefix: ,
              type: numeric,
              value: 0,
              min: 2,
              max: NA,
              step: NA,
              label: "Keep OTU if it is present in at least this number of samples. Set 0 to not apply this filter",
            },
            {
              name: FROGS_filter_OTU_min_abundance_proportion,
              prefix: ,
              type: numeric,
              value: 0.00005,
              min: 0,
              max: 1,
              step: NA,
              label: "Minimum proportion of sequences abundancy to keep OTU.",
            },
            {
              name: FROGS_filter_OTU_nb_biggest_otu,
              prefix: ,
              type: numeric,
              value: 0,
              min: 0,
              max: NA,
              step: NA,
              label: "Keep the N biggest OTU. Set 0 to not apply this filter",
            },
            {
              name: FROGS_filter_OTU_contaminantSource,
              prefix: "",
              type:  "radio",
              choices:
                [
                  Don't apply this treatment: "no",
                  Input a contaminant file: "history",
                ],
              value: "no",
              label: "Chose where to find the list of contaminant OTU, either from your own fasta file, or from a database",
            },
            {
              name: FROGS_filter_OTU_contaminantFile,
              condition: "FROGS_filter_OTU_contaminantSource == 'history'",
              type: input_file,
              value: "",
              label: "Path to your contaminant OTU fasta file and associate blast database (nsq, nin, nhr). Keep empty to use a contaminant database from FROGS (phiX or Araport11).",
            },
            {
              name: FROGS_filter_OTU_contaminants_db,
              condition: "FROGS_filter_OTU_contaminantSource == 'history'",
              type: select,
              choices: [
                Araport11: "/Download/FROGS_contaminant/Araport11",
                PhiX: "/Download/FROGS_contaminant/PhiX",
              ],
              value: "/Download/FROGS_contaminant/Araport11",
              label: "Contaminant database from FROGS https://web-genobioinfo.toulouse.inrae.fr/frogs_databanks/contaminants/"
            },
          ],
      },
    ],
  install:
    {
      # perl: [
      #   "sudo apt-get install python3 perl" # deja installé
      # ],
      # frogs: [
      #   "cd /opt/biotools",
      #   "wget https://github.com/geraldinepascal/FROGS/archive/v4.1.0.tar.gz",
      #   "tar -zxvf v4.1.0.tar.gz",
      #   "cd FROGS-4.1.0",
      #   "mkdir bin"
      # ],
      # scipy: [
      #   "apt-get install python3-scipy" # demande confirmation
      # ],
      # mbb_mqc_plugin:
      # [
      #   "cd /opt/biotools",
      #   "git clone https://gitlab.mbb.univ-montp2.fr/mmassaviol/mbb_mqc_plugin.git",
      #   "cd mbb_mqc_plugin",
      #   "python3 setup.py install"
      # ]
    },
  data: [
    {
      name: FROGS_contaminant,
      type: directory
    }
  ],
  citations:  {
    frogs: [
      "Maria Bernard, Olivier Rué, Mahendra Mariadassou and Géraldine Pascal; FROGS: a powerful tool to analyse the diversity of fungi with special management of internal transcribed spacers, Briefings in Bioinformatics 2021, 10.1093/bib/bbab318 "
    ]
  }
}
