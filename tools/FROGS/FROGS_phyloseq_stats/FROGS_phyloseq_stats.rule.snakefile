rule <step_name>__FROGS_phyloseq_stats:
        input:
            **<step_name>__FROGS_phyloseq_stats_inputs(),
        output:
           alpha_div = config["results_dir"]+'/'+config["<step_name>__FROGS_phyloseq_stats_output_dir"]+'/phyloseq_alpha_diversity.tsv',
           wunifrac = config["results_dir"]+'/'+config["<step_name>__FROGS_phyloseq_stats_output_dir"]+'/wunifrac.tsv',
        log: config["results_dir"]+'/logs/' + config["<step_name>__FROGS_phyloseq_stats_output_dir"] + '/FROGS_phyloseq_stats_log.txt',
        params:
            command = config["<step_name>__FROGS_phyloseq_stats_command"],
            outDir  = config["results_dir"]+'/'+config["<step_name>__FROGS_phyloseq_stats_output_dir"],
            varExp  = config["<step_name>__FROGS_phyloseq_stats_varExp"],
            stepDir = config["<step_name>__FROGS_phyloseq_stats_output_dir"],
        conda: 
            "envs/frogs.yaml"
        shell: 
            """
            cd {params.outDir};
            export HOME="/root"
            tree.py -i {input.otu_sequence_file} -b {input.biom_file};
            echo \"1\t./{params.stepDir}/tree.html\" > {params.outDir}/text.generic.tsv;
            phyloseq_import_data.py -b {input.biom_file} -s {input.popmap_file} -t tree.nwk --rdata phyloseq_data.Rdata ;
            echo \"2\t./{params.stepDir}/phyloseq_import_summary.nb.html\" >> {params.outDir}/text.generic.tsv;
            phyloseq_composition.py  -r phyloseq_data.Rdata -r1 Kingdom -s1 Bacteria -r2 Phylum -n 20 -v {params.varExp} ;
            echo \"3\t./{params.stepDir}/phyloseq_composition.nb.html\" >> {params.outDir}/text.generic.tsv;
            phyloseq_alpha_diversity.py  -r phyloseq_data.Rdata -m Observed Chao1 Shannon  InvSimpson -v {params.varExp};
            echo \"4\t./{params.stepDir}/phyloseq_alpha_diversity.nb.html\" >> {params.outDir}/text.generic.tsv;  
            phyloseq_beta_diversity.py  -r phyloseq_data.Rdata -v {params.varExp} -m bray,cc,unifrac,wunifrac,jaccard --matrix-outdir . ;
            echo \"5\t./{params.stepDir}/phyloseq_beta_diversity.nb.html\" >> {params.outDir}/text.generic.tsv;
            phyloseq_structure.py  -r phyloseq_data.Rdata -v {params.varExp}  -d jaccard.tsv;
            echo \"6\t./{params.stepDir}/phyloseq_structure.nb.html\" >> {params.outDir}/text.generic.tsv;
            phyloseq_clustering.py  -r phyloseq_data.Rdata -v {params.varExp}  -d jaccard.tsv;
            echo \"7\t./{params.stepDir}/phyloseq_clustering.nb.html\" >> {params.outDir}/text.generic.tsv;
            phyloseq_manova.py  -r phyloseq_data.Rdata -v {params.varExp}  -m jaccard.tsv;
            echo \"8\t./{params.stepDir}/phyloseq_manova.nb.html\" >> {params.outDir}/text.generic.tsv;
            """