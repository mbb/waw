rule <step_name>__FROGS_tsv_to_biom:
    input:
        **<step_name>__FROGS_tsv_to_biom_inputs(),
    output:
        convert = config["results_dir"]+'/'+config["<step_name>__FROGS_tsv_to_biom_output_dir"]+'/tsvfile_convert.biom'      
    log: 
        config["results_dir"]+'/logs/' + config["<step_name>__FROGS_tsv_to_biom_output_dir"] + '/FROGS_tsv_to_biom_log.txt',
    params:
        command = config["<step_name>__FROGS_tsv_to_biom_command"],
        out_dir = config["results_dir"]+'/'+config["<step_name>__FROGS_tsv_to_biom_output_dir"]
    conda: 
        "envs/frogs.yaml"
    shell: 
        """
        mkdir -p {params.out_dir}
        ### Define input
        lfile_i=({input});
        len=${{#lfile_i[@]}};

        ### Verify if tsv is from FROGS or is a simple abundance table
        is_frogs=$(head -n 1 {input.tsvfile} | grep -o "observation_name" || true)
        if [ ! -z $is_frogs ]; then
            echo "TSV file is from frogs"
            input_tsv="{input.tsvfile}"
        else
            echo "TSV file is not from frogs, modification of the input file to be compatible"
            awk -F "\\t" '{{
                if(NR==1){{
                    bheader="observation_name\\tobservation_sum"
                    out = ""
                    for (i = 2; i <= NF; i++){{
                        if($i ~ /^[0-9]+$/){{
                            print "ERROR : Do you have an header with sample name in you tsv file ? \\nThe line with a problem is line "NR" because not consider as header (one column between the 2 to the end is numeric): "$0
                        }}else{{
                            out = out"\\t"$i
                        }}
                    }}
                    print bheader out > "{params.out_dir}/Abundance_Table_FROGS.tsv"
                }}else{{
                    sum=0
                    out = ""
                    for (i = 2; i <= NF; i++){{
                        if($i ~ /^[0-9]+$/){{
                            out = out"\\t"$i
                            sum+=$i
                        }}else{{
                            print "ERROR : Does your data has format ClusterName\\tabundance_integer_sample_1\\tabundance_integer_sample_2...\\tabundance_integer_sample_n ? \\nThe line with a problem is line "NR" : "$0
                        }}
                    }}
                    print $1"\\t"sum out >"{params.out_dir}/Abundance_Table_FROGS.tsv"
                }}
            }}' {input.tsvfile} > {log}
            input_tsv="{params.out_dir}/Abundance_Table_FROGS.tsv"
            problem=$(grep -o "ERROR" {log} | uniq || true)
            if [ ! -z $problem ];then
                error=$(grep "ERROR" {log})
                echo $error
                exit 1
            fi
        fi

        ### Run tsv to biom
        if [ $len -ge 2 ]; then
            {params.command} -t $input_tsv -m ${{lfile_i[2]}} -b {output.convert} -l {log}
        else
            {params.command} -t $input_tsv -b {output.convert} -l {log}
        fi
        """