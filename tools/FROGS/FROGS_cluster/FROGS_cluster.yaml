{
  id: FROGS_cluster,
  name: FROGS cluster,
  article: 10.1093/bib/bbab318,
  website: "http://frogs.toulouse.inrae.fr/",
  git: "https://github.com/geraldinepascal/FROGS",
  description: "Single-linkage clustering on sequences",
  version: "4.1.0",
  documentation: "http://frogs.toulouse.inrae.fr/html/commands_utax.html",
  multiqc: "generic_iframe",
  commands:
    [
      {
        name: FROGS_cluster,
        command: "clustering.py",
        category: "FROGS",
        output_dir: FROGS_cluster,
        inputs: 
          [
            { name: sequence_file, type: "fasta_file", description: "the sequences in fasta format"},
            { name: count_file, type: "tsv", description: "the count by sample for each sequence"}
          ],
        outputs:
          [
            { name: "cluster_seeds", file: "seed_sequences.fasta", type: "fasta_file", description: "The clusters representative sequences"},
            { name: "abundance_file", file: "clustering_abundance.biom", type: "biom", description: "The abundance of each cluster in each sample"},
            { name: "cluster_composition", file: "swarms_composition.tsv", type: "tsv", description: "A text file representing the read composition of each cluster"}
          ],
        options:
          [
            {
              name: FROGS_cluster_threads,
              prefix: "",
              type: numeric,
              value: 4,
              min: 1,
              max: NA,
              step: 1,
              label: "Number of threads to use",
            },
            {
              name: FROGS_cluster_guidelines_version,
              prefix: "",
              type:  "radio",
              choices:
                [
                  No guidelines choose your distance and fastidious and denoising params: FALSE,
                  FROGS 3.2 force d=1 and --fastidious option : 3.2,
                  FROGS 3.1 perform a denoising (clustering with d=1) followed with d=3: 3.1,
                ],
              value: 3.2,
              label: "FROGS guidelines for clustering",
            },
            {
              name: FROGS_cluster_denoise,
              prefix: "",
              type: "checkbox",
              condition: "FROGS_cluster_guidelines_version == 'FALSE'",
              value: False,
              label: "If check, clustering will be perform in two steps, first with distance = 1 and then with an aggregation d value given in distance parameter (sam as method in guideline 3.1 but with your value)"
            },
            {
              name: FROGS_cluster_distance,
              prefix: "",
              type: numeric,
              condition: "FROGS_cluster_guidelines_version == 'FALSE'",
              value: 1,
              min: 1,
              max: 15,
              step: 1,
              label: "Aggregation distance clustering: Maximum number of differences between sequences in each aggregation Swarm step. If cluster denoise is check a first round of clustering with d=1 is done then another round is done with the value set here, if cluster denoise uncheck only one round is done with the value set here.",
            },
            {
              name: FROGS_cluster_fastidious,
              prefix: "",
              type: "checkbox",
              condition: "FROGS_cluster_guidelines_version == 'FALSE'",
              value: False,
              label: "Clustering will be performed with the Swarm --fastidious option, this is slower but more accurate. This parameter will be usable only in association with a distance of 1. If check same as guideline 3.2",
            },
          ],
      },
    ],
  install:
    {
      # perl: [
      #   "sudo apt-get install python3 perl" # deja installé
      # ],
      # frogs: [
      #   "cd /opt/biotools",
      #   "wget https://github.com/geraldinepascal/FROGS/archive/v4.1.0.tar.gz",
      #   "tar -zxvf v4.1.0.tar.gz",
      #   "cd FROGS-4.1.0",
      #   "mkdir bin"
      # ],
      # scipy: [
      #   "apt-get install python3-scipy" # demande confirmation
      # ],
      # swarm 3.0.O: [
      #   "cd /opt/biotools/",
      #   "wget https://github.com/torognes/swarm/archive/v3.0.0.tar.gz",
      #   "tar -vxzf v3.0.0.tar.gz",
      #   "cd swarm-3.0.0/src",
      #   "make",
      #   "cd ..",
      # ],
      # frog_swarm: [
      # " ln -s /opt/biotools/swarm-3.0.0/bin/swarm /opt/biotools/FROGS-4.1.0/libexec/."
      # ],
      # mbb_mqc_plugin:
      # [
      #   "cd /opt/biotools",
      #   "git clone https://gitlab.mbb.univ-montp2.fr/mmassaviol/mbb_mqc_plugin.git",
      #   "cd mbb_mqc_plugin",
      #   "python3 setup.py install"
      # ],
    },
  citations:  {
    frogs: [
      " Maria Bernard, Olivier Rué, Mahendra Mariadassou and Géraldine Pascal; FROGS: a powerful tool to analyse the diversity of fungi with special management of internal transcribed spacers, Briefings in Bioinformatics 2021, 10.1093/bib/bbab318 "
    ]
  }
}
