rule <step_name>__FROGS_cluster:
    input:
        **<step_name>__FROGS_cluster_inputs(),
    output:
        abundance_file = config["results_dir"]+'/'+config["<step_name>__FROGS_cluster_output_dir"]+'/clustering_abundance.biom',
        cluster_seeds = config["results_dir"]+'/'+config["<step_name>__FROGS_cluster_output_dir"]+'/seed_sequences.fasta',
        #cluster_stats = config["results_dir"]+'/'+config["<step_name>__FROGS_cluster_output_dir"]+'/clusters_stat.html',
        cluster_composition = config["results_dir"]+'/'+config["<step_name>__FROGS_cluster_output_dir"]+'/swarms_composition.tsv',
    log: config["results_dir"]+'/logs/' + config["<step_name>__FROGS_cluster_output_dir"] + '/FROGS_cluster_log.txt',
    params:
        command = config["<step_name>__FROGS_cluster_command"],
        cmdStat = "cluster_stats.py",
        outDir = config["results_dir"]+'/'+config["<step_name>__FROGS_cluster_output_dir"],
        stepDir = config["<step_name>__FROGS_cluster_output_dir"],
        guidelines_version = config["<step_name>__FROGS_cluster_guidelines_version"],
        distance = config["<step_name>__FROGS_cluster_distance"],
        fastidious = " --fastidious " if config["<step_name>__FROGS_cluster_fastidious"] else "",
        denoising = " --denoising " if config["<step_name>__FROGS_cluster_denoise"] else "",
        cluster_guideline = "--distance 1 --fastidious " if config["<step_name>__FROGS_cluster_guidelines_version"]==3.2 else "--distance 3 --denoising " if config["<step_name>__FROGS_cluster_guidelines_version"]==3.1 else "",
    threads: 
        config["<step_name>__FROGS_cluster_threads"],
    conda: 
        "envs/frogs.yaml"
    shell: 
        """
        swarmParams="{params.cluster_guideline}"
        if [ "$swarmParams" == "" ]; then
            swarmParams="--distance {params.distance}"
            if [ {params.distance} == 1 ];  then
                swarmParams=$swarmParams"{params.fastidious}"
            else 
                swarmParams=$swarmParams"{params.denoising}" 
            fi 
        fi

        sed -i '1 s/^/#/' {input.count_file}
        {params.command} \
        --input-fasta {input.sequence_file} \
        --input-count {input.count_file} \
        --output-biom {output.abundance_file} \
        --output-fasta {output.cluster_seeds} \
        --output-compo {output.cluster_composition} \
        --nb-cpus {threads} \
        $swarmParams;

        {params.cmdStat} -i {output.abundance_file} -o {params.outDir}/clusters_stat.html;
        echo \"1\t./{params.stepDir}/clusters_stat.html\" > {params.outDir}/text.generic.tsv
        """