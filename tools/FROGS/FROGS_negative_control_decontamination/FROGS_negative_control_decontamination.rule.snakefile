rule <step_name>__FROGS_negative_control_decontamination:
    input:
        **<step_name>__FROGS_negative_control_decontamination_inputs(),
    output:
        abundance_table_filter = config["results_dir"]+'/'+config["<step_name>__FROGS_negative_control_decontamination_output_dir"]+'/Abundance_table_filtered.biom',
        curated_sequence = config["results_dir"]+'/'+config["<step_name>__FROGS_negative_control_decontamination_output_dir"]+ "/curated_seq.fasta",
    log: 
        config["results_dir"]+'/logs/' + config["<step_name>__FROGS_negative_control_decontamination_output_dir"] + '/decontam.txt',
    params:
        output_dir = config["results_dir"]+'/'+config["<step_name>__FROGS_negative_control_decontamination_output_dir"],
        column = "-c " + config["<step_name>__FROGS_negative_control_decontamination_column"] if config["<step_name>__FROGS_negative_control_decontamination_column"] !="" else "",
        name = "-n " + config["<step_name>__FROGS_negative_control_decontamination_name"] if config["<step_name>__FROGS_negative_control_decontamination_name"] != "" else "",
        method = config["<step_name>__FROGS_negative_control_decontamination_method"],
    conda: 
        "envs/frogs.yaml",
    shell: 
        """
        # Conversion biom to tsv
        biom_to_tsv.py -b {input.biom} -t {params.output_dir}/Abundance_table.tsv -m {params.output_dir}/multihits.tsv |& tee {log}

        # Decontamination
        python {snake_dir}/scripts/FROGS_negative_control_decontamination.py {params.column} {params.name} -m {params.method} {params.output_dir}/Abundance_table.tsv {input.sequence_file} {input.metadata_table} {params.output_dir}/Abundance_table_filtered.tsv

        # Extract curated fasta cluster
        awk -F "\\t" 'fname != FILENAME {{fname = FILENAME; idx++}} idx == 1 {{if(NR!=1){{seq2keep[$1]=1}}}} idx == 2 {{if($0~/^>/){{match($0, /^>([^; ]+)/, q);toprint=0;if(q[1] in seq2keep){{print ">"q[1];toprint=1}}}}else{{if(toprint==1){{print}}}}}}' {params.output_dir}/OTU_table_decontam.tsv {input.sequence_file} > {output.curated_sequence}

        # Conversion tsv to biom
        tsv_to_biom.py -t {params.output_dir}/Abundance_table_filtered.tsv -m {params.output_dir}/multihits.tsv -b {output.abundance_table_filter} |& tee {log}
        """