import pandas as pd
import sys
import argparse as ap
import re
import os

### Command line arguments : Define parameter

# header and positional arguments
PARSER = ap.ArgumentParser(description=__doc__,
                           formatter_class=ap.ArgumentDefaultsHelpFormatter)


## Required argument
# Path and file of the Abundance Table file.
INPUT_HELP = """
 Set TSV file that contains abundance information foreach cluster and sample (Cluster_Table.tsv -> OTU,ASV,mOTU...)"""

PARSER.add_argument('abundance_table', help=INPUT_HELP)


# Path and file of the fasta file that contain cluster name in header (in fasta format).
FASTA_HELP = """
 Set fasta file that contains Cluster name in header and cluster sequence"""

PARSER.add_argument('fasta_file', help=FASTA_HELP)


# Path and file of the metadata file (in tsv format).
METADATA_HELP = """
Set the TSV file that contains metadata of each sample. This file must contain in first column the sample name and another column with the information of Negative Control and Sample"""

PARSER.add_argument('metadata_table', help=METADATA_HELP)


# Path and file to tsv output
OUTPUT_HELP = """
 Set the filtered TSV file create as output that contains abundance information foreach cluster and sample filtered"""

PARSER.add_argument('output_table', help=INPUT_HELP)


## Optional argument 

# Column abundance

COLUMN_HELP = """
Set the 'column name' (in header) where information about the type of sample is (If sample is Negative Control or Positive Control or a Sample). By default will take the first column after Sample Name (column 2) """

PARSER.add_argument('-c', '--column',
                    help=COLUMN_HELP, type=str)


TNEG_HELP = """
Set the name to recognize Negative control in the 'column name' set by -c. Default is NC"""

PARSER.add_argument('-n', '--name',
                    help=COLUMN_HELP, type=str, default="NC")


METHOD_HELP = """
Set the decontamination method based on negative control to filter the abundance table. Choices are 'remove' to remove all Clusters that are in Negative Control, or 'subtract' to subtract by cluster the abundance of the negative control in each sample. """

PARSER.add_argument('-m', '--method',
                    help=METHOD_HELP, choices=["remove","subtract"], default="subtract")





### set options

ARGS = PARSER.parse_args()

###############################


### Read metadata file to extract in a list samples that are Negative Control and in an other list the orther sample.


# Define header of fasta file

headers= []

pattern = r">([^ ;]+)"

with open(ARGS.fasta_file,"r") as FASTA_file:
    for line in FASTA_file:
        line = line.strip()
        if line.startswith(">"):
            match = re.search(pattern, line)
            headers.append(match.group(1))

# Define list
TNEG_tosearch=[]
SAMPLES_tosearch=[]

## Define columns to read

# define sample and tneg columnd
with open(ARGS.metadata_table,"r") as METADATA_file:
    nline=0
    for line in METADATA_file:
        line = line.rstrip()
        nline+=1
        tdecoupe1=line.split("\t")
        if nline==1:
            # Define column index of the column set in parameter.
            if ARGS.column==None:
                col=1
            else:
                for i,colname in enumerate(tdecoupe1):
                    if colname==ARGS.column:
                        col=int(i)
            if not 'col' in globals():    # If col is not int that means no column found with name set in parameter
                sys.exit("\nERROR : Not found '"+str(ARGS.column)+"' in the header of '"+str(ARGS.metadata_table)+"'. The header is '"+str(line)+"'\n")
        else:
            if tdecoupe1[col]==ARGS.name:
                TNEG_tosearch.append(tdecoupe1[0])
            else:
                SAMPLES_tosearch.append(tdecoupe1[0])

TNEG=[]
SAMPLES=[]

if len(TNEG)==0: # Test if Tneg in abundance table
    if str(ARGS.column)=="None":
        ARGS.column="2nd"
    sys.exit("\nERROR : No sample found as Negative Control in your metadata file, so no filter will be done. Are you sure that control negative can be recognize by '"+str(ARGS.name)+"' in '"+str(ARGS.column)+"' column in metadata '"+str(ARGS.metadata_table)+"' file\n")
else:
    df = pd.read_table(ARGS.abundance_table)
    for colname in df.columns:
        for tneg in TNEG_tosearch:
            if colname.startswith(tneg):
                TNEG.append(colname)
        for samples in SAMPLES_tosearch:
            if colname.startswith(samples):
                SAMPLES.append(colname)
    if ARGS.method=="subtract": # Subtract Tneg value in sample set 0 if negative value as result.
        df[SAMPLES] = df[SAMPLES].apply(lambda x: (x - df[TNEG].sum(axis=1)).clip(lower=0))
    elif ARGS.method=="remove": # Remove line when Tneg is positive
        df.drop(df[df[TNEG].gt(0).any(axis=1)].index, inplace=True)

# Remove Negative Control column in the filtered abundance table

#df.drop(labels=TNEG, axis=1, inplace=True)

# Recalculate observation_sum

ALL_S=SAMPLES + TNEG
if 'observation_sum' in df.columns:
    df['observation_sum'] = df[ALL_S].sum(axis=1)
    df = df[df['observation_sum'] > 0]
else:
    df = df[df[SAMPLES].sum(axis=1) > 0 ]


# Define cluster name column based on fasta file header

sample=[]
otu=[]
for col in df.columns:
    if col in ALL_S:
        sample.append(col)
    if df[col].isin(headers).any():
        otu.append(col)

if len(otu)==1:
    otu.extend(sample)
    otu_table=df[otu]
    otu_table.set_index(otu[0], inplace=True)
else:
    sys.exit("\nERROR : Two columns contain Cluster name that are in fasta file header '"+str(otu)+"'\n")


dirname = os.path.dirname(ARGS.output_table)

if dirname == "":
    otu_file ="OTU_table_decontam.tsv"
else:
    otu_file = dirname+"/OTU_table_decontam.tsv"

otu_table.to_csv(otu_file, sep='\t', index=True, header=True)

df.to_csv(ARGS.output_table, sep='\t', index=False, header=True)



