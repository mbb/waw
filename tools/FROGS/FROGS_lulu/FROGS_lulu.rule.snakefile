rule <step_name>__FROGS_lulu:
    input:
        **<step_name>__FROGS_lulu_inputs(),
    output: 
        curated_file = config["results_dir"] + "/" + config["<step_name>__FROGS_lulu_output_dir"] + "/curated_table.biom",
        curated_sequence = config["results_dir"] + "/" + config["<step_name>__FROGS_lulu_output_dir"] + "/curated_seq.fasta",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__FROGS_lulu_output_dir"]+ "/",
        method = config["<step_name>__FROGS_lulu_matcher_method"],
        minimum_ratio = config["<step_name>__FROGS_lulu_minimum_ratio"],
        minimum_match = config["<step_name>__FROGS_lulu_minimum_match"],
        script = config["<step_name>__FROGS_lulu_script"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__FROGS_lulu_output_dir"] + "/lulu_log.txt"
    conda:
        "envs/frogs.yaml"
    shell:
        """
        # Conversion biom to tsv
        biom_to_tsv.py -b {input.abundance_file} -t {params.output_dir}/abundance_table.tsv -m {params.output_dir}/multihits.tsv |& tee {log}

        # Define sample name in biom file
        biom2tsv=$(echo $CONDA_PREFIX"/libexec/biom2tsv.py")
        python $biom2tsv --input-file {input.abundance_file} --output-file {params.output_dir}/sample.txt --fields '@sample_count'
        head -n 1 {params.output_dir}/sample.txt | sed 's/^#//' | awk -F "\\t" 'BEGIN{{print "\\tPresent"}}{{for(i=1;i<=NF;i++){{print $i"\\t1"}}}}' > {params.output_dir}/List_sample_in_biom.txt
        rm {params.output_dir}/sample.txt

        # Auto alinment to determine parents and child 
        if [ "{params.method}" ==  "blast" ]; then
            makeblastdb -in {input.sequence_file} -parse_seqids -dbtype nucl;
            blastn -db {input.sequence_file} -outfmt '6 qseqid sseqid pident' -out {params.output_dir}/match_list.txt -qcov_hsp_perc 80 -perc_identity 84 -query {input.sequence_file} ;
        else
            vsearch --usearch_global {input.sequence_file} --db {input.sequence_file} --self --id .84 --iddef 1 --userout {params.output_dir}/match_list.txt -userfields query+target+id --maxaccepts 0 --query_cov .9 --maxhits 10
        fi

        # Run lulu
        Rscript {snake_dir}/{params.script} {params.output_dir}/abundance_table.tsv {params.output_dir}/match_list.txt {params.minimum_ratio} {params.minimum_match} {params.output_dir}/curated_table.tsv {params.output_dir}/otu_map.tsv {params.output_dir}/List_sample_in_biom.txt {input.sequence_file} |& tee {log}

        # Extract curated fasta cluster
        awk -F "\\t" 'fname != FILENAME {{fname = FILENAME; idx++}} idx == 1 {{if(NR!=1){{seq2keep[$1]=1}}}} idx == 2 {{if($0~/^>/){{match($0, /^>([^; ]+)/, q);toprint=0;if(q[1] in seq2keep){{print ">"q[1];toprint=1}}}}else{{if(toprint==1){{print}}}}}}' {params.output_dir}/OTU_Table_lulu.tsv {input.sequence_file} > {output.curated_sequence}

        # Conversion tsv to biom
        if [ -f "{params.output_dir}/multihits.tsv" ];then
            tsv_to_biom.py -t {params.output_dir}/curated_table.tsv -m {params.output_dir}/multihits.tsv -b {output.curated_file} |& tee {log}
        else
            tsv_to_biom.py -t {params.output_dir}/curated_table.tsv  -b {output.curated_file} |& tee {log}
        fi
        """
