if config["SeOrPe"] == "SE":

    rule <step_name>__FROGS_preprocess_SE:
        input:
            **<step_name>__FROGS_preprocess_SE_inputs(),
        output:
            sequence_file = config["results_dir"]+'/'+config["<step_name>__FROGS_preprocess_SE_output_dir"]+'/dereplicated.fasta',
            count_file = config["results_dir"]+'/'+config["<step_name>__FROGS_preprocess_SE_output_dir"]+'/count.tsv',
            summary = config["results_dir"]+'/'+config["<step_name>__FROGS_preprocess_SE_output_dir"]+'/report.html',
        log: config["results_dir"]+'/logs/' + config["<step_name>__FROGS_preprocess_SE_output_dir"] + '/FROGS_preprocess_SE_log.txt',
        params:
            command = config["<step_name>__FROGS_preprocess_SE_command"],
            outDir = config["results_dir"]+'/'+config["<step_name>__FROGS_preprocess_SE_output_dir"],
            min_amplicon_size = config["<step_name>__FROGS_preprocess_min_amplicon_size"],
            max_amplicon_size = config["<step_name>__FROGS_preprocess_max_amplicon_size"],
            sequencing_protocol = config["<step_name>__FROGS_preprocess_sequencing_protocol"],
            five_prim_primer = "--five-prim-primer "+config["<step_name>__FROGS_preprocess_five_prim_primer"] if config["<step_name>__FROGS_preprocess_sequencing_protocol"]=="standard" else "",
            three_prim_primer = "--three-prim-primer "+config["<step_name>__FROGS_preprocess_three_prim_primer"] if config["<step_name>__FROGS_preprocess_sequencing_protocol"]=="standard" else "--without-primers",
        threads: 
            config["<step_name>__FROGS_preprocess_threads"],
            # reads=`ls {input.read_dir}/*.{{fastq,fq}}{{,.gz}} 2>/dev/null`;
        conda: 
            "envs/frogs.yaml"
        shell: 
            """
            {params.command}  \
            illumina \
            --output-dereplicated {output.sequence_file}  \
            --output-count {output.count_file}  \
            --summary {output.summary}  \
            --nb-cpus {threads}  \
            --min-amplicon-size {params.min_amplicon_size}  \
            --max-amplicon-size {params.max_amplicon_size} \
            {params.five_prim_primer}  \
            {params.three_prim_primer} \
            --already-contiged \
            --input-R1 $(ls -1  {input.read_dir}/*.fastq{{,.gz}} 2>/dev/null )
            """
# ATTENTION : PEUT ETRE BESOIN DU PATH COMPLET DES FICHIER D'INPUT !!


# ls /Data/MiSeq_SOP/ | grep -E 'R1.*fastq.gz|R1.*fq.gz|R1.*fastq|R1.*fq' | tr '\n' ' '

# /opt/biotools/FROGS-4.1.0/app/preprocess.py illumina --min-amplicon-size 10 --max-amplicon-size 10000 --without-primers  --input-R1 $(ls /Data/MiSeq_SOP/ | grep -E 'R1.*fastq.gz|R1.*fq.gz|R1.*fastq|R1.*fq' | tr '\n' ' ') --input-R2 $(ls /Data/MiSeq_SOP/ | grep -E 'R2.*fastq.gz|R2.*fq.gz|R2.*fastq|R2.*fq' | tr '\n' ' ') --merge-software vsearch --R1-size 12 --R2-size 12

if config["SeOrPe"] == "PE":

    rule <step_name>__FROGS_preprocess_PE:
        input:
            **<step_name>__FROGS_preprocess_PE_inputs(),
        output:
            sequence_file = config["results_dir"]+'/'+config["<step_name>__FROGS_preprocess_PE_output_dir"]+'/dereplicated.fasta',
            count_file = config["results_dir"]+'/'+config["<step_name>__FROGS_preprocess_PE_output_dir"]+'/count.tsv',
            summary = config["results_dir"]+'/'+config["<step_name>__FROGS_preprocess_PE_output_dir"]+'/report.html',
        log: config["results_dir"]+'/logs/' + config["<step_name>__FROGS_preprocess_PE_output_dir"] + '/FROGS_preprocess_PE_log.txt',
        params:
            command = config["<step_name>__FROGS_preprocess_PE_command"],
            outDir = config["results_dir"]+'/'+config["<step_name>__FROGS_preprocess_PE_output_dir"],
            min_amplicon_size = config["<step_name>__FROGS_preprocess_min_amplicon_size"],
            max_amplicon_size = config["<step_name>__FROGS_preprocess_max_amplicon_size"],
            R1_size = config["<step_name>__FROGS_preprocess_PE_R1_size"],
            R2_size = config["<step_name>__FROGS_preprocess_PE_R2_size"],
            sequencing_protocol = config["<step_name>__FROGS_preprocess_sequencing_protocol"],
            five_prim_primer = "--five-prim-primer"+config["<step_name>__FROGS_preprocess_five_prim_primer"] if config["<step_name>__FROGS_preprocess_sequencing_protocol"]=="standard" else "",
            three_prim_primer = "--three-prim-primer"+config["<step_name>__FROGS_preprocess_three_prim_primer"] if config["<step_name>__FROGS_preprocess_sequencing_protocol"]=="standard" else "--without-primers",
        threads: 
            config["<step_name>__FROGS_preprocess_threads"],
            # reads=`ls {input.read_dir}/*.{{fastq,fq}}{{,.gz}} 2>/dev/null`;
        conda: 
            "envs/frogs.yaml"
        shell: 
            """
            {params.command}  \
            illumina \
            --output-dereplicated {output.sequence_file}  \
            --output-count {output.count_file}  \
            --summary {output.summary}  \
            --nb-cpus {threads}  \
            --min-amplicon-size {params.min_amplicon_size}  \
            --max-amplicon-size {params.max_amplicon_size} \
            {params.five_prim_primer}  \
            {params.three_prim_primer} \
            --merge-software vsearch \
            --input-R1 $(ls {input.read_dir} | grep -E 'R1.*fastq.gz|R1.*fq.gz|R1.*fastq|R1.*fq' | sed -e 's|^|{input.read_dir}/|' | tr '\\n' ' ') \
            --input-R2 $(ls {input.read_dir} | grep -E 'R2.*fastq.gz|R2.*fq.gz|R2.*fastq|R2.*fq' | sed -e 's|^|{input.read_dir}/|' | tr '\\n' ' ') \
            --R1-size {params.R1_size} \
            --R2-size {params.R2_size}
            """
