rule <step_name>__FROGS_affiliation_customDB:
    input:
        **<step_name>__FROGS_affiliation_customDB_inputs(),
    output:
        abundance_file = config["results_dir"]+'/'+config["<step_name>__FROGS_affiliation_customDB_output_dir"]+'/tax_affiliation.biom',
        summary = config["results_dir"]+'/'+config["<step_name>__FROGS_affiliation_customDB_output_dir"]+'/affi_report.html',
    log: config["results_dir"]+'/logs/' + config["<step_name>__FROGS_affiliation_customDB_output_dir"] + '/FROGS_affiliation_log.txt',
    params:
        command = config["<step_name>__FROGS_affiliation_customDB_command"],
        cmdStat = "affiliation_stats.py",
        outDir = config["results_dir"]+'/'+config["<step_name>__FROGS_affiliation_customDB_output_dir"],
        stepDir = config["<step_name>__FROGS_affiliation_customDB_output_dir"],
        taxfile = config["results_dir"] + '/' + config["<step_name>__FROGS_affiliation_customDB_output_dir"]+"/Database/train_set.fasta",
        rdp = " --rdp " if config["<step_name>__FROGS_affiliation_customDB_rdp"] else "",
        memory = config["<step_name>__FROGS_affiliation_customDB_memory"],
    threads:
        config["<step_name>__FROGS_affiliation_customDB_threads"],
    conda: 
        "envs/frogs.yaml"
    shell: 
        """
        mkdir -p {params.outDir}/
        dordp=$(echo "{params.rdp}" |sed 's/-//g'|sed 's/ //g')
        fastatordp=$(echo $CONDA_PREFIX"/libexec")
        pythonfrogs=$(echo $CONDA_PREFIX"/lib")
        export PYTHONPATH=$pythonfrogs
        export PATH=$fastatordp:$PATH
        n=$(awk -F "\\t" '{{print NF}}' {input.tax_db} | sort |uniq)
        problem=$(echo $n | wc -c)
        if [ $problem -ne "2" ];then
            echo "All lines should contain only two columns"
            echo "The taxonomy file mustn't contain header and format should be : 'SeqID\tKingdom;Phylum;Class;Order;Family;Genus;Species'. All taxonomies need to be define with the exact same number of rank separate by ';'. Set 'unclassified' for unknown rank"
            exit 1
        fi
        if [ $n -ne "2" ];then
            echo "All lines should contain only two columns"
            echo "The taxonomy file mustn't contain header and format should be : 'SeqID\tKingdom;Phylum;Class;Order;Family;Genus;Species'. All taxonomies need to be define with the exact same number of rank separate by ';'. Set 'unclassified' for unknown rank"
            exit 1
        fi
        n=$(awk -F "\\t" '{{split($2,tax,";");out = "R1"; for (i = 2; i <= length(tax); i++){{out =out" R"i}}; print out}}' {input.tax_db} | sort |uniq)
        problem=$(echo $n | grep -o "R1 "| wc -l)
        if [ $problem -ne "1" ];then
            echo "Not same number of rank between seqID in taxonomy file"
            "The taxonomy file mustn't contain header and format should be : 'SeqID\tKingdom;Phylum;Class;Order;Family;Genus;Species'. All taxonomies need to be define with the exact same number of rank separate by ';'. Set 'unclassified' for unknown rank"
            exit 1
        fi
        mkdir -p {params.outDir}/Database
        fasta2RDP.py -d {input.fasta_db} -t {input.tax_db} -r $n --rdp-taxonomy {params.outDir}/Database/train_set.tax --rdp-fasta {params.outDir}/Database/train_set.fasta
        makeblastdb -in {params.outDir}/Database/train_set.fasta -dbtype nucl
        if ! [ -z $dordp ] && [ $dordp = "rdp" ];then
            classifier -Xmx{params.memory}g train -o {params.outDir}/Database/ -s {params.outDir}/Database/train_set.fasta -t {params.outDir}/Database/train_set.tax
            echo -e "# Sample ResourceBundle properties file\nbergeyTree=bergeyTrainingTree.xml\n\nprobabilityList=genus_wordConditionalProbList.txt\n\nprobabilityIndex=wordConditionalProbIndexArr.txt\n\nwordPrior=logWordPrior.txt\n\nclassifierVersion=RDP Naive Bayesian rRNA Classifier Version 2.0.3, May 2012" > {params.outDir}/Database/train_set.fasta.properties
        fi
        taxonomic_affiliation.py \
        --reference {params.taxfile} \
        --input-fasta {input.fasta} \
        --input-biom {input.biom} \
        --output-biom {output.abundance_file} \
        --summary {output.summary} \
        --nb-cpus {threads} \
        {params.rdp} \
        --java-mem {params.memory};
        echo \"1\t./{params.stepDir}/affi_report.html\" > {params.outDir}/text.generic.tsv
        affiliation_stats.py -i {output.abundance_file}  --tax-consensus-tag 'blast_taxonomy' --multiple-tag 'blast_affiliations' -o {params.outDir}/affiliations_stat.html ;
        echo \"2\t./{params.stepDir}/affiliations_stat.html\" >> {params.outDir}/text.generic.tsv
        """