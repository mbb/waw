rule <step_name>__FROGS_affiliation:
    input:
        **<step_name>__FROGS_affiliation_inputs(),
    output:
        abundance_file = config["results_dir"]+'/'+config["<step_name>__FROGS_affiliation_output_dir"]+'/tax_affiliation.biom',
        summary = config["results_dir"]+'/'+config["<step_name>__FROGS_affiliation_output_dir"]+'/affi_report.html',
    log: config["results_dir"]+'/logs/' + config["<step_name>__FROGS_affiliation_output_dir"] + '/FROGS_affiliation_log.txt',
    params:
        command = config["<step_name>__FROGS_affiliation_command"],
        cmdStat = "affiliation_stats.py",
        outDir = config["results_dir"]+'/'+config["<step_name>__FROGS_affiliation_output_dir"],
        stepDir = config["<step_name>__FROGS_affiliation_output_dir"],
        taxonomypath = config["<step_name>__FROGS_affiliation_reference_fasta"],
        taxfile = config["<step_name>__FROGS_affiliation_reference_fasta"]+"/train_set.fasta",
        yamlfile = config["<step_name>__FROGS_affiliation_reference_fasta"]+"/info.yaml",
        # taxonomic_ranks = "-t Domain Phylum Class Order Family Genus Species",
        rdp = " --rdp " if config["<step_name>__FROGS_affiliation_rdp"] else "",
    threads: 
        config["<step_name>__FROGS_affiliation_threads"],
    conda: 
        "envs/frogs.yaml"
    shell: 
        """
        if [ ! -f {params.taxfile} ];
        then
            taxURL=$(grep train_set: {params.yamlfile} | sed 's/train_set://' | sed 's/"//g')
            cd {params.taxonomypath};
			echo Downloading database from:  $taxURL;
            wget $taxURL -O {params.taxfile}.tar.gz -q ;
            tar --strip-components=1 -xzf {params.taxfile}.tar.gz && rm -f {params.taxfile}.tar.gz;
            for i in `ls *.fasta*`; do newname=$(echo $i | sed 's/.*fasta/train_set.fasta/'); mv $i $newname; done
        fi
        {params.command} \
        --reference {params.taxfile} \
        --input-fasta {input.fasta} \
        --input-biom {input.biom} \
        --output-biom {output.abundance_file} \
        --summary {output.summary} \
        --nb-cpus {threads} \
        {params.rdp} \
        --java-mem 20;
        echo \"1\t./{params.stepDir}/affi_report.html\" > {params.outDir}/text.generic.tsv
        {params.cmdStat} -i {output.abundance_file}  --tax-consensus-tag 'blast_taxonomy' --multiple-tag 'blast_affiliations' -o {params.outDir}/affiliations_stat.html ;
        echo \"2\t./{params.stepDir}/affiliations_stat.html\" >> {params.outDir}/text.generic.tsv
        """
