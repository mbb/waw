rule <step_name>__FROGS_filter_affiliation:
    input:
        **<step_name>__FROGS_filter_affiliation_inputs(),
    output:
        biom_filtered = config["results_dir"]+'/'+config["<step_name>__FROGS_filter_affiliation_output_dir"]+'/affiFilter_abundance.biom',
        sequence_file = config["results_dir"]+'/'+config["<step_name>__FROGS_filter_affiliation_output_dir"]+'/affiFilter_sequences.fasta', 
        summary = config["results_dir"]+'/'+config["<step_name>__FROGS_filter_affiliation_output_dir"]+'/filtAff_report.html',
        impacted = config["results_dir"]+'/'+config["<step_name>__FROGS_filter_affiliation_output_dir"]+'/impacted.tsv',
        multihit = config["results_dir"]+'/'+config["<step_name>__FROGS_filter_affiliation_output_dir"]+'/impacted.multihit.tsv',
    log: config["results_dir"]+'/logs/' + config["<step_name>__FROGS_filter_affiliation_output_dir"] + '/FROGS_filter_affiliation_log.txt',
    params:
        command = config["<step_name>__FROGS_filter_affiliation_command"],
        outDir = config["results_dir"]+'/'+config["<step_name>__FROGS_filter_affiliation_output_dir"],
        stepDir = config["<step_name>__FROGS_filter_affiliation_output_dir"],
        mode = config["<step_name>__FROGS_filter_affiliation_mode"],
        out_mode = "--delete --output-fasta "+config["results_dir"]+'/'+config["<step_name>__FROGS_filter_affiliation_output_dir"]+'/affiFilter_sequences.fasta' if config["<step_name>__FROGS_filter_affiliation_mode"]=="delete" else "--mask",
        # taxonomic_ranks = "--taxonomic-ranks Domain Phylum Class Order Family Genus Species",
        min_rdp_node = "--min-rdp-bootstrap "+str(config["<step_name>__FROGS_filter_affiliation_rdp_node"]) if config["<step_name>__FROGS_filter_affiliation_rdp_node"]!="NoFilter" else "",
        min_rdp_bootstrap = ":" + str(config["<step_name>__FROGS_filter_affiliation_min_rdp_bootstrap"]) if config["<step_name>__FROGS_filter_affiliation_rdp_node"] !="NoFilter" else "",
        min_blast_length = "--min-blast-length "+str(config["<step_name>__FROGS_filter_affiliation_min_blast_length"]) if config["<step_name>__FROGS_filter_affiliation_min_blast_length"] else "",
        max_blast_evalue = "--max-blast-evalue "+str(config["<step_name>__FROGS_filter_affiliation_max_blast_evalue"]) if config["<step_name>__FROGS_filter_affiliation_max_blast_evalue"] else "",
        min_blast_identity = "--min-blast-identity "+str(config["<step_name>__FROGS_filter_affiliation_min_blast_identity"]) if config["<step_name>__FROGS_filter_affiliation_min_blast_identity"] else "",
        min_blast_coverage = "--min-blast-coverage "+str(config["<step_name>__FROGS_filter_affiliation_min_blast_coverage"]) if config["<step_name>__FROGS_filter_affiliation_min_blast_coverage"] else "",
    # threads: 
    #     config["<step_name>__FROGS_filter_affiliation_threads"],
    conda: 
        "envs/frogs.yaml"
    shell: 
        """
        mkdir -p {params.outDir};
        {params.command} \
        --input-biom {input.biom} \
        --input-fasta {input.fasta} \
        --output-biom {output.biom_filtered} \
        --output-fasta {output.sequence_file} \
        --impacted {output.impacted} \
        --impacted-multihit {output.multihit} \
        --summary {output.summary} \
        {params.out_mode} \
        {params.min_rdp_node}{params.min_rdp_bootstrap} \
        {params.min_blast_length} \
        {params.max_blast_evalue} \
        {params.min_blast_identity} \
        {params.min_blast_coverage}
        echo \"1\t./{params.stepDir}/filtAff_report.html\" > {params.outDir}/text.generic.tsv;
        if [ "{params.out_mode}" == "--mask" ]; then cp {input.fasta} {output.sequence_file}; fi;
        """
