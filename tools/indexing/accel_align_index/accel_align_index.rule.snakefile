rule <step_name>__accel_align_index:
    input:
        **<step_name>__accel_align_index_inputs()
    output:
       index = (
            config["<step_name>__accel_align_index_output_dir"]+"/index",
            config["<step_name>__accel_align_index_output_dir"]+"/index.hash",
        ),
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__accel_align_index_output_dir"] + "/index.log"
    params:
        command = config["<step_name>__accel_align_index_command"],
        accel_align_index_kmers_length = config["<step_name>__accel_align_index_kmers_length"],
        out_dir = config["<step_name>__accel_align_index_output_dir"]
    shell:
        "{params.command} "
        "-l  {params.accel_align_index_kmers_length} "        
        "{input.genome_fasta} "
        "|& tee {log};"
        "cp {input.genome_fasta}  {params.out_dir}/index; "
        "mv {input.genome_fasta}.hash  {params.out_dir}/index.hash ;"
