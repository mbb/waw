rule <step_name>__bwa_mem_index:
    input:
        **<step_name>__bwa_mem_index_inputs()
    output:
        index = (
            config["<step_name>__bwa_mem_index_output_dir"]+"/index.amb",
            config["<step_name>__bwa_mem_index_output_dir"]+"/index.ann",
            config["<step_name>__bwa_mem_index_output_dir"]+"/index.bwt",
            config["<step_name>__bwa_mem_index_output_dir"]+"/index.pac",
            config["<step_name>__bwa_mem_index_output_dir"]+"/index.sa"
        )
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__bwa_mem_index_output_dir"] + "/index.log"
    params:
        command = config["<step_name>__bwa_mem_index_command"],
        algorithm = config["<step_name>__bwa_mem_index_algorithm"],
        output_prefix =  config["<step_name>__bwa_mem_index_output_dir"]+"/index"
    conda:
        "envs/bwa.yaml"       
    shell:
        "{params.command} "
        "-p {params.output_prefix} "
        "-a {params.algorithm} "
        "{input.genome_fasta} |& tee {log}"
