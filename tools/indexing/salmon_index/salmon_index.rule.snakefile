rule <step_name>__salmon_index:
    input:
        **<step_name>__salmon_index_inputs()
    output:
        #index = config["results_dir"]+"/"+config["<step_name>__salmon_index_output_dir"]+"/indexDir/versionInfo.json"
        index = (
            config["<step_name>__bwa_mem_index_output_dir"]+"/pos.bin",
            config["<step_name>__bwa_mem_index_output_dir"]+"/ctable.bin",
            config["<step_name>__bwa_mem_index_output_dir"]+"/ctg_offsets.bin",
            config["<step_name>__bwa_mem_index_output_dir"]+"/mphf.bin",
            config["<step_name>__bwa_mem_index_output_dir"]+"/pos.bin",
            config["<step_name>__bwa_mem_index_output_dir"]+"/rank.bin",
            config["<step_name>__bwa_mem_index_output_dir"]+"/reflengths.bin",
            config["<step_name>__bwa_mem_index_output_dir"]+"/refseq.bin",
            config["<step_name>__bwa_mem_index_output_dir"]+"/seq.bin",
            config["<step_name>__bwa_mem_index_output_dir"]+"/versionInfo.json",
            config["<step_name>__bwa_mem_index_output_dir"]+"/duplicate_clusters.tsv",
            config["<step_name>__bwa_mem_index_output_dir"]+"/info.json"
        )
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__salmon_index_output_dir"] + '/salmon_index_log.txt'
    params:
        command = config["<step_name>__salmon_index_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__salmon_index_output_dir"]+"/indexDir"
    conda:
        "envs/salmon.yaml"    
    shell:
        "{params.command} -t {input.transcriptome_fasta} -i {params.output_dir} |& tee {log}"
