rule <step_name>__kallisto_index:
    input:
        **<step_name>__kallisto_index_inputs()
    output:
        index = config["results_dir"]+"/"+config["<step_name>__kallisto_index_output_dir"]+"/index.kidx"
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__kallisto_index_output_dir"] + '/kallisto_index_log.txt'
    params:
        command = config["<step_name>__kallisto_index_command"]
    conda:
        "envs/kallisto.yaml"
    shell:
        "{params.command} -i {output} {input.transcriptome_fasta} |& tee {log}"
