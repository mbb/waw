rule <step_name>__GATK4_index_reference:
    input:
        **<step_name>__GATK4_index_reference_inputs(),
    output:
        fasta = config["results_dir"] + "/" + config["<step_name>__GATK4_index_reference_output_dir"] + "/reference.fa",
        index = config["results_dir"] + "/" + config["<step_name>__GATK4_index_reference_output_dir"] + "/reference.fa.fai",
        fadict = config["results_dir"] + "/" + config["<step_name>__GATK4_index_reference_output_dir"] + "/reference.dict"
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__GATK4_index_reference_output_dir"] + "/gatk_index_reference_log.txt"
    conda:
        "envs/gatk4.yaml" 
    shell:
        """
        if [[ {input.genome_fasta} =~ \.gz$ ]]; then
            gunzip {input.genome_fasta} -c > {output.fasta}
        else
            ln -s {input.genome_fasta} {output.fasta}
        fi
        gatk CreateSequenceDictionary -R {output.fasta} -O {output.fadict}
        samtools faidx {output.fasta} -o {output.index}
        """
