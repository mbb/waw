rule <step_name>__hisat2_index:
    input:
        **<step_name>__hisat2_index_inputs()
    output:
        index = (
            expand(config["<step_name>__hisat2_index_output_dir"]+"/index.{num}.ht2",num=[1,2,3,4,5,6,7,8]),
        )
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__hisat2_index_output_dir"] + "/index.log"
    threads:
        config["<step_name>__hisat2_index_threads"]
    params:
        command = config["<step_name>__hisat2_index_command"],
        output_prefix = config["results_dir"]+"/"+config["<step_name>__hisat2_index_output_dir"]+"/index"
    conda:
        "envs/hisat2.yaml"
    shell:
        "{params.command} "
        "{input.genome_fasta} "
        "{params.output_prefix} "
        "--threads {threads} "
        "|& tee {log}"
