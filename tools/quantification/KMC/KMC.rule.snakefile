rule <step_name>__KMC:
    input:
        **<step_name>__KMC_inputs()
    output:
        kmer_histo = config["results_dir"] + "/" + config["<step_name>__KMC_output_dir"] + "/kmer_histo_KMC.hist",    
        kmc_db     = config["results_dir"] + "/" + config["<step_name>__KMC_output_dir"] + "/kmcdb/kmcdb.kmc_suf",   
    threads:
        config["<step_name>__KMC_threads"]
    params:
        kmcDir  = config["results_dir"] + "/" + config["<step_name>__KMC_output_dir"] + "/kmcdb",
        command = config["<step_name>__KMC_command"],
        kmer_len= config["<step_name>__KMC_kmer_len"],
        memory  = config["<step_name>__KMC_mem"],
        ci      = config["<step_name>__KMC_ci"],
        cs      = config["<step_name>__KMC_cs"],
        cx      = config["<step_name>__KMC_cx"],
    conda:
        "envs/kmc-jellyfish-smudgeplot.yaml"
    shell:
        "mkdir -p {params.kmcDir}; "
        "echo {input.read}  > {params.kmcDir}/FILES ;"
        "sed -i 's/ /\\n/' {params.kmcDir}/FILES;"
        "{params.command} "
        "-k{params.kmer_len} "
        "-t{threads} "
        "-m{params.memory} "
        "-ci{params.ci} "
        "-cs{params.cs} "
        "@{params.kmcDir}/FILES {params.kmcDir}/kmcdb {params.kmcDir} ; "
        "kmc_tools transform {params.kmcDir}/kmcdb histogram {output.kmer_histo} -cx{params.cx};"
        
