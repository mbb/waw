rule <step_name>__smudgeplotJelly:
    input:
        **<step_name>__smudgeplotJelly_inputs()
    output:
        smudgeplot = config["results_dir"] + "/" + config["<step_name>__smudgeplotJelly_output_dir"] + "/smudgeplotJF_mqc.png", 
        smudgeplotlog = config["results_dir"] + "/" + config["<step_name>__smudgeplotJelly_output_dir"] + "/smudgeplotJF_log10_mqc.png",         
    params:
        command = config["<step_name>__smudgeplotJelly_command"],
        kmer_len= config["<step_name>__smudgeplotJelly_kmer_len"],
        L       = config["<step_name>__smudgeplotJelly_L"],
        U       = config["<step_name>__smudgeplotJelly_U"],
        plotDir = config["results_dir"] + "/" + config["<step_name>__smudgeplotJelly_output_dir"]
    conda:
       "envs/kmc-jellyfish-smudgeplot.yaml"         
    shell:
        #extract genomic kmers using reasonable coverage thresholds. 
        #You can either inspect the kmer spectra and choose the L (lower) and U (upper) coverage thresholds via visual inspection
        "if [ {params.L} == -1 ] ; then L=$(smudgeplot.py cutoff {input.kmer_histo} L); else L={params.L}; fi; "
        "if [ {params.U} == -1 ] ; then U=$(smudgeplot.py cutoff {input.kmer_histo} U); else U={params.U}; fi; "
        #extract kmers in the coverage range from L to U using kmc_tools
        "jellyfish dump -c -L $L -U $U {input.jelly_db} | smudgeplot.py hetkmers -o {params.plotDir}/kmer_pairs; "

        #finally generate the smudgeplot using the coverages of the identified kmer pairs
        "smudgeplot.py plot {params.plotDir}/kmer_pairs_coverages.tsv -o {params.plotDir}/plot -t \"JellyFish counts w/coverage between $L and $U \"; "
        "mv  {params.plotDir}/plot_smudgeplot.png {output.smudgeplot} ;"
        "mv  {params.plotDir}/plot_smudgeplot_log10.png {output.smudgeplotlog}; "
