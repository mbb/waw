rule <step_name>__smudgeplotKMC:
    input:
        **<step_name>__smudgeplotKMC_inputs()
    output:
        smudgeplot = config["results_dir"] + "/" + config["<step_name>__smudgeplotKMC_output_dir"] + "/smudgeplotKMC_mqc.png", 
        smudgeplotlog = config["results_dir"] + "/" + config["<step_name>__smudgeplotKMC_output_dir"] + "/smudgeplotKMC_log10_mqc.png",         
    params:
        command = config["<step_name>__smudgeplotKMC_command"],
        kmer_len= config["<step_name>__smudgeplotKMC_kmer_len"],
        L       = config["<step_name>__smudgeplotKMC_L"],
        U       = config["<step_name>__smudgeplotKMC_U"],
        plotDir = config["results_dir"] + "/" + config["<step_name>__smudgeplotKMC_output_dir"]
    conda:
       "envs/kmc-jellyfish-smudgeplot.yaml"    
    shell:
        #extract genomic kmers using reasonable coverage thresholds. 
        #You can either inspect the kmer spectra and choose the L (lower) and U (upper) coverage thresholds via visual inspection
        "if [ {params.L} == -1 ] ; then L=$(smudgeplot.py cutoff {input.kmer_histo} L); else L={params.L}; fi; "
        "if [ {params.U} == -1 ] ; then U=$(smudgeplot.py cutoff {input.kmer_histo} U); else U={params.U}; fi; "
        #extract kmers in the coverage range from L to U using kmc_tools
        "x={input.kmc_db}; "
        "kmcDBprefix=${{x%.*}};"
        "kmcDBdir=$(dirname {input.kmc_db});"
        "kmc_tools transform "
        "$kmcDBprefix "
        "-ci$L "
        "-cx$U "
        "reduce $kmcDBdir/kmcdb_L${{L}}_U${{U}}; "
        #run smudge_pairs on the reduced file to compute the set of kmer pairs.    
        "smudge_pairs $kmcDBdir/kmcdb_L${{L}}_U${{U}} $kmcDBdir/kmcdb_L${{L}}_U${{U}}_coverages.tsv $kmcDBdir/kmcdb_L${{L}}_U${{U}}_pairs.tsv > $kmcDBdir/kmcdb_L${{L}}_U${{U}}_familysizes.tsv; "
        #finally generate the smudgeplot using the coverages of the identified kmer pairs
        "smudgeplot.py plot $kmcDBdir/kmcdb_L${{L}}_U${{U}}_coverages.tsv -o {params.plotDir}/plot -t 'KMC counts w/coverage between $L and $U '; "
        "mv  {params.plotDir}/plot_smudgeplot.png {output.smudgeplot} ;"
        "mv  {params.plotDir}/plot_smudgeplot_log10.png {output.smudgeplotlog}; "
