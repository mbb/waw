rule <step_name>__featureCounts:
    input:
        **<step_name>__featureCounts_inputs()
    output:
        stats = config["results_dir"]+"/" + config["<step_name>__featureCounts_output_dir"] + "/Genomic_Features_Count.tsv",
        normalize = config["results_dir"]+"/" + config["<step_name>__featureCounts_output_dir"] + "/Gene_Count_RPKM_RPM.tsv",
        gene_count = config["results_dir"]+"/" + config["<step_name>__featureCounts_output_dir"] + "/Gene_Count.tsv",
        rpkm = config["results_dir"]+"/" + config["<step_name>__featureCounts_output_dir"] + "/Gene_RPKM.tsv",
        rpm = config["results_dir"]+"/" + config["<step_name>__featureCounts_output_dir"] + "/Gene_RPKM.tsv",
    log:
        config["results_dir"]+ "/logs/" + config["<step_name>__featureCounts_output_dir"] + "/Features_Count.log",
    params:
        command = config["<step_name>__featureCounts_command"],
        features_type = config["<step_name>__featureCounts_type"],
        features_id = config["<step_name>__featureCounts_id"],
        ended = "-p --countReadPairs" if config["<step_name>__featureCounts_end"] else "",
        output_dir = config["results_dir"]+ "/" +config["<step_name>__featureCounts_output_dir"],
    threads:
        config["<step_name>__featureCounts_threads"],
    conda:
        "envs/subread.yaml",
    shell:
        """
        featureCounts -T {threads} -O -t {params.features_type} -g {params.features_id} {params.ended} -a {input.annotation} -o {output.stats} {input.bam}

        awk -F "\\t" 'BEGIN
        {{
            header="#GeneID"
            count_header=""
            rpkm_header=""
            rpm_header=""
        }}NR==FNR {{
            if (FNR == 2){{
                for(i=7;i<=NF;i++){{
                    n=split($i,sample,"/")
                    sub(".bam","",sample[n])
                    dcol2name[i]=sample[n]
                    count_header=count_header"\\t"sample[n]"_COUNT"
                    rpkm_header=rpkm_header"\\t"sample[n]"_RPKM"
                    rpm_header=rpm_header"\\t"sample[n]"_RPM"
                }}
                print header""count_header""rpkm_header""rpm_header
                print header""count_header > "{output.gene_count}"
                print header""rpkm_header > "{output.rpkm}"
                print header""rpm_header > "{output.rpm}"
            }}else if (NR > 2){{
                for(i=7;i<=NF;i++){{
                    dsample2sum[dcol2name[i]]+=$i
                }}
            }}
        }}NR!=FNR{{
            if (FNR > 2){{
                for(i=7;i<=NF;i++){{
                    if(i==7){{
                        rpkm = ( $i * 1000000000 )/ ($6 * dsample2sum[dcol2name[i]])
                        rpm = ( $i*1000000 ) / dsample2sum[dcol2name[i]]
                        count = $i
                    }}else{{
                        rpkm = rpkm"\\t"( $i * 1000000000 )/ ($6 * dsample2sum[dcol2name[i]])
                        rpm = rpm"\\t"(( $i*1000000 ) / dsample2sum[dcol2name[i]])
                        count = count "\\t"$i
                    }}
                }}
                print $1"\\t"count"\\t"rpkm"\\t"rpm
                print $1"\\t"count >"{output.gene_count}"
                print $1"\\t"rpkm >"{output.rpkm}"
                print $1"\\t"rpm >"{output.rpm}"
            }}
        }}' {output.stats} {output.stats} > {output.normalize}
        """
