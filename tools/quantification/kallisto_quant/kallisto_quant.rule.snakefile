if config["SeOrPe"] == "SE":

    rule <step_name>__kallisto_quant_SE:
        input:
            **<step_name>__kallisto_quant_SE_inputs()
        output:
            h5 = config["results_dir"]+"/"+config["<step_name>__kallisto_quant_SE_output_dir"]+"/{sample}/abundance.h5",
            counts = config["results_dir"]+"/"+config["<step_name>__kallisto_quant_SE_output_dir"]+"/{sample}/abundance.tsv",
            run_info = config["results_dir"]+"/"+config["<step_name>__kallisto_quant_SE_output_dir"]+"/{sample}/run_info.json"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__kallisto_quant_SE_output_dir"] + '/{sample}_kallisto_quant_log.txt'
        threads:
            config["<step_name>__kallisto_quant_threads"]
        params:
            command = config["<step_name>__kallisto_quant_SE_command"],
            fragment_length = config["<step_name>__kallisto_quant_fragment_length_SE"],
            standard_deviation = config["<step_name>__kallisto_quant_standard_deviation_SE"],
            output_dir = config["results_dir"]+"/"+config["<step_name>__kallisto_quant_SE_output_dir"]+"/{sample}"
        conda:
            "envs/kallisto.yaml"
        shell:
            "{params.command} "
            "-t {threads} "
            "-i {input.index} "
            "-o {params.output_dir} "
            "--single "
            "-l {params.fragment_length} "
            "-s {params.standard_deviation} "
            "{input.read} |& tee {log}"


elif config["SeOrPe"] == "PE":

    rule <step_name>__kallisto_quant_PE:
        input:
            **<step_name>__kallisto_quant_PE_inputs(),
        output:
            h5 = config["results_dir"]+"/"+config["<step_name>__kallisto_quant_PE_output_dir"]+"/{sample}/abundance.h5",
            counts = config["results_dir"]+"/"+config["<step_name>__kallisto_quant_PE_output_dir"]+"/{sample}/abundance.tsv",
            run_info = config["results_dir"]+"/"+config["<step_name>__kallisto_quant_PE_output_dir"]+"/{sample}/run_info.json"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__kallisto_quant_PE_output_dir"] + '/{sample}_kallisto_quant_log.txt'
        threads:
            config["<step_name>__kallisto_quant_threads"]
        params:
            command = config["<step_name>__kallisto_quant_PE_command"],
            stranded = config["<step_name>__kallisto_quant_stranded_PE"],
            output_dir = config["results_dir"]+"/"+config["<step_name>__kallisto_quant_PE_output_dir"]+"/{sample}"
        conda:
            "envs/kallisto.yaml"
        shell:
            "{params.command} "
            "-t {threads} "
            "-i {input.index} "
            "-o {params.output_dir} "
            "{input.read} {input.read2} |& tee {log}"
