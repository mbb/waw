rule <step_name>__pyrodigal:
    input:
        **<step_name>__pyrodigal_inputs(),
    output:
        fasta_protein = config["results_dir"] + "/" + config["<step_name>__pyrodigal_output_dir"] + "/protein_CDS.faa",
        fasta_nt = config["results_dir"] + "/" + config["<step_name>__pyrodigal_output_dir"] + "/nucleotid_CDS.fna",
        annotation = config["results_dir"] + "/" + config["<step_name>__pyrodigal_output_dir"] + "/CDS.gff3",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__pyrodigal_output_dir"]+ "/",
        command = config["<step_name>__pyrodigal_command"],
        mode = config["<step_name>__pyrodigal_mode"],
    log:
        config["results_dir"]+ '/logs/' + config["<step_name>__pyrodigal_output_dir"] + '/pyrodigal_log.txt',
    conda:
        "envs/pyrodigal.yaml",
    threads:
        config["<step_name>__pyrodigal_threads"],
    shell:
        """"
        mkdir -p {params.output_dir}/protein_CDS;
        mkdir -p {params.output_dir}/nucleotide_CDS;
        mkdir -p {params.output_dir}/annotation_CDS;
        for fastafic in ` ls {input.fasta_dir}/*.{{fasta,fas,fa,fna}} 2>/dev/null` ; \
        do  \
          ext="${{fastafic##*.}}"; \
          r=$(basename $fastafic .$ext);  \
          pyrodigal -i ${fastafic} -p {params.mode} -o {params.output_dir}/annotation_CDS/${{r}}.gff3 -a {params.output_dir}/protein_CDS/${{r}}.faa -d {params.output_dir}/nucleotide_CDS/${{r}}.fna -j {threads}
        done |& tee {log}; 

        cat {params.output_dir}/annotation_CDS/*.gff3 > {output.annotation}
        cat {params.output_dir}/protein_CDS/*.faa > {output.fasta_protein}
        cat {params.output_dir}/nucleotide_CDS/*.fna > {output.fasta_nt}
        """
