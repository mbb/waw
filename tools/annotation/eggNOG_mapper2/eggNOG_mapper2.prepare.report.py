import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.text as mtext
import sys
import re

### Plot for COG

df_COG = pd.read_table(sys.argv[1],index_col=0)

COG_out = re.sub(".txt","_mqc.png",sys.argv[1])

column_mapping_COG = {
    'A': 'A: RNA processing and modification',
    'B': 'B: Chromatin structure and dynamics',
    'C': 'C: Energy production and conversion',
    'D': 'D: Cell cycle control, cell division, chromosome partitioning',
    'E': 'E: Amino acid transport and metabolism',
    'F': 'F: Nucleotide transport and metabolism',
    'G': 'G: Carbohydrate transport and metabolism',
    'H': 'H: Coenzyme transport and metabolism',
    'I': 'I: Lipid transport and metabolism',
    'J': 'J: Translation, ribosomal structure and biogenesis',
    'K': 'K: Transcription',
    'L': 'L: Replication, recombination and repair',
    'M': 'M: Cell wall/membrane/enveloppe biogenesis',
    'N': 'N: Cell motility',
    'O': 'O: Posttranslational modification, protein turnover, chaperones',
    'P': 'P: Inorganic ion transport and metabolism',
    'Q': 'Q: Secondary metabolites biosynthesis, transport and catabolism',
    'R': 'R: General function prediction only',
    'S': 'S: Function unknown',
    'T': 'T: Signal transduction mechanisms',
    'U': 'U: Intracellular trafficking, secretion, and vesicular transport',
    'V': 'V: Defense mechanisms',
    'W': 'W: Extracellular structures',
    'X': 'X: Mobilome: prophages, transposons',
    'Y': 'Y: Nuclear structure',
    'Z': 'Z: Cytoskeleton',
}

# Rename the columns
df_COG.rename(index=column_mapping_COG, inplace=True)

# Define category order
category_order_COG = [
    # Information storage and processing
    'A: RNA processing and modification',
    'B: Chromatin structure and dynamics',
    'J: Translation, ribosomal structure and biogenesis',
    'K: Transcription',
    'L: Replication, recombination and repair',
    # Cellular processes and signaling
    'D: Cell cycle control, cell division, chromosome partitioning',
    'M: Cell wall/membrane/enveloppe biogenesis',
    'N: Cell motility',
    'O: Posttranslational modification, protein turnover, chaperones',
    'T: Signal transduction mechanisms',
    'U: Intracellular trafficking, secretion, and vesicular transport',
    'W: Extracellular structures',
    'X: Mobilome: prophages, transposons',
    'Y: Nuclear structure',
    'Z: Cytoskeleton',
    # Metabolism
    'C: Energy production and conversion',
    'E: Amino acid transport and metabolism',
    'F: Nucleotide transport and metabolism',
    'G: Carbohydrate transport and metabolism',
    'H: Coenzyme transport and metabolism',
    'I: Lipid transport and metabolism',
    'P: Inorganic ion transport and metabolism',
    'Q: Secondary metabolites biosynthesis, transport and catabolism',
    # Unknwown
    'R: General function prediction only',
    'S: Function unknown',
]

df_COG = df_COG.reindex(category_order_COG)

# Color mapping
color_mapping_COG = {
    # Information storage and processing
    'A: RNA processing and modification': 'yellow',
    'B: Chromatin structure and dynamics': 'orange',
    'J: Translation, ribosomal structure and biogenesis': 'salmon',
    'K: Transcription': 'goldenrod',
    'L: Replication, recombination and repair': 'bisque',
    # Cellular processes and signaling
    'D: Cell cycle control, cell division, chromosome partitioning': 'blue',
    'M: Cell wall/membrane/enveloppe biogenesis': 'royalblue',
    'N: Cell motility': 'dodgerblue',
    'O: Posttranslational modification, protein turnover, chaperones': 'lightsteelblue',
    'T: Signal transduction mechanisms': 'cyan',
    'U: Intracellular trafficking, secretion, and vesicular transport': 'teal',
    'W: Extracellular structures': 'seagreen',
    'X: Mobilome: prophages, transposons': 'green',
    'Y: Nuclear structure': 'lightgreen',
    'Z: Cytoskeleton': 'olivedrab',
    # Metabolism
    'C: Energy production and conversion': 'mediumpurple',
    'E: Amino acid transport and metabolism': 'blueviolet',
    'F: Nucleotide transport and metabolism': 'indigo',
    'G: Carbohydrate transport and metabolism': 'darkviolet',
    'H: Coenzyme transport and metabolism': 'deeppink',
    'I: Lipid transport and metabolism': 'indianred',
    'P: Inorganic ion transport and metabolism': 'red',
    'Q: Secondary metabolites biosynthesis, transport and catabolism': 'darkred',
    # Unknwown
    'R: General function prediction only': 'black',
    'S: Function unknown': 'gray',
}

df_COG =df_COG.transpose()

# Create stacked bar plot
fig, ax = plt.subplots(figsize=(20, 10))

df_COG.plot(kind='bar', stacked=True, color=[color_mapping_COG[col] for col in df_COG.columns], ax=ax)

# Set legend outside of the plot
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))


# Set subtitle in legend https://stackoverflow.com/questions/38463369/subtitles-within-matplotlib-legend

h,l = ax.get_legend_handles_labels()


class LegendTitle(object):
    def __init__(self, text_props=None):
        self.text_props = text_props or {}
        super(LegendTitle, self).__init__()

    def legend_artist(self, legend, orig_handle, fontsize, handlebox):
        x0, y0 = handlebox.xdescent, handlebox.ydescent
        title = mtext.Text(x0, y0, orig_handle,  **self.text_props)
        handlebox.add_artist(title)
        return title


ax.legend(['Information storage and processing'] + h[:5] + ['','Cellular processes and signaling'] + h[5:15] + ['','Metabolism'] + h[15:23] + ['','Unknown'] + h[23:26], 
          ['']         + l[:5] + ['','']          + l[5:15] + ['','']          + l[15:23] + ['','']          + l[23:26],
           handler_map={str: LegendTitle({'fontsize': 13})},
          bbox_to_anchor=(1, 1), title= "COG Category"
        )


ax.set_xlabel('Sample name')
ax.set_ylabel('Abundance functional category')
ax.set_title("Functionnal annotation : COG category abundance by sample")

fig.savefig(COG_out, format='png', bbox_inches='tight')

### Plot for EC


df_EC = pd.read_table(sys.argv[2],index_col=0)

EC_out = re.sub(".txt","_mqc.png",sys.argv[2])

column_mapping_EC = {
    1: 'EC 1 Oxydoreductases: catalyze oxydation-reduction reactions such as oxygenases',
    2: 'EC 2 Transferases: Transfer a functional group',
    3: 'EC 3 Hydrolases: Catalyze the hydrolysis of various bonds',
    4: 'EC 4 Lyases: Break various bonds by processes other than hydrolysis and oxidation',
    5: 'EC 5 Isomerases: catalyze isomerization reactions in a single molecule',
    6: 'EC 6 Ligases: Bind two molecules by covalent bonds',
    7: 'EC 7 Translocases: One molecules displaces another molecule',
}

# Rename the columns
df_EC.rename(index=column_mapping_EC, inplace=True)

# Define category order
category_order_EC = [
    'EC 1 Oxydoreductases: catalyze oxydation-reduction reactions such as oxygenases',
    'EC 2 Transferases: Transfer a functional group',
    'EC 3 Hydrolases: Catalyze the hydrolysis of various bonds',
    'EC 4 Lyases: Break various bonds by processes other than hydrolysis and oxidation',
    'EC 5 Isomerases: catalyze isomerization reactions in a single molecule',
    'EC 6 Ligases: Bind two molecules by covalent bonds',
    'EC 7 Translocases: One molecules displaces another molecule',
]

df_EC = df_EC.reindex(category_order_EC)

# Color mapping
color_mapping_EC = {
    # Information storage and processing
    'EC 1 Oxydoreductases: catalyze oxydation-reduction reactions such as oxygenases': 'blue',
    'EC 2 Transferases: Transfer a functional group': 'orange',
    'EC 3 Hydrolases: Catalyze the hydrolysis of various bonds': 'green',
    'EC 4 Lyases: Break various bonds by processes other than hydrolysis and oxidation': 'red',
    'EC 5 Isomerases: catalyze isomerization reactions in a single molecule': 'purple',
    'EC 6 Ligases: Bind two molecules by covalent bonds': 'olive',
    'EC 7 Translocases: One molecules displaces another molecule': 'brown',
}

df_EC =df_EC.transpose()

# Create stacked bar plot
fig, ax = plt.subplots(figsize=(20, 10))

df_EC.plot(kind='bar', stacked=True, color=[color_mapping_EC[col] for col in df_EC.columns], ax=ax)

# Set legend outside of the plot
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), title= "EC Category")


ax.set_xlabel('Sample name')
ax.set_ylabel('Abundance functional category')
ax.set_title("Functionnal annotation : Enzyme Commission abundance by sample")

fig.savefig(EC_out, format='png', bbox_inches='tight')