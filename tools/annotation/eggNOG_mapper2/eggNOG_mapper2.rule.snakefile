rule <step_name>__eggNOG_mapper2:
    input:
        **<step_name>__eggNOG_mapper2_inputs()
    output:
        emapper_annotation = config["results_dir"] + "/" + config["<step_name>__eggNOG_mapper2_output_dir"] + "/" + config["<step_name>__eggNOG_mapper2_mode"] + ".emapper.annotations",
        gff = config["results_dir"] + "/" + config["<step_name>__eggNOG_mapper2_output_dir"] + "/" + config["<step_name>__eggNOG_mapper2_mode"] + ".emapper.decorated.gff",
        quantification_annotation = config["results_dir"] + "/" + config["<step_name>__eggNOG_mapper2_output_dir"] + "/All_emapper_annotations_quantification.tsv",
    log:
        config["results_dir"]+ "/logs/" + config["<step_name>__eggNOG_mapper2_output_dir"] + "/eggNOG.log"
    threads:
        config["<step_name>__eggNOG_mapper2_threads"]
    params:
        command = config["<step_name>__eggNOG_mapper2_command"],
        output_dir = config["results_dir"]+'/'+config["<step_name>__eggNOG_mapper2_output_dir"],
        itype = config["<step_name>__eggNOG_mapper2_type"],
        mode = config["<step_name>__eggNOG_mapper2_mode"],
        pfam = config["<step_name>__eggNOG_mapper2_pfam_realign"],
    conda:
        "envs/eggnog.yaml"
    shell:
        """
        #### Download database for eggnog functional annotation
        mkdir -p /Download/eggNOG
        if [ "{params.mode}" == "diamond" ];
        then
            download_eggnog_data.py -P -y --data_dir /Download/eggNOG/
        elif [ "{params.mode}" == "mmseqs" ];
        then
            download_eggnog_data.py -D -P -M -y --data_dir /Download/eggNOG/
        elif [ "{params.mode}" == "novel_fams" ];
        then
            download_eggnog_data.py -D -P -F -y --data_dir /Download/eggNOG/
        fi

        #### If fasta not prot convert it to protein with genepred or translate
        if [ "{params.itype}" == "CDS" ];
        then
            param_type="--translate"
        elif [ "{params.itype}" == "genome" ];
        then
            param_type="--genepred search"
        elif [ "{params.itype}" == "metagenome" ];
        then
            param_type="--genepred prodigal"
        else
            param_type=""
        fi

        #### Run Eggnog
        emapper.py --cpu {threads} -i {input.fa} -m {params.mode} --report_orthologs --data_dir /Download/eggNOG/ --output_dir {params.output_dir} -o {params.mode} --decorate_gff {input.annotation} --itype {params.itype} --pfam_realign {params.pfam} $param_type 2> {log}


        #### Create abundance table results with value of 1 for CDS and if count table is gave, it joins to the eggNOG results
        # List input file to determine number of input.
        lfile_i=({input});
        len=${{#lfile_i[@]}};
        n=$(($len-1))

        if [ $len -ge "3" ]; then   # If count table will join the count table to each eggNOG results, a column CDS_Count with a value of 1 is also set 
            header_annotation=$(grep "^#query" {output.emapper_annotation})
            header_count=$(grep "^#" {input.gene_count} | cut -f 2-)
            echo -e "${{header_annotation}}\\t${{header_count}}\\tCDS_COUNT" > {output.quantification_annotation}
            join -t $'\\t' -a1 -a2 -e - -o auto <(grep -v "^#" {output.emapper_annotation} | sort -k1,1) <(grep -v "^#" {input.gene_count} | sort -k1,1) >> {output.quantification_annotation} 
            sed -i '2,$s/$/\\t1/' {output.quantification_annotation}
        else
            sed '1s/$/\\tCDS_COUNT/' {output.emapper_annotation} > {output.quantification_annotation}
            sed -i '2,$s/$/\\t1/' {output.quantification_annotation}
        fi

        #### Resume results

        # Create Abundance table with Orthologous Group (OG) identifier call seed
        cut -f 2,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_OG_Seed_Abundance.txt

        # Create Abundance table with list of OG
        cut -f 5,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_OG_Abundance.txt

        # Create Abundance table with the taxonomic level at which the functionnal annotation is applied
        cut -f 6,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_Taxo_Abundance.txt

        # Create Abundance tabith Clusters of Orthologous Groups (COG) category assignment. COG categories classify proteins into functional categories such as metabolism, information storage, and processing.
        cut -f 7,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_COG_category_Abundance.txt

        # Create Abundance table with Description protein
        cut -f 8,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_Description_Abundance.txt

        # Create Abundance table with a standardize name accross organism
        cut -f 9,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_GeneName_Abundance.txt

        # Create Abundance table with Gene Ontology (GO) list. These terms describe the molecular function, biological process, and cellular component of the predicted gene product.
        cut -f 10,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_GO_Abundance.txt

        # Create Abundance table with Enzyme Commission (EC) list. These numbers classify enzymes based on the reactions they catalyze. 
        cut -f 11,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_EC_Abundance.txt

        # Create Abundance table with KEGG ko list, indicating functional information such as metabolic pathways.
        cut -f 12,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_KEGG_ko_Abundance.txt

        # Create Abundance table with KEGG pathway list, indicating the pathways that the annotated gene might participate in, based on KEGG ko and metabolic function.
        cut -f 13,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_KEGG_pathway_Abundance.txt

        # Create Abundance table with KEGG module list that defined functional units of gene sets and reaction sets.
        cut -f 14,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_KEGG_module_Abundance.txt

        # Create Abundance table with KEGG reaction list, indicating the chemical reactions, mostly enzymatic reactions, containing all reactions that appear in the KEGG metabolic pathway
        cut -f 15,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_KEGG_reaction_Abundance.txt

        # Create Abundance table with KEGG rclass list that contains classification of reactions based on the chemical structure transformation patterns of substrate-product pairs
        cut -f 16,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_KEGG_rclass_Abundance.txt

        # Create Abundance table with BRITE list. This is a collection of hierarchical classification systems capturing functional hierarchies of various biological objects.
        cut -f 17,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_BRITE_Abundance.txt

        # Create Abundance table with KEGG TC list, indicating the transporter classification, so proteins that help move of molecules across cellular membrane.
        cut -f 18,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_KEGG_TC_Abundance.txt

        # Create Abundance table with CAZy list, that contains families related to catalytic and carbohydrate binding modules.
        cut -f 19,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_CAZY_Abundance.txt

        # Create Abundance table with BiGG reaction list, indicating biochemically, genetically and genomically structured genome-scale metabolic network reconstructions
        cut -f 20,22- {output.quantification_annotation} > {params.output_dir}/EggNOG_BiGG_reaction_Abundance.txt

        # Create Abundance table with PFAM list, indicating the protein families and domains.
        cut -f 21- {output.quantification_annotation} > {params.output_dir}/EggNOG_PFAM_Abundance.txt


        ### Figure
        
        if [ $len -ge "3" ]; then
            # KEGG decoder metabolic pathway
            awk -F "\\t" '{{
                if(NR==1){{
                    for(i=2;i<=NF-1;i++){{
                        gsub(/_/,"-",$i)
                        sample[i]=$i
                    }}
                }}else{{
                    gsub("ko:","",$1)
                    k=split($1,a,",")
                    for(i=2;i<=NF-1;i++){{
                        if($i>0){{
                            for(j=1;j<=k;j++){{
                                print sample[i]"\\t"a[j]
                            }}
                        }}
                    }}
                }}
            }}' {params.output_dir}/EggNOG_KEGG_ko_Abundance.txt > {params.output_dir}/EggNOG_KEGG_MetabolicPathway_Resume.txt
            KEGG-decoder -i {params.output_dir}/EggNOG_KEGG_MetabolicPathway_Resume.txt -o {params.output_dir}/EggNOG_KEGG_MetabolicPathway_Resume_mqc -v static
            magick -size 1024x1024 {params.output_dir}/MetabolicPathway_mqc.svg {params.output_dir}/MetabolicPathway_mqc.png
            
            # EC
            awk -F "\\t" 'BEGIN{{
                header=""
            }}{{
                if(NR==1){{
                    for(i=2;i<=NF-1;i++){{
                        sample[i]=$i
                        header=header"\\t"$i
                    }}
                }}else{{
                    delete dec
                    n=split($1,e,",")
                    for(c=1; c <= n; c++){{
                        ec=substr(e[c],1,1)
                        if (ec in dec == 0){{
                            dec[ec]=1
                        }}
                    }}
                    for(enz in dec){{
                        for(i=2;i<=NF-1;i++){{
                            if(enz ~ /^[1-9]/){{
                                dec2sample[enz][sample[i]]+=$i
                            }}
                        }}
                    }}
                }}
            }}END{{
                print header
                for(ne in dec2sample){{
                    toprint=ne
                    for(s in dec2sample[ne]){{
                        toprint=toprint"\\t"dec2sample[ne][s]
                    }}
                    print toprint
                }}
            }}' {params.output_dir}/EggNOG_EC_Abundance.txt > {params.output_dir}/EggNOG_EC_Resume.txt
            
            # COG Category
            awk -F "\\t" 'BEGIN{{
                header=""
            }}{{
                if(NR==1){{
                    for(i=2;i<=NF-1;i++){{
                        sample[i]=$i
                        header=header"\\t"$i
                    }}
                }}else{{
                    split($1, cog, "")
                    if($1 ~ /^[A-Z]/){{
                        for(i=2;i<=NF-1;i++){{
                            for(c=1; c <= length($1); c++){{
                                dcog2sample[cog[c]][sample[i]]+=$i
                            }}
                        }}
                    }}
                }}
            }}END{{
                print header
                for(c in dcog2sample){{
                    toprint=c
                    for(s in dcog2sample[c]){{
                        toprint=toprint"\\t"dcog2sample[c][s]
                    }}
                    print toprint
                }}
            }}' {params.output_dir}/EggNOG_COG_category_Abundance.txt > {params.output_dir}/EggNOG_COG_category_Resume.txt
        else
            # KEGG decoder metabolic pathway
            awk -F "\\t" '{{
                if(NR!=1){{
                    gsub("ko:","",$1)
                    k=split($1,a,",")
                    for(j=1;j<=k;j++){{
                        print "EggNOG Metabolic Pathway\\t"a[j]
                    }}
                }}
            }}' {params.output_dir}/EggNOG_KEGG_ko_Abundance.txt > {params.output_dir}/EggNOG_KEGG_MetabolicPathway.txt
            KEGG-decoder -i {params.output_dir}/EggNOG_KEGG_MetabolicPathway.txt {params.output_dir}/MetabolicPathway_mqc
            magick -size 1024x1024 {params.output_dir}/MetabolicPathway_mqc.svg {params.output_dir}/MetabolicPathway_mqc.png

            # EC
            awk -F "\\t" '{{
                if(NR!=1){{
                    delete denz
                    n=split($1,e,",")
                    for(c=1; c <= n; c++){{
                        ec=substr(e[c],1,1)
                        if (ec in denz == 0){{
                            denz[ec]=1
                        }}
                    }}
                    for(enz in denz){{
                        if(enz ~ /^[1-9]/){{
                            dec[enz]+=1
                        }}
                    }}
                }}
            }}END{{
                print "\\tGene"
                for(ne in dec){{
                    print ne"\\t"dec[ne]
                }}
            }}' {params.output_dir}/EggNOG_EC_Abundance.txt > {params.output_dir}/EggNOG_EC_Resume.txt

            # COG category
            awk -F "\\t" '{{
                if(NR!=1){{
                    split($1, cog, "")
                    if($1 ~ /^[A-Z]/){{
                        for(c=1; c <= length($1); c++){{
                            dcog[cog[c]]+=1
                        }}
                    }}
                }}
            }}END{{
                print "\\tGene"
                for(c in dcog){{
                    print c"\\t"dcog[c]
                }}
            }}' {params.output_dir}/EggNOG_COG_category_Abundance.txt > {params.output_dir}/EggNOG_COG_category_Resume.txt
        fi
        

        python {snake_dir}/scripts/<step_name>__eggNOG_mapper2.prepare.report.py {params.output_dir}/EggNOG_COG_category_Resume.txt {params.output_dir}/EggNOG_EC_Resume.txt 
        """