rule <step_name>__metaeuk:
    input:
        **<step_name>__metaeuk_inputs(),
    output:
        protein = config["results_dir"] + "/" + config["<step_name>__metaeuk_output_dir"] + "/Eukaryota_protein.faa",
        gene = config["results_dir"] + "/" + config["<step_name>__metaeuk_output_dir"] + "/Eukaryota_gene.fna",
        annotation = config["results_dir"] + "/" + config["<step_name>__metaeuk_output_dir"] + "/Eukaryota_annotation.gff3",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__metaeuk_output_dir"]+ "/",
        db = config["<step_name>__metaeuk_db"],
        command = config["<step_name>__CAT_command"],
        sensitive = " --sensitive " if config["<step_name>__CAT_sensitivity"] else "",
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__CAT_output_dir"] + '/CAT_log.txt',
    conda:
        "envs/metaeuk.yaml",
    threads:
        config["<step_name>__metaeuk_threads"],
    shell:
        """
        metaeuk easy-predict {input.query} {params.db} {params.output_dir}/Eukaryota {params.output_dir}/tmp/ --threads {threads}
        mv {params.output_dir}/Eukaryota.fas {output.protein}
        mv {params.output_dir}/Eukaryota.codon.fas {output.gene}
        mv {params.output_dir}/Eukaryota.gff {output.annotation}
        """
