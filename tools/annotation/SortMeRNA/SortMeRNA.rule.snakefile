import time


if config["SeOrPe"] == "SE":
    rule <step_name>__SortMeRNA_SE:
        input:
            **<step_name>__SortMeRNA_SE_inputs()
        output:
            rRNA = config["results_dir"] + "/" + config["<step_name>__SortMeRNA_SE_output_dir"] + "/{sample}_rRNA_reads.fq.gz",
            norRNA = config["results_dir"] + "/" + config["<step_name>__SortMeRNA_SE_output_dir"] + "/{sample}_non_rRNA_reads.fq.gz",
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__SortMeRNA_SE_output_dir"] + '/{sample}_sortmerna_log.txt'
        threads:
            config["<step_name>__sortmerna_threads"]
        params:
            command = config["<step_name>__SortMeRNA_SE_command"],
            output_dir = config["results_dir"]+'/'+config["<step_name>__SortMeRNA_SE_output_dir"],
            db = config["<step_name>__sortmerna_database"],
        conda:
            "envs/sortmerna.yaml"
        shell:
            """
            if [ ! -f {params.db} ];
            then
                dir=$(dirname {params.db})
                yaml=$(echo $dir"/info.yaml")
                taxURL=$(grep train_set: $yaml | sed 's/train_set://' | sed 's/"//g')
                cd $dir;
                echo Downloading database from:  $taxURL;
                wget $taxURL -O database.tar.gz -q ;
                tar -xzf database.tar.gz && rm -f database.tar.gz;
            fi
            cd {params.output_dir}
            sortmerna --ref {params.db} --reads {input.read} --fastx --zip-out 1 --threads {threads} --workdir {params.output_dir}/{wildcards.sample} --aligned {params.output_dir}/{wildcards.sample}_rRNA_reads --other {params.output_dir}/{wildcards.sample}_non_rRNA_reads
            """


elif config["SeOrPe"] == "PE":
    rule <step_name>__SortMeRNA_PE:
        input:
            **<step_name>__SortMeRNA_PE_inputs()
        output:
            R1_rRNA = config["results_dir"] + "/" + config["<step_name>__SortMeRNA_PE_output_dir"] + "/{sample}_rRNA_reads_R1.fq.gz",
            R2_rRNA = config["results_dir"] + "/" + config["<step_name>__SortMeRNA_PE_output_dir"] + "/{sample}_rRNA_reads_R2.fq.gz",
            R1_norRNA = config["results_dir"] + "/" + config["<step_name>__SortMeRNA_PE_output_dir"] + "/{sample}_non_rRNA_reads_R1.fq.gz",
            R2_norRNA = config["results_dir"] + "/" + config["<step_name>__SortMeRNA_PE_output_dir"] + "/{sample}_non_rRNA_reads_R2.fq.gz"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__SortMeRNA_PE_output_dir"] + '/{sample}_sortmerna_log.txt'
        threads:
            config["<step_name>__sortmerna_threads"]
        params:
            command = config["<step_name>__SortMeRNA_PE_command"],
            output_dir = config["results_dir"]+'/'+config["<step_name>__SortMeRNA_PE_output_dir"],
            db = config["<step_name>__sortmerna_database"],
        conda:
            "envs/sortmerna.yaml"
        shell:
            """
            if [ ! -f {params.db} ];
            then
                dir=$(dirname {params.db})
                yaml=$(echo $dir"/info.yaml")
                taxURL=$(grep train_set: $yaml | sed 's/train_set://' | sed 's/"//g')
                cd $dir;
                echo Downloading database from:  $taxURL;
                wget $taxURL -O database.tar.gz -q ;
                tar -xzf database.tar.gz && rm -f database.tar.gz;
            fi
            cd {params.output_dir};
            mkdir -p {params.output_dir}/{wildcards.sample}
            sortmerna --ref {params.db} --reads {input.read} --reads {input.read2} --fastx --zip-out 1 --threads {threads} --workdir {params.output_dir}/{wildcards.sample} --aligned {params.output_dir}/{wildcards.sample}_rRNA_reads --other {params.output_dir}/{wildcards.sample}_non_rRNA_reads --paired_in --out2
            mv {wildcards.sample}_non_rRNA_reads_fwd.fq.gz {output.R1_norRNA}
            mv {wildcards.sample}_non_rRNA_reads_rev.fq.gz {output.R2_norRNA}
            mv {wildcards.sample}_rRNA_reads_fwd.fq.gz {output.R1_rRNA}
            mv {wildcards.sample}_rRNA_reads_rev.fq.gz {output.R2_rRNA}
            """

