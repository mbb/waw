rule <step_name>__mitoz_findmitoscaf:
    input:
        **<step_name>__mitoz_findmitoscaf_inputs()
    output:
        mitogenome = config["results_dir"] + "/" + config["<step_name>__mitoz_findmitoscaf_output_dir"] + "/mitoz_findmitoscaf.result/mitoz_findmitoscaf.mitogenome.fa",
    log: config["results_dir"] + '/logs/' + config["<step_name>__mitoz_findmitoscaf_output_dir"] + '/mitoz_findmitoscaf_log.txt'
    threads: config["<step_name>__mitoz_findmitoscaf_threads"]
    params:
        command = config["<step_name>__mitoz_findmitoscaf_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__mitoz_findmitoscaf_output_dir"],
        prefix = "mitoz_findmitoscaf",
        result_dir = config["results_dir"] + "/" + config["<step_name>__mitoz_findmitoscaf_output_dir"] + "/mitoz_findmitoscaf.result",
        clade = config["<step_name>__mitoz_findmitoscaf_clade"],
        min_abundance = config["<step_name>__mitoz_findmitoscaf_minabundance"]
    conda: 
        "envs/mitoz.yaml"        
    shell:
        #enlever un eventuel dossier tmp car find_mitoscafold ne peut pas tourner s'il en existe un
        "rm -rf {params.output_dir}/* ;"
        
        # Si l'on veut eviter l'étape cal_bwa_abundance.py qui fait un mapping couteux sur tous les scaffolds de l'assemblage :
        # convert Megahit or flye output to format similar to one produced by abundance estimate procedure with a dummy abundance of 100
        # N.B. assembly fasta file must be one line per sequence format
        """awk '{{posit = index($0,">"); if (posit == 1) {{split($0,a," "); sub(">",">C",a[1]); ident=a[1]"\\tabun=100.0\\tlength="}} else {{long=length($0);if (long > 0) {{ident=ident long; print ident"\\n"$0 }} }}}}' {input.fasta} > /tmp/assembly.contigs.fa;"""
        "mkdir -p {params.output_dir} && "
        "cd {params.output_dir}; "
        "{params.command} "
        "--fastafile /tmp/assembly.contigs.fa "
        "--thread_number {threads} "
        "--outprefix {params.prefix} "
        "--genetic_code auto " #'auto' means determined by '--clade' option.
        "--clade {params.clade} "
        "--requiring_taxa {params.clade} "
        "--min_abundance {params.min_abundance} "
        "|& tee {log}"
        