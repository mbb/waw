# File generate with generate_tool_snakefile.py

if config['SeOrPe'] == 'SE':

    rule <step_name>__get_organelle_SE:
        input:
            **<step_name>__get_organelle_SE_inputs(),
        output: 
            assembly = config["results_dir"] + "/" + config["<step_name>__get_organelle_SE_output_dir"] + "/assembly.fasta",
        params:
            input_line = lambda w, input: ",".join(input.read), 
            output_dir = config["results_dir"] + "/" + config["<step_name>__get_organelle_SE_output_dir"],
            command = config["<step_name>__get_organelle_SE_command"],
            organelle_type = config["<step_name>__get_organelle_SE_organelle_type"],
            max_rounds = config["<step_name>__get_organelle_SE_max_rounds"],
        log: 
            config["results_dir"] + "/logs/" + config["<step_name>__get_organelle_SE_output_dir"] + "/get_organelle_SE_log.txt"
        threads:  config["<step_name>__get_organelle_SE_threads"]
        conda:
            "envs/get_organelle.yaml"
        shell: 
            "{params.command} "
            "--overwrite "
            "-u {params.input_line} "
            "-o {params.output_dir} "
            "-t {threads} "
            "-F {params.organelle_type} "
            "-R {params.max_rounds}; "
            "for fasta in $(ls {params.output_dir}/*.path_sequence.fasta); "
            "do cat $fasta >> {output.assembly}; "
            "done; "
            "sed -i -E -e 's/\(|\)//g' {output.assembly};  "
            # Si l'on veut eviter l'étape cal_bwa_abundance.py qui fait un mapping couteux sur tous les scaffolds de l'assemblage :
            # convert Megahit or flye output to format similar to one produced by abundance estimate procedure with a dummy abundance of 0.1
            # N.B. assembly fasta file must be one line per sequence format
            """awk '{{posit = index($0,">"); if (posit == 1) {{split($0,a," "); sub(">",">C",a[1]); ident=a[1]"\\t100.0\\tlength="}} else {{long=length($0);if (long > 0) {{ident=ident long; print ident"\\n"$0 }} }}}}' {output.assembly} > /tmp/assembly.contigs.fa;"""
            "mv /tmp/assembly.contigs.fa {output.assembly} "


if config['SeOrPe'] == 'PE':

    rule <step_name>__get_organelle_PE:
        input:
            **<step_name>__get_organelle_PE_inputs(),
        output: 
            assembly = config["results_dir"] + "/" + config["<step_name>__get_organelle_PE_output_dir"] + "/assembly.fasta",
        params:
            input_line1 = lambda w, input: ",".join(input.read),
            input_line2 = lambda w, input: ",".join(input.read2),
            output_dir = config["results_dir"] + "/" + config["<step_name>__get_organelle_PE_output_dir"],
            command = config["<step_name>__get_organelle_PE_command"],
            PE_organelle_type = config["<step_name>__get_organelle_SE_organelle_type"],
            PE_max_rounds = config["<step_name>__get_organelle_SE_max_rounds"],
        log: 
            config["results_dir"] + "/logs/" + config["<step_name>__get_organelle_PE_output_dir"] + "/get_organelle_PE_log.txt"
        threads:  config["<step_name>__get_organelle_PE_threads"]
        conda:
            "envs/get_organelle.yaml"
        shell: 
            "{params.command} "
            "--overwrite "            
            "-1 {params.input_line1} "
            "-2 {params.input_line2} "
            "-o {params.output_dir} "
            "-t {threads} "
            "-F {params.PE_organelle_type} "
            "-R {params.PE_max_rounds}; "
            "for fasta in $(ls {params.output_dir}/*.path_sequence.fasta); "
            "do cat $fasta >> {output.assembly}; "
            "done; "
            "sed -i -E -e 's/\(|\)//g' {output.assembly};  "
            # Si l'on veut eviter l'étape cal_bwa_abundance.py qui fait un mapping couteux sur tous les scaffolds de l'assemblage :
            # convert Megahit or flye output to format similar to one produced by abundance estimate procedure with a dummy abundance of 0.1
            # N.B. assembly fasta file must be one line per sequence format
            """awk '{{posit = index($0,">"); if (posit == 1) {{split($0,a," "); sub(">",">C",a[1]); ident=a[1]"\\t100.0\\tlength="}} else {{long=length($0);if (long > 0) {{ident=ident long; print ident"\\n"$0 }} }}}}' {output.assembly} > /tmp/assembly.contigs.fa;"""
            "mv /tmp/assembly.contigs.fa {output.assembly} "

