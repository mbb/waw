rule <step_name>__GATK4_SplitNCigarReads:
    input:
        **<step_name>__GATK4_SplitNCigarReads_inputs(),
    output: 
        modified_bam = config["results_dir"] + "/" + config["<step_name>__GATK4_SplitNCigarReads_output_dir"] + "{sample}_splitNcigar.bam",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__GATK4_SplitNCigarReads_output_dir"],
        command = config["<step_name>__GATK4_SplitNCigarReads_command"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__GATK4_SplitNCigarReads_output_dir"] + "{sample}_GATK4_SplitNCigarReads_log.txt"
    threads: 1
    conda:
        "envs/gatk4.yaml"
    shell: 
        """
        samtools faidx {input.reference}
        {params.command} -R {input.reference} -I {input.bam_in} -O {output.modified_bam}
        """

