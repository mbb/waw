rule <step_name>__samtools_fixmate:
    input:
        **<step_name>__samtools_fixmate_inputs(),
    output:
        bam = config["results_dir"]+"/"+config["<step_name>__samtools_fixmate_output_dir"]+"/{sample}.matefixed.bam"
    params:
        command = "samtools sort -n " + <step_name>__samtools_fixmate_inputs()["bam_in"] + " --threads " +str(config["<step_name>__samtools_fixmate_threads"]) + " -- | "+ config["<step_name>__samtools_fixmate_command"] +" - " if config["<step_name>__samtools_fixmate_sort_by_name"] == True else config["<step_name>__samtools_fixmate_command"] + " " + <step_name>__samtools_fixmate_inputs()["bam_in"],
        addmatescore = "-m " if config["<step_name>__samtools_fixmate_addmatescore"] == True else "",
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__samtools_fixmate_output_dir"] + "/{sample}_fixmate_log.txt"
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"
    threads:
        config["<step_name>__samtools_fixmate_threads"]        
    shell:
        "{params.command} "
        "{params.addmatescore} "
        " {output.bam} "        
        "--threads {threads} "
        " |& tee {log}"
