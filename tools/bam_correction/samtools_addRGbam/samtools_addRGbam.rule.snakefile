rule <step_name>__samtools_addRGbam:
    input:
        **<step_name>__samtools_addRGbam_inputs(),
    output:
        bam = config["results_dir"]+"/"+config["<step_name>__samtools_addRGbam_output_dir"]+"/{sample}.bam"
    params:
        command = config["<step_name>__samtools_addRGbam_command"],
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__samtools_addRGbam_output_dir"] + "/{sample}_addRGbam_log.txt"
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"
    shell:
        "{params.command} "
        "-r 'ID:WAW' -r 'SM:{wildcards.sample}' "
        " -o {output.bam} "
        "{input.bam_in} "
        " |& tee {log}"
