rule <step_name>__samtools_sort_by_position:
    input:
        **<step_name>__samtools_sort_by_position_inputs(),
    output:
        bams = config["results_dir"]+"/"+config["<step_name>__samtools_sort_by_position_output_dir"]+"/{sample}.sorted.bam"
    params:
        command = config["<step_name>__samtools_sort_by_position_command"],
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__samtools_sort_by_position_output_dir"] + "/{sample}_sortedname_log.txt"
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"
    threads:
        config["<step_name>__samtools_sort_by_position_threads"]        
    shell:
        "{params.command} "
        " -o {output.bams} "
        "{input.bam_in} "
        "--threads {threads} "
        " |& tee {log}"
