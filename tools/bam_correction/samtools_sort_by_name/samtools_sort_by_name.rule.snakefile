rule <step_name>__samtools_sort_by_name:
    input:
        **<step_name>__samtools_sort_by_name_inputs(),
    output:
        bam = config["results_dir"]+"/"+config["<step_name>__samtools_sort_by_name_output_dir"]+"/{sample}.sorted.bam"
    params:
        command = config["<step_name>__samtools_sort_by_name_command"],
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__samtools_sort_by_name_output_dir"] + "/{sample}_sortedname_log.txt"
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"
    threads:
        config["<step_name>__samtools_sort_by_name_threads"]        
    shell:
        "{params.command} "
        " -o {output.bam} "
        "{input.bam_in} "
        "--threads {threads} "
        " |& tee {log}"
