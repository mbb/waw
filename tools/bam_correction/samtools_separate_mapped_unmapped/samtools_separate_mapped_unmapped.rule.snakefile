rule <step_name>__samtools_separate_mapped_unmapped:
    input:
        **<step_name>__samtools_separate_mapped_unmapped_inputs(),
    output:
        mapped_bams = config["results_dir"]+"/"+config["<step_name>__samtools_separate_mapped_unmapped_output_dir"]+"/{sample}_mapped.sorted.bam",
        unmapped_bams = config["results_dir"]+"/"+config["<step_name>__samtools_separate_mapped_unmapped_output_dir"]+"/{sample}_unmapped.sorted.bam",
    params:
        command = config["<step_name>__samtools_separate_mapped_unmapped_command"],
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__samtools_separate_mapped_unmapped_output_dir"] + "/{sample}_samtools_log.txt"
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"
    threads:
        config["<step_name>__samtools_separate_mapped_unmapped_threads"]
    message:
        "From a bam file separate mapped reads and unmapped reads in different sorted bam files : {output.mapped_bams} for reads mapped against the reference and {output.unmapped_bams} for reads that not mapped against the reference"
    shell:
        """
        samtools view -@ {threads} -b -F 4 {input.bam_in} | samtools sort -@ {threads} -o {output.mapped_bams} |& tee {log}
        samtools index -@ {threads} {output.mapped_bams}

        samtools view -@ {threads} -b -f 4 {input.bam_in} | samtools sort -@ {threads} -o {output.unmapped_bams} |& tee {log}
        samtools index -@ {threads} {output.unmapped_bams}
        """




