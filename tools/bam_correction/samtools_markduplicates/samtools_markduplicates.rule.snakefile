rule <step_name>__samtools_markduplicates:
    input:
        **<step_name>__samtools_markduplicates_inputs(),
    output:
        bam = config["results_dir"]+"/"+config["<step_name>__samtools_markduplicates_output_dir"]+"/{sample}.dedup.bam"
    params:
        command = "samtools sort " + <step_name>__samtools_markduplicates_inputs()["bam_in"] + " --threads "+ str(config["<step_name>__samtools_markduplicates_threads"])  + " -- | " + config["<step_name>__samtools_markduplicates_command"] + " - "  if config["<step_name>__samtools_markduplicates_sort_by_position"] == True else config["<step_name>__samtools_markduplicates_command"] +" "+ <step_name>__samtools_markduplicates_inputs()["bam_in"],
        remove_dup = "-r " if config["<step_name>__samtools_markduplicates_remove_dup"] == True else "",
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__samtools_markduplicates_output_dir"] + "/{sample}_markdup_log.txt"
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"
    threads:
        config["<step_name>__samtools_markduplicates_threads"]        
    shell:
        "{params.command} "
        "{params.remove_dup} "
        " {output.bam} "        
        "--threads {threads} "
        " |& tee {log}"
