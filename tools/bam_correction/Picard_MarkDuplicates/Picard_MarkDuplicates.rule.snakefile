rule <step_name>__Picard_MarkDuplicates:
    input:
        **<step_name>__Picard_MarkDuplicates_inputs(),
    output:
        sorted_bam = config["results_dir"]+"/"+config["<step_name>__Picard_MarkDuplicates_output_dir"]+"/{sample}_mapped_dedup_sorted.bam",
        bai = config["results_dir"]+"/"+config["<step_name>__Picard_MarkDuplicates_output_dir"]+"/{sample}_mapped_dedup_sorted.bam.bai",
        metric = config["results_dir"]+"/"+config["<step_name>__Picard_MarkDuplicates_output_dir"]+"/duplicate_metrics_{sample}.txt"
    #shadow:
    #    "full" # allow for the automatic removal of temporary files not removed otherwise
    params:
        output_dir = config["results_dir"]+"/"+config["<step_name>__Picard_MarkDuplicates_output_dir"]+"/",
        command = config["<step_name>__Picard_MarkDuplicates_command"],
        remove_all_duplicates = "--REMOVE_DUPLICATES true" if config["<step_name>__Picard_MarkDuplicates_remove_all_duplicates"] else "",
        m_samtools_sort = config["<step_name>__Picard_MarkDuplicates_samtools_memory"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__Picard_MarkDuplicates_output_dir"] + "/{sample}_Picard_MarkDuplicates_log.txt"
    threads:
        config["<step_name>__Picard_MarkDuplicates_threads"]
    conda:
        "envs/gatk4.yaml"
    shell:
        """
        # Mark duplicates with picard tools
        gatk MarkDuplicates -I {input.bam_in} -O {params.output_dir}{wildcards.sample}_mapped_dedup.bam -M {output.metric} {params.remove_all_duplicates} |& tee {log}
        # sort and indexing bam with samtools
        samtools sort -m {params.m_samtools_sort}G -O BAM -@ {threads} -o {output.sorted_bam} {params.output_dir}{wildcards.sample}_mapped_dedup.bam |& tee -a {log}
        samtools index -@ {threads} {output.sorted_bam} |& tee -a {log}
        """
