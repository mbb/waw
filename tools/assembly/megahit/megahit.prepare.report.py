import sys
import os
import re

stats_file_dir = sys.argv[1]
mqc_out_dir = sys.argv[2]
with open(stats_file_dir,"r") as stats_file:
                stats = stats_file.readline()

matches = re.match(r"^[\d -:]*- (\d+) contigs, total (\d+) bp, min (\d+) bp, max (\d+) bp, avg (\d+) bp, N50 (\d+) bp$",stats)
with open(mqc_out_dir,"w") as staTab:
    staTab.write("# id: 'assembly_stats'\n")
    staTab.write("# section_name: 'Megahit Assembly Stats'\n")
    staTab.write("# plot_type: 'table'\n")
    staTab.write("Stat\tValue\n")
    staTab.write("Nombre de contigs\t"+matches.group(1)+"\n")
    staTab.write("Nombre de pb total\t"+matches.group(2)+"\n")
    staTab.write("Min pb\t"+matches.group(3)+"\n")
    staTab.write("Max pb\t"+matches.group(4)+"\n")
    staTab.write("Moyenne pb\t"+matches.group(5)+"\n")
    staTab.write("N50\t"+matches.group(6))