import time

def get_readlineSE(read):
    readline = ""
    for r1 in read:
        readline += "-r "+r1+" "
    return readline

def get_readlinePE(read, read2):
    readline=""
    for r1,r2 in zip(read,read2):
            readline += "-1 "+r1+" -2 "+r2+" "
    return readline

if config["SeOrPe"] == "SE":

    rule <step_name>__megahit_SE:
        input:
            **<step_name>__megahit_SE_inputs()
        output:
            contigs = config["results_dir"] + "/" + config["<step_name>__megahit_SE_output_dir"] + "/assembly.contigs.fa"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__megahit_SE_output_dir"] + '/megahit_log.txt'
        threads:
            config["<step_name>__megahit_threads"]
        params:
            command = config["<step_name>__megahit_SE_command"],
            output_dir = config["results_dir"]+'/'+config["<step_name>__megahit_SE_output_dir"],
            min_contig_len = config["<step_name>__megahit_min_contig_len"],
            k_min = config["<step_name>__megahit_minKmer"],
            k_max = config["<step_name>__megahit_maxKmer"],
            k_step = config["<step_name>__megahit_Kstep"],
            readline = get_readlineSE(<step_name>__megahit_SE_inputs()["read"])
        conda:
            "envs/megahit.yaml"            
        shell:
            "rm -rf {params.output_dir}.tmp/ ;  "
            "{params.command} "
            "{params.readline} "
            "-t {threads} "
            "-o {params.output_dir}.tmp " #be carefull because Megahit will create a fifo in this dir (not possible for fat )
            "--out-prefix assembly "
            "--min-contig-len {params.min_contig_len} "
            "--k-min {params.k_min} "
            "--k-max {params.k_max} "
            "--k-step {params.k_step};  "
            " cp -r {params.output_dir}.tmp/* {params.output_dir}/ && rm -rf {params.output_dir}.tmp/ "
            "|& tee {log}; "
            "tail -n 2 {params.output_dir}/assembly.log | head -n 1 > {params.output_dir}/stats.txt;  "
            "sleep 5 ; "
            "python {snake_dir}/scripts/<step_name>__megahit.prepare.report.py {params.output_dir}/stats.txt {params.output_dir}/Assembly_stats_mqc.tsv |& tee {log}; "


elif config["SeOrPe"] == "PE":

    rule <step_name>__megahit_PE:
        input:
            **<step_name>__megahit_PE_inputs()
        output:
            contigs = config["results_dir"] + "/" + config["<step_name>__megahit_PE_output_dir"] + "/assembly.contigs.fa"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__megahit_PE_output_dir"] + '/megahit_log.txt'
        threads:
            config["<step_name>__megahit_threads"]
        params:
            command = config["<step_name>__megahit_PE_command"],
            output_dir = config["results_dir"]+'/'+config["<step_name>__megahit_PE_output_dir"],
            min_contig_len = config["<step_name>__megahit_min_contig_len"],
            k_min = config["<step_name>__megahit_minKmer"],
            k_max = config["<step_name>__megahit_maxKmer"],
            k_step = config["<step_name>__megahit_Kstep"],
            readline = get_readlinePE(<step_name>__megahit_PE_inputs()["read"], <step_name>__megahit_PE_inputs()["read2"])
        conda:
            "envs/megahit.yaml"    
        shell:
            "rm -rf {params.output_dir}.tmp/ ;  "   
            "{params.command} "
            "{params.readline} " 
            "-t {threads} "
            "-o {params.output_dir}.tmp "
            "--out-prefix assembly "
            "--min-contig-len {params.min_contig_len} "
            "--k-min {params.k_min} "
            "--k-max {params.k_max} "
            "--k-step {params.k_step} ;"
            " cp -r {params.output_dir}.tmp/* {params.output_dir}/ && rm -rf {params.output_dir}.tmp/ "
            "|& tee {log}; "
            "tail -n 2 {params.output_dir}/assembly.log | head -n 1 > {params.output_dir}/stats.txt ; "
            "sleep 5 ;"
            "python {snake_dir}/scripts/<step_name>__megahit.prepare.report.py {params.output_dir}/stats.txt {params.output_dir}/Assembly_stats_mqc.tsv |& tee {log}; "

           