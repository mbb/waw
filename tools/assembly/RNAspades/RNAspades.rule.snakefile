#Lancer avec un paramètre variable

#https://snakemake.readthedocs.io/en/stable/snakefiles/rules.html#parameter-space-exploration


KMER = str(config["<step_name>__RNAspades_kmer"]).replace(" ","").split(",")
KMER = list(map(lambda x: x.replace('0', 'auto'), KMER))


if config["SeOrPe"] == "SE":
    rule <step_name>__RNAspades_kmer_assembly_SE:
        input:
            **<step_name>__RNAspades_SE_inputs(),
        output:
            kmer_assembly = config["results_dir"] + "/" + config["<step_name>__RNAspades_SE_output_dir"] + "/K{kmer}/RNAspades_K{kmer}.fa",
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__RNAspades_SE_output_dir"]+"/K{kmer}",
            res_dir = config["results_dir"],
            max_memory = str(config["<step_name>__RNAspades_max_memory"]),
            ss= "" if config["<step_name>__RNAspades_Strand_Specific_Orientation"] == "unstranded" else config["<step_name>__RNAspades_Strand_Specific_Orientation"],
        threads:
            config["<step_name>__RNAspades_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__RNAspades_SE_output_dir"] + '/K{kmer}/RNAspades_log.txt'
        conda:
            "envs/spades.yaml"
        shell:
            """
            rnaspades.py -s {input.read} -o {params.output_dir} -k {wildcards.kmer} -t {threads} -m {params.max_memory} {params.ss}
            awk -v k="{wildcards.kmer}" 'BEGIN{{cnt=1}}{{if($0 ~/^>/){{print ">RNAspades_K"k"_"cnt ; cnt++}}else{{print $0}}}}' {params.output_dir}/transcripts.fasta > {output.kmer_assembly}
            """
    
    rule <step_name>__RNAspades_SE:
        input:
            expand(config["results_dir"] + "/" + config["<step_name>__RNAspades_SE_output_dir"] + "/K{kmer}/RNAspades_K{kmer}.fa", kmer = KMER),
        output:
            concatenate_assembly_file = config["results_dir"] + "/" + config["<step_name>__RNAspades_SE_output_dir"] + "/RNAspades_Concatenate_transcripts.fa",
            kmer_assembly_dir = directory(config["results_dir"] + "/" + config["<step_name>__RNAspades_SE_output_dir"] + "/All_Assembly/"),
        shell:
            """
            cat {input} > {output.concatenate_assembly_file}
            mkdir -p {output.kmer_assembly_dir}
            for i in {input}
            do
                ln -s $i {output.kmer_assembly_dir}
            done
            ln -s {output.concatenate_assembly_file} {output.kmer_assembly_dir}
            """


elif config["SeOrPe"] == "PE":
    rule <step_name>__RNAspades_kmer_assembly_PE:
        input:
            **<step_name>__RNAspades_PE_inputs(),
        output:
            kmer_assembly = config["results_dir"] + "/" + config["<step_name>__RNAspades_PE_output_dir"] + "/K{kmer}/RNAspades_K{kmer}.fa",
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__RNAspades_PE_output_dir"]+"/K{kmer}",
            res_dir = config["results_dir"],
            max_memory = str(config["<step_name>__RNAspades_max_memory"]),
            ss= "" if config["<step_name>__RNAspades_Strand_Specific_Orientation"] == "unstranded" else config["<step_name>__RNAspades_Strand_Specific_Orientation"],
        threads:
            config["<step_name>__RNAspades_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__RNAspades_PE_output_dir"] + '/K{kmer}/RNAspades_log.txt'
        conda:
            "envs/spades.yaml"
        shell:
            """
            rnaspades.py -1 {input.read} -2 {input.read2} -o {params.output_dir} -k {wildcards.kmer} -t {threads} -m {params.max_memory} {params.ss}
            awk -v k="{wildcards.kmer}" 'BEGIN{{cnt=1}}{{if($0 ~/^>/){{print ">RNAspades_K"k"_"cnt ; cnt++}}else{{print $0}}}}' {params.output_dir}/transcripts.fasta > {output.kmer_assembly}
            """
    
    rule <step_name>__RNAspades_PE:
        input:
            expand(config["results_dir"] + "/" + config["<step_name>__RNAspades_PE_output_dir"] + "/K{kmer}/RNAspades_K{kmer}.fa", kmer = KMER),
        output:
            concatenate_assembly_file = config["results_dir"] + "/" + config["<step_name>__RNAspades_PE_output_dir"] + "/RNAspades_Concatenate_transcripts.fa",
            kmer_assembly_dir = directory(config["results_dir"] + "/" + config["<step_name>__RNAspades_PE_output_dir"] + "/All_Assembly/"),
        shell:
            """
            cat {input} > {output.concatenate_assembly_file}
            mkdir -p {output.kmer_assembly_dir}
            for i in {input}
            do
                ln -s $i {output.kmer_assembly_dir}
            done
            ln -s {output.concatenate_assembly_file} {output.kmer_assembly_dir}
            """



