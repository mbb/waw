KMER = str(config["<step_name>__SOAPdenovoTrans_kmer"]).replace(" ","").split(",")



if config["SeOrPe"] == "SE":
    rule <step_name>__SOAPdenovoTrans_kmer_assembly_SE:
        input:
            **<step_name>__SOAPdenovoTrans_SE_inputs(),
        output:
            kmer_assembly = config["results_dir"] + "/" + config["<step_name>__SOAPdenovoTrans_SE_output_dir"] + "/K{kmer}/SOAPdenovoTrans_K{kmer}.fa",
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__SOAPdenovoTrans_SE_output_dir"]+"/K{kmer}",
            res_dir = config["results_dir"],
        threads:
            config["<step_name>__SOAPdenovoTrans_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__SOAPdenovoTrans_SE_output_dir"] + '/K{kmer}/SOAPdenovoTrans_log.txt'
        conda:
            "envs/soapdenovotrans.yaml"
        shell:
            """
            max=$(seqkit stats -Ta {input.read} |cut -f 8 |grep "^[0-9]" |sort|uniq|head -n 1)
            echo "maxReadLen=$max" >> {params.output_dir}/config_soap.txt
            echo "[LIB]" >> {params.output_dir}/config_soap.txt
            echo "rd_len_cutof=$max" >> {params.output_dir}/config_soap.txt
            echo "reverse_seq=0" >> {params.output_dir}/config_soap.txt
            echo "asm_flags=3" >> {params.output_dir}/config_soap.txt
            echo "map_len=32" >> {params.output_dir}/config_soap.txt
            echo "q={input.read}" >> {params.output_dir}/config_soap.txt

            SOAPdenovo-Trans-127mer all -s {params.output_dir}/config_soap.txt -K {wildcards.kmer} -o {params.output_dir}/SOAPdenovoTrans_K{wildcards.kmer} -p {threads}
            awk -v k="{wildcards.kmer}" 'BEGIN{{cnt=1}}{{if($0 ~/^>/){{print ">SOAPdenovoTrans_K"k"_"cnt ; cnt++}}else{{print $0}}}}' {params.output_dir}/SOAPdenovoTrans_K{wildcards.kmer}.scafSeq > {output.kmer_assembly}
            """
    
    rule <step_name>__SOAPdenovoTrans_SE:
        input:
            expand(config["results_dir"] + "/" + config["<step_name>__SOAPdenovoTrans_SE_output_dir"] + "/K{kmer}/SOAPdenovoTrans_K{kmer}.fa", kmer = KMER),
        output:
            concatenate_assembly_file = config["results_dir"] + "/" + config["<step_name>__SOAPdenovoTrans_SE_output_dir"] + "/SOAPdenovoTrans_Concatenate_transcripts.fa",
            kmer_assembly_dir = directory(config["results_dir"] + "/" + config["<step_name>__SOAPdenovoTrans_SE_output_dir"] + "/All_Assembly/"),
        shell:
            """
            cat {input} > {output.concatenate_assembly_file}
            mkdir -p {output.kmer_assembly_dir}
            for i in {input}
            do
                ln -s $i {output.kmer_assembly_dir}
            done
            ln -s {output.concatenate_assembly_file} {output.kmer_assembly_dir}
            """


elif config["SeOrPe"] == "PE":
    rule <step_name>__SOAPdenovoTrans_kmer_assembly_PE:
        input:
            **<step_name>__SOAPdenovoTrans_PE_inputs(),
        output:
            kmer_assembly = config["results_dir"] + "/" + config["<step_name>__SOAPdenovoTrans_PE_output_dir"] + "/K{kmer}/SOAPdenovoTrans_K{kmer}.fa",
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__SOAPdenovoTrans_PE_output_dir"]+"/K{kmer}",
            res_dir = config["results_dir"],
        threads:
            config["<step_name>__SOAPdenovoTrans_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__SOAPdenovoTrans_PE_output_dir"] + '/K{kmer}/SOAPdenovoTrans_log.txt'
        conda:
            "envs/soapdenovotrans.yaml"
        shell:
            """
            max=$(seqkit stats -Ta {input.read} {input.read2}|cut -f 8 |grep "^[0-9]" |sort|uniq|head -n 1)
            echo "maxReadLen=$max" >> {params.output_dir}/config_soap.txt
            echo "[LIB]" >> {params.output_dir}/config_soap.txt
            echo "rd_len_cutof=$max" >> {params.output_dir}/config_soap.txt
            echo "reverse_seq=0" >> {params.output_dir}/config_soap.txt
            echo "asm_flags=3" >> {params.output_dir}/config_soap.txt
            echo "map_len=32" >> {params.output_dir}/config_soap.txt
            echo "q1={input.read}" >> {params.output_dir}/config_soap.txt
            echo "q2={input.read2}" >> {params.output_dir}/config_soap.txt

            SOAPdenovo-Trans-127mer all -s {params.output_dir}/config_soap.txt -K {wildcards.kmer} -o {params.output_dir}/SOAPdenovoTrans_K{wildcards.kmer} -p {threads}
            awk -v k="{wildcards.kmer}" 'BEGIN{{cnt=1}}{{if($0 ~/^>/){{print ">SOAPdenovoTrans_K"k"_"cnt ; cnt++}}else{{print $0}}}}' {params.output_dir}/SOAPdenovoTrans_K{wildcards.kmer}.scafSeq > {output.kmer_assembly}
            """
    
    rule <step_name>__SOAPdenovoTrans_PE:
        input:
            expand(config["results_dir"] + "/" + config["<step_name>__SOAPdenovoTrans_PE_output_dir"] + "/K{kmer}/SOAPdenovoTrans_K{kmer}.fa", kmer = KMER),
        output:
            concatenate_assembly_file = config["results_dir"] + "/" + config["<step_name>__SOAPdenovoTrans_PE_output_dir"] + "/SOAPdenovoTrans_Concatenate_transcripts.fa",
            kmer_assembly_dir = directory(config["results_dir"] + "/" + config["<step_name>__SOAPdenovoTrans_PE_output_dir"] + "/All_Assembly/"),
        shell:
            """
            cat {input} > {output.concatenate_assembly_file}
            mkdir -p {output.kmer_assembly_dir}
            for i in {input}
            do
                ln -s $i {output.kmer_assembly_dir}
            done
            ln -s {output.concatenate_assembly_file} {output.kmer_assembly_dir}
            """



