if config["SeOrPe"] == "SE":
    rule <step_name>__TrinityReadNormalisation_SE:
        input:
            **<step_name>__TrinityReadNormalisation_SE_inputs(),
        output:
            r_norm = config["results_dir"] + "/" + config["<step_name>__TrinityReadNormalisation_SE_output_dir"] + "/NormalizedDataset_single.fq",
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__TrinityReadNormalisation_SE_output_dir"],
            res_dir = config["results_dir"],
            max_memory = str(config["<step_name>__TrinityReadNormalisation_max_memory"])+"G",
            ss= "" if config["<step_name>__TrinityReadNormalisation_Strand_Specific_Orientation"] == "unstranded" else config["<step_name>__TrinityReadNormalisation_Strand_Specific_Orientation"],
            max_cov=config["<step_name>__TrinityReadNormalisation_max_cov"],
        threads:
            config["<step_name>__TrinityReadNormalisation_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__TrinityReadNormalisation_SE_output_dir"] + '/trinity_normalisation_log.txt'
        conda:
            "envs/trinity.yaml"
        shell:
            """
            read=$(echo {input.read} | sed 's/ /,/g')
            insilico_read_normalization.pl --seqType fq --JM {params.max_memory} --single $read --output {params.output_dir} --CPU {threads} --max_cov {params.max_cov} {params.ss}
            mv {params.output_dir}/single.norm.fq {output.r_norm}
            """


elif config["SeOrPe"] == "PE":
    rule <step_name>__TrinityReadNormalisation_PE:
        input:
            **<step_name>__TrinityReadNormalisation_PE_inputs(),
        output:
            r1_norm = config["results_dir"] + "/" + config["<step_name>__TrinityReadNormalisation_PE_output_dir"] + "/NormalizedDataset_R1.fq",
            r2_norm = config["results_dir"] + "/" + config["<step_name>__TrinityReadNormalisation_PE_output_dir"] + "/NormalizedDataset_R2.fq",
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__TrinityReadNormalisation_PE_output_dir"],
            res_dir = config["results_dir"],
            max_memory = str(config["<step_name>__TrinityReadNormalisation_max_memory"])+"G",
            ss= "" if config["<step_name>__TrinityReadNormalisation_Strand_Specific_Orientation"] == "unstranded" else config["<step_name>__TrinityReadNormalisation_Strand_Specific_Orientation"],
            max_cov=config["<step_name>__TrinityReadNormalisation_max_cov"],
            read_parallel = "--PARALLEL_STATS" if config["<step_name>__TrinityReadNormalisation_parallel"] else "",
        threads:
            config["<step_name>__TrinityReadNormalisation_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__TrinityReadNormalisation_PE_output_dir"] + '/trinity_normalisation_log.txt'
        conda:
            "envs/trinity.yaml"
        shell:
            """
            readF=$(echo {input.read} | sed 's/ /,/g')
            readR=$(echo {input.read2} | sed 's/ /,/g')
            insilico_read_normalization.pl --seqType fq --JM {params.max_memory} --left $readF --right $readR --output {params.output_dir} --CPU {threads} --max_cov {params.max_cov} {params.ss} --pairs_together {params.read_parallel}
            mv {params.output_dir}/left.norm.fq {output.r1_norm}
            mv {params.output_dir}/right.norm.fq {output.r2_norm}
            """



