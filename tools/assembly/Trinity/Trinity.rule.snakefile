if config["SeOrPe"] == "PE":

    rule <step_name>__Trinity_PE:
        input:
            **<step_name>__Trinity_PE_inputs(),
        output:
            assembly = config["results_dir"] + "/" + config["<step_name>__Trinity_PE_output_dir"] + ".Trinity.fasta"
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__Trinity_PE_output_dir"],
            res_dir = config["results_dir"],
            max_memory = str(config["<step_name>__Trinity_max_memory"])+"G" if config["<step_name>__Trinity_max_memory"] != "" else "10G",
            normalisation = "" if config["<step_name>__Trinity_normalisation"] else "--no_normalize_reads",
            ss= "" if config["<step_name>__Trinity_Strand_Specific_Orientation"] == "unstranded" else config["<step_name>__Trinity_Strand_Specific_Orientation"],
        threads:
            config["<step_name>__Trinity_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__Trinity_PE_output_dir"] + '/trinity_log.txt'
        conda:
            "envs/trinity.yaml"
        shell:
            """
            readF=$(echo {input.read} | sed 's/ /,/g');
            readR=$(echo {input.read2} | sed 's/ /,/g');
            Trinity --seqType fq --max_memory {params.max_memory} --left $readF --right $readR --output {params.output_dir} --CPU {threads} --full_cleanup {params.normalisation} |& tee > {log}
            """


elif config["SeOrPe"] == "SE":

    rule <step_name>__Trinity_SE:
        input:
            **<step_name>__Trinity_SE_inputs(),
        output:
            assembly = config["results_dir"] + "/" + config["<step_name>__Trinity_SE_output_dir"] + ".Trinity.fasta"
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__Trinity_SE_output_dir"],
            res_dir = config["results_dir"],
            max_memory = str(config["<step_name>__Trinity_max_memory"])+"G" if config["<step_name>__Trinity_max_memory"] != "" else "10G",
            normalisation = "" if config["<step_name>__Trinity_normalisation"] else "--no_normalize_reads",
            ss= "" if config["<step_name>__Trinity_Strand_Specific_Orientation"] == "unstranded" else config["<step_name>__Trinity_Strand_Specific_Orientation"],
        threads:
            config["<step_name>__Trinity_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__Trinity_SE_output_dir"] + '/trinity_log.txt'
        conda:
            "envs/trinity.yaml"
        shell:
            """
            read=$(echo {input.read} | sed 's/ /,/g')
            Trinity --seqType fq --max_memory {params.max_memory} --single $read --output {params.output_dir} --CPU {threads} --full_cleanup {params.normalisation} |& tee > {log}
            """
