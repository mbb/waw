import time

def get_readlineSE(read):
    readline = ""
    for r1 in read:
        readline += "-r "+r1+" "
    return readline

def get_readlinePE(read, read2):
    readline=""
    for r1,r2 in zip(read, read2):
            readline += "-1 "+r1+" -2 "+r2+" "
    return readline

if config["SeOrPe"] == "SE":

    rule <step_name>__spades_SE:
        input:
            **<step_name>__spades_SE_inputs()
        output:
            contigs = config["results_dir"] + "/" + config["<step_name>__spades_SE_output_dir"] + "/contigs.fasta"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__spades_SE_output_dir"] + '/spades_log.txt'
        threads:
            config["<step_name>__spades_threads"]
        params:
            command = config["<step_name>__spades_SE_command"],
            output_dir = config["results_dir"]+'/'+config["<step_name>__spades_SE_output_dir"],
            kmin = config["<step_name>__spades_k_min"],
            kmax = config["<step_name>__spades_k_max"],
            kinc = config["<step_name>__spades_k_inc"],
            readline = get_readlineSE(<step_name>__spades_SE_inputs()["read"]);
        conda:
            "envs/spades.yaml"    
        shell:
            "klist=$(seq {params.kmin} {params.kinc} {params.kmax}); "
            "{params.command} "+
            "{params.readline} "+
            "-t {threads} "+
            "-o {params.output_dir} " +            
            "-k ${{klist}}; " +
            "awk '/^>/ {{printf(\"\\n%s\\n\",$0);next; }} {{ printf(\"%s\",$0);}}  END {{printf(\"\\n\");}}' < {params.output_dir}/contigs.fasta > {params.output_dir}/contigs.fa; " +
            "tail -n +2 {params.output_dir}/contigs.fa > {params.output_dir}/contigs.fasta; " +
            "rm -f {params.output_dir}/contigs.fa"
            

elif config["SeOrPe"] == "PE":

    rule <step_name>__spades_PE:
        input:
            **<step_name>__spades_PE_inputs()
        output:
            contigs = config["results_dir"] + "/" + config["<step_name>__spades_PE_output_dir"] + "/contigs.fasta"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__spades_PE_output_dir"] + '/spades_log.txt'
        threads:
            config["<step_name>__spades_threads"]
        params:
            command = config["<step_name>__spades_PE_command"],
            output_dir = config["results_dir"]+'/'+config["<step_name>__spades_PE_output_dir"],
            kmin = config["<step_name>__spades_k_min"],
            kmax = config["<step_name>__spades_k_max"],
            kinc = config["<step_name>__spades_k_inc"],
            readline = get_readlinePE(<step_name>__spades_PE_inputs()["read"], <step_name>__spades_PE_inputs()["read2"])
        conda:
            "envs/spades.yaml"    
        shell:
            "klist=$(seq {params.kmin} {params.kinc} {params.kmax}); "
            "{params.command} "+
            "{params.readline} "+
            "-t {threads} "+
            "-o {params.output_dir} "+
            "-k ${{klist}}; " +
            "awk '/^>/ {{printf(\"\\n%s\\n\",$0);next; }} {{ printf(\"%s\",$0);}}  END {{printf(\"\\n\");}}' < {params.output_dir}/contigs.fasta > {params.output_dir}/contigs.fa; " +
            "tail -n +2 {params.output_dir}/contigs.fa > {params.output_dir}/contigs.fasta; " +
            "rm -f {params.output_dir}/contigs.fa"
