KMER = str(config["<step_name>__TransAbyss_kmer"]).replace(" ","").split(",")



if config["SeOrPe"] == "SE":
    rule <step_name>__TransAbyss_kmer_assembly_SE:
        input:
            **<step_name>__TransAbyss_SE_inputs(),
        output:
            kmer_assembly = config["results_dir"] + "/" + config["<step_name>__TransAbyss_SE_output_dir"] + "/K{kmer}/TransAbyss_K{kmer}.fa",
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__TransAbyss_SE_output_dir"]+"/K{kmer}",
            res_dir = config["results_dir"],
            ss= "--SS" if config["<step_name>__TransAbyss_Strand_Specific_Orientation"] else "",
        threads:
            config["<step_name>__TransAbyss_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__TransAbyss_SE_output_dir"] + '/K{kmer}/TransAbyss_log.txt'
        conda:
            "envs/transabyss.yaml"
        shell:
            """
            transabyss --se {input.read} --outdir {params.output_dir} --name TransAbyss_K{wildcards.kmer} -k {wildcards.kmer} --threads {threads} {params.ss}
            awk -v k="{wildcards.kmer}" 'BEGIN{{cnt=1}}{{if($0 ~/^>/){{print ">TransAbyss_K"k"_"cnt ; cnt++}}else{{print $0}}}}' {params.output_dir}/TransAbyss_K{wildcards.kmer}-final.fa > {output.kmer_assembly}
            """
    
    rule <step_name>__TransAbyss_SE:
        input:
            expand(config["results_dir"] + "/" + config["<step_name>__TransAbyss_SE_output_dir"] + "/K{kmer}/TransAbyss_K{kmer}.fa", kmer = KMER),
        output:
            concatenate_assembly_file = config["results_dir"] + "/" + config["<step_name>__TransAbyss_SE_output_dir"] + "/TransAbyss_Concatenate_transcripts.fa",
            kmer_assembly_dir = directory(config["results_dir"] + "/" + config["<step_name>__TransAbyss_SE_output_dir"] + "/All_Assembly/"),
        shell:
            """
            cat {input} > {output.concatenate_assembly_file}
            mkdir -p {output.kmer_assembly_dir}
            for i in {input}
            do
                ln -s $i {output.kmer_assembly_dir}
            done
            ln -s {output.concatenate_assembly_file} {output.kmer_assembly_dir}
            """


elif config["SeOrPe"] == "PE":
    rule <step_name>__TransAbyss_kmer_assembly_PE:
        input:
            **<step_name>__TransAbyss_PE_inputs(),
        output:
            kmer_assembly = config["results_dir"] + "/" + config["<step_name>__TransAbyss_PE_output_dir"] + "/K{kmer}/TransAbyss_K{kmer}.fa",
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__TransAbyss_PE_output_dir"]+"/K{kmer}",
            res_dir = config["results_dir"],
            ss= "--SS" if config["<step_name>__TransAbyss_Strand_Specific_Orientation"] else "",
        threads:
            config["<step_name>__TransAbyss_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__TransAbyss_PE_output_dir"] + '/K{kmer}/TransAbyss_log.txt'
        conda:
            "envs/transabyss.yaml"
        shell:
            """
            transabyss --pe {input.read} {input.read2} --outdir {params.output_dir} --name TransAbyss_K{wildcards.kmer} -k {wildcards.kmer} --threads {threads} {params.ss}
            awk -v k="{wildcards.kmer}" 'BEGIN{{cnt=1}}{{if($0 ~/^>/){{print ">TransAbyss_K"k"_"cnt ; cnt++}}else{{print $0}}}}' {params.output_dir}/TransAbyss_K{wildcards.kmer}-final.fa > {output.kmer_assembly}
            """
    
    rule <step_name>__TransAbyss_PE:
        input:
            expand(config["results_dir"] + "/" + config["<step_name>__TransAbyss_PE_output_dir"] + "/K{kmer}/TransAbyss_K{kmer}.fa", kmer = KMER),
        output:
            concatenate_assembly_file = config["results_dir"] + "/" + config["<step_name>__TransAbyss_PE_output_dir"] + "/TransAbyss_Concatenate_transcripts.fa",
            kmer_assembly_dir = directory(config["results_dir"] + "/" + config["<step_name>__TransAbyss_PE_output_dir"] + "/All_Assembly/"),
        shell:
            """
            cat {input} > {output.concatenate_assembly_file}
            mkdir -p {output.kmer_assembly_dir}
            for i in {input}
            do
                ln -s $i {output.kmer_assembly_dir}
            done
            ln -s {output.concatenate_assembly_file} {output.kmer_assembly_dir}
            """



