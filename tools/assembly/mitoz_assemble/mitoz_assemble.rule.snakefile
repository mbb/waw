if config["SeOrPe"] == "SE":

    rule <step_name>__mitoz_assemble_SE:
        input:
            **<step_name>__mitoz_assemble_SE_inputs()
        output:
            read_assembled = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_SE_output_dir"] + "/final.contigs.fa",
            mitogenome = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_SE_output_dir"] + "/mitoz_assemble_SE.mitogenome.fa",
        log: config["results_dir"]+'/logs/' + config["<step_name>__mitoz_assemble_SE_output_dir"] + '/mitoz_assemble_log.txt'
        threads: config["<step_name>__mitoz_assemble_threads"]
        params:
            command = config["<step_name>__mitoz_assemble_SE_command"],
            output_dir = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_SE_output_dir"],
            prefix = "mitoz_assemble_SE",
            read_length = config["<step_name>__mitoz_assemble_read_length"],
            clade = config["<step_name>__mitoz_assemble_clade"],
            in_line = lambda w, input: " ".join(["--fq1 "+fq for fq in input.read]),
            assembler = config["<step_name>__mitoz_assemble_assembler"],
            kmers = config["<step_name>__mitoz_assemble_kmers"],
        conda: 
            "envs/mitoz.yaml"             
        shell:
            #enlever un eventuel dossier tmp car find_mitoscafold ne peut pas tourner s'il en existe un
            "rm -rf {params.output_dir}/* ;"
            "mkdir -p {params.output_dir} && "
            "cd {params.output_dir}; "
            "{params.command} "
            "{params.in_line} "
            "--thread_number {threads} "
            "--fastq_read_length {params.read_length} "
            "--assembler {params.assembler} "
            "--outprefix {params.prefix} "
            "--genetic_code auto " #'auto' means determined by '--clade' option.
            "--clade {params.clade} "
            "--kmers {params.kmers} "
            "--requiring_taxa {params.clade} "
            "|& tee {log}; "
            "mv {params.output_dir}/{params.assembler}/{params.assembler}_out/final.contigs.fa {output.read_assembled}; "
            "mv {params.output_dir}/{params.assembler}/mitoz_assemble_SE.{params.assembler}.mitogenome.fa {output.mitogenome}; "            



elif config["SeOrPe"] == "PE":

    rule <step_name>__mitoz_assemble_PE:
        input:
            **<step_name>__mitoz_assemble_PE_inputs()
        output:
            read_assembled = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_PE_output_dir"] + "/final.contigs.fa",
            mitogenome = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_PE_output_dir"] + "/mitoz_assemble_PE.mitogenome.fa",            
        log: config["results_dir"]+'/logs/mitoz_assemble/mitoz_assemble_log.txt'
        threads: config["<step_name>__mitoz_assemble_threads"]
        params:
            command = config["<step_name>__mitoz_assemble_PE_command"],
            output_dir = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_PE_output_dir"],
            prefix = "mitoz_assemble_PE",
            read_length = config["<step_name>__mitoz_assemble_read_length"],
            insert_size = config["<step_name>__mitoz_assemble_insert_size_PE"],
            clade = config["<step_name>__mitoz_assemble_clade"],
            #assemble = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_PE_output_dir"] + "/mitoz_assemble_PE.tmp/mitoz_assemble_PE.assembly/work71.scafSeq",
            in_line = lambda w, input: " ".join([" ".join(["--fq1 "+input.read[i],"--fq2 "+input.read2[i]]) for i in range(0,len(input.read))]),
            assembler = config["<step_name>__mitoz_assemble_assembler"],
            kmers = config["<step_name>__mitoz_assemble_kmers"],
        conda:
            "envs/mitoz.yaml"
        shell:
            #enlever un eventuel dossier tmp car find_mitoscafold ne peut pas tourner s'il en existe un
            "rm -rf {params.output_dir}/* ;"
            "mkdir -p {params.output_dir} && "
            "cd {params.output_dir}; "
            "{params.command} "
            "{params.in_line} "
            "--thread_number {threads} "
            "--assembler {params.assembler} "
            "--fastq_read_length {params.read_length} "
            "--insert_size {params.insert_size} "
            "--outprefix {params.prefix} "
            "--genetic_code auto " #'auto' means determined by '--clade' option.
            "--clade {params.clade} "
            "--kmers {params.kmers} "
            "--requiring_taxa {params.clade} "
            "|& tee {log}; "
            "mv {params.output_dir}/{params.assembler}/{params.assembler}_out/final.contigs.fa {output.read_assembled}; "
            "mv {params.output_dir}/{params.assembler}/mitoz_assemble_PE.{params.assembler}.mitogenome.fa {output.mitogenome}; "    


