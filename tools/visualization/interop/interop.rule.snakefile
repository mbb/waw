rule <step_name>__interop:
    input:
        **<step_name>__interop_inputs()
    output:
        summary = config["results_dir"]+"/"+config["<step_name>__interop_output_dir"]+"/summary.csv",
        index_summary = config["results_dir"]+"/"+config["<step_name>__interop_output_dir"]+"/index_summary_mqc.csv",
        dump_text = config["results_dir"]+"/"+config["<step_name>__interop_output_dir"]+"/dump_text.csv",
    log: config["results_dir"]+'/logs/' + config["<step_name>__interop_output_dir"] + '/interop_log.txt'
    params:
        #illumina_dir = config["<step_name>__interop_illumina_dir"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__interop_output_dir"],
    conda:
        "envs/illumina-interop.yaml"
    shell: 
        "cd {params.output_dir}; "
        "interop_summary {input.illumina_dir} | sed 's/ *,/,/g' > summary.csv " #remove unneeded whitespaces
        "&& interop_index-summary {input.illumina_dir} --csv=1 > index_summary.csv " # format as a csv for multiqc
        "&& interop_dumptext {input.illumina_dir} > dump_text.csv "
        "&& interop_imaging_table {input.illumina_dir} | sed 's/;/,/g' > imaging_table.csv "
        "&& interop_plot_qscore_heatmap {input.illumina_dir} | sed \"s/set output '.*'/set output 'Qscore_heatmap_mqc.png'/\" | gnuplot "
        "&& interop_plot_qscore_histogram {input.illumina_dir} | sed \"s/set output '.*'/set output 'Qscore_histogram_mqc.png'/\" | gnuplot "
        "&& interop_plot_by_cycle {input.illumina_dir} | sed \"s/set output '.*'/set output 'Intensity_by_cycle_mqc.png'/\" | gnuplot "
        "&& interop_plot_by_lane {input.illumina_dir} | sed \"s/set output '.*'/set output 'Cluster_count_by_lane_mqc.png'/\" | gnuplot "
        "&& interop_plot_flowcell {input.illumina_dir} | sed \"s/set output '.*'/set output 'Flowcell_intensity_mqc.png'/\" | gnuplot "
        "&& interop_plot_sample_qc  {input.illumina_dir} | sed \"s/set output '.*'/set output 'Sample_qc_mqc.png'/\" | gnuplot; "  
        "awk 'f{{print;}} /Data/{{f=1}}' {input.illumina_dir}/SampleSheet.csv > samples.csv;"
        "awk 'f{{print;}} /Index Number/{{print;f=1}}' index_summary.csv > table_summary.csv;"
        "pf=$(awk 'BEGIN{{FS=\",\"}}  f{{print $2;f=0}} /Total Reads/{{f=1}}  ' index_summary.csv );" 
        """awk 'BEGIN{{OFS="," ;FS=","}}  
           f{{for (i=3; i<=6; i++) {{$i=$i"%"}};
           print "Lane1",$0; f=0}} 
           /Total Reads/{{print "Lane",$0;f=1}}  ' index_summary.csv >  Lane1_mqc.csv ;"""
        "rm -f index_summary.csv;  "
        """awk -v pf=$pf 'BEGIN{{FS=",";OFS=","}}{{
              if (NR==FNR) {{sample[$1]=$2}} 
              else 
              {{  if(FNR==1) {{$6 = "Read Identified (PF)"; header=$0}} 
                  else 
                  {{ 
                    #check if we have only Index 1 then remove one column  
                    if (FNR==2) {{ if(NF==5){{gsub(/Index 2 \(I5\),/, "" , header)}} ; print header}} 
                    $1=$2"-"sample[$2];
                    $NF=int($NF*pf/100);
                    print $0 
                  }}
              }}
              }}' samples.csv table_summary.csv > index_summary_mqc.csv; """