rule <step_name>__jellyfish_histo:
    input:
        **<step_name>__jellyfish_histo_inputs()
    output:
        kmer_histo = config["results_dir"] + "/" + config["<step_name>__jellyfish_histo_output_dir"] + "/kmer_histo_jf.hist",    
    threads:
        config["<step_name>__jellyfish_histo_threads"]
    params:
        command = config["<step_name>__jellyfish_histo_command"],
        high_count = config["<step_name>__jellyfish_high_count"],
    conda:
        "envs/kmc-jellyfish-smudgeplot.yaml"
    shell:
        "{params.command} "
        "-t {threads} "
        "-h {params.high_count} " 
        "{input.kmer_counts} > {output.kmer_histo} "