rule <step_name>__muscle:
    input:
        **<step_name>__muscle_inputs(),
    output: 
        aligned_fasta = config["results_dir"] + "/" + config["<step_name>__muscle_output_dir"] + "/output.afa",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__muscle_output_dir"]+ "/",
        command = config["<step_name>__muscle_command"],
        consiters = config["<step_name>__muscle_consiters"],
        refineiters = config["<step_name>__muscle_refineiters"],
    threads: 
        config["<step_name>__muscle_threads"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__muscle_output_dir"] + "/muscle_log.txt"
    conda:
       "envs/muscle.yaml"    
    shell: 
        "{params.command} "
        "-align {input.fasta} "
        "-output {output.aligned_fasta} "
        "-consiters {params.consiters} "
        "-refineiters {params.refineiters} "
        "-threads {threads} "
        "-log {log} "
