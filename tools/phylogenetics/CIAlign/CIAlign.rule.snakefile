rule <step_name>__CIAlign:
    input:
        **<step_name>__CIAlign_inputs(),
    output: 
        cleaned_alignment = config["results_dir"] + "/" + config["<step_name>__CIAlign_output_dir"] + "/CIAlign_cleaned.fasta",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__CIAlign_output_dir"]+ "/",
        command = config["<step_name>__CIAlign_command"],
        remove_divergent = "--remove_divergent " if  config["<step_name>__CIAlign_remove_divergent"] else " ",
        remove_divergent_minperc = " --remove_divergent_minperc " + str(config["<step_name>__CIAlign_remove_divergent_minperc"]) if config["<step_name>__CIAlign_remove_divergent"] else " ",
        remove_insertions = "--remove_insertions  " if  config["<step_name>__CIAlign_remove_insertions"] else " ",

        insertion_min_size = " --insertion_min_size " + str(config["<step_name>__CIAlign_insertion_min_size"]) if config["<step_name>__CIAlign_remove_insertions"] else " ",

        insertion_max_size = " --insertion_max_size " + str(config["<step_name>__CIAlign_insertion_max_size"]) if config["<step_name>__CIAlign_remove_insertions"] else " ",

        insertion_min_flank = " --insertion_min_flank " + str(config["<step_name>__CIAlign_insertion_min_flank"]) if config["<step_name>__CIAlign_remove_insertions"] else " ",

        crop_ends = "--crop_ends  " if  config["<step_name>__CIAlign_crop_ends"] else " ",

        crop_ends_mingap_perc = " --crop_ends_mingap_perc " + str(config["<step_name>__CIAlign_crop_ends_mingap_perc"]) if config["<step_name>__CIAlign_crop_ends"] else " ",

        crop_ends_redefine_perc = " --crop_ends_redefine_perc " + str(config["<step_name>__CIAlign_crop_ends_redefine_perc"]) if config["<step_name>__CIAlign_crop_ends"] else " ",

        remove_short = "--remove_short  " if  config["<step_name>__CIAlign_remove_short"] else " ",

        remove_min_length = " --remove_min_length " + str(config["<step_name>__CIAlign_remove_min_length"]) if config["<step_name>__CIAlign_remove_short"] else " ",

        keep_gaponly = "--keep_gaponly  " if  config["<step_name>__CIAlign_keep_gaponly"] else " ",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__CIAlign_output_dir"] + "/CIAlign_log.txt"
    conda:
       "envs/cialign.yaml"    
    shell: 
        "{params.command} "
        "--infile {input.align_file} "
        "--outfile_stem {params.output_dir}/CIAlign "
        "{params.remove_divergent} "
        "{params.remove_divergent_minperc} "
        "{params.remove_insertions} "
        "{params.insertion_min_size} "
        "{params.insertion_max_size} "
        "{params.insertion_min_flank} "
        "{params.crop_ends} "
        "{params.crop_ends_redefine_perc} "
        "{params.remove_short} "
        "{params.remove_min_length} "
        "{params.keep_gaponly} "
        "--keep_gaponly --plot_output --plot_markup "
        "|& tee {log}; "
        "mv {params.output_dir}/CIAlign_markup.png {params.output_dir}/CIAlign_markup_mqc.png; " 
        "mv {params.output_dir}/CIAlign_output.png {params.output_dir}/CIAlign_output_mqc.png; " 
        "mv {params.output_dir}/CIAlign_markup_legend.png {params.output_dir}/CIAlign_markup_legend_mqc.png; " 
