rule <step_name>__gblocks:
    input:
        **<step_name>__gblocks_inputs(),
    output: 
        clean_fasta = config["results_dir"] + "/" + config["<step_name>__gblocks_output_dir"] + "/out.fasta-gb",
        report = config["results_dir"] + "/" + config["<step_name>__gblocks_output_dir"] + "/gblocks_report_mqc.html",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__gblocks_output_dir"]+ "/",
        command = config["<step_name>__gblocks_command"],
        seqtype = config["<step_name>__gblocks_seqtype"],
        b1 = "-b1=" + str(config["<step_name>__gblocks_b1"]) if str(config["<step_name>__gblocks_b1"]) != 'NA' else "",
        b2 = "-b2=" + str(config["<step_name>__gblocks_b2"]) if str(config["<step_name>__gblocks_b2"]) != 'NA' else "",
        b3 = "-b3=" + str(config["<step_name>__gblocks_b3"]) if str(config["<step_name>__gblocks_b3"]) != 'NA' else "",
        b4 = "-b4=" + str(config["<step_name>__gblocks_b4"]) if str(config["<step_name>__gblocks_b4"]) != 'NA' else "",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__gblocks_output_dir"] + "/gblocks_log.txt"
    threads: 1
    conda:
        "envs/gblocks.yaml"
    shell: 
        "cd {params.output_dir} && "
        "ln -s {input.fasta} out.fasta && "
        "set +e ;{params.command} "
        "out.fasta "
        "-t={params.seqtype} "
        "{params.b1} "
        "{params.b2} "
        "{params.b3} "
        "{params.b4} "
        "-p=y ;"
        " exitcode=$? ; "
        " if [ $exitcode -le 1 ]; then  exit 0;  else  exit $exitcode;  fi "
        "|& tee {log} "
        "&& mv out.fasta-gb.htm gblocks_report_mqc.html "
        #"&& unlink out.fasta"
#gblocks return an exit code of 1 even if is in success so force exit code to 0 :
# see  https://snakemake.readthedocs.io/en/stable/project_info/faq.html#i-don-t-want-snakemake-to-detect-an-error-if-my-shell-command-exits-with-an-exitcode-1-what-can-i-do