rule <step_name>__beast:
    input:
        **<step_name>__beast_inputs(),
    output: 
        beast_trees = config["results_dir"] + "/" + config["<step_name>__beast_output_dir"] + "/beast.trees",
        log = config["results_dir"] + "/" + config["<step_name>__beast_output_dir"] + "/beast.log",
    params: 
        command = config["<step_name>__beast_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__beast_output_dir"]+ "/",
        prefix = "beast",
        seed = config["<step_name>__beast_seed"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__beast_output_dir"] + "/beast_log.txt"
    threads: 
        config["<step_name>__beast_threads"]
    conda:
        "envs/beast2.yaml"    
    shell: 
        """
        cd {params.output_dir}; 
        {params.command} \
        -overwrite  \
        -prefix {params.prefix} \
        -threads {threads} \
        -seed {params.seed} \
        {input.beast_xml} \
        |& tee {log};
        mv *.log beast.log; 
        mv *.trees beast.trees;
        #mv *.ops beast.ops;
        treeannotator beast.trees annotatedTree.nex; 
        #https://github.com/MullinsLab/figtree-enhanced-command-line; 
        java -jar /workflow/scripts/beast_figtree.jar -colors extract -graphic PNG -height 1024 -width 1024 annotatedTree.nex annotatedTree.nex_mqc.png;
        convert annotatedTree.nex_mqc.png -fuzz 25%  -opaque white -flatten annotatedTree.nex_mqc.png;
        """