rule <step_name>__iqtree:
    input:
        **<step_name>__iqtree_inputs(),
    output: 
        treefile = config["results_dir"] + "/" + config["<step_name>__iqtree_output_dir"] + "/out.treefile",
        report = config["results_dir"] + "/" + config["<step_name>__iqtree_output_dir"] + "/IQ-TREE_report_mqc.html"
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__iqtree_output_dir"]+ "/",
        command = config["<step_name>__iqtree_command"],
        seed = "-seed " + str(config["<step_name>__iqtree_seed"]) if config["<step_name>__iqtree_seed"] != -1 else "",
        model = config["<step_name>__iqtree_model"],
        bootstrap = config["<step_name>__iqtree_bootstrap"] if config["<step_name>__iqtree_bootstrap"] != "none" else "",
        bootstrap_val = config["<step_name>__iqtree_bootstrap_val"] if config["<step_name>__iqtree_bootstrap"] != "none" else "",
        partition_file = config["<step_name>__iqtree_partition_file"] if config["<step_name>__iqtree_partition_file"] != "" else "",
        iqtree_edge_status = config["<step_name>__iqtree_edge_status"] if config["<step_name>__iqtree_partition_file"] != "" else ""
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__iqtree_output_dir"] + "/iqtree_log.txt"
    conda:
        "envs/iqtree.yaml"    
    shell:
        "cd {params.output_dir} && "
        "ln -sf {input.sequence_file} seq.fa; "
        "{params.command} "
        "-s seq.fa "
        "-m {params.model} "
        "{params.iqtree_edge_status} {params.partition_file} "
        "{params.bootstrap} {params.bootstrap_val} "
        "-nt AUTO "
        "-pre out "
        "{params.seed} "
        "|& tee {log}; "
        "unlink seq.fa; "
        "sed '1 i\<pre>' out.iqtree > {output.report}"


