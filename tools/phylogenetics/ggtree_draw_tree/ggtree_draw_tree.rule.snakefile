rule <step_name>__ggtree_draw_tree:
    input:
        **<step_name>__ggtree_draw_tree_inputs(),
    output: 
        tree_image = config["results_dir"] + "/" + config["<step_name>__ggtree_draw_tree_output_dir"] + "/Tree_visualisation_mqc.png",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__ggtree_draw_tree_output_dir"]+ "/",
        command = config["<step_name>__ggtree_draw_tree_command"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__ggtree_draw_tree_output_dir"] + "/ggtree_draw_tree_log.txt"
    conda:
        "envs/ggtree.yaml"
    shell: 
        """ 
         Rscript -e  ' library(ape);library(ggtree);library(ggplot2); \
           tr = read.tree("{input.tree_file}"); \
           pp = read.table("{input.popmap}", header=F, sep="\\t"); \
           grp = split(pp$V1, as.factor(pp$V2), drop=T); \
           tree = groupOTU(tr, grp); \
           png(file="{output.tree_image}", width=1200, height=1200); \
           p = ggtree(tree, aes(color=group), size=2, linetype=1) + geom_tiplab(fontface = "bold", family= "Times", text = element_text(size=30)); \
           p; \
           dev.off() '         |& tee {log}
        """
