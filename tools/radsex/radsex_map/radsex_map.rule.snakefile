rule <step_name>__radsex_map:
        input:
            **<step_name>__radsex_map_inputs()
        output:
            map_results = config["results_dir"] + "/" + config["<step_name>__radsex_map_output_dir"] + "/map_results.tsv",
            signi_map_results = config["results_dir"] + "/" + config["<step_name>__radsex_map_output_dir"] + "/signi_map_results_mqc.tsv",
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__radsex_map_output_dir"] + '/radsex_log.txt'
        # threads:
        #     config["<step_name>__radsex_threads"]
        params:
            command = config["<step_name>__radsex_map_command"],
            outDir=config["results_dir"] + "/" + config["<step_name>__radsex_map_output_dir"] + "/",
            min_quality = config["<step_name>__radsex_map_min_quality"],
            min_frequency = config["<step_name>__radsex_map_min_frequency"],
            min_depth = config["<step_name>__radsex_map_min_depth"],
            signif_threshold = config["<step_name>__radsex_map_signif_threshold"],
        shell:
            "rm -rf {params.outDir}/*.png ; " +
            "{params.command} "+
            "--markers-file {input.markers_table} "+
            "--popmap {input.popmap_file} "+
            "--genome-file {input.genome_fasta} "+
            "--output-file {output.map_results} "+
            "--min-depth {params.min_depth} " +
            "--min-quality {params.min_quality} "+
            "--min-frequency {params.min_frequency} "+
            #"--groups F,M " +
            "--signif-threshold {params.signif_threshold}; "+ 
            "grep -i \"true\|Signif\" {output.map_results} | grep -v \"#\" | awk '{{print $4,$1,$2,$3,$5,$6,$7,$8}}' >  {output.signi_map_results}"