rule <step_name>__radsex_process:
        input:
            **<step_name>__radsex_process_inputs()
        output:
           markers_table = config["results_dir"] + "/" + config["<step_name>__radsex_process_output_dir"] + "/markers_table.tsv"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__radsex_process_output_dir"] + '/radsex_log.txt'
        threads:
            config["<step_name>__radsex_threads"]
        params:
            command = config["<step_name>__radsex_process_command"],
            min_depth = config["<step_name>__radsex_min_depth"]
        shell:
            "{params.command} "+ # Attention, radsex demande un dossier de reads et pas une liste de reads
            "-i {input.read_dir} "+
            "-T {threads} "+
            "-o {output} "+
            "-d {params.min_depth}"
