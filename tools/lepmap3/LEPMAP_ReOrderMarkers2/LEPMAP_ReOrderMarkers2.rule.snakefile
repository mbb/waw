checkpoint <step_name>__re_l_lg:
    input:
        **<step_name>__LEPMAP_ReOrderMarkers2_inputs(),
    output:
        dir=directory(config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/list_lg/")
    conda:
        "envs/lepmap.yaml"
    shell:
        """
        mkdir -p {output.dir}
        x=$(grep -v "^#" {input.map_tsv} | cut -f 1 | sort |uniq |grep -vw "0" | tr "\\n" " ")
        for lg in $x; do touch {output.dir}/$lg.txt; done
        """

rule <step_name>__LEPMAP_ReOrderMarkers2_byLG:
    input:
        list_lg = config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/list_lg/" + "{lg}.txt",
        **<step_name>__LEPMAP_ReOrderMarkers2_inputs(),
    output: 
        reorder_distances = config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/{lg}/ReOrderMarkers_ONmap_{lg}.tsv",
        reorder_distances_physicInf = config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/{lg}/ReOrderMarkers_ONmap_{lg}_PhysicalPosition.tsv",
        recombines_lg = config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/{lg}/ReOrderMarkers_{lg}_recombination.tsv",
        linkage_mapping = config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/{lg}/Linkage_Mapping_{lg}.png",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/",
        mrecombination = config["<step_name>__LEPMAP_ReOrderMarkers2_recombination_male"],
        frecombination = config["<step_name>__LEPMAP_ReOrderMarkers2_recombination_female"],
        error = config["<step_name>__LEPMAP_ReOrderMarkers2_minError"],
        iteration = config["<step_name>__LEPMAP_ReOrderMarkers2_MergeIteration"],
        mapping = config["<step_name>__LEPMAP_ReOrderMarkers2_mapping"],
        phased = "phasedData=1" if config["<step_name>__LEPMAP_ReOrderMarkers2_phased"] else "",
        phaseout = config["<step_name>__LEPMAP_ReOrderMarkers2_phasedOutput"],
    log:
        config["results_dir"] + "/logs/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/{lg}/ReOrderMarkers2_{lg}_log.txt"
    threads:
        config["<step_name>__LEPMAP_ReOrderMarkers2_threads"]
    conda:
        "envs/lepmap.yaml"
    shell: 
        """
        # Regenerate evaluate order file by LG (necessary for evaluate order to have a file by LG)
        awk -v clg={wildcards.lg} '{{
            if($0 ~ /^#java OrderMarkers2/){{
                match($0, /.* chromosome=([0-9]+) /,q);
                lg=q[1];
                if(clg==lg){{
                    print;
                }}
            }}else{{
                if(clg==lg){{
                    print;
                }}
            }}
        }}' {input.distances_filtered} > {params.output_dir}/{wildcards.lg}/ordered_filtered_{wildcards.lg}.txt


        # Order the marker within each LG based on map file
        cmd=$(echo $CONDA_PREFIX"/bin/lepmap3/bin")
        java -cp $cmd OrderMarkers2 evaluateOrder={params.output_dir}/{wildcards.lg}/ordered_filtered_{wildcards.lg}.txt data={input.calls} map={input.map_tsv} recombination1={params.mrecombination} recombination2={params.frecombination} minError={params.error} chromosome={wildcards.lg} numThreads={threads} {params.mapping} numMergeIterations={params.iteration} {params.phased} {params.phaseout} > {output.reorder_distances} 2> {log}

        rm {params.output_dir}/{wildcards.lg}/ordered_filtered_{wildcards.lg}.txt

        # Determine number of recombination for each sample by LG
        grep "recombin" {log} | awk -v lg={wildcards.lg} -F "\t" '{{print $0"\\t"lg}}' > {output.recombines_lg}

        # Add physical information on ordered genetic map

        sed -i -e 's/LG \= 0/LG \= {wildcards.lg}/g' {output.reorder_distances}

        awk -v lg={wildcards.lg} -F "\\t" 'fname != FILENAME {{ fname = FILENAME; idx++ }}
            BEGIN{{
                nline=0;
            }}
            idx == 1{{
                if($0 !~ /^CHR/){{
                    if($0 !~ /^#/){{
                        nline++;
                        physical_pos[nline]=$1"\\t"$2;
                    }}
                }}
            }}
            idx == 2{{
                if($0 !~ /^#/){{
                    if($1 in physical_pos){{
                        print lg"\\t"physical_pos[$1]"\\t"$0
                    }}
                }}else{{
                    if($0~/^#marker_number/){{
                        sub(/^#/, "", $0);
                        print "#LG\\tCHR\\tPOS\\t"$0;
                    }}else{{
                        print $0;
                    }}
                }}
            }}' {input.calls} {output.reorder_distances} > {output.reorder_distances_physicInf}
        
        java -cp $cmd LMPlot {output.reorder_distances} > {params.output_dir}/{wildcards.lg}/Linkage_Mapping_{wildcards.lg}.dot


        dot -Tpng {params.output_dir}/{wildcards.lg}/Linkage_Mapping_{wildcards.lg}.dot -o {output.linkage_mapping}


        magick {output.linkage_mapping} -resize 750x750 -size 850x782 xc:white +swap -gravity center -composite -gravity north -pointsize 24 -annotate +0+50 "Linkage Mapping of LG = {wildcards.lg} after second round OrderMarkers2" {output.linkage_mapping}
        """

def aggregate_input_ReOrderMarkers2(wildcards):
    checkpoint_output = checkpoints.<step_name>__re_l_lg.get(**wildcards).output[0]
    reorder_distances = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/{lg}/ReOrderMarkers_ONmap_{lg}.tsv", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    reorder_distances_physicInf = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/{lg}/ReOrderMarkers_ONmap_{lg}_PhysicalPosition.tsv", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    reorder_recombine = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/{lg}/ReOrderMarkers_{lg}_recombination.tsv", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    linkage_mapping = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/{lg}/Linkage_Mapping_{lg}.png", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    return reorder_distances, reorder_distances_physicInf, reorder_recombine, linkage_mapping


rule <step_name>__LEPMAP_ReOrderMarkers2:
    input:
        reorder_distances = lambda wildcards: aggregate_input_ReOrderMarkers2(wildcards)[0],
        reorder_distances_physicInf = lambda wildcards: aggregate_input_ReOrderMarkers2(wildcards)[1],
        recombine_summary = lambda wildcards: aggregate_input_ReOrderMarkers2(wildcards)[2],
        linkage_mapping = lambda wildcards: aggregate_input_ReOrderMarkers2(wildcards)[3],
    output:
        reorder_distances = config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/ReOrderMarkers_ONmap_All.tsv",
        reorder_distances_physicInf = config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/ReOrderMarkers_ONmap_All_PhysicalPosition.tsv",
        recombine_summary = config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/ReOrderMarkers_Recombination_summary.tsv",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__LEPMAP_ReOrderMarkers2_output_dir"] + "/",
    conda:
        "envs/lepmap.yaml"
    shell:
        """
        cat $(echo "{input.reorder_distances}" | tr " " "\\n" |sort -V | tr "\\n" " ") > {output.reorder_distances}
        cat $(echo "{input.reorder_distances_physicInf}" | tr " " "\\n" |sort -V | tr "\\n" " ") > {output.reorder_distances_physicInf}

        magick $(echo "{input.linkage_mapping}" | tr " " "\\n" |sort -V | tr "\\n" " ") -append {params.output_dir}/ReOrderMarkers_Linkage_mapping_All_mqc.png

        cat $(echo "{input.recombine_summary}" | tr " " "\\n" |sort -V | tr "\\n" " ") > {params.output_dir}/All_recombines.txt
        
        awk -F "\\t" 'BEGIN{{
            lg=""
            maxall=0
            sumall=0
        }}
        {{
            if($0!~/^#/){{
                if($0~/^number/){{
                    match($1, /number of recombinations = ([0-9]+)/, q);
                    nrecombination[$2]=q[1];
                    if(q[1]>maxall){{
                        maxall=q[1];
                    }}
                    sumall=sumall+q[1]
                }}else{{
                    nrecomb_by_progeny[$3][$NF]=$5;
                    if ($3 in family == 0){{
                        family[$3]=$2
                    }}
                    if(lg!=""){{
                        if($NF>lg){{
                            lg=$NF
                        }}
                    }}else{{
                        lg=$NF
                    }}
                }}
            }}
        }}END{{
            header_lg=""
            all_recombination=""
            for(l=1;l<=lg;l++){{
                header_lg=header_lg"\\tLG"l
                all_recombination=all_recombination"\\t"nrecombination[l]
            }}
            print "family\\tsample\\tmean\\tmax"header_lg >"{output.recombine_summary}";
            for(progeny in family){{
                lg_to_print=""
                max=0
                nlg=0
                sumlg=0
                for(l=1;l<=lg;l++){{
                    if(nrecomb_by_progeny[progeny][l] != 0){{
                        lg_to_print=lg_to_print"\\t"nrecomb_by_progeny[progeny][l]
                        if (nrecomb_by_progeny[progeny][l] > max){{
                            max=nrecomb_by_progeny[progeny][l]
                        }}
                        nlg=nlg+1
                        sumlg=sumlg+nrecomb_by_progeny[progeny][l]
                    }}else{{
                        lg_to_print=lg_to_print"\\tNA"
                    }}
                }}
                line_to_print=family[progeny]"\\t"progeny"\\t"int(sumlg/nlg*100+0.5)/100"\\t"max""lg_to_print;
                print line_to_print;
            }}
            print "Total\\tNumber_of_recombinations\\t"int(sumall/lg*100+0.5)/100"\\t"maxall""all_recombination >"{params.output_dir}/tail.txt";
        }}' {params.output_dir}/All_recombines.txt |sort -V -k1,1 -k2,2 | cat - {params.output_dir}/tail.txt >> {output.recombine_summary}

        rm {params.output_dir}/tail.txt {params.output_dir}/All_recombines.txt
        """

