import oyaml as yaml
import sys
import os
from collections import OrderedDict
from collections import defaultdict
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pandas as pd
import itertools


################### GRAPH 1


# Read global_stats_LG_by_LOD.txt file to plot data
x=[]
y=[]
y2=[]

nline=0
with open(sys.argv[1], 'r') as stat_LG:
    for line in stat_LG:
        nline+=1
        if nline != 1:
            line = line.strip()
            tdecoupe1=line.split("\t")
            x.append(tdecoupe1[0])
            y.append(tdecoupe1[1])
            y2.append(tdecoupe1[3])


# Create figure with secondary y-axis
y = [eval(i) for i in y]
y2 = [eval(i) for i in y2]


fig = make_subplots(specs=[[{"secondary_y": True}]])

fig.add_trace(
    go.Scatter(x=x, y=y2, name="nMarkers"),
    secondary_y=False,
)

fig.add_trace(
    go.Scatter(x=x, y=y, name="nLG"),
    secondary_y=True,
)

# Add figure title
fig.update_layout(
    title_text="For tested LOD values, the average number of markers in LG and the number of LG", title_x=0.5
)

# Set x-axis title
fig.update_xaxes(title_text="Applied LOD")

# Set y-axes titles
fig.update_yaxes(title_text="Average Number of Markers", secondary_y=False, title_font_color="blue")
fig.update_yaxes(title_text="Number of linkage groups", secondary_y=True, title_font_color="red")

fig.write_image(os.path.splitext(sys.argv[1])[0]+"_mqc.png", width=1200, height=800)




################### GRAPH 2

df1=pd.read_table(sys.argv[2])


combinations = list(itertools.product(df1['LOD'].unique(), df1['LG'].unique()))
combinations_df = pd.DataFrame(combinations, columns=['LOD', 'LG'])
df1 = pd.merge(combinations_df, df1, on=['LOD', 'LG'], how='left')

fig = go.Figure(data=go.Heatmap(
    z=df1.Number_Markers_on_LG,
    x=df1.LG,
    y=df1.LOD,
    text=df1.Number_Markers_on_LG,
    colorscale='Viridis', # Choose a colorscale that suits your data
))

fig.update_xaxes(tickson='labels')
fig.update_xaxes(tickmode='array', tickvals=list(range(len(df1.LG))))

fig.update_yaxes(tickson='labels')
fig.update_yaxes(tickmode='array', tickvals=list(range(len(df1.LOD))))


fig.update_layout(
    title='nMarkers by LG and LOD',
    xaxis_title='LG',
    yaxis_title='LOD',
    title_x=0.5
)

fig.write_image(os.path.splitext(sys.argv[2])[0]+"_mqc.png", width=1200, height=800)