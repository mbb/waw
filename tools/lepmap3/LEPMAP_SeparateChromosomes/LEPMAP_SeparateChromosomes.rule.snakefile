LOD = list(range(config["<step_name>__LEPMAP_SeparateChromosomes_LOD_min"], config["<step_name>__LEPMAP_SeparateChromosomes_LOD_max"]+1))


rule <step_name>__LEPMAP_SeparateChromosomes_LOD:
    input:
        **<step_name>__LEPMAP_SeparateChromosomes_inputs(),
    output: 
        map_tsv_lod = config["results_dir"] + "/" + config["<step_name>__LEPMAP_SeparateChromosomes_output_dir"] + "/LOD{lod}/map_LOD{lod}.txt",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__LEPMAP_SeparateChromosomes_output_dir"] + "/LOD{lod}/",
        mtheta = config["<step_name>__LEPMAP_SeparateChromosomes_male_theta"],
        ftheta = config["<step_name>__LEPMAP_SeparateChromosomes_female_theta"],
        size = config["<step_name>__LEPMAP_SeparateChromosomes_sizeLimit"],
        phased = "phasedData=1" if config["<step_name>__LEPMAP_SeparateChromosomes_phased"] else "",
        distortionLOD = "distortionLod=1" if config["<step_name>__LEPMAP_SeparateChromosomes_distortionLOD"] else "",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__LEPMAP_SeparateChromosomes_output_dir"] + "/LOD{lod}/SeparateChromosomes_log.txt"
    threads:
        config["<step_name>__LEPMAP_SeparateChromosomes_threads"]
    conda:
        "envs/lepmap.yaml"
    shell: 
        """
        # Generate data.call format based on pedigree file and vcf file
        cmd=$(echo $CONDA_PREFIX"/bin/lepmap3/bin")
        java -cp $cmd SeparateChromosomes2 data={input} lodLimit={wildcards.lod} maleTheta={params.mtheta} femaleTheta={params.ftheta} sizeLimit={params.size} numThreads={threads} {params.phased} {params.distortionLOD} > {output.map_tsv_lod} 2> {log}
        """

rule <step_name>__LEPMAP_SeparateChromosomes:
    input:
        expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_SeparateChromosomes_output_dir"] + "/LOD{lod}/map_LOD{lod}.txt", lod = LOD),
    output:
        map_tsv = config["results_dir"] + "/" + config["<step_name>__LEPMAP_SeparateChromosomes_output_dir"] + "/map_tsv_global.txt",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__LEPMAP_SeparateChromosomes_output_dir"] + "/",
    conda:
        "envs/lepmap.yaml"
    shell:
        """
        echo -e "LOD\tNumber_of_LinkageGroup\tNumber_of_Marker\tMean_of_Marker_by_LG" > {params.output_dir}/global_stats_LG_by_LOD.txt
        echo -e "LOD\tLG\tNumber_Markers_on_LG" > {params.output_dir}/Repartion_Markers_by_LG.txt
        for i in {input}
        do
            elod=$(echo $i | awk -F "/" '{{print $(NF-1)}}')
            grep -v "^#" $i | awk -v lod=$elod 'BEGIN{{total_lg=0; total_marker=0;}}{{if ($1 in sum){{sum[$1]++;if($1 !~ /^0$/){{total_marker++}}}}else{{sum[$1]=1;if($1 !~ /^0$/){{total_lg++;total_marker++}}}}}}END{{mean=total_marker/total_lg; print lod"\\t"total_lg"\\t"total_marker"\\t"mean >> "{params.output_dir}global_stats_LG_by_LOD.txt"; for(j=0;j<=total_lg-1;j++){{if(j !~ /^0$/){{print lod"\\t"j"\\t"sum[j]}}else{{print lod"\\tNot_assign_to_LG\\t"sum[j]}}}}}}' >> {params.output_dir}/Repartion_Markers_by_LG.txt
            echo -e "$elod\t$i" >> {output.map_tsv}
        done
        python {snake_dir}/scripts/<step_name>__LEPMAP_SeparateChromosomes.prepare.report.py {params.output_dir}/global_stats_LG_by_LOD.txt {params.output_dir}/Repartion_Markers_by_LG.txt
        """

