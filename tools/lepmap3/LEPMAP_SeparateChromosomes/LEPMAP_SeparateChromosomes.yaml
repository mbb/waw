{
  id: LEPMAP_SeparateChromosomes,
  name: LEPMAP_SeparateChromosomes,
  article: 10.1093/bioinformatics/btx494,
  website: "https://sourceforge.net/p/lep-map3/wiki/LM3%20Home/",
  git: "https://github.com/pdimens/LepWrap/tree/main",
  description: "Linkage map construction software : Assign marker with relative linkage into chromosome based on Lod score.",
  version: "0.5",
  documentation: "https://sourceforge.net/p/lep-map3/wiki/LM3%20Home/#lep-map3-documentation",
  multiqc: "custom",
  commands:
    [
      {
        name: LEPMAP_SeparateChromosomes,
        cname: "LEPMAP SeparateChromosomes",
        command: SeparateChromosomes2,
        category: "lepmap3",
        output_dir: AssignMarker2LinkageGroup,
        inputs: [
          { name: calls, type: "lepmap3_calls", description: "Lepmap call format : Pedigree and vcf file merge" },
        ],
        outputs: [
          { name: map_tsv, type: "tsv", description: "map file : marker and his associate linkage group" },
        ],
        options:
          [
            {
              name: LEPMAP_SeparateChromosomes_threads,
              prefix: "numThreads",
              type: "numeric",
              min: "0",
              max: "100",
              step: "1",
              value: "4",
              label: "Number of threads.",
            },
            {
              name: LEPMAP_SeparateChromosomes_LOD_min,
              prefix: 'lodLimitmin',
              type: numeric,
              min: "1",
              max: "100",
              step: "1",
              value: "1",
              label: "Minimum Lod score limit to test. All value with a step of 1 between this value and maximum lod score will be test as Lod score limit. Therefore set same value in minimum and maximum if you want to test only one value as Lod score limit.",
            },
            {
              name: LEPMAP_SeparateChromosomes_LOD_max,
              prefix: 'lodLimitmax',
              type: numeric,
              min: "1",
              max: "100",
              step: "1",
              value: "25",
              label: "Maximum Lod score limit to test. All value with a step of 1 between minimum lod score and this value will be test as Lod score limit. Therefore set same value in minimum and maximum if you want to test only one value as Lod score limit.",
            },
            {
              name: LEPMAP_SeparateChromosomes_male_theta,
              prefix: "maleTheta",
              type: "numeric",
              min: "0",
              max: "1",
              step: "0.01",
              value: "0.03",
              label: "Fixed recombination fraction for male.",
            },
            {
              name: LEPMAP_SeparateChromosomes_female_theta,
              prefix: "femaleTheta",
              type: "numeric",
              min: "0",
              max: "1",
              step: "0.01",
              value: "0.03",
              label: "Fixed recombination fraction for female.",
            },
            {
              name: LEPMAP_SeparateChromosomes_sizeLimit,
              prefix: "sizeLimit",
              type: "numeric",
              min: "1",
              max: "",
              step: "1",
              value: "1",
              label: "Remove Linkage Group that contain less markers than this value : (1 to keep all LG).",
            },
            {
              name: LEPMAP_SeparateChromosomes_phased,
              prefix: "phasedData",
              type: "checkbox",
              value: False,
              label: "Data is phased ?",
            },
            {
              name: LEPMAP_SeparateChromosomes_distortionLOD,
              prefix: "distortionLod",
              type: checkbox,
              value: False,
              label: "Use segregation aware LOD scores ?",
            },
          ],
      },
    ],
  prepare_report_script:  LEPMAP_SeparateChromosomes.prepare.report.py,
  prepare_report_outputs: [
    global_stats_LG_by_LOD_mqc.png,
    Repartion_Markers_by_LG_mqc.png,
  ],
  install:
    {
    },
  citations:  {
    lepmap3: [
        "Pasi Rastas, Lep-MAP3: robust linkage mapping even for low-coverage whole genome sequencing data, Bioinformatics, Volume 33, Issue 23, 01 December 2017, Pages 3726–3732,https://doi.org/10.1093/bioinformatics/btx494"
    ],
    lepwrap: [
        "Pavel V. Dimens. (2022). pdimens/LepWrap: link with zenodo (4.0). Zenodo. https://doi.org/10.5281/zenodo.6055565"
    ],
  }
}
