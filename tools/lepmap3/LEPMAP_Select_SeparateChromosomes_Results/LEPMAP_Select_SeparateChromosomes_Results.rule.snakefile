rule <step_name>__LEPMAP_Select_SeparateChromosomes_Results:
    input:
        **<step_name>__LEPMAP_Select_SeparateChromosomes_Results_inputs(),
    output:
        selected_map_tsv = config["results_dir"] + "/" + config["<step_name>__LEPMAP_Select_SeparateChromosomes_Results_output_dir"] + "/selected_map_tsv.txt",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__LEPMAP_Select_SeparateChromosomes_Results_output_dir"] + "/",
        lod = "LOD"+str(config["<step_name>__LEPMAP_Select_SeparateChromosomes_Results_LOD_value"]),
    conda:
        "envs/lepmap.yaml"
    shell:
        """
        in=$(grep "^{params.lod}" {input.map_tsv} | cut -f 2)
        echo "select $in file for the following step"
        ln -s $in {output.selected_map_tsv}
        """