# File generate with generate_tool_snakefile.py

rule <step_name>__LEPMAP_JoinSingles2All:
    input:
        **<step_name>__LEPMAP_JoinSingles2All_inputs(),
    output: 
        map_tsv_out = config["results_dir"] + "/" + config["<step_name>__LEPMAP_JoinSingles2All_output_dir"] + "/map_JoinSingle.txt",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__LEPMAP_JoinSingles2All_output_dir"],
        command = config["<step_name>__LEPMAP_JoinSingles2All_command"],
        LOD_limit = config["<step_name>__LEPMAP_JoinSingles2All_LOD_limit"],
        LOD_difference = config["<step_name>__LEPMAP_JoinSingles2All_LOD_difference"],
        male_theta = config["<step_name>__LEPMAP_JoinSingles2All_male_theta"],
        female_theta = config["<step_name>__LEPMAP_JoinSingles2All_female_theta"],
        distortionLOD = "distortionLod=1" if config["<step_name>__LEPMAP_JoinSingles2All_distortionLOD"] else "",
    conda:
        "envs/lepmap.yaml"
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__LEPMAP_JoinSingles2All_output_dir"] + "/LEPMAP_JoinSingles2All_log.txt"
    threads:  config["<step_name>__LEPMAP_JoinSingles2All_threads"]
    shell:
        """
        # Generate data.call format based on pedigree file and vcf file
        cmd=$(echo $CONDA_PREFIX"/bin/lepmap3/bin")
        java -cp $cmd JoinSingles2All map={input.map_tsv} data={input.calls} numThreads={threads} lodLimit={params.LOD_limit} lodDifference={params.LOD_difference} maleTheta={params.male_theta} femaleTheta={params.female_theta} iterate=1 {params.distortionLOD} > {output.map_tsv_out} 2> {log}
        """
