checkpoint <step_name>__distance_l_lg:
    input:
        **<step_name>__LEPMAP_CalculateDistance_inputs(),
    output:
        dir=directory(config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/list_lg/")
    conda:
        "envs/lepmap.yaml"
    shell:
        """
        mkdir -p {output.dir}
        x=$(grep "LG =" {input.reorder_distances} | sed -e 's/.*LG = \\([0-9]\\+\\) likelihood.*/\\1/' | tr "\\n" " ")
        for lg in $x; do touch {output.dir}/$lg.txt; done
        """

rule <step_name>__LEPMAP_CalculateDistance_byLG:
    input:
        list_lg = config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/list_lg/" + "{lg}.txt",
        **<step_name>__LEPMAP_CalculateDistance_inputs(),
    output: 
        distances = config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/{lg}/Ordered_final_{lg}_distances.tsv",
        sex_average = config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/{lg}/Ordered_final_{lg}_sexavg.tsv",
        intervals = config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/{lg}/Ordered_final_{lg}_intervals.tsv",
        distances_physicInf = config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/{lg}/Ordered_final_{lg}_distances_PhysicPosition.tsv",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/",
        mapping = config["<step_name>__LEPMAP_CalculateDistance_mapping"],
    log:
        sex_average = config["results_dir"] + "/logs/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/{lg}/Ordered_final_{lg}_sexavg_log.txt",
        intervals = config["results_dir"] + "/logs/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/{lg}/Ordered_final_{lg}_intervals_log.txt",
    threads:
        config["<step_name>__LEPMAP_CalculateDistance_threads"]
    conda:
        "envs/lepmap.yaml"
    shell: 
        """
        # Regenerate evaluate order file by LG (necessary for evaluate order to have a file by LG)
        awk -v clg={wildcards.lg} '{{
            if($0 ~ /^#java OrderMarkers2/){{
                match($0, /.* chromosome=([0-9]+) /,q);
                lg=q[1];
                if(clg==lg){{
                    print;
                }}
            }}else{{
                if(clg==lg){{
                    print;
                }}
            }}
        }}' {input.reorder_distances} > {output.distances}


        # Sex-averaged distances
        cmd=$(echo $CONDA_PREFIX"/bin/lepmap3/bin")
        java -cp $cmd OrderMarkers2 evaluateOrder={output.distances} data={input.calls} chromosome={wildcards.lg} numThreads={threads} {params.mapping} improveOrder=0 sexAveraged=1 > {output.sex_average} 2> {log.sex_average}

        sed -i -e 's/LG \= 0/LG \= {wildcards.lg}/g' {output.sex_average}
        
        # Intervals for each marker in a linkage group 
        java -cp $cmd OrderMarkers2 evaluateOrder={output.distances} data={input.calls} chromosome={wildcards.lg} numThreads={threads} {params.mapping} calculateIntervals={output.intervals} > {log.intervals} 2>&1
        sed -i '1s/^/# LG = {wildcards.lg} intervals\\n/' {output.intervals}

        # Add physical information on ordered distances genetic map
        awk -v lg={wildcards.lg} -F "\\t" 'fname != FILENAME {{ fname = FILENAME; idx++ }}
            BEGIN{{
                nline=0;
            }}
            idx == 1{{
                if($0 !~ /^CHR/){{
                    if($0 !~ /^#/){{
                        nline++;
                        physical_pos[nline]=$1"\\t"$2;
                    }}
                }}
            }}
            idx == 2{{
                if($0 !~ /^#/){{
                    if($1 in physical_pos){{
                        print lg"\\t"physical_pos[$1]"\\t"$0
                    }}
                }}else{{
                    if($0~/^#marker_number/){{
                        sub(/^#/, "", $0);
                        print "#LG\\tCHR\\tPOS\\t"$0;
                    }}else{{
                        print $0;
                    }}
                }}
            }}' {input.calls} {output.distances} > {output.distances_physicInf}
        """

def aggregate_input_CalculateDistance(wildcards):
    checkpoint_output = checkpoints.<step_name>__distance_l_lg.get(**wildcards).output[0]
    distances = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/{lg}/Ordered_final_{lg}_distances.tsv", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    sex_average = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/{lg}/Ordered_final_{lg}_sexavg.tsv", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    intervals = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/{lg}/Ordered_final_{lg}_intervals.tsv", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    distances_physicInf = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/{lg}/Ordered_final_{lg}_distances_PhysicPosition.tsv", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    return distances, sex_average, intervals, distances_physicInf


rule <step_name>__LEPMAP_CalculateDistance:
    input:
        distances = lambda wildcards: aggregate_input_CalculateDistance(wildcards)[0],
        sex_average = lambda wildcards: aggregate_input_CalculateDistance(wildcards)[1],
        intervals = lambda wildcards: aggregate_input_CalculateDistance(wildcards)[2],
        distances_physicInf = lambda wildcards: aggregate_input_CalculateDistance(wildcards)[3],
    output:
        distances = config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/Ordered_final_All_distances.tsv",
        sex_average = config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/Ordered_final_All_sexavg.tsv",
        intervals = config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/Ordered_final_All_intervals.tsv",
        distances_physicInf = config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/Ordered_final_All_distances_PhysicPosition.tsv",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__LEPMAP_CalculateDistance_output_dir"] + "/",
    conda:
        "envs/lepmap.yaml"
    shell:
        """
        cat $(echo "{input.distances}" | tr " " "\\n" |sort -V | tr "\\n" " ") > {output.distances}
        cat $(echo "{input.sex_average}" | tr " " "\\n" |sort -V | tr "\\n" " ") > {output.sex_average}
        cat $(echo "{input.intervals}" | tr " " "\\n" |sort -V | tr "\\n" " ") > {output.intervals}
        cat $(echo "{input.distances_physicInf}" | tr " " "\\n" |sort -V | tr "\\n" " ") > {output.distances_physicInf}
        """

