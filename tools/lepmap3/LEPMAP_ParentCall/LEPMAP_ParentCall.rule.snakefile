rule <step_name>__LEPMAP_ParentCall:
    input:
        **<step_name>__LEPMAP_ParentCall_inputs(),
    output: 
        calls = config["results_dir"] + "/" + config["<step_name>__LEPMAP_ParentCall_output_dir"] + "/data.call",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__LEPMAP_ParentCall_output_dir"]+ "/",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__LEPMAP_ParentCall_output_dir"] + "/Parentcall_log.txt"
    conda:
        "envs/lepmap.yaml"
    shell: 
        """
        # Generate data.call format based on pedigree file and vcf file
        cmd=$(echo $CONDA_PREFIX"/bin/lepmap3/bin")
        java -cp $cmd ParentCall2 data={input.pedigree} vcfFile={input.vcf} removeNonInformative=1 > {output.calls} 2> {log}
        """

