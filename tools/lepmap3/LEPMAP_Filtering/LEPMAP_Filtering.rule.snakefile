rule <step_name>__LEPMAP_Filtering:
    input:
        **<step_name>__LEPMAP_Filtering_inputs(),
    output: 
        calls_filter = config["results_dir"] + "/" + config["<step_name>__LEPMAP_Filtering_output_dir"] + "/data_filter.call",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__LEPMAP_Filtering_output_dir"]+ "/",
        maf = config["<step_name>__LEPMAP_Filtering_minor_allele_frequency"],
        missing = config["<step_name>__LEPMAP_Filtering_missing_individual"],
        family = config["<step_name>__LEPMAP_Filtering_family_limit"],
        heterozygote = config["<step_name>__LEPMAP_Filtering_heterozygote_rate"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__LEPMAP_Filtering_output_dir"] + "/Filtering_log.txt"
    conda:
        "envs/lepmap.yaml"
    shell: 
        """
        # Generate data.call format based on pedigree file and vcf file
        cmd=$(echo $CONDA_PREFIX"/bin/lepmap3/bin")
        java -cp $cmd Filtering2 data={input} dataTolerance=0.001 removeNonInformative=1 convert2Biallelic=1 outputHWE=1 MAFLimit={params.maf} missingLimit={params.missing} familyInformativeLimit={params.family} heterozygoteRate={params.heterozygote}> {output.calls_filter} 2> {log}
        """

