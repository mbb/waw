checkpoint <step_name>__l_lg:
    input:
        **<step_name>__LEPMAP_OrderMarkers2_inputs(),
    output:
        dir=directory(config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/list_lg/")
    conda:
        "envs/lepmap.yaml"
    shell:
        """
        mkdir -p {output.dir}
        x=$(grep -v "^#" {input.map_tsv} | cut -f 1 | sort |uniq |grep -vw "0" | tr "\\n" " ")
        for lg in $x; do touch {output.dir}/$lg.txt; done
        """

rule <step_name>__LEPMAP_OrderMarkers2_byLG:
    input:
        list_lg = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/list_lg/" + "{lg}.txt",
        **<step_name>__LEPMAP_OrderMarkers2_inputs(),
    output: 
        distances = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/OrderMarkers_ONmap_{lg}.tsv",
        distances_filtered = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/OrderMarkers_ONmap_{lg}_filtered.tsv",
        distances_physicInf = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/OrderMarkers_ONmap_{lg}_PhysicalPosition.tsv",
        distances_physicInf_filtered = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/OrderMarkers_ONmap_{lg}_PhysicalPosition_filtered.tsv",
        recombines_lg = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/OrderMarkers_{lg}_recombination.tsv",
        linkage_mapping = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/Linkage_Mapping_{lg}.png",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/",
        mrecombination = config["<step_name>__LEPMAP_OrderMarkers2_recombination_male"],
        frecombination = config["<step_name>__LEPMAP_OrderMarkers2_recombination_female"],
        error = config["<step_name>__LEPMAP_OrderMarkers2_minError"],
        iteration = config["<step_name>__LEPMAP_OrderMarkers2_MergeIteration"],
        mapping = config["<step_name>__LEPMAP_OrderMarkers2_mapping"],
        phased = "phasedData=1" if config["<step_name>__LEPMAP_OrderMarkers2_phased"] else "",
        rate_dist = config["<step_name>__LEPMAP_OrderMarkers2_filter_rate_gap"],
        rate_edge = config["<step_name>__LEPMAP_OrderMarkers2_filter_rate_length_edge"],
        phaseout = config["<step_name>__LEPMAP_OrderMarkers2_phasedOutput"],
    log:
        config["results_dir"] + "/logs/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/OrderMarkers2_{lg}_log.txt"
    threads:
        config["<step_name>__LEPMAP_OrderMarkers2_threads"]
    conda:
        "envs/lepmap.yaml"
    shell: 
        """
        # Order the marker within each LG based on map file
        cmd=$(echo $CONDA_PREFIX"/bin/lepmap3/bin")
        java -cp $cmd OrderMarkers2 data={input.calls} map={input.map_tsv} recombination1={params.mrecombination} recombination2={params.frecombination} minError={params.error} chromosome={wildcards.lg} numThreads={threads} {params.mapping} numMergeIterations={params.iteration} {params.phased} {params.phaseout} > {output.distances} 2> {log}

        # Determine number of recombination for each sample by LG
        grep "recombin" {log} | awk -v lg={wildcards.lg} -F "\t" '{{print $0"\\t"lg}}' > {output.recombines_lg}

        # Add physical information on ordered genetic map
        awk -v lg={wildcards.lg} -F "\\t" 'fname != FILENAME {{ fname = FILENAME; idx++ }}
            BEGIN{{
                nline=0;
            }}
            idx == 1{{
                if($0 !~ /^CHR/){{
                    if($0 !~ /^#/){{
                        nline++;
                        physical_pos[nline]=$1"\\t"$2;
                    }}
                }}
            }}
            idx == 2{{
                if($0 !~ /^#/){{
                    if($1 in physical_pos){{
                        print lg"\\t"physical_pos[$1]"\\t"$0
                    }}
                }}else{{
                    if($0~/^#marker_number/){{
                        sub(/^#/, "", $0);
                        print "#LG\\tCHR\\tPOS\\t"$0;
                    }}else{{
                        print $0;
                    }}
                }}
            }}' {input.calls} {output.distances} > {output.distances_physicInf}

        # Generate representation
        min_male=$(grep -v "^#" {output.distances_physicInf} | cut -f 5 | sort -n | head -n 1)
        max_male=$(grep -v "^#" {output.distances_physicInf} | cut -f 5 | sort -n | tail -n 1)
        min_female=$(grep -v "^#" {output.distances_physicInf} | cut -f 6 | sort -n | head -n 1)
        max_female=$(grep -v "^#" {output.distances_physicInf} | cut -f 6 | sort -n | tail -n 1)
        nline=$(grep -v "^#" {output.distances_physicInf} |wc -l)

        awk -v rate_gendist={params.rate_dist} -v rate_length={params.rate_edge} -v min_male=$min_male -v max_male=$max_male -v min_female=$min_female -v max_female=$max_female -v nline=$nline -F "\t" '
        BEGIN{{
            # Determine threshold distance to not exceed for male and female
            dist_thresh_male=(max_male-min_male)*rate_gendist;
            dist_thresh_female=(max_female-min_female)*rate_gendist;
            # Var for line number (only line with a given pattern)
            line=0;
            # Determine the variant on which the filter will be apply (on start and end of LG)
            edge_length_f=int(nline*rate_length+0.5);
            edge_length_r=nline-edge_length_f;
            # Default value for filter will be turn on false if the distance is above the threshold
            mfalse_f="true";
            ffalse_f="true";
            mfalse_r="true";
            ffalse_r="true";
        }}
        {{
            if($0!~/^#/){{
                line++;
                male_dist[line]=$5
                female_dist[line]=$6
                line_saved[line]=$0
            }}else{{
                if($0~/^#LG/){{
                    print $0"\\tPASS_FILTER_MALE\\tPASS_FILTER_FEMALE\\tPASS_FILTER" >"{output.distances_physicInf_filtered}";
                    sub(/LG\\tCHR\\tPOS\\t/, "", $0);
                    print $0;
                }}else if($0~/^#\*\*\*/){{
                    print $0;
                    print $0" Edge_Marker_Fwd_Rev = "edge_length_f" distance_cM_Threshold_Male = "dist_thresh_male" distance_cM_Threshold_Female = "dist_thresh_female >"{output.distances_physicInf_filtered}";
                }}else{{
                    print $0;
                    print $0 >"{output.distances_physicInf_filtered}";
                }}
            }}
        }}
        END{{
            
            # Determine if PASS or FAIL filter on the start of LG
            for(i=edge_length_f;i>=1;i--){{
                diff_all_state[i]="TRUE";
                # Determine for Male
                if(mfalse_f=="false"){{
                    diff_male_state[i]="FALSE";
                    diff_all_state[i]="FALSE";
                }}else{{
                    diff_male_dist=male_dist[i+1]-male_dist[i];
                    if(diff_male_dist > dist_thresh_male){{
                        diff_male_state[i]="FALSE";
                        diff_all_state[i]="FALSE";
                        # If a false is set all marker before are false
                        mfalse_f="false";
                    }}else{{
                        diff_male_state[i]="TRUE";
                    }}
                }}
                
                # Determine for Female
                if(ffalse_f=="false"){{
                    diff_female_state[i]="FALSE";
                    diff_all_state[i]="FALSE";
                }}else{{
                    diff_female_dist=female_dist[i+1]-female_dist[i];
                    if(diff_female_dist > dist_thresh_female){{
                        diff_female_state[i]="FALSE";
                        diff_all_state[i]="FALSE";
                        # If a false is set all marker before are false
                        ffalse_f="false";
                    }}else{{
                        diff_female_state[i]="TRUE";
                    }}
                }}
            }}
            
            # Set as PASS filter for all variant on which no filter are apply
            for(j=edge_length_f+1;j<=edge_length_r;j++){{
                diff_male_state[j]="TRUE";
                diff_female_state[j]="TRUE";
                diff_all_state[j]="TRUE";
            }}
            
            # Determine if PASS or FAIL filter on the end of LG
            for(k=edge_length_r+1;k<=nline;k++){{
                diff_all_state[k]="TRUE";
                # Determine for Male
                if(mfalse_r=="false"){{
                    diff_male_state[k]="FALSE";
                    diff_all_state[k]="FALSE";
                }}else{{
                    diff_male_dist=male_dist[k]-male_dist[k-1];
                    if(diff_male_dist > dist_thresh_male){{
                        diff_male_state[k]="FALSE";
                        diff_all_state[k]="FALSE";
                        # If a false is set all marker after are false
                        mfalse_r="false";
                    }}else{{
                        diff_male_state[k]="TRUE";
                    }}
                }}
                
                # Determine for Female
                if(ffalse_r=="false"){{
                    diff_female_state[k]="FALSE";
                    diff_all_state[k]="FALSE";
                }}else{{
                    diff_female_dist=female_dist[k]-female_dist[k-1];
                    if(diff_female_dist > dist_thresh_female){{
                        diff_female_state[k]="FALSE";
                        diff_all_state[k]="FALSE";
                        # If a false is set all marker after are false
                        ffalse_r="false";
                    }}else{{
                        diff_female_state[k]="TRUE";
                    }}
                }}
            }}
            
            # Print Results
            for(m=1;m<=nline;m++){{
                if(diff_all_state[m]=="FALSE"){{
                    split(line_saved[m],q,"\\t");
                    print "#"q[4]"\\t"q[5]"\\t"q[6]"\\t"q[7]"\\t"q[8]"\\t"q[9]"\\t"q[10];
                    print "#"line_saved[m]"\\t"diff_male_state[m]"\\t"diff_female_state[m]"\\t"diff_all_state[m] >"{output.distances_physicInf_filtered}";
                }}else{{
                    split(line_saved[m],q,"\\t");
                    print q[4]"\\t"q[5]"\\t"q[6]"\\t"q[7]"\\t"q[8]"\\t"q[9]"\\t"q[10];
                    print line_saved[m]"\\t"diff_male_state[m]"\\t"diff_female_state[m]"\\t"diff_all_state[m] >"{output.distances_physicInf_filtered}";
                }}
            }}
        }}' {output.distances_physicInf} > {output.distances_filtered}

        java -cp $cmd LMPlot {output.distances} > {params.output_dir}/{wildcards.lg}/Linkage_Mapping_{wildcards.lg}.dot


        dot -Tpng {params.output_dir}/{wildcards.lg}/Linkage_Mapping_{wildcards.lg}.dot -o {output.linkage_mapping}


        magick {output.linkage_mapping} -resize 750x750 -size 850x782 xc:white +swap -gravity center -composite -gravity north -pointsize 24 -annotate +0+50 "Linkage Mapping of LG = {wildcards.lg} after first round OrderMarkers2" {output.linkage_mapping}
        """

def aggregate_input_OrderMarkers2(wildcards):
    checkpoint_output = checkpoints.<step_name>__l_lg.get(**wildcards).output[0]
    distances = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/OrderMarkers_ONmap_{lg}.tsv", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    distances_filtered = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/OrderMarkers_ONmap_{lg}_filtered.tsv", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    distances_physicInf = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/OrderMarkers_ONmap_{lg}_PhysicalPosition.tsv", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    distances_physicInf_filtered = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/OrderMarkers_ONmap_{lg}_PhysicalPosition_filtered.tsv", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    order_recombine = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/OrderMarkers_{lg}_recombination.tsv", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    linkage_mapping = expand(config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/{lg}/Linkage_Mapping_{lg}.png", lg=glob_wildcards(os.path.join(checkpoint_output, "{lg}.txt")).lg)
    return distances, distances_filtered, distances_physicInf, distances_physicInf_filtered, order_recombine, linkage_mapping



rule <step_name>__LEPMAP_OrderMarkers2:
    input:
        distances = lambda wildcards: aggregate_input_OrderMarkers2(wildcards)[0],
        distances_filtered = lambda wildcards: aggregate_input_OrderMarkers2(wildcards)[1],
        distances_physicInf = lambda wildcards: aggregate_input_OrderMarkers2(wildcards)[2],
        distances_physicInf_filtered = lambda wildcards: aggregate_input_OrderMarkers2(wildcards)[3],
        order_recombine = lambda wildcards: aggregate_input_OrderMarkers2(wildcards)[4],
        linkage_mapping = lambda wildcards: aggregate_input_OrderMarkers2(wildcards)[5],
    output:
        distances = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/OrderMarkers_ONmap_All.tsv",
        distances_filtered = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/OrderMarkers_ONmap_All_Filtered.tsv",
        distances_physicInf = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/OrderMarkers_ONmap_All_PhysicalPosition.tsv",
        distances_physicInf_filtered = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/OrderMarkers_ONmap_All_PhysicalPosition_filtered.tsv",
        recombine_summary = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/OrderMarkers_Recombination_summary.tsv",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__LEPMAP_OrderMarkers2_output_dir"] + "/",
    conda:
        "envs/lepmap.yaml"
    shell:
        """
        cat $(echo "{input.distances}" | tr " " "\\n" |sort -V | tr "\\n" " ") > {output.distances}
        cat $(echo "{input.distances_filtered}" | tr " " "\\n" |sort -V | tr "\\n" " ") > {output.distances_filtered}
        cat $(echo "{input.distances_physicInf}" | tr " " "\\n" |sort -V | tr "\\n" " ") > {output.distances_physicInf}

        cat $(echo "{input.distances_physicInf_filtered}" | tr " " "\\n" |sort -V | tr "\\n" " ") > {output.distances_physicInf_filtered}
        python {snake_dir}/scripts/<step_name>__LEPMAP_OrderMarkers2.prepare.report.py {output.distances_physicInf_filtered}
        magick $(find {params.output_dir}/Edge_Markers_Trim_LG*.png | sort -V | tr "\\n" " ") -append {params.output_dir}/Edge_Markers_Filtered_All_mqc.png
        rm {params.output_dir}/Edge_Markers_Trim_LG*.png
        rm {params.output_dir}/distance_Filter_toprint.txt
        

        magick $(echo "{input.linkage_mapping}" | tr " " "\\n" |sort -V | tr "\\n" " ") -append {params.output_dir}/OrderMarkers_Linkage_mapping_All_mqc.png


        cat $(echo "{input.order_recombine}" | tr " " "\\n" |sort -V | tr "\\n" " ") > {params.output_dir}/All_recombines.txt

        awk -F "\\t" 'BEGIN{{
            lg=""
            maxall=0
            sumall=0
        }}
        {{
            if($0!~/^#/){{
                if($0~/^number/){{
                    match($1, /number of recombinations = ([0-9]+)/, q);
                    nrecombination[$2]=q[1];
                    if(q[1]>maxall){{
                        maxall=q[1];
                    }}
                    sumall=sumall+q[1]
                }}else{{
                    nrecomb_by_progeny[$3][$NF]=$5;
                    if ($3 in family == 0){{
                        family[$3]=$2
                    }}
                    if(lg!=""){{
                        if($NF>lg){{
                            lg=$NF
                        }}
                    }}else{{
                        lg=$NF
                    }}
                }}
            }}
        }}END{{
            header_lg=""
            all_recombination=""
            for(l=1;l<=lg;l++){{
                header_lg=header_lg"\\tLG"l
                all_recombination=all_recombination"\\t"nrecombination[l]
            }}
            print "family\\tsample\\tmean\\tmax"header_lg >"{output.recombine_summary}";
            for(progeny in family){{
                lg_to_print=""
                max=0
                nlg=0
                sumlg=0
                for(l=1;l<=lg;l++){{
                    if(nrecomb_by_progeny[progeny][l] != 0){{
                        lg_to_print=lg_to_print"\\t"nrecomb_by_progeny[progeny][l]
                        if (nrecomb_by_progeny[progeny][l] > max){{
                            max=nrecomb_by_progeny[progeny][l]
                        }}
                        nlg=nlg+1
                        sumlg=sumlg+nrecomb_by_progeny[progeny][l]
                    }}else{{
                        lg_to_print=lg_to_print"\\tNA"
                    }}
                }}
                line_to_print=family[progeny]"\\t"progeny"\\t"int(sumlg/nlg*100+0.5)/100"\\t"max""lg_to_print;
                print line_to_print;
            }}
            print "Total\\tNumber_of_recombinations\\t"int(sumall/lg*100+0.5)/100"\\t"maxall""all_recombination >"{params.output_dir}/tail.txt";
        }}' {params.output_dir}/All_recombines.txt |sort -V -k1,1 -k2,2 | cat - {params.output_dir}/tail.txt >> {output.recombine_summary}

        rm {params.output_dir}/tail.txt {params.output_dir}/All_recombines.txt
        """

