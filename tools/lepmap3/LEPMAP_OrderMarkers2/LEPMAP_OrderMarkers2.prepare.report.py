import oyaml as yaml
import sys
import os
import re
from collections import OrderedDict
from collections import defaultdict
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pandas as pd
import itertools
import plotly.express as px


regex_lg = re.compile("LG = ([0-9]+)")

regex_edge_marker = re.compile("Edge_Marker_Fwd_Rev = ([0-9]+)")

regex_thresh_male = re.compile("distance_cM_Threshold_Male = ([0-9]+.?[0-9]*)")

regex_thresh_female = re.compile("distance_cM_Threshold_Female = ([0-9]+.?[0-9]*)")


directory_out = os.path.dirname(sys.argv[1])

dmthresh = {}
dfthresh = {}
dedge = {}

with open(directory_out + "/distance_Filter_toprint.txt","w") as out:
    with open(sys.argv[1], 'r') as Filtered_marker:
        for line in Filtered_marker:
            line = line.strip()
            if line.startswith("#"):
                if re.match("#[0-9]",line):
                    out.write(line[1:]+"\n")
                elif re.match(r"#\*\*\*",line):
                    dmthresh[regex_lg.search(line).group(1)]=regex_thresh_male.search(line).group(1)
                    dfthresh[regex_lg.search(line).group(1)]=regex_thresh_female.search(line).group(1)
                    dedge[regex_lg.search(line).group(1)]=regex_edge_marker.search(line).group(1)
            else:
                out.write(line+"\n")


out.close()


column_names=["LG","CHR","POS","marker_number","male_position","female_position","phase_parent","phase_data","Pass_filter_male","Pass_filter_female","Pass_filter"]

df = pd.read_csv(directory_out + "/distance_Filter_toprint.txt", comment='#', header=None ,sep="\t", names=column_names)

df['Marker_number_byLG'] = df.groupby('LG')['LG'].cumcount() + 1

l_LG = list(df['LG'].unique())


for lg in l_LG:
    # Generate subplot
    fig = make_subplots(rows=1, cols=2, subplot_titles=("Male cM threshold apply for filter = "+str(dmthresh[str(lg)]), "Female cM threshold apply for filter = "+str(dfthresh[str(lg)])))
    
    # Male
    fig.add_trace(go.Scatter(x=df[ (df["LG"]==lg) & (df["Pass_filter_male"]==True) ]["Marker_number_byLG"], y=df[ (df["LG"]==lg) & (df["Pass_filter_male"]==True) ]["male_position"], mode='markers', marker=dict(color="blue"), name="Pass Filter"), row=1, col=1)
    fig.add_trace(go.Scatter(x=df[ (df["LG"]==lg) & (df["Pass_filter_male"]==False) ]["Marker_number_byLG"], y=df[ (df["LG"]==lg) & (df["Pass_filter_male"]==False) ]["male_position"], mode='markers', marker=dict(color="red"), name="Fail Filter"), row=1, col=1)
    fig.add_vrect(x0=0, x1=int(dedge[str(lg)]), annotation_text="Edge markers", annotation_position="top", fillcolor="green", opacity=0.25, line_width=0, row=1, col=1)
    fig.add_vrect(x0=max(df[df["LG"]==lg]['Marker_number_byLG'])+1-int(dedge[str(lg)]), x1=max(df[df["LG"]==lg]['Marker_number_byLG'])+1, annotation_text="Edge markers", annotation_position="top", fillcolor="green", opacity=0.25, line_width=0, row=1, col=1)
    
    # Female
    fig.add_trace(go.Scatter(x=df[ (df["LG"]==lg) & (df["Pass_filter_female"]==True) ]["Marker_number_byLG"], y=df[ (df["LG"]==lg) & (df["Pass_filter_female"]==True) ]["female_position"], mode='markers', marker=dict(color="blue"), name="Pass Filter"), row=1, col=2)
    fig.add_trace(go.Scatter(x=df[ (df["LG"]==lg) & (df["Pass_filter_female"]==False) ]["Marker_number_byLG"], y=df[ (df["LG"]==lg) & (df["Pass_filter_female"]==False) ]["female_position"], mode='markers', marker=dict(color="red"), name="Fail Filter"), row=1, col=2)
    fig.add_vrect(x0=0, x1=int(dedge[str(lg)]), annotation_text="Edge markers", annotation_position="top", fillcolor="green", opacity=0.25, line_width=0, row=1, col=2)
    fig.add_vrect(x0=max(df[df["LG"]==lg]['Marker_number_byLG'])+1-int(dedge[str(lg)]), x1=max(df[df["LG"]==lg]['Marker_number_byLG'])+1, annotation_text="Edge markers", annotation_position="top", fillcolor="green", opacity=0.25, line_width=0, row=1, col=2)
    
    # update legend
    names = set()
    fig.for_each_trace(
        lambda trace:
            trace.update(showlegend=False)
            if (trace.name in names) else names.add(trace.name))
    
    # Title
    fig.update_layout(
        title='Edge markers results filtered for LG'+str(lg),
        title_x=0.5,
        xaxis_title='Marker Number',
        yaxis_title='Position (cM)',
        legend_title_text='Filter',
        autosize=False,
        width=1000,
        height=500
    )
    
    # Write file
    fig.write_image(directory_out+"/Edge_Markers_Trim_LG"+str(lg)+".png")




