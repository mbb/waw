rule <step_name>__negative_control_decontamination:
    input:
        **<step_name>__negative_control_decontamination_inputs(),
    output:
        curated_sequence = config["results_dir"]+'/'+config["<step_name>__negative_control_decontamination_output_dir"]+ "/curated_seq.fasta",
        abundance_table_filter = config["results_dir"]+'/'+config["<step_name>__negative_control_decontamination_output_dir"]+'/Abundance_table_filtered.tsv',
    log: 
        config["results_dir"]+'/logs/' + config["<step_name>__negative_control_decontamination_output_dir"] + '/decontam.txt',
    params:
        output_dir = config["results_dir"]+'/'+config["<step_name>__negative_control_decontamination_output_dir"],
        column = "-c " + config["<step_name>__negative_control_decontamination_column"] if config["<step_name>__negative_control_decontamination_column"] !="" else "",
        name = "-n " + config["<step_name>__negative_control_decontamination_name"] if config["<step_name>__negative_control_decontamination_name"] != "" else "",
        method = config["<step_name>__negative_control_decontamination_method"],
    conda: 
        "envs/frogs.yaml",
    message:
        "This step cleans or deletes the clusters (ASV/OTU/...) present in the negative controls using a popmap metadata file containing information on samples that are Tneg. The table can be decontaminate by subtracting abundance found in negative control or by removing cluster found in negative control. The popmap file to identify negative control sample in the dataset must have the following tabulated format (sample column and metadata1 minimum)"
    shell: 
        """
        # Decontamination
        python {snake_dir}/scripts/negative_control_decontamination.py {params.column} {params.name} -m {params.method} {input.abundance_table} {input.metadata_table} {output.abundance_table_filter}

        # Extract curated fasta cluster
        awk -F "\\t" 'fname != FILENAME {{fname = FILENAME; idx++}} idx == 1 {{if(NR!=1){{seq2keep[$1]=1}}}} idx == 2 {{if($0~/^>/){{match($0, /^>([^; ]+)/, q);toprint=0;if(q[1] in seq2keep){{print ">"q[1];toprint=1}}}}else{{if(toprint==1){{print}}}}}}' {output.abundance_table_filter} {input.sequence_file} > {output.curated_sequence}
        """