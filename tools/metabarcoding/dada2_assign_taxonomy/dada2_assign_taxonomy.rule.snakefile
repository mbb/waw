rule <step_name>__dada2_assign_taxonomy:
    input:
        **<step_name>__dada2_assign_taxonomy_inputs(),
    output:
        taxa_bootstrap = config["results_dir"] + "/" + config["<step_name>__dada2_assign_taxonomy_output_dir"] + "/Taxa_bootstrap_mqc.tsv",
        taxa_assignment = config["results_dir"] + "/" + config["<step_name>__dada2_assign_taxonomy_output_dir"] + "/taxassign.tsv",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__dada2_assign_taxonomy_output_dir"],
        step_dir = config["<step_name>__dada2_assign_taxonomy_output_dir"],
        script = config["<step_name>__dada2_assign_taxonomy_script"],
        #taxonomypath = config["<step_name>__dada2_assign_taxonomy_taxonomypath"],
        
        taxonomypath = config["<step_name>__dada2_assign_taxonomy_reference_fasta"],
        taxfile = config["<step_name>__dada2_assign_taxonomy_reference_fasta"]+"/train_set.fasta",
        yamlfile = config["<step_name>__dada2_assign_taxonomy_reference_fasta"]+"/info.yaml",

        #OTUabundance = config["<step_name>__dada2_assign_taxonomy_abundance"],
        richness = config["results_dir"] + "/" + config["<step_name>__dada2_assign_taxonomy_output_dir"] + "/richness_mqc.png",
        family = config["results_dir"] + "/" + config["<step_name>__dada2_assign_taxonomy_output_dir"] + "/top20_family_mqc.png",
        ordination = config["results_dir"] + "/" + config["<step_name>__dada2_assign_taxonomy_output_dir"] + "/ordination_mqc.png",
        tryRC = config["<step_name>__dada2_assign_taxonomy_tryRC"],
        min_boot= config["<step_name>__dada2_assign_taxonomy_minBoot"],
    threads:
        config["<step_name>__dada2_assign_taxonomy_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__dada2_assign_taxonomy_output_dir"] + '/dada2_assign_taxonomy_log.txt'
    conda:
        "envs/dada2.yaml"
    message:
        "This step makes a taxonomic assignment of ASVs by using database available in FROGS (http://genoweb.toulouse.inra.fr/frogs_databanks/assignation/readme.txt). The algorithm use assignTaxonomy dada2 function and is an adaptation of the RDP classifier software (https://sourceforge.net/projects/rdp-classifier/)"
    script:
        config["<step_name>__dada2_assign_taxonomy_script"]

    