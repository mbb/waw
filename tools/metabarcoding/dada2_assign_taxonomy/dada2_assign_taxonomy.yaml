{
  id: dada2_assign_taxonomy,
  name: dada2_assign_taxonomy,
  article: 10.1038/nmeth.3869,
  website: "https://benjjneb.github.io/dada2",
  git: "https://github.com/benjjneb/dada2",
  description: "This step makes a taxonomic assignment of ASVs by using database available in FROGS (http://genoweb.toulouse.inra.fr/frogs_databanks/assignation/readme.txt). The algorithm use assignTaxonomy dada2 function and is an adaptation of the RDP classifier software (https://sourceforge.net/projects/rdp-classifier/).",
  version: "1.26",
  documentation: "https://benjjneb.github.io/dada2",
  multiqc:  "krona_iframe",
  commands:
    [
      {
        name: dada2_assign_taxonomy,
        command: ~,
        category: "metabarcoding",
        output_dir: dada2_assign_taxonomy,
        inputs: [
          { name: seqs, type: "fasta_file", description: "Fasta sequence of the cluster describe in abundance table" },
          { name: abundance, type: "tsv", description: "Abundance table with ASV as row and sample as column"} #not mandatory !
        ],
        outputs:  [          
          { name: taxa_bootstrap, type: "tsv", file: "Taxa_bootstrap_mqc.tsv", description: "Table that give the number of bootstrap obtained for each taxonomy level assignment" },
          { name: taxa_assigment, type: "tsv", file: "taxassign.tsv", description: "Table with each row corresponding to an input sequence, and each column corresponding to a taxonomic level. Main taxonomic level are kingdom, phylum, class, order, family, genus and species." },
          # { name: richness, type: "png", file: "Richness_mqc.png", description: "Richness plot" },
          # { name: top20, type: "png", file: "Top_20_mqc.png", description: "Top 20 OTU" },
          # { name: ps_out, type: "rds", file: "ps_out.rds", description: "Phyloseq object" },
          # { name: ordination, type: "png", file: "Ordination_mqc.png", description: "Ordination plot" },
          # { name: tax_table_biom, type: "txt", file: "tax_table.qiime.txt", description: "Taxa table for qiime" },
          # { name: out_table_biom, type: "biom", file: "otu_table.qiime.biom", description: "OTU table for qiime" },
        ],
        options:
          [
            {
              name: dada2_assign_taxonomy_threads,
              prefix: -t,
              type: numeric,
              value: 4,
              min: 1,
              max: NA,
              step: 1,
              label: "Number of threads to use",
            },
            {
              name: dada2_assign_taxonomy_reference_fasta,
              type: select,
              choices: [
                Frogs-BOLD_COI-5P: "/Download/FROGS_taxonomy/COI/BOLD_COI-5P",
                Frogs-MIDORI: "/Download/FROGS_taxonomy/COI/MIDORI",
                Frogs-DAIRYdb: "/Download/FROGS_taxonomy/DAIRYdb",
                Frogs-Diat.barcode: "/Download/FROGS_taxonomy/Diat.barcode",
                Frogs-EZBioCloud: "/Download/FROGS_taxonomy/EZBioCloud",
                Frogs-MaarjAM: "/Download/FROGS_taxonomy/MaarjAM",
                Frogs-MiDas: "/Download/FROGS_taxonomy/MiDas",
                Frogs-PHYMYCO_DB: "/Download/FROGS_taxonomy/PHYMYCO_DB",
                Frogs-PR2: "/Download/FROGS_taxonomy/PR2",
                Frogs-rbcL: "/Download/FROGS_taxonomy/rbcL",
                Frogs-rpoB: "/Download/FROGS_taxonomy/rpoB",
                Frogs-silva_138.1_16S: "/Download/FROGS_taxonomy/SILVA/16S",
                Frogs-silva_138.1_18S: "/Download/FROGS_taxonomy/SILVA/18S",
                Frogs-silva_138.1_23S: "/Download/FROGS_taxonomy/SILVA/23S",
                Frogs-silva_138.1_28S: "/Download/FROGS_taxonomy/SILVA/28S ",
                Frogs-silva_138.1_LSU: "/Download/FROGS_taxonomy/SILVA/LSU",
                Frogs-silva_138.1_SSU: "/Download/FROGS_taxonomy/SILVA/SSU",
                Frogs-Unite: "/Download/FROGS_taxonomy/Unite",
                gyrB: "/Download/FROGS_taxonomy/gyrB",
                microgreendb: "/Download/FROGS_taxonomy/microgreendb",
                rdp16 (16S): "/Download/taxonomy/RDP",
              ],
              value: "/Download/FROGS_taxonomy/SILVA/16S",
              label: "reference_fasta: Choose the database against which to make the taxonomic assignment.",
              popover: "Databases come from http://genoweb.toulouse.inra.fr/frogs_databanks/assignation/readme.txt. This parameter is used as the second argument of the assignTaxonomy dada2 function.",
            },
            {
              name: dada2_assign_taxonomy_tryRC,
              prefix: ,
              type: checkbox,
              value: FALSE,
              label: "tryRC: Try the reverse-complement orientation for assignment.",
              popover: "By default the assignment will be done only on the forward orientation. This parameter is used as tryRC=TRUE|FALSE of the assignTaxonomy dada2 function.",
            },
            {
              name: dada2_assign_taxonomy_minBoot,
              type: numeric,
              value: 50,
              min: 10,
              max: 100,
              step: 1,
              label: "minBoot: Define the minimum number of bootstrap to validate taxonomic assignement.",
              popover: "The value must be between 0 and 100, with 0 for a low stringency and 100 for a high. This parameter is used as minBoot=integer of the assignTaxonomy dada2 function.",
            },

          ],
      },
    ],
  script: dada2_assign_taxonomy.script.R,  
  install: {
    # dada2: [
    #   "Rscript -e 'library(\"devtools\");devtools::install_github(\"benjjneb/dada2\", ref=\"v1.18\");library(\"dada2\")'",
    # ],
    # phyloseq: [
    #   "Rscript -e 'library(\"devtools\");devtools::install_github(\"joey711/phyloseq\");library(\"phyloseq\")'"
    # ],
    # biomformat: [
    #   "Rscript -e 'library(\"devtools\");devtools::install_github(\"joey711/biomformat\");library(\"biomformat\")'"
    # ],
    # yaml: [
    # "Rscript -e 'install.packages(\"yaml\");'"
    # ],
    # krona: [
    #   "cd /opt/biotools",
    #   "git clone https://github.com/marbl/Krona.git",
    #   "cd Krona/KronaTools",
    #   "./install.pl --prefix /opt/biotools",
    #   "cd /opt/biotools",
    #   "rm -r Krona/ExcelTemplate"
    # ],
    # mbb_mqc_plugin:
    # [
    #   "cd /opt/biotools",
    #   "git clone https://gitlab.mbb.univ-montp2.fr/mmassaviol/mbb_mqc_plugin.git",
    #   "cd mbb_mqc_plugin",
    #   "python3 setup.py install"
    # ]
  },
  data: [
    {
      name: FROGS_taxonomy,
      type: directory
    },
    {
      name: taxonomy,
      type: directory
    }
  ],
  citations:  {
    dada2: [
      "Callahan BJ, McMurdie PJ, Rosen MJ, Han AW, Johnson AJ, Holmes SP. DADA2: High-resolution sample inference from Illumina amplicon data. Nat Methods. 2016;13(7):581-583. doi:10.1038/nmeth.3869"
    ],
    phyloseq: [
      "McMurdie and Holmes (2013) phyloseq: An R package for reproducible interactive analysis and graphics of microbiome census data PLoS ONE 8(4):e61217"
    ],
    biomformat: [
      "Daniel McDonald, Jose C Clemente, Justin Kuczynski, Jai Ram Rideout, Jesse Stombaugh, Doug Wendel, Andreas Wilke, Susan Huse, John Hufnagle, Folker Meyer, Rob Knight, J Gregory Caporaso, The Biological Observation Matrix (BIOM) format or: how I learned to stop worrying and love the ome-ome, GigaScience, Volume 1, Issue 1, December 2012, 2047-217X-1-7, https://doi.org/10.1186/2047-217X-1-7"
    ],
    krona: [
      "Ondov BD, Bergman NH, and Phillippy AM. Interactive metagenomic visualization in a Web browser. BMC Bioinformatics. 2011 Sep 30; 12(1):385"
    ],
    frogs: [
      "Maria Bernard, Olivier Rué, Mahendra Mariadassou and Géraldine Pascal; FROGS: a powerful tool to analyse the diversity of fungi with special management of internal transcribed spacers, Briefings in Bioinformatics 2021, 10.1093/bib/bbab318 "
    ]

  }
}
