rule <step_name>__dada2_quality:
    input:
        **<step_name>__dada2_inputs(),
    output:
        qualityForward = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/Quality_forward_mqc.png",
        qualityReverse = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/Quality_reverse_mqc.png",
    params:
        script = config["<step_name>__dada2_script"],
        step = "quality",
        datapath = lambda w, input: os.path.dirname(input.read[0]),
        samples = SAMPLES,
    threads:
        config["<step_name>__dada2_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__dada2_output_dir"] + '/dada2_quality_log.txt'
    conda:
        "envs/dada2.yaml"
    script:
        config["<step_name>__dada2_script"]


def <step_name>__dada2_filter_and_trim_out_reads():
    outs = dict()
    if config["<step_name>__dada2_no_filter_and_trim"] == False:
        outs["filtFs"] = expand(config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/filtered/{sample}_F_filt.fastq.gz",sample=SAMPLES),
        outs["filtRs"] = expand(config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/filtered/{sample}_R_filt.fastq.gz",sample=SAMPLES),
    return outs

# possibilité de désactiver cette étape a dev
rule <step_name>__dada2_filter_and_trim:
    input:
        **<step_name>__dada2_inputs(),
    output:
        filter_and_trim_out = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/filter_and_trim_out.rds",
        **<step_name>__dada2_filter_and_trim_out_reads()
    params:
        script = config["<step_name>__dada2_script"],
        step = "filter_and_trim",
        datapath = lambda w, input: os.path.dirname(input.read[0]),
        samples = SAMPLES,
        truncLen_forward = config["<step_name>__dada2_truncLen_forward"],
        truncLen_reverse = config["<step_name>__dada2_truncLen_reverse"],
        maxEE_forward = "Inf" if config["<step_name>__dada2_maxEE_forward"]==-1 else config["<step_name>__dada2_maxEE_forward"],
        maxEE_reverse = "Inf" if config["<step_name>__dada2_maxEE_reverse"]==-1 else config["<step_name>__dada2_maxEE_reverse"],
        no_filter_and_trim = config["<step_name>__dada2_no_filter_and_trim"],
    threads:
        config["<step_name>__dada2_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__dada2_output_dir"] + '/dada2_filter_and_trim_log.txt'
    conda:
        "envs/dada2.yaml"
    script:
        config["<step_name>__dada2_script"]

rule <step_name>__dada2_learn_errors:
    input:
        **<step_name>__dada2_learn_errors_inputs()
    output:
        errorsForward = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/Errors_forward_mqc.png",
        errorsReverse = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/Errors_reverse_mqc.png",
        errF_rds = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/errF.rds",
        errR_rds = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/errR.rds",
    params:
        script = config["<step_name>__dada2_script"],
        step = "learn_errors",
        samples = SAMPLES,
    threads:
        config["<step_name>__dada2_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__dada2_output_dir"] + '/dada2_learn_errors_log.txt'
    conda:
        "envs/dada2.yaml"
    script:
        config["<step_name>__dada2_script"]

# need dada2 version >= 1.12 to work directly on filtered files
rule <step_name>__dada2_construct_seqtab:
    input:
        **<step_name>__dada2_learn_errors_inputs(),
        errF_rds = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/errF.rds",
        errR_rds = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/errR.rds",
        filter_and_trim_out = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/filter_and_trim_out.rds",
    output:
        track = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/Track_reads_mqc.tsv",
        seqtab = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/seqtab.rds",
        uniques = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/uniques.fasta",
    params:
        script = config["<step_name>__dada2_script"],
        step = "construct_seqtab",
        samples = SAMPLES,
        output_dir = config["<step_name>__dada2_output_dir"],
    threads:
        config["<step_name>__dada2_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__dada2_output_dir"] + '/dada2_construct_seqtab_log.txt'
    conda:
        "envs/dada2.yaml"
    script:
        config["<step_name>__dada2_script"]

rule <step_name>__dada2_remove_chimeras:
    input:
        seqtab = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/seqtab.rds"
    output:
        seqtab_nochim_csv = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/seqtab_nochim.csv",
        seqtab_nochim_rds = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/seqtab_nochim.rds",
        uniques_nochim = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/uniques_nochim.fasta",
    params:
        script = config["<step_name>__dada2_script"],
        step = "remove_chimeras",
    threads:
        config["<step_name>__dada2_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__dada2_output_dir"] + '/dada2_remove_chimeras_log.txt'
    conda:
        "envs/dada2.yaml"
    script:
        config["<step_name>__dada2_script"]

rule <step_name>__dada2_assign_taxonomy:
    input:
        seqtab = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/seqtab_nochim.rds" if(config["<step_name>__dada2_remove_chim"]) else config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/seqtab.rds"
    output:
        taxa_bootstrap = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/Taxa_bootstrap_mqc.tsv",
        taxa_rds = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/taxa.rds",
    params:
        script = config["<step_name>__dada2_script"],
        step = "assign_taxonomy",
        #dada2_marker_type = config["<step_name>__dada2_marker_type"],
        taxonomypath = config["<step_name>__dada2_taxonomypath"],
    threads:
        config["<step_name>__dada2_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__dada2_output_dir"] + '/dada2_assign_taxonomy_log.txt'
    conda:
        "envs/dada2.yaml"
    script:
        config["<step_name>__dada2_script"]

rule <step_name>__dada2_phyloseq_run:
    input:
        seqtab = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/seqtab_nochim.rds" if(config["<step_name>__dada2_remove_chim"]) else config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/seqtab.rds",
        taxa_rds = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/taxa.rds",
    output:
        otu_table = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/otu_table.csv",
        tax_table_csv = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/tax_table.csv",
        richness = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/Richness_mqc.png",
        top20 = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/Top_20_mqc.png",
        ps_out = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/ps_out.rds",
        ordination = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/Ordination_mqc.png",
    params:
        script = config["<step_name>__dada2_script"],
        step = "phyloseq_run",
    threads:
        config["<step_name>__dada2_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__dada2_output_dir"] + '/dada2_phyloseq_run_log.txt'
    conda:
        "envs/dada2.yaml"
    script:
        config["<step_name>__dada2_script"]

rule <step_name>__dada2_export_qiime:
    input:
        ps_out = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/ps_out.rds",
    output:
        tax_table_biom = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/tax_table.qiime.txt",
        otu_table_biom = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/otu_table.qiime.biom",
    params:
        script = config["<step_name>__dada2_script"],
        step = "export_qiime",
    threads:
        config["<step_name>__dada2_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__dada2_output_dir"] + '/dada2_export_qiime_log.txt'
    conda:
        "envs/dada2.yaml"
    script:
        config["<step_name>__dada2_script"]

rule <step_name>__dada2_krona:
    input:
        ps_rds = rules.<step_name>__dada2_phyloseq_run.output.ps_out,
        otu_table = rules.<step_name>__dada2_export_qiime.output.otu_table_biom
    output:
        krona_html = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/Krona_files/text.krona.html"
    params:
        script = config["<step_name>__dada2_script"],
        step = "krona",
        output_dir = config["results_dir"] + "/" + config["<step_name>__dada2_output_dir"] + "/Krona_files",
        nb_samples = len(SAMPLES),
    conda:
        "envs/dada2.yaml"
    script:
        config["<step_name>__dada2_script"]