if config["SeOrPe"] == "PE":

    rule <step_name>__dada2_filter_trim_PE:
        input:
            **<step_name>__dada2_filter_trim_PE_inputs(),
        output:
            #outs = dict()
            filtFs = config["results_dir"] + "/" + config["<step_name>__dada2_filter_trim_PE_output_dir"] + "/filtered/{sample}_R1_filtered.fastq.gz",
            filtRs = config["results_dir"] + "/" + config["<step_name>__dada2_filter_trim_PE_output_dir"] + "/filtered/{sample}_R2_filtered.fastq.gz",
        params:
            truncLen_forward = config["<step_name>__dada2_filter_trim_truncLen_forward"],
            truncLen_reverse = config["<step_name>__dada2_filter_trim_truncLen_reverse"],
            maxEE_forward = "Inf" if config["<step_name>__dada2_filter_trim_maxEE_forward"]==-1 else config["<step_name>__dada2_filter_trim_maxEE_forward"],
            maxEE_reverse = "Inf" if config["<step_name>__dada2_filter_trim_maxEE_reverse"]==-1 else config["<step_name>__dada2_filter_trim_maxEE_reverse"],
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__dada2_filter_trim_PE_output_dir"] + '/{sample}-dada2_filter_trim_log.txt'
        conda:
            "envs/dada2.yaml"
        message:
            """
            This step first, truncates reads to {params.truncLen_forward} for forward reads and {params.truncLen_reverse} for reverse reads.
            Then, discards all reads :
                - with N
                - assign to phiX 
                - with a length shorter than {params.truncLen_forward} for forward reads and {params.truncLen_reverse} for reverse reads
                - with maxEE higher than {params.maxEE_forward} for forward reads and {params.maxEE_reverse} for reverse reads
            """
        script:
            config["<step_name>__dada2_filter_trim_script"]
        
elif config["SeOrPe"] == "SE":

    rule <step_name>__dada2_filter_trim_SE:    
        input:
            **<step_name>__dada2_filter_trim_SE_inputs(),
        output:
          filtFs = config["results_dir"] + "/" + config["<step_name>__dada2_filter_trim_SE_output_dir"] + "/filtered/{sample}_F_filtered.fastq.gz",
        params:
            truncLen_single = config["<step_name>__dada2_filter_trim_SE_truncLen"],
            maxEE_single = "Inf" if config["<step_name>__dada2_filter_trim_SE_maxEE"]==-1 else config["<step_name>__dada2_filter_trim_SE_maxEE"],
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__dada2_filter_trim_SE_output_dir"] + '/{sample}-dada2_filter_trim_log.txt'
        conda:
            "envs/dada2.yaml"
        message:
            """
            This step first, truncates reads to {params.truncLen_single}.
            Then, discards all reads :
                - with N
                - assign to phiX 
                - with a length shorter than {params.truncLen_single}
                - with maxEE higher than {params.maxEE_single}
            """
        script:
            config["<step_name>__dada2_filter_trim_script"]