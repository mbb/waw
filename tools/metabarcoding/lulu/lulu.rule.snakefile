rule <step_name>__lulu:
    input:
        **<step_name>__lulu_inputs(),
    output: 
        curated_sequence = config["results_dir"] + "/" + config["<step_name>__lulu_output_dir"] + "/curated_seq.fasta",
        curated_table = config["results_dir"] + "/" + config["<step_name>__lulu_output_dir"] + "/curated_table.tsv",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__lulu_output_dir"]+ "/",
        method = config["<step_name>__lulu_matcher_method"],
        minimum_ratio = config["<step_name>__lulu_minimum_ratio"],
        minimum_match = config["<step_name>__lulu_minimum_match"],
        script = config["<step_name>__lulu_script"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__lulu_output_dir"] + "/lulu_log.txt"
    conda:
        "envs/frogs.yaml"
    message:
     "This step filters and groups clusters (ASV/OTU/...) by considering the co-occurrences of similar clusters using lulu R package."
    shell: 
        """
        # Auto alignment
        if [ "{params.method}" ==  "blast" ]; then
            makeblastdb -in {input.sequence_file} -parse_seqids -dbtype nucl;
            blastn -db {input.sequence_file} -outfmt '6 qseqid sseqid pident' -out {params.output_dir}/match_list.txt -qcov_hsp_perc 80 -perc_identity 84 -query {input.sequence_file} ;
        else
            vsearch --usearch_global {input.sequence_file} --db {input.sequence_file} --self --id .84 --iddef 1 --userout {params.output_dir}/match_list.txt -userfields query+target+id --maxaccepts 0 --query_cov .9 --maxhits 10
        fi

        # Run lulu
        Rscript {snake_dir}/{params.script} {input.abundance_file} {params.output_dir}/match_list.txt {params.minimum_ratio} {params.minimum_match} {output.curated_table} {params.output_dir}/otu_map.tsv |& tee {log}

        # Extract curated and clustered fasta sequence
        awk -F "\\t" 'fname != FILENAME {{fname = FILENAME; idx++}} idx == 1 {{if(NR!=1){{seq2keep[$1]=1}}}} idx == 2 {{if($0~/^>/){{match($0, /^>([^; ]+)/, q);toprint=0;if(q[1] in seq2keep){{print ">"q[1];toprint=1}}}}else{{if(toprint==1){{print}}}}}}' {output.curated_table} {input.sequence_file} > {output.curated_sequence}
        """
