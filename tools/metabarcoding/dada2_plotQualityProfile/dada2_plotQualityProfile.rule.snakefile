if config["SeOrPe"] == "PE":

    rule <step_name>__dada2_plotQualityProfile_PE:
        input:
            **<step_name>__dada2_plotQualityProfile_PE_inputs(),
        output:
            qualityF = config["results_dir"] + "/" + config["<step_name>__dada2_plotQualityProfile_PE_output_dir"] + "/qualityPlotF_mqc.png",
            qualityR = config["results_dir"] + "/" + config["<step_name>__dada2_plotQualityProfile_PE_output_dir"] + "/qualityPlotR_mqc.png",
        params:
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__dada2_plotQualityProfile_PE_output_dir"] + '/dada2_plotQualityProfile_log.txt'
        conda:
            "envs/dada2.yaml"
        message:
            """
            This step generates a sequencing quality report for each sample. It gives an overview of sequence length and quality evolution (phred score) throughout the sequence. This report can be used to adjust the parameters of the second step, which consists in cleaning the sequences. This step uses the plotQualityProfile dada2 function.
            """
        script:
            config["<step_name>__dada2_plotQualityProfile_script"]
        
elif config["SeOrPe"] == "SE":

    rule <step_name>__dada2_plotQualityProfile_SE:    
        input:
            **<step_name>__dada2_plotQualityProfile_SE_inputs(),
        output:
            qualityF = config["results_dir"] + "/" + config["<step_name>__dada2_plotQualityProfile_SE_output_dir"] + "/qualityPlot_mqc.png",
        params:
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__dada2_plotQualityProfile_SE_output_dir"] + '/dada2_plotQualityProfile_log.txt'
        conda:
            "envs/dada2.yaml"
        message:
            """
            Generates a sequencing quality report for each sample. It gives an overview of sequence length and quality evolution (phred score) throughout the sequence. This report can be used to adjust the parameters of the second step, which consists in cleaning the sequences.
            """
        script:
            config["<step_name>__dada2_plotQualityProfile_script"]