if config["SeOrPe"] == "PE":

    rule <step_name>__dada2_Err_Derep_makeSequenceTable_PE:
        input:
            **<step_name>__dada2_Err_Derep_makeSequenceTable_PE_inputs(),
        output:
            errRplotF = config["results_dir"] + "/" + config["<step_name>__dada2_Err_Derep_makeSequenceTable_PE_output_dir"] + "/errorProfilePlotF_mqc.png",
            errRplotR = config["results_dir"] + "/" + config["<step_name>__dada2_Err_Derep_makeSequenceTable_PE_output_dir"] + "/errorProfilePlotR_mqc.png",
            ASVseqfile = config["results_dir"] + "/" + config["<step_name>__dada2_Err_Derep_makeSequenceTable_PE_output_dir"] + "/ASVseqs.fasta",
            ASVtable = config["results_dir"] + "/" + config["<step_name>__dada2_Err_Derep_makeSequenceTable_PE_output_dir"] + "/ASVabundance.tsv"
        params:
            randomize = config["<step_name>__dada2_Err_Derep_makeSequenceTable_randomize"],
            nbases = config["<step_name>__dada2_Err_Derep_makeSequenceTable_nbases"],
            pooling = config["<step_name>__dada2_Err_Derep_makeSequenceTable_pool"],
            minoverlap = config["<step_name>__dada2_Err_Derep_makeSequenceTable_minoverlap"],
            remove_chim = config["<step_name>__dada2_Err_Derep_makeSequenceTable_remove_chim"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__dada2_Err_Derep_makeSequenceTable_PE_output_dir"] + '/dada2_Err_Derep_makeSequenceTable_log.txt'
        conda:
            "envs/dada2.yaml"
        message:
            """
            This step clusters sequences into ASVs, the aim being to correct sequencing errors based on error models. This step is divided in multiple parts :
                1- Dereplicate Reads, i.e. groups identical sequences together while retaining abundance information.
                2- Build an error model on the sequencing data by using parametric models based on {params.nbases} nucleotides and on randomize ({params.randomize}) samples.
                3- Use the model to denoise the samples and to determine the ASVs. The denoising is done by pooling ({params.pooling}) samples.
                4- Merges paired datas with a minimum overlap of {params.minoverlap} nucleotides. Merging is performed by aligning the denoised forward reads with the reverse-complement of the corresponding denoised reverse reads, and then constructing the merged “contig” sequences.
                5- Construct abundance table. An amplicon sequence variant (ASV) table is built with ASV in rows, samples in columns and abundance in value. Then, the abundance is the number of reads that belongs to an ASV and comes from a sample. 
                6- Optionally detect and discard bimera ({params.remove_chim}).
            """
        script:
            config["<step_name>__dada2_Err_Derep_makeSequenceTable_script"]
        
elif config["SeOrPe"] == "SE":

    rule <step_name>__dada2_Err_Derep_makeSequenceTable_SE:    
        input:
            **<step_name>__dada2_Err_Derep_makeSequenceTable_SE_inputs(),
        output:
            errRplotF = config["results_dir"] + "/" + config["<step_name>__dada2_Err_Derep_makeSequenceTable_SE_output_dir"] + "/errorProfilePlot_mqc.png",
            ASVseqfile = config["results_dir"] + "/" + config["<step_name>__dada2_Err_Derep_makeSequenceTable_PE_output_dir"] + "/ASVseqs.fasta",
            ASVtable = config["results_dir"] + "/" + config["<step_name>__dada2_Err_Derep_makeSequenceTable_PE_output_dir"] + "/ASVabundance.tsv"
        params:
            randomize = config["<step_name>__dada2_Err_Derep_makeSequenceTable_randomize"],
            nbases = config["<step_name>__dada2_Err_Derep_makeSequenceTable_nbases"],
            pooling = config["<step_name>__dada2_Err_Derep_makeSequenceTable_pool"],
            remove_chim = config["<step_name>__dada2_Err_Derep_makeSequenceTable_remove_chim"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__dada2_Err_Derep_makeSequenceTable_SE_output_dir"] + '/dada2_Err_Derep_makeSequenceTable_log.txt'
        conda:
            "envs/dada2.yaml"
        message:
            """
            This step clusters sequences into ASVs, the aim being to correct sequencing errors based on error models. This step is divided in multiple parts :
                1- Dereplicate Reads, i.e. groups identical sequences together while retaining abundance information.
                2- Build an error model on the sequencing data by using parametric models based on {params.nbases} nucleotides and on randomize ({params.randomize}) samples.
                3- Use the model to denoise the samples and to determine the ASVs. The denoising is done by pooling ({params.pooling}) samples.
                4- Construct abundance table. An amplicon sequence variant (ASV) table is built with ASV in rows, samples in columns and abundance in value. Then, the abundance is the number of reads that belongs to an ASV and comes from a sample. 
                5- Optionally detect and discard bimera ({params.remove_chim}).
            """
        script:
            config["<step_name>__dada2_Err_Derep_makeSequenceTable_script"]