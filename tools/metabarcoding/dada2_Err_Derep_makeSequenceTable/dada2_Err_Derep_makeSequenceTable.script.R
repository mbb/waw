suppressMessages(library(dada2,quietly=T));print(paste0("dada2: ",packageVersion("dada2")))
suppressMessages(library(RcppParallel,quietly=T))

# Put a fixed seed to have the same random choice when learning errors (reproducibility)
set.seed(1)



fnFs = snakemake@input[["read"]]
fnRs= snakemake@input[["read2"]] #this can be missing

seqfile = snakemake@output[["ASVseqfile"]]
ASVfile = snakemake@output[["ASVtable"]]

nbases = snakemake@params[["nbases"]]
overlap = snakemake@params[["minoverlap"]]
random = snakemake@params[["randomize"]]
if (random=="FALSE"){
  random=FALSE
}
if (random=="TRUE"){
  random=TRUE
}

pooling = snakemake@params[["pooling"]]
if (pooling=="FALSE"){
  pooling=FALSE
}
if (pooling=="TRUE"){
  pooling=TRUE
}

remove_chim = snakemake@params[["remove_chim"]]

######################
# estimated error rates
######################

errFplot = snakemake@output[["errRplotF"]]
errRplot = snakemake@output[["errRplotR"]]

errF <- learnErrors(fnFs, nbases=nbases, randomize=random, multithread=TRUE)

png(filename = errFplot, height=800, width=800)
 print(plotErrors(errF, nominalQ=TRUE))
dev.off()

if (! is.null(fnRs))
{
errR <- learnErrors(fnRs, nbases=nbases, randomize=random, multithread=TRUE)
png(filename = errRplot, height=800, width=800)
 print(plotErrors(errR, nominalQ=TRUE) )
dev.off()
}

## Dereplication
derepFs <- derepFastq(fnFs, verbose=TRUE)
if (! is.null(fnRs))
{
derepRs <- derepFastq(fnRs, verbose=TRUE)
}

### Sample Inference
dadaFs <- dada(derepFs, err=errF, pool=pooling, multithread=TRUE)
if (! is.null(fnRs))
{
dadaRs <- dada(derepRs, err=errR, pool=pooling, multithread=TRUE)
}

### Merge paired reads
if (! is.null(fnRs))
{
 mergers <- mergePairs(dadaFs, derepFs, dadaRs, derepRs, minOverlap = overlap, verbose=TRUE)
}

### Construct sequence table
if (! is.null(fnRs))
{
   seqtab <- makeSequenceTable(mergers)
} else {
   seqtab <- makeSequenceTable(dadaFs)
}

print(remove_chim)
if (remove_chim == TRUE)
{
  seqtab.nochim =  removeBimeraDenovo(seqtab, method="consensus", multithread=TRUE, verbose=TRUE)
  seqtab = seqtab.nochim
}

OTUids = paste0("OTU_", 1:ncol(seqtab))

#save ASV
uniquesToFasta(seqtab, seqfile, ids=OTUids )

#save abundance
colnames(seqtab) = OTUids
write.table(t(seqtab), ASVfile, quote=F, col.names=NA, sep="\t")
