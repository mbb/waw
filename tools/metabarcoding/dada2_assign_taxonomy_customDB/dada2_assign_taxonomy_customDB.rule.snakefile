rule <step_name>__dada2_assign_taxonomy_customDB:
    input:
        **<step_name>__dada2_assign_taxonomy_customDB_inputs(),
    output:
        taxa_bootstrap = config["results_dir"] + "/" + config["<step_name>__dada2_assign_taxonomy_customDB_output_dir"] + "/Taxa_bootstrap_mqc.tsv",
        taxa_assignment = config["results_dir"] + "/" + config["<step_name>__dada2_assign_taxonomy_customDB_output_dir"] + "/taxassign.tsv",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__dada2_assign_taxonomy_customDB_output_dir"],
        step_dir = config["<step_name>__dada2_assign_taxonomy_customDB_output_dir"],
        script = config["<step_name>__dada2_assign_taxonomy_customDB_script"],
        richness = config["results_dir"] + "/" + config["<step_name>__dada2_assign_taxonomy_customDB_output_dir"] + "/richness_mqc.png",
        family = config["results_dir"] + "/" + config["<step_name>__dada2_assign_taxonomy_customDB_output_dir"] + "/top20_family_mqc.png",
        ordination = config["results_dir"] + "/" + config["<step_name>__dada2_assign_taxonomy_customDB_output_dir"] + "/ordination_mqc.png",
        tryRC = config["<step_name>__dada2_assign_taxonomy_customDB_tryRC"],
        minboot= config["<step_name>__dada2_assign_taxonomy_customDB_minBoot"],
    threads:
        config["<step_name>__dada2_assign_taxonomy_customDB_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__dada2_assign_taxonomy_customDB_output_dir"] + '/dada2_assign_taxonomy_log.txt'
    conda:
        "envs/dada2.yaml"
    shell:
        """
        mkdir -p {params.output_dir}/
        n=$(awk -F "\\t" '{{print NF}}' {input.tax_db} | sort |uniq)
        problem=$(echo $n | wc -c)
        if [ $problem -ne "2" ];then
            echo "All lines should contain only two columns"
            echo "The taxonomy file mustn't contain header and format should be : 'SeqID\tKingdom;Phylum;Class;Order;Family;Genus;Species'. All taxonomies need to be define with the exact same number of rank separate by ';'. Set 'unclassified' for unknown rank"
            exit 1
        fi
        if [ $n -ne "2" ];then
            echo "All lines should contain only two columns"
            echo "The taxonomy file mustn't contain header and format should be : 'SeqID\tKingdom;Phylum;Class;Order;Family;Genus;Species'. All taxonomies need to be define with the exact same number of rank separate by ';'. Set 'unclassified' for unknown rank"
            exit 1
        fi
        n=$(awk -F "\\t" '{{split($2,tax,";");out = "R1"; for (i = 2; i <= length(tax); i++){{out =out" R"i}}; print out}}' {input.tax_db} | sort |uniq)
        problem2=$(echo $n | grep -o "R1 "| wc -l)
        if [ $problem2 -ne "1" ];then
            echo "Not same number of rank between seqID in taxonomy file"
            echo "The taxonomy file mustn't contain header and format should be : 'SeqID\tKingdom;Phylum;Class;Order;Family;Genus;Species'. All taxonomies need to be define with the exact same number of rank separate by ';'. Set 'unclassified' for unknown rank"
            exit 1
        fi
        mkdir -p {params.output_dir}/Database
        awk -F "\\t" 'fname != FILENAME {{fname = FILENAME; idx++}} idx == 1 {{id2tax[$1]=$2";"}} idx == 2 {{if($0~/^>/){{match($0, /^>(.+)/, q);if(q[1] in id2tax){{print ">"id2tax[q[1]];toprint=1}}else{{toprint=0}}}}else{{if(toprint==1){{print}}else{{nextline}}}}}}' {input.tax_db} {input.fasta_db} > {params.output_dir}/Database/train_set_dada2.fa
        gzip {params.output_dir}/Database/train_set_dada2.fa
        Rscript {snake_dir}/{params.script} {output.taxa_assignment} {output.taxa_bootstrap} {input.seqs} {input.abundance} {params.output_dir} {params.step_dir} {params.richness} {params.family} {params.ordination} {params.tryRC} {params.minboot}
        """
