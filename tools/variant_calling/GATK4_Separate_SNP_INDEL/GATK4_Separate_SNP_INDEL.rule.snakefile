# File generate with generate_tool_snakefile.py

rule <step_name>__GATK4_Separate_SNP_INDEL:
    input:
        **<step_name>__GATK4_Separate_SNP_INDEL_inputs(),
    output: 
        vcf_snp = config["results_dir"] + "/" + config["<step_name>__GATK4_Separate_SNP_INDEL_output_dir"] + "/output_SNP.vcf.gz",
        vcf_indel = config["results_dir"] + "/" + config["<step_name>__GATK4_Separate_SNP_INDEL_output_dir"] + "/output_INDEL.vcf.gz",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__GATK4_Separate_SNP_INDEL_output_dir"],
        command = config["<step_name>__GATK4_Separate_SNP_INDEL_command"],
        biallelic = "--restrict-alleles-to BIALLELIC" if config["<step_name>__GATK4_Separate_SNP_INDEL_biallelic"] else "",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__GATK4_Separate_SNP_INDEL_output_dir"] + "/GATK4_Separate_SNP_INDEL_log.txt"
    threads: 1
    conda:
        "envs/gatk4.yaml"
    shell: 
        """
        gatk SelectVariants -R {input.genome_fasta} -V {input.vcf} --select-type-to-include SNP -O {output.vcf_snp} {params.biallelic}

        gatk SelectVariants -R {input.genome_fasta} -V {input.vcf} --select-type-to-include INDEL -O {output.vcf_indel}
        """

