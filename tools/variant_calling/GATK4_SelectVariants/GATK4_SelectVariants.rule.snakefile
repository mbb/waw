rule <step_name>__GATK4_SelectVariants:
    input:
        **<step_name>__GATK4_SelectVariants_inputs(),
    output:
        vcf_filtered = config["results_dir"] + "/" + config["<step_name>__GATK4_SelectVariants_output_dir"] + "/Variant_filtered.vcf",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__GATK4_SelectVariants_output_dir"],
        command = config["<step_name>__GATK4_SelectVariants_command"],
        nocall = config["<step_name>__GATK4_SelectVariants_Max_NoCall_GT"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__GATK4_SelectVariants_output_dir"] + "/GATK4_SelectVariants_log.txt"
    threads: 1
    conda:
        "envs/gatk4.yaml"
    shell: 
        """
        gatk SelectVariants -V {input.vcf} -R {input.genome_fasta} -O {params.output_dir}/Variant_filtered.temp.vcf --set-filtered-gt-to-nocall --exclude-filtered --exclude-non-variants --max-nocall-fraction {params.nocall}

        gatk SelectVariants -V {params.output_dir}/Variant_filtered.temp.vcf -R {input.genome_fasta} -O {output.vcf_filtered} --set-filtered-gt-to-nocall --exclude-filtered --exclude-non-variants --max-nocall-fraction {params.nocall}
        """

