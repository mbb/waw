rule <step_name>__deep_variant:
    input:
        **<step_name>__deep_variant_inputs()
    output: 
        vcf = config["results_dir"] + "/" + config["<step_name>__deep_variant_output_dir"] + "/{sample}_output.vcf.gz",
        #gvcf = config["results_dir"] + "/" + config["<step_name>__deep_variant_output_dir"] + "/{sample}_output.g.vcf.gz",
    params:
        command = config["<step_name>__deep_variant_command"],
        #bam_dir = lambda w, input: os.path.dirname(input.bams[0]),
        output_dir = config["results_dir"] + "/" + config["<step_name>__deep_variant_output_dir"]+ "/",
        out_dir_name = config["<step_name>__deep_variant_output_dir"],
        model_type = config["<step_name>__deep_variant_model_type"],
        log_dir = config["results_dir"] + "/logs/" + config["<step_name>__deep_variant_output_dir"] 

    log: 
        logfile = config["results_dir"] + "/logs/" + config["<step_name>__deep_variant_output_dir"] + "/{sample}_deep_variant_log.txt",
    threads: 
        config["<step_name>__deep_variant_threads"]
    shell:
        "samtools faidx {input.genome_fasta} ; \n "
        " eval \"$(conda shell.bash hook)\"; \n"
        " conda activate deepvariant; \n "
        "BIN_DIR=$(ls -d $CONDA_PREFIX/share/deepvariant*/binaries/DeepVariant/*/DeepVariant*); \n"
        "Basename=` basename {input.bams} .bam `; "
        "tmp_dir=`mktemp -d ` ;"
        "dv_make_examples.py "
        " --ref {input.genome_fasta} "
        " --reads {input.bams} "
        " --examples ${{tmp_dir}}/"
        " --sample ${{Basename}}"
        " --logdir  {params.log_dir} "
        " --cores {threads}; \n"
        "dv_call_variants.py "
        "--cores {threads} "
        "--outfile ${{tmp_dir}}/${{Basename}}.tmp "
        "--sample ${{Basename}} "
        "--examples ${{tmp_dir}} "
        "--model {params.model_type} \n"
        "python $BIN_DIR/postprocess_variants.zip "
        "--ref {input.genome_fasta} "
        "--infile ${{tmp_dir}}/${{Basename}}.tmp "
        "--outfile {output.vcf} ;\n "
        "conda deactivate; \n"
        "echo \"1\t./{params.out_dir_name}/output.visual_report.html\" > {params.output_dir}/text.deepvariant.tsv"
