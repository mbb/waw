rule <step_name>__octopus:
    input:
        **<step_name>__octopus_inputs()
    output:
        vcf = config["results_dir"]+"/"+config["<step_name>__octopus_output_dir"]+"/{sample}_output.vcf.gz",
    params:
        command = config["<step_name>__octopus_command"],
        vcf_out = config["results_dir"]+"/"+config["<step_name>__octopus_output_dir"]+"/{sample}_output.vcf",
    threads:
        config["<step_name>__octopus_threads"]        
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__octopus_output_dir"] + "/{sample}_octopus_log.txt"
    conda:
        "envs/octopus.yaml"    
    shell:
        " filename=$(basename -- {input.genome_fasta}); "
        " dirname=$(dirname -- {input.genome_fasta});"
        " extension=${{filename##*.}};"
        " filename=${{filename%.*}};"
        "if [[ ${{extension}} != 'fa' ]];"
        "then "
        "  reference=${{dirname}}/${{filename}}.fa; "
        "  cp -f {input.genome_fasta} $reference; "
        "  samtools faidx $reference; "
        "else "
        "  reference={input.genome_fasta};"
        "  samtools faidx $reference;"
        "fi; "
        "{params.command} "
        "--reference $reference "
        "--reads {input.bams} "
        "--threads {threads} "
        #"--filter-expression=\"QUAL < 10 | AD < 0 \" --annotations AD " #AD here shows only alt allels counts
        "| bcftools view -i \"%FILTER='PASS' | %FILTER='.'\" - > {params.vcf_out} "
        "2> >(tee {log} >&2); "
        "bgzip {params.vcf_out} && "
        "tabix -p vcf {output.vcf} "
