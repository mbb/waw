rule <step_name>__GATK4_VariantFiltration:
    input:
        **<step_name>__GATK4_VariantFiltration_inputs(),
    output: 
        vcf_filtered = config["results_dir"] + "/" + config["<step_name>__GATK4_VariantFiltration_output_dir"] + "/variant_filtered.vcf",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__GATK4_VariantFiltration_output_dir"],
        command = config["<step_name>__GATK4_VariantFiltration_command"],
        qd = "--filter-name \"QD"+str(config["<step_name>__GATK4_VariantFiltration_QualByDepth"])+"\" -filter \"QD < "+str(float(config["<step_name>__GATK4_VariantFiltration_QualByDepth"]))+"\"" if config["<step_name>__GATK4_VariantFiltration_QualByDepth"] != "NA" else "",
        qual = " --filter-name \"QUAL"+str(config["<step_name>__GATK4_VariantFiltration_QualPhred"])+"\" -filter \"QUAL < "+str(float(config["<step_name>__GATK4_VariantFiltration_QualPhred"]))+"\"" if config["<step_name>__GATK4_VariantFiltration_QualPhred"] != "NA" else "",
        fs = "--filter-name \"FS"+str(config["<step_name>__GATK4_VariantFiltration_FisherStrand"])+"\" -filter \"FS > "+str(float(config["<step_name>__GATK4_VariantFiltration_FisherStrand"]))+"\""  if config["<step_name>__GATK4_VariantFiltration_FisherStrand"] != "NA" else "",
        sor = "--filter-name \"SOR"+str(config["<step_name>__GATK4_VariantFiltration_StrandOddsRatio"])+"\" -filter \"SOR > "+str(float(config["<step_name>__GATK4_VariantFiltration_StrandOddsRatio"]))+"\"" if config["<step_name>__GATK4_VariantFiltration_StrandOddsRatio"] != "NA" else "",
        mq = "--filter-name \"MQ"+str(config["<step_name>__GATK4_VariantFiltration_RMSMappingQuality"])+"\" -filter \"MQ < "+str(float(config["<step_name>__GATK4_VariantFiltration_RMSMappingQuality"]))+"\"" if config["<step_name>__GATK4_VariantFiltration_RMSMappingQuality"] != "NA" else "",
        mqranksum = "--filter-name \"MQRankSum"+str(config["<step_name>__GATK4_VariantFiltration_MappingQualityRankSumTest"])+"\" -filter \"MQRankSum < "+str(float(config["<step_name>__GATK4_VariantFiltration_MappingQualityRankSumTest"]))+"\"" if config["<step_name>__GATK4_VariantFiltration_MappingQualityRankSumTest"] != "NA" else "",
        readposranksum = "--filter-name \"ReadPosRankSum"+str(config["<step_name>__GATK4_VariantFiltration_ReadPosRankSumTest"])+"\" -filter \"ReadPosRankSum < "+str(float(config["<step_name>__GATK4_VariantFiltration_ReadPosRankSumTest"]))+"\""  if config["<step_name>__GATK4_VariantFiltration_ReadPosRankSumTest"] != "NA" else "",
        dpall = "--filter-name \"LowDPAll"+str(config["<step_name>__GATK4_VariantFiltration_TotalDepth_All"])+"\" -filter \"DP < "+str(config["<step_name>__GATK4_VariantFiltration_TotalDepth_All"])+"\"" if config["<step_name>__GATK4_VariantFiltration_TotalDepth_All"] != "NA" else "",
        dpsample = "--G-filter-name \"LowDPGenotype"+str(config["<step_name>__GATK4_VariantFiltration_TotalDepth_Sample"])+"\" -G-filter \"DP < "+str(config["<step_name>__GATK4_VariantFiltration_TotalDepth_Sample"])+"\"" if config["<step_name>__GATK4_VariantFiltration_TotalDepth_Sample"] != "NA" else "",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__GATK4_VariantFiltration_output_dir"] + "GATK4_VariantFiltration_log.txt"
    threads: 1
    conda:
        "envs/gatk4.yaml"
    shell: 
        """
        gatk VariantFiltration -V {input.vcf} -R {input.genome_fasta} {params.qd} {params.qual} {params.fs} {params.sor} {params.mq} {params.mqranksum} {params.readposranksum} {params.dpall} {params.dpsample} -O {output.vcf_filtered}
        """

