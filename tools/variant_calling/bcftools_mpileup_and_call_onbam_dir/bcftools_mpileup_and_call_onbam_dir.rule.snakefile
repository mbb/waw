rule <step_name>__bcftools_mpileup_and_call_onbam_dir:
    input:
        **<step_name>__bcftools_mpileup_and_call_onbam_dir_inputs()
    output:
        vcf = config["results_dir"]+"/"+config["<step_name>__bcftools_mpileup_and_call_onbam_dir_output_dir"]+"/variants.vcf.gz",
        stats = config["results_dir"]+"/"+config["<step_name>__bcftools_mpileup_and_call_onbam_dir_output_dir"]+"/vcf_stats.txt"
    params:
        command = config["<step_name>__bcftools_mpileup_and_call_onbam_dir_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__bcftools_mpileup_and_call_onbam_dir_output_dir"],
        minctgSize = config["<step_name>__bcftools_mpileup_and_call_onbam_dir_minctgSize"],
        removeindels = "--remove-indels" if config["<step_name>__bcftools_mpileup_and_call_onbam_dir_snpOnly"] else " ",
        minMeanDP = config["<step_name>__bcftools_mpileup_and_call_onbam_dir_minMeanDepth"],
        missing   = config["<step_name>__bcftools_mpileup_and_call_onbam_dir_missing"],
        maf       = config["<step_name>__bcftools_mpileup_and_call_onbam_dir_minmaf"],
        biallelic = "--min-alleles 2 --max-alleles 2" if config["<step_name>__bcftools_mpileup_and_call_onbam_dir_biallelicOnly"] else " "
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__bcftools_mpileup_and_call_onbam_dir_output_dir"] + "/bcftools_mpileup_and_call_onbam_dir_log.txt"
    threads:
        config["<step_name>__bcftools_mpileup_and_call_onbam_dir_threads"]
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"            
    shell:
        # prepare fasta index (transform gzip to bgzip then index)
        "if gzip -t '{input.genome_fasta}'; then "
        "mv {input.genome_fasta} tmp_genome.gz; "
        "zcat tmp_genome.gz | bgzip -c > {input.genome_fasta}; "
        "rm tmp_genome.gz; "
        "fi; "
        "if [ ! -f '{input.genome_fasta}.fai' ] ; then "
        "samtools faidx {input.genome_fasta}; "
        "fi; "
        "ls  {input.bamDir}/*.bam > {params.output_dir}/bams.txt; "
        #ctgs are always ordered by size. So the ctgs at the end of the list will run very fast while those at the top will be slow. So mix the ctgs with shuf
        "awk -v minctg={params.minctgSize} '{{if($2 >= minctg) print $1}}' {input.genome_fasta}.fai | shuf > {params.output_dir}/ctgs_liste.txt; "
        "nbCtgs=$(cat {params.output_dir}/ctgs_liste.txt | wc -l); "
        "size=$(($nbCtgs/{threads})); "
        "if [ $size -eq 0 ]; then size=1; fi ;"
        "split -l $size --numeric-suffixes=1 {params.output_dir}/ctgs_liste.txt {params.output_dir}/regions; "
        "nbregions=$(ls -1 {params.output_dir}/regions* | wc -l) ;"
        "parallel -j {threads} '{params.command} mpileup "
        "-Ou " 
        "-a FORMAT/AD,FORMAT/DP "
        "-f {input.genome_fasta} "
        "-R {params.output_dir}/regions{{}} "
        "-b {params.output_dir}/bams.txt  "
        "| {params.command} call "
        "-vmO z "
        "-o {params.output_dir}/tmp{{}}.vcf.gz' ::: $(for i in `seq 1 $nbregions` ; do printf '%02d ' $i  ; done)  "
        "|& tee {log} ; "
        "for i in {params.output_dir}/*.vcf.gz; do echo $i; done | sort -V > {params.output_dir}/vcf_list.txt; "
        "bcftools concat --file-list {params.output_dir}/vcf_list.txt | "
        "vcftools --vcf - {params.removeindels} --maf {params.maf}  --min-meanDP {params.minMeanDP} --max-missing {params.missing} {params.biallelic} --recode --stdout | bgzip -c > {output.vcf}; "
        "tabix -p vcf {output.vcf} "
        "|& tee -a {log}; "
        "rm -f {params.output_dir}/tmp*.vcf.gz; "
        "rm -f {params.output_dir}/regions*; "
        " {params.command} stats "
        "-F {input.genome_fasta} "
        "-s - "
        "{output.vcf} > {output.stats}"
        "|& tee -a {log} "
