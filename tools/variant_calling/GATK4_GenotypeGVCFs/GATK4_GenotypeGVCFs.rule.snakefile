# File generate with generate_tool_snakefile.py

rule <step_name>__GATK4_GenotypeGVCFs:
    input:
        **<step_name>__GATK4_GenotypeGVCFs_inputs(),
    output:
        vcf = config["results_dir"] + "/" + config["<step_name>__GATK4_GenotypeGVCFs_output_dir"] + "/output.vcf.gz"
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__GATK4_GenotypeGVCFs_output_dir"],
        command = config["<step_name>__GATK4_GenotypeGVCFs_command"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__GATK4_GenotypeGVCFs_output_dir"] + "/GATK4_GenotypeGVCFs_log.txt"
    threads: 
        1
    conda:
        "envs/gatk4.yaml"
    shell: 
        """
        db=$(basename {input.vcfdb})
        path=$(dirname {input.vcfdb})
        cd $path
        mkdir -p {params.output_dir}/tmp
        gatk GenotypeGVCFs -V  gendb://$db -R {input.genome_fasta} -O {output.vcf} -tmp-dir {params.output_dir}/tmp
        """

