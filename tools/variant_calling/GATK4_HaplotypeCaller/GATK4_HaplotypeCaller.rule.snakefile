rule <step_name>__GATK4_HaplotypeCaller:
    input:
        **<step_name>__GATK4_HaplotypeCaller_inputs(),
    output: 
        vcf = config["results_dir"] + "/" + config["<step_name>__GATK4_HaplotypeCaller_output_dir"] + "/{sample}_variants.vcf.gz",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__GATK4_HaplotypeCaller_output_dir"],
        command = config["<step_name>__GATK4_HaplotypeCaller_command"],
        ploidy = config["<step_name>__GATK4_HaplotypeCaller_ploidy"],
        minphred_threshold_to_call = config["<step_name>__GATK4_HaplotypeCaller_minphred_threshold_to_call"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__GATK4_HaplotypeCaller_output_dir"] + "/{sample}_GATK4_HaplotypeCaller_log.txt"
    threads:  
        config["<step_name>__GATK4_HaplotypeCaller_threads"]
    conda:
        "envs/gatk4.yaml"
    shell:
        """
        gatk HaplotypeCaller -R {input.genome_fasta} -I {input.bams} -O {output.vcf} -ERC GVCF -ploidy {params.ploidy} --native-pair-hmm-threads {threads}
        """


