rule <step_name>__GATK4_GenomicsDBImport:
    input:
        **<step_name>__GATK4_GenomicsDBImport_inputs(),
    output:
        vcfdb = directory(config["results_dir"] + "/" + config["<step_name>__GATK4_GenomicsDBImport_output_dir"] + "/dgbi/"),
        vcfheader = config["results_dir"] + "/" + config["<step_name>__GATK4_GenomicsDBImport_output_dir"] + "/dgbi/vcfheader.vcf",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__GATK4_GenomicsDBImport_output_dir"],
        command = config["<step_name>__GATK4_GenomicsDBImport_command"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__GATK4_GenomicsDBImport_output_dir"] + "/GATK4_GenomicsDBImport_log.txt"
    threads: 1
    conda:
        "envs/gatk4.yaml"
    shell: 
        """
        lfile_i=({input});
        len=${{#lfile_i[@]}};
        n=$(($len-1))
        mkdir -p {params.output_dir}
        # Define intervals : all the genome if no bed file in input 3 and the coordinate in bed file if bed file options
        if [[ ${{lfile_i[$n]}} =~ \.bed$ ]]; then
            awk -F "\\t" '{{print $1":"$2+1"-"$3+1}}' ${{lfile_i[$n]}} > {params.output_dir}/gatk.intervals
        else
            if [ "${{lfile_i[$n]}}" = "{input.genome_fasta}" ]; then
                samtools faidx ${{lfile_i[$n]}} -o {params.output_dir}/coordinates.fa.fai
                cut -f 1 {params.output_dir}/coordinates.fa.fai > {params.output_dir}/gatk.intervals
            else
                echo "ERROR : You probably set a bed file that not have extension .bed"
                exit 1
            fi
        fi
        
        # Define input vcf
        vcfinput=$(echo {input.vcf} | sed 's/ / -V /g');

        rm -rf {params.output_dir}/dgbi
        mkdir -p {params.output_dir}/tmp
        gatk GenomicsDBImport -V $vcfinput --genomicsdb-workspace-path {params.output_dir}/dgbi -L {params.output_dir}/gatk.intervals -tmp-dir {params.output_dir}/tmp
        """

