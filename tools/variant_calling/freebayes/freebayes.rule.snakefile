rule <step_name>__freebayes:
    input:
        **<step_name>__freebayes_inputs()
    output:
        vcf = config["results_dir"]+"/"+config["<step_name>__freebayes_output_dir"]+"/{sample}_output.vcf.gz",
        #gvcf = config["results_dir"]+"/"+config["<step_name>__freebayes_output_dir"]+"/output.g.vcf",
    params:
        command = config["<step_name>__freebayes_command"],
        #bams = list(map("--bam {}".format, input.bams)),
        vcf_out = config["results_dir"]+"/"+config["<step_name>__freebayes_output_dir"]+"/{sample}_output.vcf",
    threads:
        config["<step_name>__freebayes_threads"]     
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__freebayes_output_dir"] + "/{sample}_freebayes_log.txt"
    conda:
        "envs/freebayes.yaml"    
    shell:
        "if [ ! -f {input.genome_fasta}.fai ]; then \n"
        " samtools faidx {input.genome_fasta} ; \n "
        "fi; \n " 
        " export PATH=$PATH:/opt/biotools/freebayes/scripts/ && "
        " {params.command} <(fasta_generate_regions.py {input.genome_fasta}.fai 100000) {threads} -f {input.genome_fasta} {input.bams} --genotype-qualities "  
        "> {params.vcf_out} "
        "2> >(tee {log} >&2); "
        "bgzip {params.vcf_out} && "
        "tabix -p vcf {output.vcf} "
