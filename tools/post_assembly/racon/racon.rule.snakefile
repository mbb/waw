rule <step_name>__racon:
    input:
        **<step_name>__racon_inputs(),
    output: 
        assembly_corrected = config["results_dir"] + "/" + config["<step_name>__racon_output_dir"] + "/assembly_corrected.fasta",
    params: 
        command = config["<step_name>__racon_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__racon_output_dir"]+ "/",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__racon_output_dir"] + "/racon_log.txt"
    threads: 
        config["<step_name>__racon_threads"]
    conda:
        "envs/racon.yaml"
    shell: 
        "{params.command} "
        "-u "
        "-t {threads} "
        "{input.reads} "
        "{input.overlaps} "
        "{input.assembly} "
        "> {output.assembly_corrected} "
        "2> >(tee {log} >&2)"
