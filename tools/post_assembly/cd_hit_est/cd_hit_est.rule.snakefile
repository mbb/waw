rule <step_name>__cd_hit_est:
    input:
        **<step_name>__cd_hit_est_inputs(),
    output:
        filtered_assembly = config["results_dir"] + "/" + config["<step_name>__cd_hit_est_output_dir"] + "/Assembly_transcripts_filtered_clustered.fa",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__cd_hit_est_output_dir"],
        res_dir = config["results_dir"],
        identity = config["<step_name>__cd_hit_est_identity"],
        word_length = config["<step_name>__cd_hit_est_word_length"],
        memory = config["<step_name>__cd_hit_est_memory_limit"],
    threads:
        config["<step_name>__cd_hit_est_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__cd_hit_est_output_dir"] + '/cd_hit_est_log.txt'
    conda:
        "envs/cdhit.yaml"
    shell:
        """
        cd-hit-est -i {input} -o {output} -c {params.identity} -n {params.word_length} -M {params.memory} -T {threads}
        """