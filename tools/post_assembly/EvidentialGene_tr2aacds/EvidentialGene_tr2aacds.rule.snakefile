rule <step_name>__EvidentialGene_tr2aacds:
    input:
        **<step_name>__EvidentialGene_tr2aacds_inputs(),
    output:
        okay = config["results_dir"] + "/" + config["<step_name>__EvidentialGene_tr2aacds_output_dir"] + "/AllAssembly_Combined_Evigene_Okay.fa",
        okall = config["results_dir"] + "/" + config["<step_name>__EvidentialGene_tr2aacds_output_dir"] + "/AllAssembly_Combined_Evigene_Okall.fa",
        okalt = config["results_dir"] + "/" + config["<step_name>__EvidentialGene_tr2aacds_output_dir"] + "/AllAssembly_Combined_Evigene_Okalt.fa"
    params:
        outdir = config["results_dir"] + "/" + config["<step_name>__EvidentialGene_tr2aacds_output_dir"],
        mem = config["<step_name>__EvidentialGene_tr2aacds_memory_limit"],
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__EvidentialGene_tr2aacds_output_dir"] + '/EvidentialGene_log.txt',
    conda:
        "envs/evigene.yaml",
    threads:
        config["<step_name>__EvidentialGene_tr2aacds_threads"],
    shell:
        """
        # Define env for analysis
        script_path=$(echo $CONDA_PREFIX"/share/evigene/scripts/prot/");
        export PATH=$script_path:$PATH;
        # List input file to determine number of input. Concatenate all input in one file to run evidentialGene on.
        lfile_i=({input});
        len=${{#lfile_i[@]}};
        echo $lfile_i
        echo $len
        mkdir -p {params.outdir}
        cd {params.outdir}
        cat {input.transcript_ass1} > {params.outdir}/AllAssembly_Concat.fa
        # Run EvidentialGene
        for (( j=1; j<$len; j++ ));
        do
            cat ${{lfile_i[$j]}} >> {params.outdir}/AllAssembly_Concat.fa
        done
        tr2aacds.pl -tidyup -NCPU={threads} -MAXMEM={params.mem} -log -cdna {params.outdir}/AllAssembly_Concat.fa
        cat okayset/AllAssembly_Concat.okay.tr okayset/AllAssembly_Concat.okalt.tr > AllAssembly_Combined_Evigene_Okall.fa
        cp okayset/AllAssembly_Concat.okay.tr AllAssembly_Combined_Evigene_Okay.fa
        cp okayset/AllAssembly_Concat.okalt.tr AllAssembly_Combined_Evigene_Okalt.fa


        # Evidential gene adapt to the number of input and renaming file depending of the input, but bug on the output which is not always the same.
        # if [ $len -ge 2 ]; then
        #     for (( j=1; j<$len; j++ ));
        #     do
        #         cat ${{lfile_i[$j]}} >> {params.outdir}/AllAssembly_Concat.fa
        #     done
        #     tr2aacds.pl -tidyup -NCPU={threads} -MAXMEM={params.mem} -log -cdna {params.outdir}/AllAssembly_Concat.fa
        #     cat okayset/AllAssembly_Concat.okay.tr okayset/AllAssembly_Concat.okalt.tr > AllAssembly_Combined_Evigene_Okall.fa
        #     cp okayset/AllAssembly_Concat.okay.tr AllAssembly_Combined_Evigene_Okay.fa
        #     cp okayset/AllAssembly_Concat.okalt.tr AllAssembly_Combined_Evigene_Okalt.fa
        # else
        #     tool=$(head -n 1 {params.outdir}/AllAssembly_Concat.fa | grep -oP '(?<=^>).*?(?=_)')
        #     mv {params.outdir}/AllAssembly_Concat.fa {params.outdir}/AllAssembly_${{tool}}.fa
        #     tr2aacds.pl -tidyup -NCPU={threads} -MAXMEM={params.mem} -log -cdna {params.outdir}/AllAssembly_${{tool}}.fa
        #     cat okayset/AllAssembly_${{tool}}.okay.tr okayset/AllAssembly_${{tool}}.okalt.tr > AllAssembly_${{tool}}_Evigene_Okall.fa
        #     cp okayset/AllAssembly_${{tool}}.okay.tr AllAssembly_${{tool}}_Evigene_Okay.fa
        #     cp okayset/AllAssembly_${{tool}}.okalt.tr AllAssembly_${{tool}}_Evigene_Okalt.fa
        # fi

        rm -r dropset/ inputset/ tmpfiles/
        find . ! -name '*.fa' -type f -maxdepth 1 -exec rm -f {{}} +
        """
