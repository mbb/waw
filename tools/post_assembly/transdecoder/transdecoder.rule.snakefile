rule <step_name>__transdecoder:
    input:
        **<step_name>__transdecoder_inputs(),
    output:
        pep = config["results_dir"] + "/" + config["<step_name>__transdecoder_output_dir"] + "/transcripts.fasta.transdecoder.pep",
        cds = config["results_dir"] + "/" + config["<step_name>__transdecoder_output_dir"] + "/transcripts.fasta.transdecoder.cds",
        gff3 = config["results_dir"] + "/" + config["<step_name>__transdecoder_output_dir"] + "/transcripts.fasta.transdecoder.gff3",
        bed = config["results_dir"] + "/" + config["<step_name>__transdecoder_output_dir"] + "/transcripts.fasta.transdecoder.bed"
    params:
        min_protein_len = config["<step_name>__transdecoder_min_protein_len"],
        useBLASTp = config["<step_name>__transdecoder_useBLASTp"],
        useHMMscan = config["<step_name>__transdecoder_useHMMscan"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__transdecoder_output_dir"],
        swissprot_db = config["databases_dir"] + "/" + config["db"]["uniprot_swissprot"]["file"],
        pfam_db = config["databases_dir"] + "/" + config["db"]["pfam_A_hmm"]["file"],
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__transdecoder_output_dir"] + '/transdecoder_log.txt'
    threads: 
        config["<step_name>__transdecoder_threads"]
    conda:
        "envs/transdecoder.yaml"
    run:
        shell("""
        TransDecoder.LongOrfs \
        -t {input.assembly} \
        -m {params.min_protein_len} \
        -O {params.output_dir} \
        |& tee {log}\
         && \
        TransDecoder.LongOrfs \
        -t {input.assembly} \
        --cpu {threads} \
        -O {params.output_dir} \
        |& tee -a {log}
        """)

        if(config["<step_name>__transdecoder_useBLASTp"]):
            shell("""
            blastp \
            -query {params.output_dir}/longest_orfs.pep  \
            -db {params.swissprot_db} \
            -max_target_seqs 1 \
            -outfmt 6 \
            -evalue 1e-5 \
            -num_threads {threads} \
            > {params.output_dir}/blastp.outfmt6
            """)

        if (config["<step_name>__transdecoder_useHMMscan"]):
            shell("""
            hmmscan \
            --cpu {threads} \
            --domtblout {params.output_dir}/pfam.domtblout \
            {params.pfam_db} \
            {params.output_dir}/longest_orfs.pep
            """)

        if (config["<step_name>__transdecoder_useBLASTp"] or config["<step_name>__transdecoder_useHMMscan"]):
            shell("""
            TransDecoder.Predict \
            -t {input.assembly} """ +
            if config["<step_name>__transdecoder_useHMMscan"] "--retain_pfam_hits {params.output_dir}/pfam.domtblout " else "" +
            if config["<step_name>__transdecoder_useBLASTp"] "--retain_blastp_hits {params.output_dir}/blastp.outfmt6 " else ""
            )