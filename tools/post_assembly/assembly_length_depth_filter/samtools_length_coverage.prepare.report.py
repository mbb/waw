import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import patchworklib as pw
import sys


df=pd.read_table(sys.argv[1])


pw.param["margin"]=0.5
pw.param["equal_spacing"]=True

hue_order = ['0-200 bp', '200-500 bp', '500-1000 bp', '1000+ bp', 'Filtered on length or depth']


ax1 = pw.Brick(figsize=(5,5))
sns.scatterplot(data=df, x="endpos", y="meandepth", hue="Length", hue_order=hue_order, palette=["C0", "C1", "C2", "C3", "C7"], markers="x",alpha=0.5,ax=ax1)

ax1.set(xscale="log")
ax1.set_xlim(-2000, max(df.endpos)+1000)
ax1.set(yscale="log")
ax1.set_ylim(-10000, max(df.meandepth)+1000)

ax1.set(xlabel="Sequences length")
ax1.set(ylabel="Sequences depth")
ax1.set_title("Distribution sequence depth and length in assembly")
ax1.get_legend().remove()






ax3=pw.Brick(figsize=(8,5))
sns.countplot(df, x="Depth_Average", hue="Length", hue_order=hue_order, palette=["C0", "C1", "C2", "C3", "C7"], ax=ax3)
ax3.set(yscale="log")
ax3.set(xlabel="Sequences average depth")
ax3.set(ylabel="Number of Sequences")
ax3.set_title("Distribution sequence depth and length in assembly")


ax123 = ax1 | ax3
#ax123 = ax1 / ax2 | ax3
ax123.savefig(sys.argv[2])


