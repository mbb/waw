rule <step_name>__assembly_length_depth_filter:
    input:
        **<step_name>__assembly_length_depth_filter_inputs(),
    output:
        fa_filter = config["results_dir"] + "/" + config["<step_name>__assembly_length_depth_filter_output_dir"] + "/Assembly_filteredLengthDepth.fa",
        stats = config["results_dir"] + "/" + config["<step_name>__assembly_length_depth_filter_output_dir"] + "/Assembly_StatLengthDepth_filtered.tsv",
    params:
        outdir = config["results_dir"] + "/" + config["<step_name>__assembly_length_depth_filter_output_dir"],
        length = config["<step_name>__filter_length"],
        depth = config["<step_name>__filter_depth"],
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__assembly_length_depth_filter_output_dir"] + '/assembly_length_depth_filter_log.txt'
    conda:
        "envs/bed-bcf-vcf-sam-tools.yaml"
    shell:
        """
        # Generate stat file
        samtools coverage {input.bam} -o {params.outdir}/assembly_samtools_coverage.txt

        # Filter
        awk -F "\\t" -v len={params.length} -v depth={params.depth} '{{if(NR==1){{print $0"\\tDepth_Average\\tLength" > "{output.stats}" }}else{{if($3<=200){{size="0-200 bp"}}else if($3>200 && $3<=500){{size="200-500 bp"}}else if($3>500 && $3 <=1000){{size="500-1000 bp"}}else{{size="1000+ bp"}};if($7>=0 && $7<=5){{average="0-5 x"}}else if($7>5 && $7<=10){{average="5-10 x"}}else if($7>10 && $7<=20){{average="10-20 x"}}else if($7>20 && $7<=50){{average="20-50 x"}}else if($7>50 && $7<=100){{average="50-100 x"}}else if($7>100 && $7<=1000){{average="100-1000 x"}}else if($7>1000){{average="1000+ x"}};if($3>=len && $7>=depth){{print $1}}else{{size="Filtered on length or depth"}};print $0"\\t"average"\\t"size > "{params.outdir}/tmp.txt"}}}}' {params.outdir}/assembly_samtools_coverage.txt | seqtk subseq {input.fa} - > {output.fa_filter}
        
        sort -V -k10 {params.outdir}/tmp.txt >> {output.stats}
        rm {params.outdir}/tmp.txt
        
        # Draw plot to visualize depth and length of the assembly
        python {snake_dir}/scripts/<step_name>__samtools_length_coverage.prepare.report.py {output.stats} {params.outdir}/length_coverage_assembly_mqc.png
        """

