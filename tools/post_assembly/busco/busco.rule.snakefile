rule <step_name>__busco:
    input:
        **<step_name>__busco_inputs(),
    output: 
        bed = config["results_dir"] + "/" + config["<step_name>__busco_output_dir"] + "/" + re.sub("\.fa(s)?(t)?(a)?", "", <step_name>__busco_inputs()["fasta_seq"].split("/")[-1]) + "/busco.bed",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__busco_output_dir"],
        command = config["<step_name>__busco_command"],
        mode = config["<step_name>__busco_mode"],
        lineage = config["<step_name>__busco_lineage"] if "auto" in config["<step_name>__busco_lineage"] else "-l "+config["<step_name>__busco_lineage"],
        name = re.sub('\.fa(s)?(t)?(a)?', '', <step_name>__busco_inputs()["fasta_seq"].split("/")[-1])
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__busco_output_dir"] + "/busco_log.txt"
    threads: 
        config["<step_name>__busco_threads"] 
    conda:
        "envs/busco.yaml"    
    shell:
        """
        cd {params.output_dir}
        busco -f -i {input.fasta_seq} {params.lineage} --out_path {params.output_dir} --out {params.name} -m {params.mode} -c {threads} |& tee {log}
        cd {params.output_dir}/{params.name}/run_*/
        mv short_summary.json summary.json
        mv short_summary.txt summary.txt
        awk -F "\\t" '{{if($2~/Complete/){{if($6~/+/){{print $3"\\t"$4"\\t"$5"\\t"$1"|"$9}}else{{print $3"\\t"$5"\\t"$4"\\t"$1"|"$9}}}}}}' full_table.tsv > {output.bed}

        #sort -V -k1,1 -k2,2 busco.bed | awk -F "\t" '{split($4,id,"|");system("grep "$1" single_copy_busco_sequences/"id[1]".gff >> busco_miniprot.gff")}'
        #agat_convert_sp_gxf2gxf.pl -g busco_miniprot.gff -o test.gff
        #agat_convert_sp_gff2gtf.pl -gff busco.gff --gtf_version 2.2 -o genes.gtf
        """

