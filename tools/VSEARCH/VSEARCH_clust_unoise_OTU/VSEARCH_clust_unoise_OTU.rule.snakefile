rule <step_name>__VSEARCH_clust_unoise_OTU:
    input:
        **<step_name>__VSEARCH_clust_unoise_OTU_inputs(),
    output: 
        centroids = config["results_dir"] + "/" + config["<step_name>__VSEARCH_clust_unoise_OTU_output_dir"]+"/unoise-nonchimaras.fasta",
        otutab = config["results_dir"] + "/" + config["<step_name>__VSEARCH_clust_unoise_OTU_output_dir"]+"/unoise-nonchimaras-OTU.tsv",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__VSEARCH_clust_unoise_OTU_output_dir"],
        command = config["<step_name>__VSEARCH_clust_unoise_OTU_command"],
        alpha = config["<step_name>__VSEARCH_clust_unoise_OTU_alpha"],
        minsize = config["<step_name>__VSEARCH_clust_unoise_OTU_minsize"],
    threads:
        config["<step_name>__VSEARCH_clust_unoise_OTU_threads"]
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__VSEARCH_clust_unoise_OTU_output_dir"] + "/VSEARCH_clust_unoise_OTU_log.txt"
    conda:
        "envs/vsearch.yaml"        
    shell: 
        """ 
         #https://groups.google.com/g/vsearch-forum/c/ws7NEP68gO8/m/eFb8m_TCAAAJ
         #The unoise3 functionality in USEARCH is separated into two commands in vsearch: cluster_unoise and uchime3_denovo. The first clusters the sequences and the other performs chimera detection.
         
         #precluster
         {params.command} --threads {threads} \
         --cluster_unoise  {input.fasta}  \
         --centroids {params.output_dir}/preclustered.fasta \
         --uc {params.output_dir}/preclustered.uc \
         --minsize {params.minsize} \
         --unoise_alpha {params.alpha} \
         --sizein \
         --sizeout ; \

         #denovo chimera removal
         {params.command} --threads {threads} \
         --uchime3_denovo {params.output_dir}/preclustered.fasta \
         --sizein \
         --sizeout \
         --fasta_width 0 \
         --nonchimeras {params.output_dir}/denovo.nonchimeras.fasta;
         
         #Extract all non-chimeric, non-singleton sequences, dereplicated
         #$PERL ../map.pl all.derep.fasta all.preclustered.uc all.ref.nonchimeras.fasta > all.nonchimeras.derep.fasta
            awk 'fname != FILENAME {{ fname = FILENAME; idx++ ;}}
                idx == 1 {{ # read fasta2 file with accepted sequences
                    if (match($0,/^>([^ ;]+)/, arr) ) {{ accepted[arr[1] ] = 1}}
                }}
                idx == 2 {{ # read uc file with mapping  
                    nbcols=split($0,col,"\t");
                    a=""; b="";
                    if (match(col[9], /^([^ ;*]+)/, arr ) ) {{	a = arr[1];}}
                    if (match(col[10], /^([^ ;*]+)/, arr ) ) {{ b = arr[1];}}
                    if ((b != "") && (accepted[b]) && (a != "")) {{ accepted[a] = 1}}
                }}
                idx == 3 {{ # read original fasta1 file
                    ok=0;
                    if (match($0,/^>([^ ;]+)/, arr) ) {{ ok = accepted[arr[1] ];}}
                    if (ok == 1) {{print $0; getline; print}}
                }}
                ' {params.output_dir}/denovo.nonchimeras.fasta {params.output_dir}/preclustered.uc  {input.fasta} > {params.output_dir}/all.nonchimeras.derep.fasta

          # Extract all non-chimeric, non-singleton sequences in each sample
          #$PERL ../map.pl all.fasta all.derep.uc all.nonchimeras.derep.fasta > all.nonchimeras.fasta
          awk 'fname != FILENAME {{ fname = FILENAME; idx++ }}
                idx == 1 {{ # read fasta2 file with accepted sequences
                    if (match($0,/^>([^ ;]+)/, arr) ) {{ accepted[arr[1] ] = 1}}
                }}
                idx == 2 {{ # read uc file with mapping  
                    nbcols=split($0,col,"\t");
                    a=""; b="";
                    if (match(col[9], /^([^ ;*]+)/, arr ) ) {{ a = arr[1];}}
                    if (match(col[10], /^([^ ;*]+)/, arr ) ) {{ b = arr[1];}}
                    if ((b != "") && (accepted[b]) && (a != "")) {{accepted[a] = 1}}
                }}
                idx == 3 {{ # read original fasta1 file
                    ok=0;
                    if (match($0,/^>([^ ;]+)/, arr) ) {{ ok = accepted[arr[1] ];}}
                    if (ok == 1) {{print $0; getline; print}}
                }}
                ' {params.output_dir}/all.nonchimeras.derep.fasta {input.ucfile} {input.allfasta} > {params.output_dir}/all.nonchimeras.fasta

         #Cluster and relabel with OTU_n, generate OTU table
         {params.command} --threads {threads} \
         --cluster_unoise {params.output_dir}/all.nonchimeras.fasta \
         --sizein \
         --relabel OTU_ \
         --centroids {output.centroids} \
         --otutabout {output.otutab} |& tee {log}; 
         echo "# plot_type: 'table'
# section_name: 'OTUtable'
# description: 'Top 20 OTUs table'
# pconfig:
#     namespace: 'Cust Data'
" > {params.output_dir}/header.txt;
         head -n 21 {output.otutab} >> {params.output_dir}/header.txt;
         mv {params.output_dir}/header.txt {params.output_dir}/top_OTU_mqc.tsv;
         sed -i 's/#OTU/OTU/' {params.output_dir}/top_OTU_mqc.tsv
        """
    