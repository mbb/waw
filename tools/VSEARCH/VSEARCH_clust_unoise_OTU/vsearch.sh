#!/bin/sh                                                                       

# This is an example of a pipeline using vsearch to process data in the         
# Mothur 16S rRNA MiSeq SOP tutorial dataset to perform initial paired-end      
# read merging, quality filtering, chimera removal and OTU clustering.          

THREADS=40
REF=../gold.fasta
PERL=$(which perl)
VSEARCH=$(which vsearch)

date

# Enter subdirectory                                                            

cd pairs2

echo
echo Checking FASTQ format version for one file

$VSEARCH --threads $THREADS \
    --fastq_chars $(ls -1 *.fastq | head -1)

# Process samples                                                               

for f in *_R1*.fastq; do

    r=$(sed -e "s/_R1/_R2/" <<< "$f")
    s=$(cut -d_ -f1 <<< "$f")

    echo
    echo ====================================
    echo Processing sample $s
    echo ====================================

    $VSEARCH --threads $THREADS \
        --fastq_mergepairs $f \
        --reverse $r \
        --fastq_minovlen 200 \
        --fastq_maxdiffs 15 \
        --fastqout /tmp/vsearch_unoise/$s.merged.fastq \
        --fastq_eeout

    # Commands to demultiplex and remove tags and primers                       
    # using e.g. cutadapt may be added here.                                    

    echo
    echo Calculate quality statistics

    $VSEARCH --threads $THREADS \
        --fastq_eestats /tmp/vsearch_unoise/$s.merged.fastq \
        --output /tmp/vsearch_unoise/$s.stats
    echo
    echo Quality filtering

    $VSEARCH --threads $THREADS \
        --fastq_filter /tmp/vsearch_unoise/$s.merged.fastq \
        --fastq_maxee 0.5 \
        --fastq_minlen 225 \
        --fastq_maxlen 275 \
        --fastq_maxns 0 \
        --fastaout /tmp/vsearch_unoise/$s.filtered.fasta \
        --fasta_width 0

    echo
    echo Dereplicate at sample level and relabel with sample_n

    $VSEARCH --threads $THREADS \
        --derep_fulllength /tmp/vsearch_unoise/$s.filtered.fasta \
        --strand plus \
        --output /tmp/vsearch_unoise/$s.derep.fasta \
        --sizeout \
        --uc /tmp/vsearch_unoise/$s.derep.uc \
        --relabel $s. \
        --fasta_width 0

done

echo Sum of unique sequences in each sample: $(cat /tmp/vsearch_unoise/*.derep.fasta | grep -c "^>")

# At this point there should be one fasta file for each sample                  
# It should be quality filtered and dereplicated.                               

echo
echo ====================================
echo Processing all samples together
echo ====================================

echo
echo Merge all samples

cd /tmp/vsearch_unoise/

rm -f all.derep.fasta all.nonchimeras.derep.fasta
cat *.derep.fasta > all.fasta

echo
echo Dereplicate across samples and remove singletons

$VSEARCH --threads $THREADS \
    --derep_fulllength all.fasta \
    --minuniquesize 2 \ #here !
    --sizein \
    --sizeout \
    --fasta_width 0 \
    --uc all.derep.uc \
    --output all.derep.fasta

echo Unique non-singleton sequences: $(grep -c "^>" all.derep.fasta)

echo
echo zOtus cluster before chimera detection
 
$VSEARCH --threads $THREADS \
    --cluster_unoise all.derep.fasta \
    --sizein \
    --sizeout \
    --uc all.preclustered.uc \
    --centroids all.preclustered.fasta

echo Unique sequences after preclustering: $(grep -c "^>" all.preclustered.fasta)

echo
echo De novo chimera detection

$VSEARCH --threads $THREADS \
    --uchime3_denovo all.preclustered.fasta \
    --sizein \
    --sizeout \
    --fasta_width 0 \
    --nonchimeras all.denovo.nonchimeras.fasta 

echo Unique sequences after de novo chimera detection: $(grep -c "^>" all.denovo.nonchimeras.fasta)

echo
echo Reference chimera detection

# $VSEARCH --threads $THREADS \
#     --uchime_ref all.denovo.nonchimeras.fasta \
#     --db $REF \
#     --sizein \
#     --sizeout \
#     --fasta_width 0 \
#     --nonchimeras all.ref.nonchimeras.fasta

# echo Unique sequences after reference-based chimera detection: $(grep -c "^>" all.ref.nonchimeras.fasta)
cp all.denovo.nonchimeras.fasta all.ref.nonchimeras.fasta


# echo
# echo Extract all non-chimeric, non-singleton sequences, dereplicated

$PERL ../map.pl all.derep.fasta all.preclustered.uc all.ref.nonchimeras.fasta > all.nonchimeras.derep.fasta

echo Unique non-chimeric, non-singleton sequences: $(grep -c "^>" all.nonchimeras.derep.fasta)

echo
echo Extract all non-chimeric, non-singleton sequences in each sample

$PERL ../map.pl all.fasta all.derep.uc all.nonchimeras.derep.fasta > all.nonchimeras.fasta

echo Sum of unique non-chimeric, non-singleton sequences in each sample: $(grep -c "^>" all.nonchimeras.fasta)

echo
echo Cluster and relabel with OTU_n, generate OTU table

$VSEARCH --threads $THREADS \
    --cluster_unoise all.nonchimeras.fasta \
    --sizein \
    --uc all.clustered.uc \
    --relabel OTU_ \
    --centroids all.otus.fasta \
    --otutabout all.otutab.txt

echo
echo Number of OTUs: $(grep -c "^>" all.otus.fasta)

echo
echo Done

date
