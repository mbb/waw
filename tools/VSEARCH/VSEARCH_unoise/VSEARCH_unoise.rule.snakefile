rule <step_name>__VSEARCH_unoise:
    input:
        **<step_name>__VSEARCH_unoise_inputs(),
    output: 
        centroids = config["results_dir"] + "/" + config["<step_name>__VSEARCH_unoise_output_dir"]+"/unoise-nonchimeras.fasta",
        otutab = config["results_dir"] + "/" + config["<step_name>__VSEARCH_unoise_output_dir"]+"/ZOTU_Table.tsv",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__VSEARCH_unoise_output_dir"],
        command = config["<step_name>__VSEARCH_unoise_command"],
        alpha = config["<step_name>__VSEARCH_unoise_alpha"],
        minsize = config["<step_name>__VSEARCH_unoise_minsize"],
    threads: 
        config["<step_name>__VSEARCH_unoise_threads"]
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__VSEARCH_unoise_output_dir"] + "/VSEARCH_unoise_log.txt"
    conda:
        "envs/vsearch.yaml"        
    shell: 
        """ 
         #https://groups.google.com/g/vsearch-forum/c/ws7NEP68gO8/m/eFb8m_TCAAAJ
         #The unoise3 functionality in VSEARCH is separated into two commands in vsearch: cluster_unoise and uchime3_denovo. The first clusters the sequences and the other performs chimera detection.
         
         #denoised
         {params.command} --threads {threads} \
         --cluster_unoise  {input.fasta}  \
         --centroids {params.output_dir}/unoise.fasta \
         --uc {params.output_dir}/unoise.uc \
         --minsize {params.minsize} \
         --unoise_alpha {params.alpha} \
         --relabel ZOTU_ \
         --sizein \
         --sizeout \
         --clusterout_sort ;

         #denovo chimera removal
         {params.command} --threads {threads} \
         --uchime3_denovo {params.output_dir}/unoise.fasta \
         --sizein \
         --sizeout \
         --fasta_width 0 \
         --nonchimeras {output.centroids};

         # Build ZOTU table from fasta result, uc table and input otu table
         # 1- Read fasta file to keep ZOTU that are not chimeras
         # 2- Read uc file beginning by S or H. Link the ZOTU_ID to Seq_ID of fasta input file -> Seq_ID = Derep, OTU, ASV
         # 3- Do sum by sample of abundance_id in input into ZOTU
         # 4- Write ZOTU_Table.tsv
         awk -F "\\t" 'fname != FILENAME {{ fname = FILENAME; idx++ }}
             BEGIN {{OFS="\\t"}}
             idx == 1 {{if(match($0, /^>([^;]+)/, nochim)){{nochim2print[nochim[1]]=substr($0,2);if(FNR!=1){{lnochim=lnochim","substr($0,2)}}else{{lnochim=substr($0,2)}}}}}}
             idx == 2 {{if($0~/^[SH]/){{if("ZOTU_"$2+1 in nochim2print){{seq2nzotu[$9]=nochim2print["ZOTU_"$2+1];print nochim2print["ZOTU_"$2+1]"\\t"$9>>"{params.output_dir}/Denoising_group.tsv"}}else{{print "Chimera\\t"$9>>"{params.output_dir}/Denoising_group.tsv"}}}}}}
             idx == 3 {{if((FNR==1) && ($0~/^#/)){{$1="#ZOTU_ID";print $0}}else{{if($1 in seq2nzotu){{for(i=2;i<=NF;i++){{zotusum[seq2nzotu[$1]][i]+=$i}}}}}}}}
             END {{n=split(lnochim,zotu,",");for(i = 1;i<= n;i++){{printf zotu[i];for(j in zotusum[zotu[i]]){{printf "\\t%s",zotusum[zotu[i]][j]}};printf "\\n"}}}}' {output.centroids} {params.output_dir}/unoise.uc {input.otutab} > {output.otutab}

         echo "# plot_type: 'table'
# section_name: 'ZOTUtable'
# description: 'Top 20 ZOTUs table'
# pconfig:
#     namespace: 'Cust Data'
" > {params.output_dir}/header.txt;
         head -n 21 {output.otutab} >> {params.output_dir}/header.txt;
         mv {params.output_dir}/header.txt {params.output_dir}/top_ZOTU_mqc.tsv;
         sed -i 's/#ZOTU/ZOTU/' {params.output_dir}/top_ZOTU_mqc.tsv
        """
    