rule <step_name>__VSEARCH_derep:
    input:
        **<step_name>__VSEARCH_derep_inputs(),
    output: 
        derepfasta_done = config["results_dir"] + "/" + config["<step_name>__VSEARCH_derep_output_dir"]+"/derep_mqc.tsv",
        derepfasta_dir = directory(config["results_dir"] + "/" + config["<step_name>__VSEARCH_derep_output_dir"]),
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__VSEARCH_derep_output_dir"],
        command = config["<step_name>__VSEARCH_derep_command"],
        maxuniquesize = config["<step_name>__VSEARCH_derep_maxuniquesize"],
        minuniquesize = config["<step_name>__VSEARCH_derep_minuniquesize"],
        sizein = "--sizein" if config["<step_name>__VSEARCH_derep_sizein"] else "",
        strand = config["<step_name>__VSEARCH_derep_strand"],
        relabel= config["<step_name>__VSEARCH_derep_relabel"] ,
    threads: 
        config["<step_name>__VSEARCH_derep_threads"]
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__VSEARCH_derep_output_dir"] + "/VSEARCH_derep_log.txt"
    conda:
        "envs/vsearch.yaml"        
    shell: 
        """ 
        mkdir -p {params.output_dir};  \
        echo -e "fasta\t#initial\t#derep" > {output.derepfasta_done}; \
        for fastafic in ` ls {input.fasta_dir}/*.{{fasta,fas,fa}} 2>/dev/null` ; \
        do  \
         ext="${{fastafic##*.}}"; \
         r=$(basename $fastafic .$ext);  \
         if [ '{params.relabel}' == 'True' ]; then newlabel="--relabel $r."; else newlabel=""; fi;  \
         {params.command} --threads {threads} --derep_fulllength  \
         $fastafic  \
         {params.strand}  \
         --output {params.output_dir}/$r.derep.fasta  \
         {params.sizein}  \
         --sizeout  \
         $newlabel  \
         --fasta_width 0 ; \
         derepCount=$(grep -c '>' {params.output_dir}/$r.derep.fasta ); \
         initialCount=$(grep -c '>' $fastafic ); \
         echo -e $fastafic"\t"$initialCount"\t"$derepCount >> {output.derepfasta_done} ;  \
        done |& tee {log}; 
        """
    