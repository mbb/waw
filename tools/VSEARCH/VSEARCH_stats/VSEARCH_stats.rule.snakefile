rule <step_name>__VSEARCH_stats:
    input:
        **<step_name>__VSEARCH_stats_inputs(),
    output: 
        stats_done=config["results_dir"] + "/" + config["<step_name>__VSEARCH_stats_output_dir"] + "/stats.done",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__VSEARCH_stats_output_dir"],
        command = config["<step_name>__VSEARCH_stats_command"],
    threads: 
        config["<step_name>__VSEARCH_stats_threads"]
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__VSEARCH_stats_output_dir"] + "/VSEARCH_stats_log.txt"
    conda:
        "envs/vsearch.yaml"        
    shell: 
        """ 
        mkdir -p {params.output_dir};  \
        
        for fwfile in  ` ls {input.read_dir}/*.{{fastq,fq}}{{,.gz}} 2>/dev/null` ; \
        do  \
         s=$(sed 's/\(.*\)\.fastq/\\1/' <<< "$(basename $fwfile)" ); \
         {params.command} --threads {threads}  --fastq_eestats  \
         $fwfile  \
         --output {params.output_dir}/$s.tsv  ;
        done |& tee {log}; 
        touch {output.stats_done}; \
        """
    