rule <step_name>__VSEARCH_derep_fastafile:
    input:
        **<step_name>__VSEARCH_derep_fastafile_inputs(),
    output: 
        derepfasta = config["results_dir"] + "/" + config["<step_name>__VSEARCH_derep_fastafile_output_dir"]+"/derep.fasta",
        uclust_like = config["results_dir"] + "/" + config["<step_name>__VSEARCH_derep_fastafile_output_dir"]+"/derep.uc",
        otutab = config["results_dir"] + "/" + config["<step_name>__VSEARCH_derep_fastafile_output_dir"]+"/Derep_Table.tsv"
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__VSEARCH_derep_fastafile_output_dir"],
        command = config["<step_name>__VSEARCH_derep_fastafile_command"],
        maxuniquesize = config["<step_name>__VSEARCH_derep_fastafile_maxuniquesize"],
        minuniquesize = config["<step_name>__VSEARCH_derep_fastafile_minuniquesize"],
        sizein = "--sizein" if config["<step_name>__VSEARCH_derep_fastafile_sizein"] else "",
        strand = config["<step_name>__VSEARCH_derep_fastafile_strand"],
        rmsingle = "--minuniquesize 2 " if config["<step_name>__VSEARCH_derep_remove_single"] else "",
    threads: 
        config["<step_name>__VSEARCH_derep_fastafile_threads"]
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__VSEARCH_derep_fastafile_output_dir"] + "/VSEARCH_derep_fastafile_log.txt"
    conda:
        "envs/vsearch.yaml"        
    shell: 
        """ 
         {params.command} --threads {threads} --derep_fulllength  {input.fasta} {params.strand} --output {output.derepfasta} --uc {output.uclust_like} {params.sizein} --sizeout {params.rmsingle} --fasta_width 0 -relabel Derep_ |& tee {log};
         # Define header by matching all seq id between > and first .
         samples=$(grep "^>" {input.fasta} | sed 's/>\([^\.]*\)\..*/\\1/'|sort -V -k1,1 |uniq|paste -s -d '\\t')
         header="#Derep_ID\\t"$samples
         echo -e "$header" > {output.otutab}
         awk -F "\\t" -v sampletoprint="$samples" 'fname != FILENAME {{ fname = FILENAME; idx++ }}
         idx == 1 {{if(match($0, /^>([^;]+)/, derep)){{derep2print[derep[1]]=substr($0,2)}}}}
         idx == 2 {{if($0~/^[SH]/){{match($9, /([^\.]+)/,sample);match($9,/size=([0-9]+)/,size); tab[$2][sample[1]]+=size[1]}}
         else{{split(sampletoprint,col,"\\t");toprint="";for(s in col){{if(col[s] in tab[$2]){{toprint=toprint"\\t"tab[$2][col[s]]}}else{{toprint=toprint"\\t0"}}}}
         if("Derep_"$2+1 in derep2print){{print derep2print["Derep_"$2+1]toprint}}}}}}' {output.derepfasta} {output.uclust_like} >> {output.otutab}
        """
