rule <step_name>__VSEARCH_mergepairs:
    input:
        **<step_name>__VSEARCH_mergepairs_inputs(),
    output: 
        mergepairs_done=directory(config["results_dir"] + "/" + config["<step_name>__VSEARCH_mergepairs_output_dir"]),
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__VSEARCH_mergepairs_output_dir"],
        command = config["<step_name>__VSEARCH_mergepairs_command"],
        maxdiffs = config["<step_name>__VSEARCH_mergepairs_maxdiffs"],
        minovlen = config["<step_name>__VSEARCH_mergepairs_minovlen"],
        minmergelen = config["<step_name>__VSEARCH_mergepairs_minmergelen"],
    threads: 
        config["<step_name>__VSEARCH_mergepairs_threads"]
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__VSEARCH_mergepairs_output_dir"] + "/VSEARCH_mergepairs_log.txt"
    conda:
        "envs/vsearch.yaml"        
    shell: 
        """ 
        mkdir -p {params.output_dir};  \
        
        for fwfile in  ` ls {input.read_dir}/*{{_R1,_1}}*.{{fastq,fq}}{{,.gz}} 2>/dev/null` ; \
        do  \
         rfile=$(sed -e "s/_R1/_R2/" <<< "$fwfile"); \
         rfile=$(sed -e "s/_1/_2/" <<< "$rfile"); \
         s=$(sed -e 's/\(.*\)_R\?2.*\.fastq.*/\\1/' <<< "$(basename $rfile)" ); \
         {params.command} --threads {threads}  --fastq_mergepairs  \
         $fwfile  \
         --reverse $rfile \
         --fastqout {params.output_dir}/$s.merged.fastq  \
         --fastq_maxdiffs {params.maxdiffs} \
         --fastq_minovlen {params.minovlen} \
         --fasta_width 0 \
         --fastq_minmergelen {params.minmergelen}; \
        done |& tee {log}; 
        #touch {output.mergepairs_done}; \
        """
    