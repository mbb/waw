rule <step_name>__VSEARCH_clustering:
    input:
        **<step_name>__VSEARCH_clustering_inputs(),
    output: 
        centroids = config["results_dir"] + "/" + config["<step_name>__VSEARCH_clustering_output_dir"]+"/clustered-nonchimeras.fasta",
        otutab = config["results_dir"] + "/" + config["<step_name>__VSEARCH_clustering_output_dir"]+"/OTU_Table.tsv",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__VSEARCH_clustering_output_dir"],
        command = config["<step_name>__VSEARCH_clustering_command"],
        identity = config["<step_name>__VSEARCH_clustering_identity"],
        strand = config["<step_name>__VSEARCH_clustering_strand"],
        method = config["<step_name>__VSEARCH_clustering_method"],
    threads: 
        config["<step_name>__VSEARCH_clustering_threads"]
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__VSEARCH_clustering_output_dir"] + "/VSEARCH_clustering_log.txt"
    conda:
        "envs/vsearch.yaml"        
    shell: 
        """ 
         #https://groups.google.com/g/vsearch-forum/c/ws7NEP68gO8/m/eFb8m_TCAAAJ
         #The UCLUST functionality in VSEARCH is separated into two commands in vsearch: cluster_smallmem and uchime3_denovo. The first clusters the sequences and the other performs chimera detection.
         
         #clustering
         method=$(echo '{params.method}')
         echo $method
         if [ "{params.method}" = "nochange" ];then
            {params.command} --cluster_smallmem {input.fasta} --id {params.identity} --centroids {params.output_dir}/clustering.fasta --uc {params.output_dir}/clustering.uc {params.strand} --sizein --sizeout -relabel OTU_ --threads {threads} --usersort --clusterout_sort
         elif [ "{params.method}" = "abundance" ];then
            {params.command} --cluster_size {input.fasta} --id {params.identity} --centroids {params.output_dir}/clustering.fasta --uc {params.output_dir}/clustering.uc {params.strand} --sizein --sizeout -relabel OTU_ --threads {threads} --clusterout_sort
         elif [ "{params.method}" = "length" ];then
            {params.command} --cluster_fast {input.fasta} --id {params.identity} --centroids {params.output_dir}/clustering.fasta --uc {params.output_dir}/clustering.uc {params.strand} --sizein --sizeout -relabel OTU_ --threads {threads} --clusterout_sort
         fi

         {params.command} --uchime3_denovo {params.output_dir}/clustering.fasta --nonchimeras {output.centroids} --sizein --sizeout --fasta_width 0 --threads {threads}


         # Build OTU table from fasta result, uc table and input otu table
         # 1- Read fasta file to keep OTU that are not chimeras
         # 2- Read uc file beginning by S or H. Link the OTU_ID to Seq_ID of fasta input file -> Seq_ID = Derep, ZOTU, ASV
         # 3- Do sum by sample of abundance_id in input into OTU
         # 4- Write OTU_Table.tsv
         awk -F "\\t" 'fname != FILENAME {{ fname = FILENAME; idx++ }}
             BEGIN {{OFS="\\t"}}
             idx == 1 {{if(match($0, /^>([^;]+)/, nochim)){{nochim2print[nochim[1]]=substr($0,2);if(FNR!=1){{lnochim=lnochim","substr($0,2)}}else{{lnochim=substr($0,2)}}}}}}
             idx == 2 {{if($0~/^[SH]/){{if("OTU_"$2+1 in nochim2print){{seq2notu[$9]=nochim2print["OTU_"$2+1];print nochim2print["OTU_"$2+1]"\\t"$9>>"{params.output_dir}/Clustering_group.tsv"}}else{{print "Chimera\\t"$9>>"{params.output_dir}/Clustering_group.tsv"}}}}}}
             idx == 3 {{if((FNR==1) && ($0~/^#/)){{$1="#OTU_ID";print $0}}else{{if($1 in seq2notu){{for(i=2;i<=NF;i++){{otusum[seq2notu[$1]][i]+=$i}}}}}}}}
             END {{n=split(lnochim,otu,",");for(i = 1;i<= n;i++){{printf otu[i];for(j in otusum[otu[i]]){{printf "\\t%s",otusum[otu[i]][j]}};printf "\\n"}}}}' {output.centroids} {params.output_dir}/clustering.uc {input.otutab} > {output.otutab}

         echo "# plot_type: 'table'
# section_name: 'OTUtable'
# description: 'Top 20 OTUs table'
# pconfig:
#     namespace: 'Cust Data'
" > {params.output_dir}/header.txt;
         head -n 21 {output.otutab} >> {params.output_dir}/header.txt;
         mv {params.output_dir}/header.txt {params.output_dir}/top_OTU_mqc.tsv;
         sed -i 's/#OTU/OTU/' {params.output_dir}/top_OTU_mqc.tsv
        """
    