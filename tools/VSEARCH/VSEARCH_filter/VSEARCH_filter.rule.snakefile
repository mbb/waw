rule <step_name>__VSEARCH_filter:
    input:
        **<step_name>__VSEARCH_filter_inputs(),
    output: 
        filter_done=directory(config["results_dir"] + "/" + config["<step_name>__VSEARCH_filter_output_dir"]),
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__VSEARCH_filter_output_dir"],
        command = config["<step_name>__VSEARCH_filter_command"],
        maxee = config["<step_name>__VSEARCH_filter_maxee"],
        minlen = config["<step_name>__VSEARCH_filter_minlen"],
        minsize = config["<step_name>__VSEARCH_filter_minsize"],
        maxsize = config["<step_name>__VSEARCH_filter_maxsize"],
        maxns = config["<step_name>__VSEARCH_filter_maxns"],
        qmin = config["<step_name>__VSEARCH_filter_qmin"],
        stripleft = config["<step_name>__VSEARCH_filter_stripleft"],
        stripright = config["<step_name>__VSEARCH_filter_stripright"],
    threads: 
        config["<step_name>__VSEARCH_filter_threads"]
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__VSEARCH_filter_output_dir"] + "/VSEARCH_filter_log.txt"
    conda:
        "envs/vsearch.yaml"        
    shell: 
        """ 
        mkdir -p {params.output_dir};  \
        
        for fwfile in  ` ls {input.read_dir}/*.{{fastq,fq}}{{,.gz}} 2>/dev/null` ; \
        do  \
         s=$(sed 's/\(.*\)\.fastq/\\1/' <<< "$(basename $fwfile)" ); \
         {params.command} --threads {threads}  --fastq_filter \
         $fwfile  \
         --fastaout {params.output_dir}/$s.filtered.fasta  \
         --fastq_maxee {params.maxee} \
         --fastq_minlen {params.minlen} \
         --fastq_qmin {params.qmin} \
         --fastq_stripleft {params.stripleft} \
         --fastq_stripright {params.stripright} \
         --fastq_maxns {params.maxns} \
         --minsize {params.minsize} \
         --maxsize {params.maxsize} ; \
        done |& tee {log}; 
        """
    