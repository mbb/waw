if config["<step_name>__VSEARCH_abundance_table_clustering_step"]=="True":
    rule <step_name>__VSEARCH_abundance_table:
        input:
            **<step_name>__VSEARCH_abundance_table_inputs(),
        output:
            centroids = config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"]+"/RefSequence.fasta",
            otutab = config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"]+"/Abundance_Table.tsv",
            fasta_cluster =  config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"]+"/clustered.fasta",
            fasta_before_clustering =  config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"]+"/RefSequence_BeforeClustering.fasta",
            uctab = config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"]+"/clustered_uctab.tsv",
            otutab_before_clustering = config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"]+"/Abundance_Table_BeforeClustering.tsv",
            otutab_before_clustering_tmp = config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"]+"/Abundance_Table_BeforeClustering_NotRenamed.tsv",
            otutab_after_clustering = config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"]+"/Abundance_Table_AfterClustering.tsv",
        params:
            output_dir = config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"],
            command = config["<step_name>__VSEARCH_abundance_table_command"],
            identity = config["<step_name>__VSEARCH_abundance_table_clustering_identity"],
        threads:
            config["<step_name>__VSEARCH_abundance_table_threads"]
        log:
            config["results_dir"] + "/logs/" + config["<step_name>__VSEARCH_abundance_table_output_dir"] + "/VSEARCH_abundance_table_log.txt"
        conda:
            "envs/vsearch.yaml"    
        shell:
            """
            mkdir -p {params.output_dir};
            header="OTU_ID"
            cnt=0;
            awk -F "\\t" '{{if(match($0, /^>([^ ;]+)/, seq_nochim)){{print seq_nochim[1]}}}}' {input.fasta_file} > {params.output_dir}/join.tmp
            for fastafic in ` ls {input.fasta_dir}/*.{{fasta,fas,fa}} 2>/dev/null` ;
            do
              cnt=$((cnt + 1));
              header+="\\t"
              header+=$(basename $fastafic)
              {params.command} --search_exact $fastafic --db {input.fasta_file} --otutabout {params.output_dir}/otutable_tmp${{cnt}}.txt --uc {params.output_dir}/uc_tmp${{cnt}}.txt --threads {threads} ;
              join -t $'\\t' -a1 -a2 -e 0 -o auto <(cat {params.output_dir}/join.tmp |sort -k1,1) <(sed 1d {params.output_dir}/otutable_tmp$cnt.txt|sort -k1,1) >{params.output_dir}/join.tmp.1 ;
              mv {params.output_dir}/join.tmp.1 {params.output_dir}/join.tmp;
            done
            ## Create abundance table by merging the perfect match results
            echo -e $header |cat - {params.output_dir}/join.tmp > {output.otutab_before_clustering_tmp}
            rm {params.output_dir}/join.tmp* {params.output_dir}/otutable_tmp*.txt
            ## Sort the OTU by decreasing abundance and generate the table and the new fasta file
            awk '{{if(NR==1){{print $0 >"{params.output_dir}/tmp_otu"}}else{{sum=0;for(i=2;i<=NF;i++){{sum+=$i}};tline[sum]=$0;print sum"\\t"$0}}}}' {output.otutab_before_clustering_tmp} |sort -k1nr,1nr > {params.output_dir}/tmp_id
            awk -F "\\t" 'fname != FILENAME {{fname = FILENAME; idx++}}
              idx == 1 {{cnt++;sum=0;out="OTU_"NR;for(i=3;i<=NF;i++){{sum=sum+$i;out=out"\\t"$i}}print out>>"{params.output_dir}/tmp_otu";seq2keep[$2]="OTU_"NR";size="sum}}
              idx == 2 {{if($0~/^>/){{match($0, /^>([^;]+)/, q);if(q[1] in seq2keep){{print ">"seq2keep[q[1]]}}}}else{{print}}}}
              ' {params.output_dir}/tmp_id {input.fasta_file} | awk '/^>/ {{printf("\\n%s\\n",$0);next; }} {{ printf("%s",$0);}}  END {{printf("\\n");}}' |sed 1d | paste - - |sort -V | tr "\\t" "\\n" > {output.fasta_before_clustering}
            mv {params.output_dir}/tmp_otu {output.otutab_before_clustering}
            rm {params.output_dir}/tmp_id
            ## Do a clustering step
            {params.command} -cluster_smallmem {output.fasta_before_clustering} -usersort -id {params.identity} -centroids {output.fasta_cluster} -uc {output.uctab} -fasta_width 0 -sizein -sizeout -relabel Cluster_ --threads {threads}
            {params.command} -uchime_denovo {output.fasta_cluster} -nonchimeras {output.centroids} -sizein --xsize -fasta_width 0 --threads {threads}
            ## Use the uc table and pass fasta sequence to generate the abundance table after the clustering step.
            # The first table contains the information of previous cluster and the new
            # The secund table contains only the information of the abundance of the new cluster
            awk -F "\\t" 'fname != FILENAME {{ fname = FILENAME; idx++ }}
              idx == 1 {{if($0~/^[SH]/){{split($9,seq,";");seq2otu[seq[1]]="Cluster_"$2+1}}}}
              idx == 2 {{if(match($0, /^>([^ ;]+)/, seq_nochim)){{otu2nochim[seq_nochim[1]]=1}}}}
              idx == 3 && FNR == 1 {{print "#Cluster\\t"$0}}
              idx == 3 && FNR != 1 {{if(otu2nochim[seq2otu[$1]]==1){{print seq2otu[$1]"\\t"$0}}else{{print "ChimeraCluster\\t"$0}}}}
              ' {output.uctab} {output.centroids} {output.otutab_before_clustering} > {output.otutab_after_clustering}
            echo -e $header > {output.otutab}
            awk -F "\\t" '{{if((NR!=1) && ($0!~/^ChimeraCluster/)){{for(i=3;i<=NF;i++){{sum[$1][i]+=$i}}}}}}END{{for(c in sum){{printf c;for(i in sum[c]){{printf "\\t%s",sum[c][i]}};printf "\\n"}}}}
              ' {output.otutab_after_clustering} |sort -V -k1,1 >> {output.otutab}
            """

elif config["<step_name>__VSEARCH_abundance_table_clustering_step"]=="False":
    rule <step_name>__VSEARCH_abundance_table:
        input:
            **<step_name>__VSEARCH_abundance_table_inputs(),
        output:
            otutab = config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"]+"/Abundance_Table.tsv",
            otutab_before_clustering_tmp = config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"]+"/Abundance_Table_BeforeClustering_NotRenamed.tsv",
            centroids = config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"]+"/RefSequence.fasta",
        params:
            output_dir = config["results_dir"] + "/" + config["<step_name>__VSEARCH_abundance_table_output_dir"],
            command = config["<step_name>__VSEARCH_abundance_table_command"],
        threads:
            config["<step_name>__VSEARCH_abundance_table_threads"]
        log:
            config["results_dir"] + "/logs/" + config["<step_name>__VSEARCH_abundance_table_output_dir"] + "/VSEARCH_abundance_table_log.txt"
        conda:
            "envs/frogs-dada2-vsearch.yaml"            
        shell:
            """
            mkdir -p {params.output_dir};
            header="OTU_ID"
            cnt=0;
            awk -F "\\t" '{{if(match($0, /^>([^ ;]+)/, seq_nochim)){{print seq_nochim[1]}}}}' {input.fasta_file} > {params.output_dir}/join.tmp
            for fastafic in ` ls {input.fasta_dir}/*.{{fasta,fas,fa}} 2>/dev/null` ;
            do
              cnt=$((cnt + 1));
              header+="\\t"
              header+=$(basename $fastafic)
              {params.command} --search_exact $fastafic --db {input.fasta_file} --otutabout {params.output_dir}/otutable_tmp${{cnt}}.txt --uc {params.output_dir}/uc_tmp${{cnt}}.txt --threads {threads} ;
              join -t $'\\t' -a1 -a2 -e 0 -o auto <(cat {params.output_dir}/join.tmp |sort -k1,1) <(sed 1d {params.output_dir}/otutable_tmp$cnt.txt|sort -k1,1) >{params.output_dir}/join.tmp.1 ;
              mv {params.output_dir}/join.tmp.1 {params.output_dir}/join.tmp;
            done
            ## Create abundance table by merging the perfect match results
            echo -e $header |cat - {params.output_dir}/join.tmp > {output.otutab_before_clustering_tmp}
            rm {params.output_dir}/join.tmp* {params.output_dir}/otutable_tmp*.txt
            ## Sort the OTU by decreasing abundance and generate the table and the new fasta file
            awk '{{if(NR==1){{print $0 >"{params.output_dir}/tmp_otu"}}else{{sum=0;for(i=2;i<=NF;i++){{sum+=$i}};tline[sum]=$0;print sum"\\t"$0}}}}' {output.otutab_before_clustering_tmp} |sort -k1nr,1nr > {params.output_dir}/tmp_id
            awk -F "\\t" 'fname != FILENAME {{fname = FILENAME; idx++}}
              idx == 1 {{seq2keep[$2]="OTU_"NR;out="OTU_"NR;for(i=3;i<=NF;i++){{out=out"\\t"$i}}print out>>"{params.output_dir}/tmp_otu"}}
              idx == 2 {{if($0~/^>/){{match($0, /^>([^;]+)/, q);if(q[1] in seq2keep){{print ">"seq2keep[q[1]]}}}}else{{print}}}}
            mv {params.output_dir}/tmp_otu {output.otutab}
            rm {params.output_dir}/tmp_id
            """