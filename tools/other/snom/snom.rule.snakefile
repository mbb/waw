rule <step_name>__snom:
    input:
        **<step_name>__snom_inputs(),
    output:
        sfs_outgroup = config["results_dir"] + "/" + config["<step_name>__snom_output_dir"] + "/outgroup.sfs.count",
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__snom_output_dir"] + '/snom_log.txt'
    shell:
        """
        # Add right to exec snom script because no right when copy in /workflow/scripts/
        chmod a+x {snake_dir}/scripts/snom_snom
        # Run snom : from blast output find homologous of specific base. Then, use som input (blast output) and snom output to create est-sfs format output. It contains qseqid, est-sfs-format and pident. 
        {snake_dir}/scripts/snom_snom {input.blast_out_snom} | cat <(echo -e "ref\\talt") - | paste {input.blast_out_snom} - | awk -F "\\t" '{{if(NR==1){{for(i=1;i<=NF;i++){{if($i=="#qseqid"){{qseqid=i}};if($i=="pident"){{pident=i}};if($i=="alt"){{alt=i}}}}}}else{{split($qseqid,n,":");allele["A"]=0;allele["C"]=0;allele["G"]=0;allele["T"]=0;if($alt in allele){{allele[$alt]=1}};if($alt=="="){{allele[n[3]]=1}};print n[1]":"n[2]"\\t"allele["A"]","allele["C"]","allele["G"]","allele["T"]"\\t"$pident}}}}' > {output.sfs_outgroup}
        """

