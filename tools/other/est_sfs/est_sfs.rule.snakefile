rule <step_name>__est_sfs:
    input:
        **<step_name>__est_sfs_inputs(),
    output:
        input_est_sfs_seqid = config["results_dir"] + "/" + config["<step_name>__est_sfs_output_dir"] + "/est_sfs_seqid.tsv",
        input_est_sfs_noseqid = config["results_dir"] + "/" + config["<step_name>__est_sfs_output_dir"] + "/est_sfs_input.tsv",
        output_est_sfs = config["results_dir"] + "/" + config["<step_name>__est_sfs_output_dir"] + "/output-file-sfs.txt",
        output_est_sfs_pval = config["results_dir"] + "/" + config["<step_name>__est_sfs_output_dir"] + "/output-file-pvalues.txt",
        est_sfs_output_aa = config["results_dir"] + "/" + config["<step_name>__est_sfs_output_dir"] + "/est-sfs-output-withAA.tsv",
        aatsv = config["results_dir"] + "/" + config["<step_name>__est_sfs_output_dir"] + "/output-file-AA.tsv",
    params:
        outdir = config["results_dir"] + "/" + config["<step_name>__est_sfs_output_dir"],
        model = config["<step_name>__est_sfs_model"],
        nrandom = config["<step_name>__est_sfs_nrandom"],
        seed = config["<step_name>__est_sfs_seed"],
        threshold = config["<step_name>__est_sfs_threshold"],
        method_order_outgroup = config["<step_name>__est_sfs_method_order_outgroup"],
        min_outgroup = config["<step_name>__est_sfs_min_outgroup"],
        ldhelmet= "TRUE" if config["<step_name>__est_sfs_ldhelmet"] else "FALSE",
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__est_sfs_output_dir"] + '/est_sfs_log.txt'
    conda:
        "envs/est_sfs.yaml"
    threads:
        config["<step_name>__est_sfs_threads"]
    shell:
        """
        # List input file to determine number of input
        lfile_i=({input});
        len=${{#lfile_i[@]}};
        n_outgroup=$(($len-1))
        nvariant=$(wc -l {input.sfs_vcf} | cut -d " " -f 1)

        # Determine pident mean and ratio outgroup where variant were found for each outgroup. 
        echo -e "# plot_type: 'table'\\n\\tMean_pident\\tNumber_Variant_inOutgroup\\tRatio_Variant_inOutgroup" > {params.outdir}/outgroup_stats_mqc.tsv
        for (( j=1; j<$len; j++ ));
        do
            awk -F "\\t" -v var=$nvariant '{{if($2!="0,0,0,0"){{ total += $3; nmatch+=1 }}}} END {{ print FILENAME"\\t"total/nmatch"\\t"nmatch"\\t"nmatch/var }}' ${{lfile_i[$j]}} >> {params.outdir}/mean_pident.tsv;
        done
        
        # Define method to order outgroup
        # if [ "{params.method_order_outgroup}" = "pident" ];then
        #     order_outgroup=2
        # elif [ "{params.method_order_outgroup}" = "match_found" ];then
        #     order_outgroup=3
        # fi

        # Convert vcf sfs and the outgroup sfs to est-sfs input format. Outgroup are ordered on their mean pident.
        cat {input.sfs_vcf} > {output.input_est_sfs_seqid};
        header="#seq_id\\t{input.sfs_vcf}\\t"
        while read -r line; 
        do
            header+="$line ";
            join -t $'\\t' -a1 -a2 -e 0,0,0,0 -o auto <(cat {output.input_est_sfs_seqid} |sort -k1,1) <(cut -f 1,2 "$line" |sort -k1,1) > {params.outdir}/join.tmp
            cat {params.outdir}/join.tmp > {output.input_est_sfs_seqid}
        done < <(sort -k{params.method_order_outgroup},{params.method_order_outgroup}nr {params.outdir}/mean_pident.tsv | cut -f 1)
        echo -e $header |sed 's/ $//' > {output.input_est_sfs_seqid};
        sort -V -k1,1 {params.outdir}/join.tmp |awk -F "\\t" '{{outgroup=$3;for(i=4;i<=NF;i++){{outgroup=outgroup" "$i}}print $1"\\t"$2"\\t"outgroup}}' >> {output.input_est_sfs_seqid};
        grep -v "^#" {output.input_est_sfs_seqid} | cut -f 2,3  > {output.input_est_sfs_noseqid};
        sort -k{params.method_order_outgroup},{params.method_order_outgroup}nr {params.outdir}/mean_pident.tsv >> {params.outdir}/outgroup_stats_mqc.tsv
        rm {params.outdir}/mean_pident.tsv
        rm {params.outdir}/join.tmp;

        # Create config-file.txt and seed file.txt
        echo "n_outgroup "$n_outgroup > {params.outdir}/config-file-est-sfs.txt
        echo "model "{params.model} >> {params.outdir}/config-file-est-sfs.txt
        echo "nrandom "{params.nrandom} >> {params.outdir}/config-file-est-sfs.txt

        if [ "{params.seed}" == "0" ];then
            echo -e "Random seed is :" | tee -a {log}  
            echo $RANDOM | tee {params.outdir}/seed-file-est-sfs.txt | tee -a {log}
        else
            echo {params.seed} > {params.outdir}/seed-file-est-sfs.txt
        fi

        # Run est-sfs with parallel
        cat {output.input_est_sfs_noseqid} |parallel --block 10M --pipe 'cp {params.outdir}/seed-file-est-sfs.txt {params.outdir}/seed-file-est-sfs-{{#}}.txt ; cat - > {output.input_est_sfs_noseqid}_{{#}} ; est-sfs {params.outdir}/config-file-est-sfs.txt {output.input_est_sfs_noseqid}_{{#}} {params.outdir}/seed-file-est-sfs-{{#}}.txt {output.output_est_sfs}_{{#}} {output.output_est_sfs_pval}_{{#}}'

        # Merge parallel output of est-sfs and remove temp file
        ls -v {output.output_est_sfs}_* |xargs cat > {output.output_est_sfs}
        ls -v {output.output_est_sfs_pval}_* |xargs cat > {output.output_est_sfs_pval}
        rm {output.output_est_sfs_pval}_* {output.output_est_sfs}_* {output.input_est_sfs_noseqid}_* {params.outdir}/seed-file-est-sfs-*.txt

        # Merge the input of est-sfs with chr and pos information and est sfs probability major allele as ancestral output.
        # Then, determine and write major allele, minor allele and ancestral allele information (base on est-sfs from snp and probability_major_allele_as_ancestral)
        grep -v "^0"  {output.output_est_sfs_pval} | cut -d " " -f 3 | cat <(echo -e "probability_major_allele_as_ancestral") - | paste {output.input_est_sfs_seqid} - | awk -F "\\t" -v vthreshold={params.threshold} -v min_outgroup={params.min_outgroup} -v LDhelmet={params.ldhelmet} '{{if(NR==1){{print $0"\\tn_Missing_Outgroup_NT_Homology\\tMajor_allele\\tMinor_allele\\tAncestral_allele\\tDerivate_allele";cnt=0}}else{{split($2,a,",");nt["A"]=a[1];nt["C"]=a[2];nt["G"]=a[3];nt["T"]=a[4];PROCINFO["sorted_in"] ="@val_num_desc"; n_na_outgroup=0; n=split($3,b," "); for(k=1;k<=n;k++){{if(b[k]=="0,0,0,0"){{n_na_outgroup++}}}};for (i in nt){{cnt+=1;if(cnt == 1){{major=i}}else if(cnt == 2){{minor=i}}else{{cnt=0;break}}}};if($NF>=vthreshold){{aa=major ; ad= minor}}else if($NF<1-vthreshold){{aa=minor ; ad=major}}else{{aa="." ; ad= "."}};if((n-n_na_outgroup)<min_outgroup){{aa="." ; ad="."}};print $0"\\t"n_na_outgroup"\\t"major"\\t"minor"\\t"aa"\\t"ad; if(LDhelmet == "TRUE"){{if($NF>=0.5){{pval=sprintf("%.3f",$NF)}}else{{pval=1-sprintf("%.3f",$NF)}};allele["A"]=0;allele["C"]=0;allele["G"]=0;allele["T"]=0;allele[aa]=pval;allele[ad]=1-pval; print $1" "allele["A"]" "allele["C"]" "allele["G"]" "allele["T"] >"{params.outdir}/Ldhelmet_Ancestral_allele_priors.txt"}}}}}}' > {output.est_sfs_output_aa}
        # Conversion to bed format with CHR,POS and ancestral allele information (AA)
        awk -F "\\t" '{{if(NR!=1){{split($1,a,":");print a[1]"\\t"a[2]"\\t"$(NF-1)}}}}' {output.est_sfs_output_aa} > {output.aatsv}

        # Draw uSFS estimation in barplot
        python {snake_dir}/scripts/<step_name>__est_sfs.prepare.report.py {conf_path} {output.output_est_sfs} {output.est_sfs_output_aa}
        """

