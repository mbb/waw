import oyaml as yaml
import sys
import os
from collections import OrderedDict
import matplotlib.pyplot as plt
import seaborn as sns
import operator
from collections import defaultdict
import matplotlib.gridspec as gridspec

# Read config file of te snakefile params.yaml
config = dict()
with open(sys.argv[1], 'r') as paramfile:
    config = yaml.load(paramfile, Loader=yaml.FullLoader)

config = config["params"]

# Read estimate usfs and plot it as barplot
c=1
usfs_tot = None
file = sys.argv[2]
with open(file,"r") as usfs_file:
    for line in usfs_file:
        line = line.strip()
        outfile =config["results_dir"]+"/"+config["<step_name>__est_sfs_output_dir"]+"/estimated_uSFS_plot_"+str(c)+".png"
        usfs=[float(value) for value in line.split(",")]
        usfs_tot = [0]*len(usfs) if usfs_tot is None else usfs_tot
        c+=1
        usfs_tot=list(map(operator.add, usfs,usfs_tot))
        nallele=list(range(1,len(usfs)+1))
        plt.figure()
        sns.histplot(x=nallele, weights=usfs, discrete=True, color='darkblue', edgecolor='black')
        plt.xlabel("Derived allele frequency")
        plt.ylabel("Estimated uSFS")
        plt.savefig(outfile)
        
if usfs_tot is None:
    raise RuntimeError(f"No line in file {file}.")

outfile =config["results_dir"]+"/"+config["<step_name>__est_sfs_output_dir"]+"/estimated_uSFS_plot_all_mqc.png"
nallele=list(range(1,len(usfs_tot)+1))
plt.figure()
sns.histplot(x=nallele, weights=usfs_tot, discrete=True, color='darkblue', edgecolor='black')
plt.xlabel("Derived allele frequency")
plt.ylabel("Estimated uSFS")
plt.savefig(outfile)



# Read file output with ancestral allele and derivate allele to plot barplot Ti/Tv and biallelic base change

purine=["A","G"]
pyrimidine=["C","T"]
snp={"A":{"A":0,"C":0,"G":0,"T":0,".":0},"C":{"A":0,"C":0,"G":0,"T":0,".":0},"G":{"A":0,"C":0,"G":0,"T":0,".":0},"T":{"A":0,"C":0,"G":0,"T":0,".":0},".":{"A":0,"C":0,"G":0,"T":0,".":0}}

nline=0
transition=0
transversion=0
missing=0
with open(sys.argv[3],"r") as est_sfs_output_file:
    for line in est_sfs_output_file:
        line = line.strip()
        nline+=1
        if nline>1:
            derivate=line.split("\t")[-1]
            ancestral=line.split("\t")[-2]
            if ancestral not in snp:
                snp[ancestral] = {}
                snp[ancestral][ancestral] = 0
            if derivate not in snp[ancestral]:
                snp[ancestral][derivate]=0
            snp[ancestral][derivate]+=1
            if ancestral in purine and derivate in purine or ancestral in pyrimidine and derivate in pyrimidine:
                transition+=1
            elif ancestral==".":
                missing+=1
            else:
                transversion+=1
        




outfile =config["results_dir"]+"/"+config["<step_name>__est_sfs_output_dir"]+"/Ancestral_allele_base_change_and_TiTv_mqc.png"
fig = plt.figure(figsize=(10,10))
fig.suptitle("Biallelic base changes from ancestral allele", x=0.35, ha="center", size=15)
gs = gridspec.GridSpec(2, 3)
#fig.suptitle("Biallelic base changes from ancestral allele")
counts={}
nt = ['A', 'C', 'G', 'T']
bar_labels = ['purine', 'pyrimidine', '_purine', '_pyrimidine']
bar_colors = ['orange', 'blue', 'darkorange', 'royalblue']
for n in "AGCT":
    counts[n] = [snp[n][nt[0]],snp[n][nt[1]],snp[n][nt[2]],snp[n][nt[3]]]

ax1 = fig.add_subplot(gs[0,0])
ax1.bar(nt, counts["A"], label=bar_labels, color=bar_colors)
ax1.set_ylabel('Count')
ax1.set_xlabel('Derivate allele')
ax1.set_title("A")


ax2 = fig.add_subplot(gs[0,1], sharey = ax1)
ax2.bar(nt, counts["G"], label=bar_labels, color=bar_colors)
ax2.set_ylabel('Count')
ax2.set_xlabel('Derivate allele')
ax2.set_title("G")

ax3 = fig.add_subplot(gs[1,0], sharey = ax1)
ax3.bar(nt, counts["C"], label=bar_labels, color=bar_colors)
ax3.set_ylabel('Count')
ax3.set_xlabel('Derivate allele')
ax3.set_title("C")

ax4 = fig.add_subplot(gs[1,1], sharey = ax1)
ax4.bar(nt, counts["T"], label=bar_labels, color=bar_colors)
ax4.set_ylabel('Count')
ax4.set_xlabel('Derivate allele')
ax4.set_title("T")

h, l = ax1.get_legend_handles_labels()
fig.legend(h, l, loc=(0.63,0))

ax5 = fig.add_subplot(gs[:,2])
state=["Transition","Transversion","Missing"]
counts=[transition,transversion,missing]
bar_colors = ["green", "darkgreen", "red"]
ax5.bar(state, counts, color=bar_colors)
ax5.set_title("Bialleliv Ti/Tv ratio: {:.2f}".format(transition/transversion))

fig.tight_layout()

plt.savefig(outfile)

