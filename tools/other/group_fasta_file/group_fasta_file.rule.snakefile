if config["<step_name>__group_fasta_file_mode"] == "none":
    rule <step_name>__group_fasta_file:
        input:
            **<step_name>__group_fasta_file_inputs(),
        output:
            fasta_file_dir = directory(config["results_dir"] + "/" + config["<step_name>__group_fasta_file_output_dir"] + "/"),
        shell:
            """
            lfile_i=({input});
            len=${{#lfile_i[@]}};
            mkdir -p {output.fasta_file_dir}
            for (( j=0; j<$len; j++ ));
            do
                i=$((j+1))
                fout=$(echo ${{lfile_i[$j]}} | awk -F "/" '{{print $NF}}' | sed "s/\.fa.*$/_$i.fa/")
                ln -s ${{lfile_i[$j]}} {output.fasta_file_dir}/$fout
            done
            """

elif config["<step_name>__group_fasta_file_mode"] == "header":
    rule <step_name>__group_fasta_file:
        input:
            **<step_name>__group_fasta_file_inputs(),
        output:
            fasta_file_dir = directory(config["results_dir"] + "/" + config["<step_name>__group_fasta_file_output_dir"]+ "/"),
        shell:
            """
            lfile_i=({input});
            len=${{#lfile_i[@]}};
            mkdir -p {output.fasta_file_dir}
            for (( j=0; j<$len; j++ ));
            do
                ntool=$(grep -oP '(?<=^>).*?(?=_)' ${{lfile_i[$j]}} | sort | uniq |wc -l)
                if [ $ntool -ge 2 ]; then
                    tool=$(grep -oP '(?<=^>).*?(?=_)' ${{lfile_i[$j]}} | sort | uniq | tr "\\n" "-" | sed 's/-$//') 
                    ln -s ${{lfile_i[$j]}} {output.fasta_file_dir}/Combined${{tool}}.fa
                else
                    tool=$(head -n 1 ${{lfile_i[$j]}} | grep -oP '(?<=^>).*?(?=_)')
                    ln -s ${{lfile_i[$j]}} {output.fasta_file_dir}/${{tool}}.fa
                fi
            done
            """

elif config["<step_name>__group_fasta_file_mode"] == "directory":
    rule <step_name>__group_fasta_file:
        input:
            **<step_name>__group_fasta_file_inputs(),
        output:
            fasta_file_dir = directory(config["results_dir"] + "/" + config["<step_name>__group_fasta_file_output_dir"]+ "/"),
        params:
            mode=config["<step_name>__group_fasta_file_mode"],
        shell:
            """
            lfile_i=({input});
            len=${{#lfile_i[@]}};
            mkdir -p {output.fasta_file_dir}
            for (( j=0; j<$len; j++ ));
            do
                fout=$(echo ${{lfile_i[$j]}} | awk -F "/" '{{print $(NF-2)}}')
                ln -s ${{lfile_i[$j]}} {output.fasta_file_dir}/${{fout}}.fa
            done
            """