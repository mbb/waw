rule <step_name>__deseq2:
    input:
        **<step_name>__deseq2_inputs()
    output:
        de_table = config["results_dir"]+'/'+config["<step_name>__deseq2_output_dir"]+'/de_table.csv',
        raw_counts = config["results_dir"]+'/'+config["<step_name>__deseq2_output_dir"]+'/raw_counts.tsv',
        PCA = config["results_dir"]+'/'+config["<step_name>__deseq2_output_dir"]+'/PCA_mqc.png',
        Top_genes = config["results_dir"]+'/'+config["<step_name>__deseq2_output_dir"]+'/Top_genes_mqc.yaml',
        MA_plot = config["results_dir"]+'/'+config["<step_name>__deseq2_output_dir"]+'/MA_plot_mqc.png',
        Volcano_plot = config["results_dir"]+'/'+config["<step_name>__deseq2_output_dir"]+'/Volcano_plot_mqc.png',
        Heatmap = config["results_dir"]+'/'+config["<step_name>__deseq2_output_dir"]+'/Heatmap_mqc.png',
    log: config["results_dir"]+'/logs/' + config["<step_name>__deseq2_output_dir"] + '/deseq2_log.txt'
    conda:
        "envs/deg.yaml" 
    params:
        script = config["<step_name>__deseq2_command"],
        tx2gene = config["<step_name>__deseq2_tx2gene"],
        annotations = config["<step_name>__deseq2_annotations"],
        outDir = config["results_dir"]+'/'+config["<step_name>__deseq2_output_dir"],
        biomaRtDataset = config["<step_name>__deseq2_biomaRtDataset"],
        minAdjPvalue = config["<step_name>__deseq2_minpvalue"],
        topGenes     = config["<step_name>__deseq2_topGenes"],
    script:
        config["<step_name>__deseq2_script"]
