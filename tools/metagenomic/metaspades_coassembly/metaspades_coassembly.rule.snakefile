if config["SeOrPe"] == "SE":
    rule <step_name>__metaspades_coassembly_SE:
        input:
            **<step_name>__metaspades_coassembly_SE_inputs(),
        output:
            assembly = config["results_dir"] + "/" + config["<step_name>__metaspades_coassembly_SE_output_dir"] + "/metaspades_assembly.fa",
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__metaspades_coassembly_SE_output_dir"],
            res_dir = config["results_dir"],
            max_memory = str(config["<step_name>__metaspades_coassembly_max_memory"]),
            kmer = str(config["<step_name>__metaspades_coassembly_kmer"]).replace(" ","").replace("0","auto"),
        threads:
            config["<step_name>__metaspades_coassembly_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__metaspades_coassembly_SE_output_dir"] + '/metaspades_log.txt'
        conda:
            "envs/spades.yaml"
        shell:
            """
            zcat -f {input.read} | spades.py --meta --s1 - -o {params.output_dir} -k {params.kmer} -t {threads} -m {params.max_memory}
            mv {params.output_dir}/scaffolds.fasta {output.assembly}
            """

elif config["SeOrPe"] == "PE":
    rule <step_name>__metaspades_coassembly_PE:
        input:
            **<step_name>__metaspades_coassembly_PE_inputs(),
        output:
            assembly = config["results_dir"] + "/" + config["<step_name>__metaspades_coassembly_PE_output_dir"] + "/metaspades_assembly.fa",
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__metaspades_coassembly_PE_output_dir"],
            res_dir = config["results_dir"],
            max_memory = str(config["<step_name>__metaspades_coassembly_max_memory"]),
            kmer = str(config["<step_name>__metaspades_coassembly_kmer"]).replace(" ","").replace("0","auto"),
        threads:
            config["<step_name>__metaspades_coassembly_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__metaspades_coassembly_PE_output_dir"] + '/metaspades_log.txt'
        conda:
            "envs/spades.yaml"
        shell:
            """
            zcat -f {input.read} > {params.res_dir}/test_R1.fastq
            zcat -f {input.read2} > {params.res_dir}/test_R2.fastq
            spades.py --meta --pe1-1 {params.res_dir}/test_R1.fastq --pe1-2 {params.res_dir}/test_R2.fastq -o {params.output_dir} -k {params.kmer} -t {threads} -m {params.max_memory}
            mv {params.output_dir}/scaffolds.fasta {output.assembly}
            rm {params.res_dir}/test_R1.fastq {params.res_dir}/test_R2.fastq
            """
