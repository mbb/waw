rule <step_name>__CAT:
    input:
        **<step_name>__CAT_inputs(),
    output:
        contigs_classification = config["results_dir"] + "/" + config["<step_name>__CAT_output_dir"] + "/out.CAT.contig2classification.txt",
        contigs_classification_names = config["results_dir"] + "/" + config["<step_name>__CAT_output_dir"] + "/out.CAT.names.contig2classification.txt",
        contigs_classification_summary = config["results_dir"] + "/" + config["<step_name>__CAT_output_dir"] + "/out.CAT.summary.txt",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__CAT_output_dir"]+ "/",
        protein = config["<step_name>__CAT_protein"],
        command = config["<step_name>__CAT_command"],
        sensitive = " --sensitive " if config["<step_name>__CAT_sensitivity"] else "",
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__CAT_output_dir"] + '/CAT_log.txt',
    conda:
        "envs/CAT.yaml",
    threads:
        config["<step_name>__CAT_threads"],
    shell:
        """
        export PATH=$PATH:${{CONDA_PREFIX}}/bin/CAT_pack/CAT_pack/
        # List input file to determine number of input.
        lfile_i=({input});
        len=${{#lfile_i[@]}};
        n=$(($len-1))
        mkdir -p {params.output_dir}
        cd {params.output_dir}


        # Define if there is already an annotation in Predicted fasta file
        if [ $len -le "3" ]; then
            CAT_pack contigs -c {input.assembly} -d {input.db} -t {input.tax} --verbose -n {threads} -o {params.output_dir}/out.CAT {params.sensitive}
        else
            CAT_pack contigs -c {input.assembly} -d {input.db} -t {input.tax} -p {params.protein} --verbose -n {threads} -o {params.output_dir}/out.CAT {params.sensitive}
        fi

        # Add names in assignment
        CAT_pack add_names -i {output.contigs_classification} -o {output.contigs_classification_names} -t {input.tax} --only_official
        CAT_pack add_names -i out.CAT.ORF2LCA.txt -o out.CAT.names.ORF2LCA.txt -t {input.tax} --only_official

        # Do summary
        CAT_pack summarise -i {output.contigs_classification_names} -c {input.assembly} -o {output.contigs_classification_summary}
        """
