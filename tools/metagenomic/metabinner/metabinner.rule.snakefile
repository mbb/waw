rule <step_name>__metabinner:
    input:
        **<step_name>__metabinner_inputs(),
    output: 
        fasta_bins = directory(config["results_dir"] + "/" + config["<step_name>__metabinner_output_dir"] + "/bins/"),
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__metabinner_output_dir"]+ "/",
        command = config["<step_name>__metabinner_command"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__metabinner_output_dir"] + "/metabinner_log.txt"
    threads: 
        config["<step_name>__metabinner_threads"]
    conda:
        "envs/metabinner.yaml"
    shell: 
        """
        # Generate coverage profile
        mkdir -p {params.output_dir}
        $CONDA_PREFIX/bin/scripts/jgi_summarize_bam_contig_depths --outputDepth {params.output_dir}/depth.txt --noIntraDepthVariance {input.mapping}
        cat {params.output_dir}/depth.txt | awk '{{if ($2>1000){{ print $0 }}}}' | cut -f -1,4- > {params.output_dir}/coverage_profile.tsv

        # Generate composition profile
        python $CONDA_PREFIX/bin/scripts/gen_kmer.py {input.contigs} 1000 4
        composition=$(echo "{input.contigs}" | sed 's/\.fa.*$//')
        cp ${{composition}}_kmer_4_f1000.csv {params.output_dir}/composition_profile.tsv

        
        # Binning with metabinner
        bash $CONDA_PREFIX/bin/run_metabinner.sh -a {input.contigs} -o {params.output_dir}metabinner_output -d {params.output_dir}coverage_profile.tsv -k {params.output_dir}composition_profile.tsv -t {threads} -p $CONDA_PREFIX/bin

        mkdir -p {output}
        cp {params.output_dir}metabinner_output/metabinner_res/ensemble_res/greedy_cont_weight_3_mincomp_50.0_maxcont_15.0_bins/ensemble_3logtrans/addrefined2and3comps/greedy_cont_weight_3_mincomp_50.0_maxcont_15.0_bins/*.fna {output}
        """

