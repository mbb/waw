rule <step_name>__RAT:
    input:
        **<step_name>__RAT_inputs(),
    output:
        complete_abundance_classification_sample = config["results_dir"] + "/" + config["<step_name>__RAT_output_dir"] + "/{sample}/{sample}.RAT.complete.abundance.txt",
        complete_abundance_classification_names_sample = config["results_dir"] + "/" + config["<step_name>__RAT_output_dir"] + "/{sample}/{sample}.RAT.names.complete.abundance.txt",
        bins_abundance_sample = config["results_dir"] + "/" + config["<step_name>__RAT_output_dir"] + "/{sample}/{sample}.RAT.bin.reads.txt",
        bins_abundance_classification_sample = config["results_dir"] + "/" + config["<step_name>__RAT_output_dir"] + "/{sample}/{sample}.RAT.bin.abundance.txt",
        bins_abundance_classification_names_sample = config["results_dir"] + "/" + config["<step_name>__RAT_output_dir"] + "/{sample}/{sample}.RAT.names.bin.abundance.txt",
        contigs_abundance_classification_sample = config["results_dir"] + "/" + config["<step_name>__RAT_output_dir"] + "/{sample}/{sample}.RAT.contig.abundance.txt",
        contigs_abundance_classification_names_sample = config["results_dir"] + "/" + config["<step_name>__RAT_output_dir"] + "/{sample}/{sample}.RAT.names.contig.abundance.txt",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__RAT_output_dir"]+ "/{sample}/",
        command = config["<step_name>__RAT_command"],
        sensitive = "--sensitive" if config["<step_name>__RAT_sensitivity"] else "",
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__RAT_output_dir"] + '/{sample}/{sample}_RAT_log.txt',
    conda:
        "envs/CAT.yaml",
    threads:
        config["<step_name>__RAT_threads"],
    shell:
        """
        export PATH=$PATH:${{CONDA_PREFIX}}/bin/CAT_pack/CAT_pack/
        cd {params.output_dir}

        # Define end file of fasta bin files
        ext=$(find {input.bin_dir} -type f -name '*.f*' | sed 's|.*\.||' | sort -u)


        CAT_pack reads -m mcr -c {input.assembly} -b {input.bin_dir} -1 {input.read} -2 {input.read2} -d {input.db} -t {input.tax} -s ${{ext}} -o {params.output_dir}/{wildcards.sample}.RAT --c2c {input.contigs_classification} --b2c {input.bins_classification} --verbose -n {threads} {params.sensitive}


        # Add names in assignment for contigs and complete
        CAT_pack add_names -i {output.complete_abundance_classification_sample} -o {output.complete_abundance_classification_names_sample} -t {input.tax} --only_official
        CAT_pack add_names -i {output.contigs_abundance_classification_sample} -o {output.contigs_abundance_classification_names_sample} -t {input.tax} --only_official

        # Reattribute a corrected Read Count
        awk -F "\\t" 'BEGIN{{sum_read=0}}{{if($0!~/^#/){{sum_read=sum_read+$2; line[$1]=$0;cfraction[$1]=$5}}else{{if($0!~/^##/){{print $0"\\tcorrected number of reads"}}else{{print}}}}}}END{{for(l in line){{creads=cfraction[l]*sum_read;print line[l]"\\t"int(creads  + 0.5)}}}}' {output.bins_abundance_sample} > {params.output_dir}/{wildcards.sample}.RAT.bin.corrected_reads.txt

        # Add names in assignment for bins by joining BAT input with RAT results
        echo -e "# bin\\tnumber of reads\\tfraction of reads\\tbin length\\tcorrected fraction\\tcorrected number of reads\\tclassification\\treason\\tlineage\\tlineage scores" > {output.bins_abundance_classification_sample}
        join -t $'\\t' -a1 -a2 -o auto <(grep -v "^#" {params.output_dir}/{wildcards.sample}.RAT.bin.corrected_reads.txt | sort -k1,1) <(grep -v "^#" {input.bins_classification} | sort -k1,1) >> {output.bins_abundance_classification_sample}
        CAT_pack add_names -i {output.bins_abundance_classification_sample} -o {output.bins_abundance_classification_names_sample} -t {input.tax} --only_official
        """

