rule <step_name>__binette:
    input:
        **<step_name>__binette_inputs(),
    output:
        fasta_bins = directory(config["results_dir"] + "/" + config["<step_name>__binette_output_dir"] + "/results/final_bins"),
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__binette_output_dir"]+ "/",
        command = config["<step_name>__binette_command"],
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__binette_output_dir"] + '/binette_log.txt',
    conda:
        "envs/binette.yaml",
    threads:
        config["<step_name>__binette_threads"],
    shell:
        """
        # List input file to determine number of input.
        ldir_i=({input});
        len=${{#ldir_i[@]}};
        echo $ldir_i
        echo $len
        mkdir -p {params.output_dir}
        cd {params.output_dir}

        for (( j=1; j<$len; j++ ));
        do
            cnt_set=$j;
            cnt_bin=0;
            for file in $(find ${{ldir_i[$j]}} -name *.f*a)
            do
                cnt_bin=$((cnt_bin+1));
                binner=$(echo $file | awk -F "/" '{{if($0 !~ /\/$/){{print $(NF-2)}}else{{print $(NF-3)}}}}');
                awk -v bin=$binner -v cnt_bin=$cnt_bin -v cnt_set=$cnt_set '{{if( $0 ~ /^>/){{sub(/^>/,"",$1);print $0"\\t"bin"_"cnt_bin >>"{params.output_dir}/bin_set"cnt_set".txt"}}}}' $file
                mv $file ${{ldir_i[$j]}}/${{binner}}_${{cnt_bin}}.fa
            done
        done


        bin_set=$(find {params.output_dir}/bin_set*.txt | tr "\\n" " ")
        binette -b $bin_set --contigs {input.contigs} -o {params.output_dir}/results -t {threads} -v |& tee {log}
        """
