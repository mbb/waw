rule <step_name>__whokaryote:
    input:
        **<step_name>__whokaryote_inputs(),
    output:
        prokaryotes = config["results_dir"] + "/" + config["<step_name>__whokaryote_output_dir"] + "/prokaryotes.fasta",
        eukaryotes = config["results_dir"] + "/" + config["<step_name>__whokaryote_output_dir"] + "/eukaryotes.fasta",
    params:
        output_dir = config["results_dir"]+"/"+config["<step_name>__whokaryote_output_dir"],
        res_dir = config["results_dir"],
        minsize = str(config["<step_name>__whokaryote_minsize"]),
    threads:
        config["<step_name>__whokaryote_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__whokaryote_output_dir"] + '/whokaryote_log.txt'
    conda:
        "envs/whokaryote.yaml"
    shell:
        """
        lfile_i=({input});
        len=${{#lfile_i[@]}};
        if [ $len -ge 2 ]; then
            whokaryote.py --contigs {input.metagenome_fasta} --outdir {params.output_dir} --gff ${{lfile_i[2]}} --f --minsize {params.minsize} --model T --threads {threads}
        else
            whokaryote.py --contigs {input.metagenome_fasta} --outdir {params.output_dir} --f --minsize {params.minsize} --model T --threads {threads}
        fi
        nprok=$(grep "^>" {output.prokaryotes} |wc -l)
        neuk=$(grep "^>" {output.eukaryotes} |wc -l)
        nunclass=$(grep "^>" {params.output_dir}/unclassified.fasta |wc -l)
        nseq=$(grep "^>" {input.metagenome_fasta} |wc -l)
        nshort=$(($nseq - $nunclass - $neuk - nprok))
        echo -e "Number\\tNumber_of_contigs" > {params.output_dir}/Predicted_Contigs_Classification_mqc.tsv
        echo -e "Contigs classified as prokaryote\\t"$nprok >> {params.output_dir}/Predicted_Contigs_Classification_mqc.tsv
        echo -e "Contigs classified as eukaryote\\t"$neuk >> {params.output_dir}/Predicted_Contigs_Classification_mqc.tsv
        echo -e "Contigs unclassified (not enough predicted gene)\\t"$nunclass >> {params.output_dir}/Predicted_Contigs_Classification_mqc.tsv
        echo -e "Contigs too short\\t"$nshort >> {params.output_dir}/Predicted_Contigs_Classification_mqc.tsv
        """
