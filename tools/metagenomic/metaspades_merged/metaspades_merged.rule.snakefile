if config["SeOrPe"] == "SE":
    rule <step_name>__metaspades_merged_sample_SE:
        input:
            **<step_name>__metaspades_merged_SE_inputs(),
        output:
            assembly = config["results_dir"] + "/" + config["<step_name>__metaspades_merged_SE_output_dir"] + "/{sample}/metaspades_{sample}.fa",
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__metaspades_merged_SE_output_dir"]+ "/{sample}",
            res_dir = config["results_dir"],
            max_memory = str(config["<step_name>__metaspades_merged_max_memory"]),
            kmer = str(config["<step_name>__metaspades_merged_kmer"]).replace(" ","").replace("0","auto"),
        threads:
            config["<step_name>__metaspades_merged_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__metaspades_merged_SE_output_dir"] + '/{sample}/metaspades_log.txt'
        conda:
            "envs/spades.yaml"
        shell:
            """
            spades.py --meta --s1 {input} -o {params.output_dir} -k {params.kmer} -t {threads} -m {params.max_memory}
            mv {params.output_dir}/scaffolds.fasta {output.assembly}
            """

    rule <step_name>__metaspades_merged_SE:
        input:
            expand(config["results_dir"] + "/" + config["<step_name>__metaspades_merged_SE_output_dir"] + "/{sample}/metaspades_{sample}.fa", samples = SAMPLES),
        output:
            assembly = config["results_dir"] + "/" + config["<step_name>__metaspades_merged_SE_output_dir"] + "/metaspades_assembly.fa",
        shell:
            """
            cat {input} > {output.assembly}
            """

elif config["SeOrPe"] == "PE":
    rule <step_name>__metaspades_merged_sample_PE:
        input:
            **<step_name>__metaspades_merged_PE_inputs(),
        output:
            assembly = config["results_dir"] + "/" + config["<step_name>__metaspades_merged_PE_output_dir"] + "/{sample}/metaspades_{sample}.fa",
        params:
            output_dir = config["results_dir"]+"/"+config["<step_name>__metaspades_merged_PE_output_dir"]+ "/{sample}",
            res_dir = config["results_dir"],
            max_memory = str(config["<step_name>__metaspades_merged_max_memory"]),
            kmer = str(config["<step_name>__metaspades_merged_kmer"]).replace(" ","").replace("0","auto"),
        threads:
            config["<step_name>__metaspades_merged_threads"]
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__metaspades_merged_PE_output_dir"] + '/{sample}/metaspades_log.txt'
        conda:
            "envs/spades.yaml"
        shell:
            """
            spades.py --meta --pe1-1 {input.read} --pe1-2 {input.read2} -o {params.output_dir} -k {params.kmer} -t {threads} -m {params.max_memory}
            mv {params.output_dir}/scaffolds.fasta {output.assembly}
            """

    rule <step_name>__metaspades_merged_PE:
        input:
            expand(config["results_dir"] + "/" + config["<step_name>__metaspades_merged_PE_output_dir"] + "/{sample}/metaspades_{sample}.fa", sample = SAMPLES),
        output:
            assembly = config["results_dir"] + "/" + config["<step_name>__metaspades_merged_PE_output_dir"] + "/metaspades_assembly.fa",
        shell:
            """
            cat {input} > {output.assembly}
            """
