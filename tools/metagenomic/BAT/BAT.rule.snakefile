rule <step_name>__BAT:
    input:
        **<step_name>__BAT_inputs(),
    output:
        bins_classification = config["results_dir"] + "/" + config["<step_name>__BAT_output_dir"] + "/out.BAT.bin2classification.txt",
        bins_classification_names = config["results_dir"] + "/" + config["<step_name>__BAT_output_dir"] + "/out.BAT.names.bin2classification.txt",
        bins_classification_summary = config["results_dir"] + "/" + config["<step_name>__BAT_output_dir"] + "/out.BAT.summary.txt",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__BAT_output_dir"]+ "/",
        command = config["<step_name>__BAT_command"],
        protein = config["<step_name>__BAT_protein"],
        sensitive = " --sensitive " if config["<step_name>__BAT_sensitivity"] else "",
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__BAT_output_dir"] + '/BAT_log.txt',
    conda:
        "envs/CAT.yaml",
    threads:
        config["<step_name>__BAT_threads"],
    shell:
        """
        export PATH=$PATH:${{CONDA_PREFIX}}/bin/CAT_pack/CAT_pack/
        # List input file to determine number of input.
        lfile_i=({input});
        len=${{#lfile_i[@]}};
        n=$(($len-1))
        mkdir -p {params.output_dir}
        cd {params.output_dir}

        # Define end file of fasta bin files
        ext=$(find {input.bin_dir} -type f -name '*.f*' | sed 's|.*\.||' | sort -u)

        # Define if there is already an annotation in Predicted fasta file
        if [ $len -le "3" ]; then
            CAT_pack bins -b {input.bin_dir} -d {input.db} -t {input.tax} -s ${{ext}} --verbose -n {threads} -o {params.output_dir}/out.BAT {params.sensitive}
        else
            CAT_pack bins -b {input.bin_dir} -d {input.db} -t {input.tax} -s ${{ext}} -p {params.protein} --verbose -n {threads} -o {params.output_dir}/out.BAT {params.sensitive}
        fi

        # Add names in assignment
        CAT_pack add_names -i {output.bins_classification} -o {output.bins_classification_names} -t {input.tax} --only_official
        CAT_pack add_names -i out.BAT.ORF2LCA.txt -o out.BAT.names.ORF2LCA.txt -t {input.tax} --only_official

        # Do summary
        CAT_pack summarise -i {output.bins_classification_names} -o {output.bins_classification_summary}
        """
