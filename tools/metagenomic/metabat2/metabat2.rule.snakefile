rule <step_name>__metabat2:
    input:
        **<step_name>__metabat2_inputs(),
    output: 
        fasta_bins = directory(config["results_dir"] + "/" + config["<step_name>__metabat2_output_dir"] + "/bins/"),
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__metabat2_output_dir"]+ "/",
        command = config["<step_name>__metabat2_command"],
        mincontig = config["<step_name>__metabat2_mincontig"] if int(config["<step_name>__metabat2_mincontig"])>=1500 else "1500",
        minclustersize = config["<step_name>__metabat2_minclustersize"],
        seed = config["<step_name>__metabat2_seed"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__metabat2_output_dir"] + "/metabat_log.txt"
    threads: 
        config["<step_name>__metabat2_threads"]
    conda:
        "envs/metabat.yaml"
    shell: 
        """
        # Generate a file having mean and variance of base coverage depth
        mkdir -p {params.output_dir}
        jgi_summarize_bam_contig_depths --outputDepth {params.output_dir}/depth.txt {input.mapping}
        
        # Binning with metabat2
        metabat2 -i {input.contigs} -o {output.fasta_bins}/metabat2_bins -a {params.output_dir}/depth.txt -m {params.mincontig} -s {params.minclustersize} --seed {params.seed} -t {threads} |& tee {log}
        """

