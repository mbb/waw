rule <step_name>__concoct:
    input:
        **<step_name>__concoct_inputs(),
    output: 
        fasta_bins = directory(config["results_dir"] + "/" + config["<step_name>__concoct_output_dir"] + "/bins/"),
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__concoct_output_dir"]+ "/",
        command = config["<step_name>__concoct_command"],
        mincontig = config["<step_name>__concoct_mincontig"],
        read_length = config["<step_name>__concoct_read_length"],
        seed = config["<step_name>__concoct_seed"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__concoct_output_dir"] + "/concoct_log.txt"
    threads: 
        config["<step_name>__concoct_threads"]
    conda:
        "envs/concoct.yaml"
    shell: 
        """
        mkdir -p {params.output_dir}

        # cut contigs into smaller parts
        cut_up_fasta.py {input.contigs} -c 10000 -o 0 --merge_last -b {params.output_dir}/MetagenomeContigs_10K.bed > {params.output_dir}/MetagenomeContigs_10K.fa

        # Generate table with coverage depth information per sample and subcontig
        concoct_coverage_table.py {params.output_dir}/MetagenomeContigs_10K.bed {input.mapping} > {params.output_dir}/coverage_table.tsv

        # Binning with concoct
        concoct --composition_file {params.output_dir}/MetagenomeContigs_10K.fa --coverage_file {params.output_dir}/coverage_table.tsv -b {params.output_dir}/concoct_output/ -l {params.mincontig} -r {params.read_length} -s {params.seed} -t {threads} |& tee {log}

        # Merge subcontig clustering into original contig clustering
        merge_cutup_clustering.py {params.output_dir}/concoct_output/clustering_gt{params.mincontig}.csv > {params.output_dir}/concoct_output/clustering_merged.csv

        # Extract bins as individual fasta
        mkdir -p {output.fasta_bins}
        extract_fasta_bins.py {input.contigs} {params.output_dir}/concoct_output/clustering_merged.csv --output_path {output.fasta_bins}
        """

