rule <step_name>__maxbin2:
    input:
        **<step_name>__maxbin2_inputs(),
    output: 
        fasta_bins = directory(config["results_dir"] + "/" + config["<step_name>__maxbin2_output_dir"] + "/bins/"),
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__maxbin2_output_dir"]+ "/",
        command = config["<step_name>__maxbin2_command"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__maxbin2_output_dir"] + "/maxbin2_log.txt"
    threads: 
        config["<step_name>__maxbin2_threads"]
    conda:
        "envs/maxbin.yaml"
    shell: 
        """
        # Generate a file having mean and variance of base coverage depth
        mkdir -p {params.output_dir}
        jgi_summarize_bam_contig_depths --outputDepth {params.output_dir}/depth.txt --noIntraDepthVariance {input.mapping}
        
        awk -F "\\t" '{{if(NR==1){{gsub(".bam","",$0);split($0,header,"\\t");for(j=4;j<=NF;j++){{print "{params.output_dir}"header[j]"_depth.txt"}}}}else{{for(i=4;i<=NF;i++){{print $1"\\t"$i > "{params.output_dir}"header[i]"_depth.txt"}}}}}}' {params.output_dir}depth.txt > {params.output_dir}Depth_list.txt
        
        # Binning with maxbin2
        mkdir -p {params.output_dir}results
        mkdir -p {params.output_dir}bins
        run_MaxBin.pl -contig {input.contigs} -out {params.output_dir}results/maxBin_out -abund_list {params.output_dir}/Depth_list.txt -thread {threads} -verbose |& tee {log}
        cp {params.output_dir}results/maxBin_out*.fasta {output}
        """

