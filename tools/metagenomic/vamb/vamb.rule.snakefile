rule <step_name>__vamb:
    input:
        **<step_name>__vamb_inputs(),
    output: 
        fasta_bins = directory(config["results_dir"] + "/" + config["<step_name>__vamb_output_dir"] + "/bins/"),
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__vamb_output_dir"]+ "/",
        command = config["<step_name>__vamb_command"],
        mincontig = config["<step_name>__vamb_mincontig"],
        minclustersize = config["<step_name>__vamb_minclustersize"]
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__vamb_output_dir"] + "/vamb_log.txt"
    conda:
        "envs/vamb.yaml"
    threads:
        config["<step_name>__vamb_threads"]
    shell: 
        """
        # Generate a file having mean and variance of base coverage depth
        mkdir -p {params.output_dir}/unsort_bam

        indir=$(echo "{input.mapping}" |tr " " "\\n" | rev |cut -d "/" -f2- |rev|uniq)
        infile=$(echo "{input.mapping}" |tr " " "\\n" | awk -F "/" '{{print $NF}}' | sed 's/.bam//' |tr "\\n" " ")

        parallel 'samtools sort -n -@ {threads} -O bam {{2}}/{{1}}.bam > {params.output_dir}/unsort_bam/{{1}}.bam' ::: $infile ::: $indir

        vamb --outdir {params.output_dir}results --fasta {input.contigs} --bamfiles {params.output_dir}unsort_bam/*.bam -m {params.mincontig} --minfasta {params.minclustersize}
        mv {params.output_dir}results/bins {params.output_dir}/
        """

