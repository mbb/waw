rule <step_name>__RAT2Krona:
    input:
        **<step_name>__RAT2Krona_inputs(),
    output:
        complete_abundance_classification = config["results_dir"] + "/" + config["<step_name>__RAT2Krona_output_dir"] + "/RAT.names.complete.abundance.txt",
        contigs_abundance_classification = config["results_dir"] + "/" + config["<step_name>__RAT2Krona_output_dir"] + "/RAT.names.contig.abundance.txt",
        bins_abundance_classification = config["results_dir"] + "/" + config["<step_name>__RAT2Krona_output_dir"] + "/RAT.names.bin.abundance.txt",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__RAT2Krona_output_dir"] + "/",
        step_dir = config["<step_name>__RAT2Krona_output_dir"] + "/",
    conda:
        "envs/CAT.yaml",
    shell:
        """
        ### Agregate and make krona plot for complete abundance
        awk -F "\\t" '{{
            if($0 !~ /^#/){{
                if($0 !~ /^[0-9]/){{        # Set taxonomy for unmapped/unclassified results and save in a dictionary the abundance for each sample and in another dictionary the taxo
                    dlineage2tax[$1]=$1"\\t"$1"\\t"$1"\\t"$1"\\t"$1"\\t"$1"\\t"$1
                    dlineage_sample2count[$1][name[n-1]]=$2
                    print $2"\\t"$1 >"{params.output_dir}"name[n-1]"-complete-taxonomy.txt"      # Write krona output with abundance and then taxo
                }}else{{           # Set taxonomy for unmapped/unclassified results and save in a dictionary the abundance for each sampleand in another dictionary the taxo
                    dlineage2tax[$1]=$6"\\t"$7"\\t"$8"\\t"$9"\\t"$10"\\t"$11"\\t"$12
                    dlineage_sample2count[$1][name[n-1]]=$2
                    krona=$2
                    for(i=6;i<=12;i++){{
                        if($i=="no support"){{
                            tax=""
                        }}else{{
                            tax=$i
                        }}
                        krona=krona"\\t"tax
                    }}
                    print krona >"{params.output_dir}"name[n-1]"-complete-taxonomy.txt"      # Write krona output with abundance and then taxo
                }}
            }}else{{        # Write header in abundance table
                header="#superkingdom\\tphylum\\tclass\\torder\\tfamily\\tgenus\\tspecies"
                n=split(FILENAME,name,"/")
                dname[name[n-1]]=1
                for(s in dname){{
                    header=header"\\t"s
                }}
            }}
        }}END{{         # Write the abundance by taxa and sample by reading dictionary
            for(l in dlineage2tax){{
                toprint=dlineage2tax[l]
                for(s in dname){{
                    if (dlineage_sample2count[l][s]){{
                        toprint=toprint"\\t"dlineage_sample2count[l][s]
                    }}else{{
                        toprint=toprint"\\t0"
                    }}
                }}
                print toprint
            }}
        }}' {input.complete_abundance_classification_names_sample} > {output.complete_abundance_classification}

        complete=$(find {params.output_dir} -name "*-complete-taxonomy.txt" |sort | tr "\\n" " " )
        ktImportText $complete -o {params.output_dir}/complete.krona.html

        # Agregate and make krona plot for contigs abundance
        awk -F "\\t" '{{
            if($0 !~ /^#/){{
                if(NF<=3){{        # Set taxonomy for unmapped/unclassified results and save in a dictionary the abundance for each sample and in another dictionary the taxo
                    dlineage2tax[$1]=$1"\\t"$1"\\t"$1"\\t"$1"\\t"$1"\\t"$1"\\t"$1
                    dlineage_sample2count[$1][name[n-1]]=$2
                    print $2"\\t"$1 >"{params.output_dir}"name[n-1]"-contigs-taxonomy.txt"      # Write krona output with abundance and then taxo
                }}else if(NF>3 && NF<6){{        # Set taxonomy for unmapped/unclassified results and save in a dictionary the abundance for each sample and in another dictionary the taxo
                    dlineage2tax["no taxid assigned"]="unclassified\\tunclassified\\tunclassified\\tunclassified\\tunclassified\\tunclassified\\tunclassified"
                    dlineage_sample2count["no taxid assigned"][name[n-1]]+=$2
                    print $2"\\tunclassified" >"{params.output_dir}"name[n-1]"-contigs-taxonomy.txt"      # Write krona output with abundance and then taxo
                }}else{{           # Set taxonomy for unmapped/unclassified results and save in a dictionary the abundance for each sample and in another dictionary the taxo
                    split($9, kingdom, ": ")
                    split($10, phylum, ": ")
                    split($11, class, ": ")
                    split($12, order, ": ")
                    split($13, family, ": ")
                    split($14, genus, ": ")
                    split($15, species, ": ")
                    if ($6 in dlineage2tax){{
                        dlineage_sample2count[$6][name[n-1]]=dlineage_sample2count[$6][name[n-1]]+$2
                    }}else{{
                        dlineage2tax[$6]=kingdom[1]"\\t"phylum[1]"\\t"class[1]"\\t"order[1]"\\t"family[1]"\\t"genus[1]"\\t"species[1]
                        dlineage_sample2count[$6][name[n-1]]=$2
                    }}
                    krona=$2"\\t"kingdom[1]"\\t"phylum[1]"\\t"class[1]"\\t"order[1]"\\t"family[1]"\\t"genus[1]"\\t"species[1]
                    gsub(/no support/, "", krona)
                    print krona >"{params.output_dir}"name[n-1]"-contigs-taxonomy.txt"      # Write krona output with abundance and then taxo
                }}
            }}else{{        # Write header in abundance table
                header="#superkingdom\\tphylum\\tclass\\torder\\tfamily\\tgenus\\tspecies"
                n=split(FILENAME,name,"/")
                dname[name[n-1]]=1
                for(s in dname){{
                    header=header"\\t"s
                }}
            }}
        }}END{{         # Write the abundance by taxa and sample by reading dictionary
            for(l in dlineage2tax){{
                toprint=dlineage2tax[l]
                for(s in dname){{
                    if (dlineage_sample2count[l][s]){{
                        toprint=toprint"\\t"dlineage_sample2count[l][s]
                    }}else{{
                        toprint=toprint"\\t0"
                    }}
                }}
                print toprint
            }}
        }}' {input.contigs_abundance_classification_names_sample} > {output.contigs_abundance_classification}

        contigs=$(find {params.output_dir} -name "*-contigs-taxonomy.txt" |sort | tr "\\n" " ")
        ktImportText $contigs -o {params.output_dir}/contigs.krona.html

        # Agregate and make krona plot for bins abundance
        awk -F "\\t" '{{
            if($0 !~ /^#/){{
                if(NF<=6){{        # Set taxonomy for unmapped/unclassified results and save in a dictionary the abundance for each sample and in another dictionary the taxoconfig["metagenomic_4__RAT2Krona_output_dir"] + "/",
                    dlineage2tax[$1]=$1"\\t"$1"\\t"$1"\\t"$1"\\t"$1"\\t"$1"\\t"$1
                    dlineage_sample2count[$1][name[n-1]]=$6
                    print $6"\\t"$1 >"{params.output_dir}"name[n-1]"-bins-taxonomy.txt"      # Write krona output with abundance and then taxo
                }}else if(NF>6 && NF<=8){{         # Set taxonomy for unmapped/unclassified results and save in a dictionary the abundance for each sample and in another dictionary the taxo
                    dlineage2tax["no taxid assigned"]="unclassified\\tunclassified\\tunclassified\\tunclassified\\tunclassified\\tunclassified\\tunclassified"
                    dlineage_sample2count["no taxid assigned"][name[n-1]]+=$2
                    print $2"\\tunclassified" >"{params.output_dir}"name[n-1]"-bins-taxonomy.txt"      # Write krona output with abundance and then taxo
                }}else{{           # Set taxonomy for unmapped/unclassified results and save in a dictionary the abundance for each sampleand in another dictionary the taxo
                    split($11, kingdom, ": ")
                    split($12, phylum, ": ")
                    split($13, class, ": ")
                    split($14, order, ": ")
                    split($15, family, ": ")
                    split($16, genus, ": ")
                    split($17, species, ": ")
                    if ($9 in dlineage2tax){{
                        dlineage_sample2count[$9][name[n-1]]=dlineage_sample2count[$6][name[n-1]]+$6
                    }}else{{
                        dlineage2tax[$9]=kingdom[1]"\\t"phylum[1]"\\t"class[1]"\\t"order[1]"\\t"family[1]"\\t"genus[1]"\\t"species[1]
                        dlineage_sample2count[$6][name[n-1]]=$6
                    }}
                    krona=$2"\\t"kingdom[1]"\\t"phylum[1]"\\t"class[1]"\\t"order[1]"\\t"family[1]"\\t"genus[1]"\\t"species[1]
                    gsub(/no support/, "", krona)
                    print krona >"{params.output_dir}"name[n-1]"-bins-taxonomy.txt"      # Write krona output with abundance and then taxo
                }}
            }}else{{        # Write header in abundance table
                header="#superkingdom\\tphylum\\tclass\\torder\\tfamily\\tgenus\\tspecies"
                n=split(FILENAME,name,"/")
                dname[name[n-1]]=1
                for(s in dname){{
                    header=header"\\t"s
                }}
            }}
        }}END{{         # Write the abundance by taxa and sample by reading dictionary
            for(l in dlineage2tax){{
                toprint=dlineage2tax[l]
                for(s in dname){{
                    if (dlineage_sample2count[l][s]){{
                        toprint=toprint"\\t"dlineage_sample2count[l][s]
                    }}else{{
                        toprint=toprint"\\t0"
                    }}
                }}
                print toprint
            }}
        }}' {input.bins_abundance_classification_names_sample} > {output.bins_abundance_classification}

        bins=$(find {params.output_dir} -name "*-bins-taxonomy.txt" |sort | tr "\\n" " ")
        ktImportText $bins -o {params.output_dir}/bins.krona.html
        
        echo -e "1\\t./{params.step_dir}/complete.krona.html" > {params.output_dir}/text.krona.tsv
        echo -e "2\\t./{params.step_dir}/contigs.krona.html" >> {params.output_dir}/text.krona.tsv
        echo -e "3\\t./{params.step_dir}/bins.krona.html" >> {params.output_dir}/text.krona.tsv
        """