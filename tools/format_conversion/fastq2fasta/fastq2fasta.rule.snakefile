if config["SeOrPe"] == "SE":
    rule <step_name>__fastq2fasta_SE:
        input:
            **<step_name>__fastq2fasta_SE_inputs(),
        output: 
            fasta = config["results_dir"] + "/" + config["<step_name>__fastq2fasta_SE_output_dir"] + "/{sample}.fasta",
        params: 
            output_dir = config["results_dir"] + "/" + config["<step_name>__fastq2fasta_SE_output_dir"]+ "/",
            SeOrPe = config["SeOrPe"]
        log: 
            config["results_dir"] + "/logs/" + config["<step_name>__fastq2fasta_SE_output_dir"] + "/{sample}_fastq2fasta_SE_log.txt"
        shell:
               " read={input.read}; "
               " gzip -t $read 2>/dev/null; "
               " [[ $? -eq 0 ]]&&   cmd='zcat' || cmd='cat'; "
               " $cmd {input.read} | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > {output.fasta}  "


if config["SeOrPe"] == "PE":
    rule <step_name>__fastq2fasta_PE:
        input:
            **<step_name>__fastq2fasta_PE_inputs(),
        output: 
            fasta = config["results_dir"] + "/" + config["<step_name>__fastq2fasta_PE_output_dir"] + "/{sample}_1.fasta",
            fasta2 = config["results_dir"] + "/" + config["<step_name>__fastq2fasta_PE_output_dir"] + "/{sample}_2.fasta",
        params: 
            output_dir = config["results_dir"] + "/" + config["<step_name>__fastq2fasta_PE_output_dir"]+ "/",
            SeOrPe = config["SeOrPe"]
        log: 
            config["results_dir"] + "/logs/" + config["<step_name>__fastq2fasta_PE_output_dir"] + "/{sample}_fastq2fasta_PE_log.txt"
        shell:
               " gzip -t {input.read} 2>/dev/null; "
               " [[ $? -eq 0 ]]&&   cmd='zcat' || cmd='cat'; "
               " $cmd {input.read} | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > {output.fasta} ; "
               " gzip -t {input.read2} 2>/dev/null; "
               " [[ $? -eq 0 ]]&&   cmd='zcat' || cmd='cat'; "
               " $cmd {input.read2} | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > {output.fasta2}  "
    
