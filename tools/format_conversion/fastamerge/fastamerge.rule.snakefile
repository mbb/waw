rule <step_name>__fastamerge:
        input:
            **<step_name>__fastamerge_inputs(),
        output: 
            fasta = config["results_dir"] + "/" + config["<step_name>__fastamerge_output_dir"] + "/merged.fasta",
        params: 
            output_dir = config["results_dir"] + "/" + config["<step_name>__fastamerge_output_dir"]+ "/",
        log: 
            config["results_dir"] + "/logs/" + config["<step_name>__fastamerge_output_dir"] + "/fastamerge_log.txt"
        shell:
            "cat  {input.fasta_dir}/*.fasta > {output.fasta}"