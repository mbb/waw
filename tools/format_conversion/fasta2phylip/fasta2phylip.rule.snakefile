<step_name>__fasta2phylip_fasta_name = os.path.basename(<step_name>__fasta2phylip_inputs()["fasta"]).split(".")[0]

rule <step_name>__fasta2phylip:
    input:
        **<step_name>__fasta2phylip_inputs(),
    output: 
        phylip = config["results_dir"] + "/" + config["<step_name>__fasta2phylip_output_dir"] + "/" + <step_name>__fasta2phylip_fasta_name + ".phylip",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__fasta2phylip_output_dir"]+ "/",
        command = config["<step_name>__fasta2phylip_command"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__fasta2phylip_output_dir"] + "/fasta2phylip_log.txt"
    script: 
        config["<step_name>__fasta2phylip_script"]