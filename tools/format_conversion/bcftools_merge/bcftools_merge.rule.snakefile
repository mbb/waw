rule <step_name>__bcftools_merge:
        input:
            **<step_name>__bcftools_merge_inputs(),
        output: 
            vcf_file = config["results_dir"] + "/" + config["<step_name>__bcftools_merge_output_dir"] + "/merged.vcf.gz",
        params: 
            output_dir = config["results_dir"] + "/" + config["<step_name>__bcftools_merge_output_dir"]+ "/",
            force_samples =  "--force-samples" if config["<step_name>__bcftools_merge_force_samples"] else "",
        log: 
            config["results_dir"] + "/logs/" + config["<step_name>__bcftools_merge_output_dir"] + "/bcftools_merge_log.txt"
        conda:
            "envs/bed-bcf-vcf-sam-tools.yaml"    
        shell:
            "mkdir -p {params.output_dir}; "
            "vcfs=` ls -1 {input.vcf_dir}/*.vcf{{,.gz}} 2>/dev/null` ;"
            " bcftools merge $vcfs '{params.force_samples}' -O z -o {output.vcf_file} 2>/dev/null"