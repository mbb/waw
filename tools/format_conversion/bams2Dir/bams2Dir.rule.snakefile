
rule <step_name>__bams2Dir:
    input:
        **<step_name>__bams2Dir_inputs(),
    output: 
        bamDir = directory(config["results_dir"] + "/" + config["<step_name>__bams2Dir_output_dir"]) ,
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__bams2Dir_output_dir"]+ "/",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__bams2Dir_output_dir"] + "/_bams2Dir_log.txt"
    shell:
           " mkdir  -p {params.output_dir} && cd {params.output_dir}; "
           " bams=\"{input.bam}\"; "
           " for i in $bams; do ln -s $i .;done"
           
