params=$1 #/workflow/params.total.yml
resultsDir=$2
outDir=$3
condaEnvs=$4
force=$5
until=$6

if [ "$condaEnvs" == "" ]; 
then  
 #use default conda 
 echo "condaEnvs=\$(conda info --system | grep CONDA_PREFIX: | awk -F": " '{print $2}' )" > $outDir/runWF.sh
 #When run from shiny the environment is missing CONDA* environment variables
 echo "if [ "\$condaEnvs" == \"\" ]; " >> $outDir/runWF.sh
 echo "then "  >> $outDir/runWF.sh
 #  #use default conda 
 echo "  condaEnvs='condaEnvs'" >> $outDir/runWF.sh
 echo "fi" >> $outDir/runWF.sh
else
 echo "condaEnvs=\$condaEnvs" >> $outDir/runWF.s  >> $outDir/runWF.sh
fi 

#echo 'mode="bash"' >> $outDir/runWF.sh


#create directories
grep "_output_dir: " $params | awk -F":" '{print "mkdir -p "$2}' >> $outDir/runWF.sh

echo "#uncomment the following if you want to create conda environments ">> $outDir/runWF.sh
echo '#bash create_envs.sh $condaEnvs' >> $outDir/runWF.sh



snakemake -s /workflow/Snakefile --configfile $params -d $resultsDir --use-conda --conda-frontend mamba --conda-prefix /var/www/html/condabin  --cores 20 $force $until -n -p all |  awk '
/Building|^\[|Job stats:|Reasons:/{flag=1;print "#"$0;next}
/total|resources:|This was a dry-run/{flag=0;print "#"$0;next}
{print (flag?"#":"") $0}
' | sed 's/mode=Snakemake/mode=bash/'>> $outDir/runWF.sh

#create conda env

if [ "$condaEnvs" == "" ]; 
then  
 
 echo "condaEnvs=\$1" > $outDir/create_envs.sh
 echo "if [ "\$condaEnvs" == \"\" ]; ">> $outDir/create_envs.sh 
 echo "then" >> $outDir/create_envs.sh 
 #use default conda 
 echo "condaEnvs=\$(conda info --system | grep CONDA_PREFIX: | awk -F": " '{print $2}' )" >> $outDir/create_envs.sh
 #When run from shiny the environment is missing CONDA* environment variables
 echo "if [ "\$condaEnvs" == \"\" ]; " >> $outDir/create_envs.sh
 echo "then " >> $outDir/create_envs.sh
 #  use a dummy dir 
 echo "  condaEnvs='condaEnvs'"  >> $outDir/create_envs.sh 
 echo "fi" >> $outDir/create_envs.sh 
 echo "fi" >> $outDir/create_envs.sh 
else
 echo "condaEnvs=\$condaEnvs"  >> $outDir/create_envs.sh 
fi

#our modified snakemake version use /var/www/html/condabin for  containerize (see waw/Docker_bases/snakemake-containerize.py)
snakemake -s /workflow/Snakefile --configfile  $params results_dir=/tmp/dummy -d /tmp/dummy --use-conda --conda-frontend mamba  --cores 20 all --containerize | sed "s|/var/www/html/condabin|condaEnvs|g" | sed 's|../../workflow|/workflow|' | sed 's/RUN //' | sed 's/COPY/cp/'| grep -v "FROM\|LABEL\|ln -s" | \
awk '
{
    if(index($0,"#   prefix:") == 1) 
    { 
        hash = $0
        split($0, a, ": condaEnvs/")
        getline
        name = $0
        split($0, b, ": ")
        name = gensub("#", "", "g", name )
        hash = gensub(a[2], b[2], "g", hash )
        hash = gensub("#", "", "g", hash )
        print "#" hash "\n" name
        dico[a[2]] = b[2]
    }
    else {
        $0 = gensub("condaEnvs", "$condaEnvs", "g", $0 )
        if (index($0,"# Conda environment") == 1) print "read -r -d \"\" VAR <<\"EOF\""
        else
        {
            hash = $0
            if(index(hash,"mkdir -p") == 1)
            {
              for (i in dico)  { hash = gensub(i, dico[i], "g", hash ) }
              print "EOF"
              print hash
            }
            else
            {
              if(index($0,"#") == 1 ) {if(index($0,"Step ") == 0 && index($0,"source: ") == 0) hash = gensub("#", "", "g", hash ) }
              else {
                for (i in dico)  { hash = gensub(i, dico[i], "g", hash ) }
                if (index(hash, ".post-deploy.sh") != 0) {
                  match(hash, /cp (\/workflow\/envs\/.*\.post-deploy\.sh) /, a)
                  file=a[1]
                  print "read -r -d \"\" VAR <<\"EOF\" "
                  while(( getline line< file) > 0 ) {
                  print line
                  }
                  print "EOF"
                  hash = gensub(/cp \/workflow\/envs\/.*\.post-deploy.sh /, "echo \"$VAR\" > ", "g", hash )
                }
                else{
                    hash = gensub(/cp \/workflow\/envs\/.*\.yaml /, "echo \"$VAR\" > ", "g", hash )
                  }
                }
              print hash            
            }
            
        }
    }
}' >> $outDir/create_envs.sh

#zip params_and_bash_scripts.zip $params $outDir/runWF.sh $outDir/create_envs.sh 
