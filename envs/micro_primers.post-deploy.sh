cd /var/www/html/condabin/
if [ -d /var/www/html/condabin/micro-primers/ ]; 
then 
 cd /var/www/html/condabin/micro-primers/
 git pull
else
 git clone https://gitlab.mbb.cnrs.fr/mbb/mbb_micro-primers.git;
fi

