export CFLAGS=-I$CONDA_PREFIX/include
export LDFLAGS=-L$CONDA_PREFIX/lib
export LD_LIBRARY_PATH=-L$CONDA_PREFIX/lib
cd $CONDA_PREFIX/share
wget https://kumisystems.dl.sourceforge.net/project/est-usfs/est-sfs-release-2.04.tar.gz
tar -xf est-sfs-release-2.04.tar.gz
cd est-sfs-release-2.04
make
cp est-sfs $CONDA_PREFIX/bin


