conda activate fastructure \
&& rep=`dirname $(which structure.py)` \
&& sed -i '2iimport matplotlib as mpl' $rep/distruct.py \
&& sed -i '3impl.use(\"svg\")' $rep/distruct.py  
