cd /var/www/html/condabin/
if [ -d /var/www/html/condabin/mbb_ssrgenotyper/ ]; 
then 
 cd /var/www/html/condabin/mbb_ssrgenotyper/
 git pull
else
 git clone https://gitlab.mbb.cnrs.fr/mbb/mbb_ssrgenotyper.git
fi