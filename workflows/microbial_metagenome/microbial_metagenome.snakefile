{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

# Tools inputs functions
def quality__trimmomatic_SE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	return inputs

def quality__trimmomatic_PE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	inputs["read2"] = raw_reads["read2"]
	return inputs

def assembly__megahit_SE_inputs():
	inputs = dict()
	inputs["read"] = expand(rules.quality__trimmomatic_SE.output.read, sample = SAMPLES)
	return inputs

def assembly__megahit_PE_inputs():
	inputs = dict()
	inputs["read"] = expand(rules.quality__trimmomatic_PE.output.readFP, sample = SAMPLES)
	inputs["read2"] = expand(rules.quality__trimmomatic_PE.output.readRP, sample = SAMPLES)
	return inputs

def metagenomic__abricate_inputs():
	inputs = dict()
	if config["SeOrPe"] == "SE":
		inputs["contigs"] = rules.assembly__megahit_SE.output.contigs
	elif config["SeOrPe"] == "PE":
		inputs["contigs"] = rules.assembly__megahit_PE.output.contigs
	return inputs

def metagenomic_2__metabat_inputs():
	inputs = dict()
	if config["SeOrPe"] == "SE":
		inputs["contigs"] = rules.assembly__megahit_SE.output.contigs
	elif config["SeOrPe"] == "PE":
		inputs["contigs"] = rules.assembly__megahit_PE.output.contigs
	return inputs

def assembly_quality__quast_inputs():
	inputs = dict()
	if config["SeOrPe"] == "SE":
		inputs["assembly"] = rules.assembly__megahit_SE.output.contigs
	elif config["SeOrPe"] == "PE":
		inputs["assembly"] = rules.assembly__megahit_PE.output.contigs
	return inputs


{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
	outputs = list()
	if (step == "quality"):
		if (config['SeOrPe'] == 'SE'):
			outputs = expand(rules.quality__trimmomatic_SE.output, sample = SAMPLES)
		elif (config['SeOrPe'] == 'SE'):
			outputs = expand(rules.quality__trimmomatic_PE.output, sample = SAMPLES)

	if (step == "assembly"):
		if (config['SeOrPe'] == 'SE'):
			outputs = rules.assembly__megahit_SE.output
		elif (config['SeOrPe'] == 'PE'):
			outputs = rules.assembly__megahit_PE.output
		
	if (step == "metagenomic"):
		outputs = rules.metagenomic__abricate.output
		
	if (step == "metagenomic_2"):
		outputs = rules.metagenomic_2__metabat.output

	if (step == "assembly_quality"):
		outputs = rules.assembly_quality__quast.output
		
	if (step == "all"):
		outputs = list(rules.multiqc.output)

	return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
	outputs = list()
	outputs.extend(step_outputs(step))
	return outputs

#########
# Rules #
#########

{import rules}
{import global_rules}
