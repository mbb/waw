{import global_imports}

##########
# Inputs #
##########


# Tools inputs functions
def dadi_inputs():
	inputs = dict()
	inputs["fs_file_name"] = rules.simul_coal.output.msout
	return inputs

def moments_inputs():
	inputs = dict()
	inputs["fs_file_name"] = rules.simul_coal.output.msout
	return inputs


{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
	outputs = list()
	if (step == "estimate_model_params_dadi"  ):
		outputs = rules.dadi.output

	if (step == "estimate_model_params_moments"  ):
		outputs = rules.moments.output
		
	if (step == "all"):
		outputs = list(rules.multiqc.output)

	return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
	outputs = list()
	outputs.extend(step_outputs(step))
	return outputs

#########
# Rules #
#########

{import rules}
{import global_rules}
