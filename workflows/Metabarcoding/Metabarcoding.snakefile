{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

# Tools inputs functions

def trimming__cutadapt_PE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    inputs["read2"] = raw_reads['read2']
    return inputs

def trimming__cutadapt_SE_inputs():
    inputs["read"] = raw_reads['read']
    return inputs

def dada2__dada2_inputs():
    inputs = dict()
    if config["trimming"] == 'null':
        inputs["read"] = expand(raw_reads["read"],sample=SAMPLES)
        if (config["SeOrPe"] == "PE"):
            inputs["read2"] = expand(raw_reads["read2"],sample=SAMPLES)
    if config["trimming"] == 'cutadapt':
        if (config["SeOrPe"] == "SE"):
            inputs["read"] = expand(rules.trimming__cutadapt_SE.output[0],sample=SAMPLES)
            inputs["cutadapt_stats"] = rules.trimming__cutadapt_stats.output.stats
        elif (config["SeOrPe"] == "PE"):
            inputs["read"] = expand(rules.trimming__cutadapt_PE.output[0],sample=SAMPLES)
            inputs["read2"] = expand(rules.trimming__cutadapt_PE.output[1],sample=SAMPLES)
            inputs["cutadapt_stats"] = rules.trimming__cutadapt_stats.output.stats
    return inputs

def dada2__dada2_learn_errors_inputs():
    inputs = dict()
    if config["dada2__dada2_no_filter_and_trim"] == False:
        inputs["filtFs"] = expand(rules.dada2__dada2_filter_and_trim.output.filtFs, sample=SAMPLES),
        inputs["filtRs"] = expand(rules.dada2__dada2_filter_and_trim.output.filtRs, sample=SAMPLES),
    elif (config["trimming"] == "cutadapt"):
        if (config["SeOrPe"] == "SE"):
            inputs["filtFs"] = expand(rules.trimming__cutadapt_SE.output.read_trimmed_trimmed,sample=SAMPLES)
            inputs["cutadapt_stats"] = rules.trimming__cutadapt_stats.output.stats
        elif (config["SeOrPe"] == "PE"):
            inputs["filtFs"] = expand(rules.trimming__cutadapt_PE.output.read_trimmed,sample=SAMPLES)
            inputs["filtRs"] = expand(rules.trimming__cutadapt_PE.output.read2_trimmed,sample=SAMPLES)
            inputs["cutadapt_stats"] = rules.trimming__cutadapt_stats.output.stats
    else: # no trimming
        inputs["filtFs"] = expand(raw_reads["read"],sample=SAMPLES)
        if (config["SeOrPe"] == "PE"):
            inputs["filtRs"] = expand(raw_reads["read2"],sample=SAMPLES)
    return inputs

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "trimming"):
        if (config[step] == "cutadapt"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.trimming__cutadapt_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.trimming__cutadapt_SE.output,sample=SAMPLES)

    elif (step == "dada2"):
        outputs = rules.dada2__dada2_quality.output
        outputs.append(expand(rules.dada2__dada2_filter_and_trim.output,sample=SAMPLES))
        outputs.append(rules.dada2__dada2_learn_errors.output)
        outputs.append(rules.dada2__dada2_construct_seqtab.output)
        outputs.append(rules.dada2__dada2_remove_chimeras.output)
        outputs.append(rules.dada2__dada2_assign_taxonomy.output)
        outputs.append(rules.dada2__dada2_phyloseq_run.output)
        outputs.append(rules.dada2__dada2_export_qiime.output)
        outputs.append(rules.dada2__dada2_krona.output)

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}