{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

# Tools inputs functions

def reads():
    inputs = dict()
    if (config["SeOrPe"] == "SE"):
        inputs["read"] = raw_reads['read']
    elif (config["SeOrPe"] == "PE"):
        inputs["read"] = raw_reads['read']
        inputs["read2"] = raw_reads['read2']
    return inputs

def fastp_inputs():
    return reads()

def bwa_mem_SE_inputs():
    inputs = dict()
    if (config["preprocess"] == "fastp"):
        inputs["read"] = rules.fastp_SE.output.read,
    else:
        return reads()
    return inputs

def bwa_mem_PE_inputs():
    inputs = dict()
    if (config["preprocess"] == "fastp"):
        inputs["read"] = rules.fastp_PE.output.R1,
        inputs["read2"] =  rules.fastp_PE.output.R2,
    else:
        return reads()
    return inputs

def mapping__bowtie_PE_inputs():
    inputs = dict()
    if (config["preprocess"] == "fastp"):
        inputs["read"] = rules.preprocess__fastp_PE.output.R1
        inputs["read2"] =  rules.preprocess__fastp_PE.output.R2
    else:
        return reads()
    return inputs

def mapping__bowtie_SE_inputs():
    inputs = dict()
    if (config["preprocess"] == "fastp"):
        inputs["read"] = rules.preprocess__fastp_SE.output.read
    else:
        return reads()
    return inputs

def Picard_MarkDuplicates_inputs():
    inputs = dict()
    if (config["mapping"] == "bwa"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.bwa_mem_PE.output.bam
        else:
            inputs["bam"] = rules.bwa_mem_SE.output.bam
    elif (config["mapping"] == "bowtie"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.bowtie_PE.output.bam
        else:
            inputs["bam"] = rules.bowtie_SE.output.bam
    return inputs

def gatk_IndelRealigner_inputs():
    inputs = dict()
    if (config["mapping"] == "bwa"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.bwa_mem_PE.output.bam
        else:
            inputs["bam"] = rules.bwa_mem_SE.output.bam
    elif (config["mapping"] == "bowtie"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.bowtie_PE.output.bam
        else:
            inputs["bam"] = rules.bowtie_SE.output.bam
    if (config["mark_duplicates"] == "Picard_MarkDuplicates"):
        inputs["bam"] = rules.Picard_MarkDuplicates.output.sorted_bam
    return inputs

def gatk_haplotype_caller_inputs():
    inputs = dict()
    if (config["mapping"] == "bwa"):
        if (config["SeOrPe"] == "PE"):
            inputs["bams"] = expand(rules.bwa_mem_PE.output.bam,sample=SAMPLES)
        else:
            inputs["bams"] = expand(rules.bwa_mem_SE.output.bam,sample=SAMPLES)
    elif (config["mapping"] == "bowtie"):
        if (config["SeOrPe"] == "PE"):
            inputs["bams"] = expand(rules.bowtie_PE.output.bam,sample=SAMPLES)
        else:
            inputs["bams"] = expand(rules.bowtie_SE.output.bam,sample=SAMPLES)
    if (config["indel_realign"] == "gatk_IndelRealigner"):
        inputs["bams"] = expand(rules.gatk_IndelRealigner.output.sorted_bam,sample=SAMPLES)
    elif (config["mark_duplicates"] == "Picard_MarkDuplicates"):
        inputs["bams"] = expand(rules.Picard_MarkDuplicates.output.sorted_bam,sample=SAMPLES)
    
    return inputs

def bcftools_mpileup_and_call_inputs():
    return gatk_haplotype_caller_inputs()

def deep_variant_inputs():
    return gatk_haplotype_caller_inputs()

def freebayes_inputs():
    return gatk_haplotype_caller_inputs()

def vcf_plot_dv_inputs():
    inputs = dict()
    vcfs = list()
    if (config["variant_calling_gatk"] == "gatk_haplotype_caller"):
        vcfs.append(rules.gatk_haplotype_caller.output.vcf)
    if (config["variant_calling_bcftools"] == "bcftools_mpileup"):
        vcfs.append(rules.bcftools_mpileup_and_call.output.vcf)
    if (config["variant_calling_deepvariant"] == "deep_variant"):
        vcfs.append(rules.deep_variant.output.vcf)
    if (config["variant_calling_freebayes"] == "freebayes"):
        vcfs.append(rules.freebayes.output.vcf)

    if (config["compare_vcfs"] == "compare_vcfs_isec"):
        vcfs.append(rules.compare_vcfs_isec.output.common_vcf)

    inputs["vcfs"] = vcfs
    return inputs

def vcfparser_inputs():
    return vcf_plot_dv_inputs()

def compare_vcfs_isec_inputs():
    inputs = dict()
    vcfs = list()
    if (config["variant_calling_gatk"] == "gatk_haplotype_caller"):
        vcfs.append(rules.gatk_haplotype_caller.output.vcf)
    if (config["variant_calling_bcftools"] == "bcftools_mpileup"):
        vcfs.append(rules.bcftools_mpileup_and_call.output.vcf)
    if (config["variant_calling_deepvariant"] == "deep_variant"):
        vcfs.append(rules.deep_variant.output.vcf)
    if (config["variant_calling_freebayes"] == "freebayes"):
        vcfs.append(rules.freebayes.output.vcf)

    inputs["vcfs"] = vcfs
    return inputs

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "preprocess"):
        if (config[step] == "fastp"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.fastp_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.fastqc_SE.output,sample=SAMPLES)

    elif (step == "mapping"):
        if (config[step] == "bwa"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.bwa_mem_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.bwa_mem_SE.output,sample=SAMPLES)
        elif (config[step] == "bowtie"):
            if (config["SeOrPe"] == "PE"):
                outputs = expand(rules.bowtie_PE.output.bam,sample=SAMPLES)
            else:
                outputs = expand(rules.bowtie_SE.output.bam,sample=SAMPLES)

    elif (step == "mark_duplicates"):
        if (config[step] == "Picard_MarkDuplicates"):
            outputs = expand(rules.Picard_MarkDuplicates.output,sample=SAMPLES)

    elif (step == "indel_realign"):
        if (config[step] == "gatk_IndelRealigner"):
            outputs = expand(rules.gatk_IndelRealigner.output,sample=SAMPLES)

    elif (step == "variant_calling_gatk"):
        if(config[step] == "gatk_haplotype_caller"):
            outputs = rules.gatk_haplotype_caller.output
    elif (step == "variant_calling_bcftools"):
        if(config[step] == "bcftools_mpileup"):
            outputs = rules.bcftools_mpileup_and_call.output
    elif (step == "variant_calling_deepvariant"):
        if(config[step] == "deep_variant"):
            outputs = rules.deep_variant.output
    elif (step == "variant_calling_freebayes"):
        if(config[step] == "freebayes"):
            outputs = rules.freebayes.output

    elif (step == "plot_vcfs"):
        if(config[step] == "vcf_plot_dv"):
            outputs = rules.vcf_plot_dv.output
        if(config[step] == "vcfparser"):
            outputs = rules.vcfparser.output

    elif (step == "compare_vcfs"):
        if(config[step] == "compare_vcfs_isec"):
            outputs = rules.compare_vcfs_isec.output

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}