
import collections

rule prepare_report:
    input:
        *prepare_report_inputs(),
    output:
        *prepare_report_outputs(),
        params_tab = config["results_dir"] + "/params_tab_mqc.csv",
        versions_tab = config["results_dir"] + "/Tools_version_mqc.csv",
        citations_tab = config["results_dir"] + "/Citations_mqc.csv",
        config_multiqc = config["results_dir"] + "/config_multiqc.yaml",
    params:
        params_file = workflow.overwrite_configfiles,
        results_dir = config["results_dir"]
    log:
        config["results_dir"]+"/logs/prepare_report_log.txt"
    shell:
        """
        python3 {snake_dir}/generate_multiqc_config.py {params.params_file} {output.config_multiqc}
        """



def getModules():
        modules_to_run = "-m custom_content "
        for module in MULTIQC.values():
            if (module != "custom"):
                modules_to_run += "-m " + module + " "
        return modules_to_run

rule multiqc:
    input:
        multiqc_inputs(),
        config_multiqc = config["results_dir"] + "/config_multiqc.yaml"
    output:
        multiqc_dir = directory(config["results_dir"]+"/multiqc_data"),
        multiqc_report = config["results_dir"]+"/multiqc_report.html"
    params:
        output_dir = config["results_dir"],
        modules_to_run = getModules()
    log:
        config["results_dir"]+'/logs/multiqc/multiqc_log.txt'
    shell:
        """
        multiqc -d -dd 2 --config {input.config_multiqc} -o {params.output_dir} -f {params.output_dir} {params.modules_to_run} |& tee {log}
        sed -i 's/<meta charset=\"utf-8\">/<meta charset=\"utf-8\">\\n<meta http-equiv=\"refresh\" content=\"3000\">/' {params.output_dir}/multiqc_report.html
        """

        

# Final Snakemake rule waiting for outputs of the final step choosen by user (default all steps)
rule all:
    input:
        step_outputs("all")
    output:
        Snakefile = config["results_dir"]+"/workflow/Snakefile",
        scripts   = directory(config["results_dir"]+"/workflow/scripts"),
        params    = config["results_dir"]+"/workflow/params.yml",
        json      = config["results_dir"]+"/workflow/workflow.json",
    params:
        params_file = workflow.overwrite_configfiles,
        results_dir = config["results_dir"]
    shell:
        "cp /workflow/Snakefile {output.Snakefile} && "
        "if [ -d /workflow/scripts ]; then cp  -r /workflow/scripts {output.scripts} ; else mkdir -p {output.scripts}; fi  &&"
        "cp {params.params_file} {output.params} && "
        "if [ -f /workflow/*.json ]; then cp /workflow/*.json {output.json}; else touch {output.json}; fi && "
        "cd -P /workflow/../.. && zip -r -q {params.results_dir}/WF.zip * && cd -"

onsuccess:
    print("Workflow finished with SUCCESS")
    shell("touch "+config["results_dir"]+"/logs/workflow_end.ok && rm -f "+config["results_dir"]+"/logs/workflow.running")

onerror:
    print("An ERROR occurred")
    shell("cat {log} > "+config["results_dir"]+"/logs/workflow_end.error && rm -f "+config["results_dir"]+"/logs/workflow.running")
    #shell("mail -s "an error occurred" youremail@provider.com < {log}")