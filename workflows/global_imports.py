import os
import re
import snakemake.utils
import csv

import sys
snake_dir = workflow.basedir
if '--configfile' in sys.argv:
    i = sys.argv.index('--configfile')
elif '--configfiles' in sys.argv:
    i = sys.argv.index('--configfiles')
conf_path = sys.argv[i+1]

#############
# Wildcards #
#############
#le nom des sample et les id ne doivent pas contenir de "/" cela evite de boucler dans les arborescences
wildcard_constraints:
        sample="[^\/]+",
        id="[^\/]+"

STEPS = config["steps"]
PREPARE_REPORT_OUTPUTS = config["prepare_report_outputs"]
PREPARE_REPORT_SCRIPTS = config["prepare_report_scripts"]
OUTPUTS = config["outputs"]
PARAMS_INFO = config["params_info"]
MULTIQC = config["multiqc"]
STOP_CASES = config["stop_cases"]
config = config["params"]