{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_long_reads = raw_long_reads(config['results_dir'], config['sample_dir'])
config.update(raw_long_reads)
SAMPLES = raw_long_reads['samples']

# Tools inputs functions
def find_overlaps__minimap2_overlap_self_inputs():
	inputs = dict()
	inputs["read"] = expand(raw_long_reads["read"],sample=SAMPLES)
	return inputs

def assembly__miniasm_inputs():
	inputs = dict()
	inputs["reads"] = expand(raw_long_reads["read"],sample=SAMPLES)
	inputs["paf"] = rules.find_overlaps__minimap2_overlap_self.output.reads_overlaps
	return inputs

def mapping__minimap2_reference_inputs():
	inputs = dict()
	inputs["reads"] = expand(raw_long_reads["read"],sample=SAMPLES)
	inputs["fasta"] = rules.assembly__miniasm.output.assembly_fasta
	return inputs

def correction__racon_inputs():
	inputs = dict()
	inputs["reads"] = expand(raw_long_reads["read"],sample=SAMPLES)
	inputs["assembly"] = rules.assembly__miniasm.output.assembly_fasta
	inputs["overlaps"] = rules.mapping__minimap2_reference.output.reads_mapping
	return inputs

def polishing__medaka_inputs():
	inputs = dict()
	inputs["reads"] = expand(raw_long_reads["read"],sample=SAMPLES)
	inputs["assembly_fasta"] = rules.correction__racon.output.assembly_corrected
	return inputs

def assembly_quality__quast_inputs():
	inputs = dict()
	inputs["assembly"] = rules.polishing__medaka.output.consensus_assembly
	return inputs

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
	outputs = list()
	if (step == "find_overlaps"):
		outputs = rules.find_overlaps__minimap2_overlap_self.output
		
	if (step == "assembly"):
		outputs = rules.assembly__miniasm.output
		
	if (step == "mapping"):
		outputs = rules.mapping__minimap2_reference.output
		
	if (step == "correction"):
		outputs = rules.correction__racon.output

	if (step == "polishing"):
		outputs = rules.polishing__medaka.output

	if (step == "assembly_quality"):
		outputs = rules.assembly_quality__quast.output
		
	if (step == "all"):
		outputs = list(rules.multiqc.output)

	return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
	outputs = list()
	outputs.extend(step_outputs(step))
	return outputs

#########
# Rules #
#########

{import rules}
{import global_rules}
