{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

## get reads (trimmed or raw)
def reads():
    inputs = dict()
    if (config["trimming"] == "null"):
        if (config["SeOrPe"] == "SE"):
            inputs["read"] = raw_reads['read']
        elif (config["SeOrPe"] == "PE"):
            inputs["read"] = raw_reads['read']
            inputs["read2"] = raw_reads['read2']
    elif (config["trimming"] == "trimmomatic"):
        if (config["SeOrPe"] == "SE"):
            inputs["read"] = rules.trimming__trimmomatic_SE.output.read_clean
        elif (config["SeOrPe"] == "PE"):
            inputs["read"] = rules.trimming__trimmomatic_PE.output.readFP
            inputs["read2"] = rules.trimming__trimmomatic_PE.output.readRP
    return inputs

def get_groups():
    with open(config["group_file"], mode="r") as infile:
        reader = csv.reader(infile,delimiter='\t')
        return {row[0]:row[1] for row in reader}

config["groups"] = get_groups()

# Tools inputs functions
def quality_check__fastqc_SE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	return inputs

def quality_check__fastqc_PE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	inputs["read2"] = raw_reads["read2"]
	return inputs

def trimming__trimmomatic_SE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    return inputs

def trimming__trimmomatic_PE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    inputs["read2"] = raw_reads['read2']
    return inputs

def indexing__kallisto_index_inputs():
    inputs = dict()
    inputs["transcriptome_fasta"] = config["indexing__kallisto_index_transcriptome_fasta"]
    return inputs

def indexing__salmon_index_inputs():
    inputs = dict()
    inputs["transcriptome_fasta"] = config["indexing__salmon_index_transcriptome_fasta"]
    return inputs

def quantification__kallisto_quant_SE_inputs():
    inputs = dict()
    inputs["read"] = reads()["read"]
    inputs["index"] = rules.indexing__kallisto_index.output.index
    return inputs

def quantification__kallisto_quant_PE_inputs():
    inputs = dict()
    inputs["read"] = reads()["read"]
    inputs["read2"] = reads()["read2"]
    inputs["index"] = rules.indexing__kallisto_index.output.index
    return inputs

def quantification__salmon_quant_SE_inputs():
    inputs = dict()
    inputs["read"] = reads()["read"]
    inputs["index"] = rules.indexing__salmon_index.output.index
    return inputs

def quantification__salmon_quant_PE_inputs():
    inputs = dict()
    inputs["read"] = reads()["read"]
    inputs["read2"] = reads()["read2"]
    inputs["index"] = rules.indexing__salmon_index.output.index
    return inputs

def DE__edger_inputs():
    inputs = dict()
    if (config["quantification"] == 'kallisto'):
        if (config["SeOrPe"] == "PE"):
            inputs["counts"] = expand(rules.quantification__kallisto_quant_PE.output.counts,sample=SAMPLES)
        else:
            inputs["counts"] = expand(rules.quantification__kallisto_quant_SE.output.counts,sample=SAMPLES)

    elif (config["quantification"] == 'salmon'):
        if (config["SeOrPe"] == "PE"):
            inputs["counts"] = expand(rules.quantification__salmon_quant_PE.output.counts,sample=SAMPLES)
        else:
            inputs["counts"] = expand(rules.quantification__salmon_quant_SE.output.counts,sample=SAMPLES)

    return inputs

def DE__deseq2_inputs():
    return DE__edger_inputs()

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "trimming"):
        if (config[step] == "trimmomatic"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.trimming__trimmomatic_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.trimming__trimmomatic_SE.output,sample=SAMPLES)

    elif (step == "quality_check"):
        if (config[step] == "fastqc"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.quality_check__fastqc_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.quality_check__fastqc_SE.output,sample=SAMPLES)

    elif (step == "indexing"):
        if (config[step] == "kallisto_index"):
            outputs = rules.indexing__kallisto_index.output
        elif (config[step] == "salmon_index"):
            outputs = rules.indexing__salmon_index.output

    elif (step == "quantification"):
        if (config[step] == "kallisto"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.quantification__kallisto_quant_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.quantification__kallisto_quant_SE.output,sample=SAMPLES)
        elif (config[step] == "salmon"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.quantification__salmon_quant_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.quantification__salmon_quant_SE.output,sample=SAMPLES)

    elif (step == "DE"):
        if (config[step] == "deseq2"):
            outputs = rules.DE__deseq2.output
        elif (config[step] == "edger"):
            outputs = rules.DE__edger.output

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs
        
# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}