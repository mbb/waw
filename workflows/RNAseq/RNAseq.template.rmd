---
title: "RNAseq pipeline final report"
author: ""
date: "`r Sys.Date()`"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: true
    theme: lumen
params:
  params_file: "params.yml"
---

```{r setup, include=FALSE}
library(yaml)
knitr::opts_chunk$set(echo = FALSE,warning = FALSE, message = FALSE)
parameters = read_yaml(params$params_file)
```

# Steps

