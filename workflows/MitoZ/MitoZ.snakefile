{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

# Tools inputs functions

def trimming__cutadapt_PE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    inputs["read2"] = raw_reads['read2']
    return inputs

def trimming__cutadapt_SE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    return inputs

def filter__mitoz_filter_SE_inputs():
    inputs = dict()
    if config["trimming"] == 'null':
        inputs["read"] = raw_reads["read"]
    if config["trimming"] == 'cutadapt':
        inputs["read"] = rules.trimming__cutadapt_SE.output.read_trimmed
    return inputs

def filter__mitoz_filter_PE_inputs():
    inputs = dict()
    if config["trimming"] == 'null':
        inputs["read"] = raw_reads["read"]
        inputs["read2"] = raw_reads["read2"]
    if config["trimming"] == 'cutadapt':
        inputs["read"] = rules.trimming__cutadapt_PE.output.read_trimmed
        inputs["read2"] = rules.trimming__cutadapt_PE.output.read2_trimmed
    return inputs

def assemble__mitoz_assemble_SE_inputs():
    inputs = dict()
    if (config["filter"] == 'null'):
        return filter__mitoz_filter_SE_inputs()
    else:
        inputs["read"] = expand(rules.filter__mitoz_filter_SE.output.read_filtered, sample=SAMPLES)
        return inputs

def assemble__mitoz_assemble_PE_inputs():
    inputs = dict()
    if (config["filter"] == 'null'):
        return filter__mitoz_filter_PE_inputs()
    else:
        inputs["read"] = expand(rules.filter__mitoz_filter_PE.output.read_filtered, sample=SAMPLES)
        inputs["read2"] = expand(rules.filter__mitoz_filter_PE.output.read2_filtered, sample=SAMPLES)
        return inputs

def annotate__mitoz_annotate_SE_inputs():
    inputs = dict()
    inputs["fastafile"] = rules.assemble__mitoz_assemble_SE.output.mitogenome
    if (config["filter"] == 'null'):
        inputs.update(filter__mitoz_filter_SE_inputs())
    else:
        inputs["read"] = expand(rules.filter__mitoz_filter_SE.output.read_filtered, sample=SAMPLES)
    return inputs

def annotate__mitoz_annotate_PE_inputs():
    inputs = dict()
    inputs["fastafile"] = rules.assemble__mitoz_assemble_PE.output.mitogenome
    if (config["filter"] == 'null'):
        inputs.update(filter__mitoz_filter_PE_inputs())
    else:
        inputs["read"] = expand(rules.filter__mitoz_filter_PE.output.read_filtered, sample=SAMPLES)
        inputs["read2"] = expand(rules.filter__mitoz_filter_PE.output.read2_filtered, sample=SAMPLES)
    return inputs

def visualize__igv_visualize_inputs():
    inputs = dict()
    if (config["SeOrPe"] == "PE"):
        inputs["circos_bam"] = rules.annotate__mitoz_annotate_PE.output.circos_bam
        inputs["genome"] = rules.assemble__mitoz_assemble_PE.output.mitogenome
    else:
        inputs["circos_bam"] = rules.annotate__mitoz_annotate_SE.output.circos_bam
        inputs["genome"] = rules.assemble__mitoz_assemble_SE.output.mitogenome
    return inputs


{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "trimming"):
        if (config[step] == "cutadapt"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.trimming__cutadapt_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.trimming__cutadapt_SE.output,sample=SAMPLES)

    elif (step == "filter"):
        if (config["filter"] == 'mitoz_filter'):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.filter__mitoz_filter_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.filter__mitoz_filter_SE.output,sample=SAMPLES)

    elif (step == "assemble"):
        if(config["SeOrPe"] == "PE"):
            outputs = rules.assemble__mitoz_assemble_PE.output
        else:
            outputs = rules.assemble__mitoz_assemble_SE.output

    #elif (step == "findmitoscaf"):
    #    if(config["SeOrPe"] == "PE"):
    #        outputs = rules.findmitoscaf__mitoz_findmitoscaf.output

    elif (step == "annotate"):
        if(config["SeOrPe"] == "PE"):
            outputs = rules.annotate__mitoz_annotate_PE.output
        else:
            outputs = rules.annotate__mitoz_annotate_SE.output

    if (step == "visualize"):
        if(config["SeOrPe"] == "PE"):
            outputs = rules.visualize__igv_visualize.output

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}
