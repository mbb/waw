# Metabarcoding workflow with Dada2_PE

## Introduction

Dada2_PE workflow do a metabarcoding analysis with [Dada2](https://benjjneb.github.io/dada2/index.html) R package on amplicon sequencing dataset. The workflow resumes the diversity of samples by infering clusters abundance table and assigning a taxonomy to clusters thanks to available database from http://genoweb.toulouse.inra.fr/frogs_databanks/assignation. The specificity of dada2 is on the inference of clusters know as ASV (Amplicon Sequence Variants). ASV are infered with single nucleotide resolution by correcting errors introduced into sequenced fragments and resolving biological differences (denoising).

## Usage

Needs docker, singularity/apptainer and git (snakemake) installed.

### Use Dada2_PE workflow by using MBBworkflow.

- Follow the [installation procedure of MBBworkflow](https://gitlab.mbb.cnrs.fr/mbb/subwaw#install).
- Use the Dada2_PE workflow by following the instruction of [connexion](https://gitlab.mbb.cnrs.fr/mbb/subwaw#a--connexion-to-the-subwaw-graphical-interface) and [usage](https://gitlab.mbb.cnrs.fr/mbb/subwaw#b--use-available-workflow).

### Use Dada2_PE workflow without installation of MBBworkflow.

By using this usage it's not possible to modify the different step of the workflow.

#### Via graphical interface (shiny app)

**(Create a dedicate readme for this)**

- Clone gitlab of the workflow and go in the directory
```
git clone ...
cd MBB_dada2
```
- Deploy the environment by running the deployLocalHost.sh script.

```
bash deployLocalHost /path/to/directory/where/data/are /path/where/results/will/be/write local
```

- Use the workflow by connecting to the link 127.0.0.1:3838 in your web browser and by setting the parameter in the shiny page (See [description](#description) part for more information about parameters).
- "Check worflow validity" and "Run pipeline" 

#### Command line (Singularity image)

**(A développer possiblité d'utiliser MBB workflow en ligne de commande grace a une image singularity : l'image doit contenir les yaml des env, snakemake, les scripts, ...)**

It's also possible to use the workflow by command line and without web interface (shiny app). To do this instead of deploy the environment you have to use the singularity image of the workflow :
- Download Dada2_PE.sif image.
- Set parameter in the params.yaml file
- Run snakemake by using the singularity image
```
singularity exec dada2.sif snakemake -s Snakefile --configfile params.yaml --use-conda -j {threads} -p -r -d . all
```
- Or run via the containerized option (Needs containerized: workflow.basedir+"/Singularity.sif" in the snakefile and snakemake install in the computer).
```
snakemake -s Snakefile --configfile params.yaml --use-singularity --use-conda -j {threads} -p -r -d . all
```

### On a HPC

Write readme for this.

## Description

By default the workflow is done on paired data and assign the taxonomy with [available database](http://genoweb.toulouse.inra.fr/frogs_databanks/assignation), but the workflow can be adapt to use single end data and/or custom database for taxonomic assignment. The workflow needs also a metadata file (popmap) that describe samples (names, controls, features...).  
The workflow consists of 6 steps that can be changed or removed depending on the input data and the desired result. Each step has parameters to set in the shiny application (or in params.yaml) before to run the analysis.

### **dada2_plotQualityProfile PE** :

This step generates a sequencing quality report for each sample. It gives an overview of sequence length and quality evolution (phred score) throughout the sequence. This report can be used to adjust the parameters of the second step, which consists in cleaning the sequences. This step uses the ["plotQualityProfile" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.plotQualityProfile.1)

This step doesn't have parameter.

Main outputs of this step are :
- qualityPlotF_mqc.png : Quality report of forward reads
- qualityPlotR_mqc.png : Quality report of reverse reads

### **dada2_filter_trim PE** : 

This step first, truncates reads to a length for forward and reverse reads.
Then, this step discards all reads :
- with N.
- assign to phiX.
- with a length shorter than a length.
- with a maxEE higher than a threshold.

This step relies on 2 parameters :
- *"TruncLenForward"* and *"TruncLenReverse”* : Truncate forward/reverse reads after *"truncLen"* bases and discards forward/reverse reads shorter than this length. This setting allows to remove low quality tail by discarding too short forward/reverse reads and truncating forward/reverse reads to a specific length. This parameter is used by "truncLen=c(numeric_fwd,numeric_rev)" option in ["filterAndTrim" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.filterAndTrim.1).
- "maxEEforward" and "maxEEreverse” : Discards after truncation, forward/reverse reads with maxEE higher than 'expected errors' sets. Expected errors are calculated from the nominal definition of the quality score: EE = sum(10^(-Q/10)). Set -1 for Infinity (<=> no filtering). This expected error is calculated by summing the error probabilities of each base in the forward/reverse read. Therefore more the maxEE value is low more the filter will be stringent. This parameter is used via "maxEE=c(numeric_fwd,numeric_rev)" option in ["filterAndTrim" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.filterAndTrim.1).


Main outputs of this steps are :
- filtered/{sample}_R1_filtered.fastq.gz : Filtered forward reads at fastq format.
- filtered/{sample}_R2_filtered.fastq.gz : Filtered reverse reads at fastq format.
 
### **dada2_Err_Derep_makeSequenceTable PE** : 

This step clusters sequences into ASVs, the aim being to correct sequencing errors based on error models. This step is divided in multiple parts :  
1. Dereplicates sequences, i.e. groups identical sequences together while retaining abundance information.  
2. Build an error model using parametric models on the sequencing data.  
3. Use the model to denoise the samples and to determine the ASVs.  
4. Merges paired datas with a minimum overlap. Merging is performed by aligning the denoised forward reads with the reverse-complement of the corresponding denoised reverse reads, and then constructing the merged “contig” sequences.  
5. Construct abundance table. An amplicon sequence variant (ASV) table is built with ASV as rows, samples as columns and abundance as value. Then, the abundance is the number of reads that belongs to an ASV and comes from a sample.  
6. Detect and discard chimera.  

Each part of this step relies on parameters :
1. Dereplicates sequences doesn't have parameter. This step uses the ["derepFastq" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.derepFastq.1)

2. Build an error model using parametric models on the sequencing data relies on 2 parameters.  
	- "randomize”: Define the sampling data method to learn the error model. If set on False, samples are read in the provided order until enough reads are obtained. If set on True samples are randomly picked. This parameter is used by "randomize=TRUE/FALSE" option in ["learnErrors" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.learnErrors.1).
	- "nbases”: Define the number of bases (nt) to use for error rate learning. Samples are read into memory until at least this number of total bases has been reached, or all provided samples have been read in. This parameter is used by "nbases=integer" option in ["learnErrors" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.learnErrors.1).

3. Use the model to denoise the samples and to determine the ASVs relies on 1 parameter.
	- "pool": Define how sample inference is processed, pooling information across samples can increase sensitivity. Set False to perform sample inference on each sample individually. Set True to pool together all samples prior to sample inference. Set Pseudo to perform pseudo pooling in which samples are processed independnetly after sharing information between samples. False is faster than pseudo and pseudo is faster than True, but False is less sensitive than pseudo and pseudo is less sensitive than True. This parameter is used by "pool=FALSE/pseudo/TRUE" option in ["dada" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.dada.1).

4. Merges paired datas relies on 1 parameter.
	- "minoverlap": Define the minimum length (in nt) of the overlap required for merging the forward and reverse reads. This step attempts to merge each denoised pair of forward and reverse reads, rejecting any pairs which do not sufficiently overlap. The value of the minimum overlap will depend of the size of the expected amplicon and the reads length. This parameter is used by "minOverlap=integer" option in ["mergePairs" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.mergePairs.1).

5. Construct abundance table doesn't have parameter. This step uses the ["makeSequenceTable" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.makeSequenceTable.1)

6. Detect and discard chimera relies on 1 parameter. 
	- "bimera" : Do you want to remove ASV bimera ? By checking this option you will detect and discard ASV assign as bimera. Chimeric sequences are identified if they can be exactly reconstructed by combining a left-segment and a right-segment from two more abundant “parent” sequences. This parameter uses the ["removeBimeraDenovo" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.isBimeraDenovo.1) with consensus method.

Main outputs of this steps are :
- dada2_Err_Derep_makeSequenceTable/PE/ASVseqs.fasta : Amplicon sequence variants (ASV) in fasta format.
- dada2_Err_Derep_makeSequenceTable/PE/ASVabundance.tsv : TSV table with number of times each exact amplicon sequence was observed in each sample.
- dada2_Err_Derep_makeSequenceTable/PE/errorProfilePlotF_mqc.png : Error profile plot for forward reads.
- dada2_Err_Derep_makeSequenceTable/PE/errorProfilePlotR_mqc.png : Error profile plot for reverse reads.


### **lulu** :

This step filters and groups clusters (ASV/OTU/...) by considering the co-occurrences of similar clusters using [lulu R package](https://github.com/tobiasgf/lulu).

This step relies on 3 parameters :
- "method” : which tool do you want for pairwise matching of clusters (OTU/ASV...) sequences ? LULU creates a match list by aligning clusters (OTU/ASV...) against each other with Blast or Vsearch to identify potential similar clusters. This parameter uses [blastn or vsearch command line](https://github.com/tobiasgf/lulu?tab=readme-ov-file#2-produce-a-match-list).
- "minimum_ratio”: Define the minimum abundance ratio between the 'parent’ OTU and the processed OTU. A ratio of 1 means that the processed OTU cannot be more abundant than the parent OTU for each sample. A ratio of 10 means that the parent OTU must be 10 times more abundant than the treated OTU for each sample. This parameter uses the ["minimum_ratio=value" option in lulu function](https://github.com/tobiasgf/lulu?tab=readme-ov-file#the-algorithm).
- "minimum_match”: Define minimum threshold (%) of sequence similarity between a cluster and its potential parent for considering any OTU as an error of another. Similarity is defined as the edit distance between the two aligned sequences: matching columns divided by the alignment length. If the similarity percentage between a query OTU and a potential parent OTU is strictly smaller than the 'minimum_match' value, then the potential parent OTU is rejected. This parameter uses the ["minimum_ratio=value" option in lulu function](https://github.com/tobiasgf/lulu?tab=readme-ov-file#the-algorithm)

Main outputs of this steps are :
- lulu/curated_seq.fasta : Fasta sequences of the retained clusters (OTUs/ASVs/...).
- lulu/curated_table.tsv : Table of retained cluster (OTUs/ASVs/...).

### **negative control decontamination** :

This step cleans or deletes the clusters (ASV/OTU/...) present in the negative controls using a popmap metadata file containing information on samples that are Tneg. The table can be decontaminate by subtracting abundance found in negative control or by removing cluster found in negative control. The popmap file to identify negative control sample in the dataset must have the following tabulated format (sample column and metadata1 minimum):
|  | Metadata1 | Metadata2 |
| --- | --- | --- |
| Sample1 | sample | popA |
| Sample2 | sample | popA |
| SampleI | sample | popB |
| SampleN | sample | popB |
| Tneg1 | neg | NAN |
| Tneg2 | neg | NAN |


This step relies on 3 parameters:
- "column": Give the column name that contains negative control information from the metadata file (popmap), "Metadata1" in the example above. The script will read this column to determine which samples names are negative control and which sample name are real sample. This parameter uses the "-c column_name" option in "negative_control_decontamination.py" script.
- "name": Give the label name in the specify column to recognize negative control, "neg" in the example above. The script will define as negative control all sample name that have the specify label in the column set. This parameter uses the "-n label_name" option in "negative_control_decontamination.py" script.
- "method": Define the decontamination method to apply on cluster with an abundance in negative control. The "remove" method will delete all cluster with an abundance above 0 in negative control samples. The "subtract" method will subtract for each cluster the value in negative control to all other sample. This parameter uses the "-m remove|subtract" option in "negative_control_decontamination.py" script.

Main outputs of this steps are :
- decontamination/curated_seq.fasta : sequences of the retained clusters (OTUs/ASVs/...).
- decontamination/Abundance_table_filtered.tsv : filtered abundance table file which contains the abundance and optionally metadata for each cluster and sample.

### **dada2_assign_taxonomy** : 

This step makes a taxonomic assignment of ASVs by using database available in FROGS (http://genoweb.toulouse.inra.fr/frogs_databanks/assignation/readme.txt). The algorithm use assignTaxonomy dada2 function and is an adaptation of the RDP classifier software (https://sourceforge.net/projects/rdp-classifier/)

This step relies on 3 parameters:
- “reference_fasta”: Choose the database against which to make the taxonomic assignment. Databases come from [frogs](http://genoweb.toulouse.inra.fr/frogs_databanks/assignation/readme.txt). This parameter is used as the second argument of the ["assignTaxonomy" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.assignTaxonomy.1).
- "tryRC": Try the reverse-complement orientation for assignment. By default the assignment will be done only on the forward orientation. This parameter is used as "tryRC=TRUE|FALSE" of the ["assignTaxonomy" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.assignTaxonomy.1).
- "minBoot”: Define the minimum number of bootstrap to validate taxonomic assignement. The value must be between 0 and 100, with 0 for a low stringency and 100 for a high. This parameter is used as "minBoot=integer" of the ["assignTaxonomy" dada2 function](https://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf#Rfn.assignTaxonomy.1).

Main outputs of this steps are :
- dada2_assign_taxonomy/Taxa_bootstrap_mqc.tsv : Table that give the number of bootstrap obtained for each taxonomy level assignment.
- dada2_assign_taxonomy/taxassign.tsv : Table with each row corresponding to an input sequence, and each column corresponding to a taxonomic level. Main taxonomic level are kingdom, phylum, class, order, family, genus and species.

## Possible Variation/Adaptation for this workflow.

MBB-workflow provides a graphical interface to [adapt or modify existing bioinformatics workflows](https://gitlab.mbb.cnrs.fr/mbb/subwaw#c--modifyadapt-available-workflows).
This workflow can be modified by different way.

### **SE**

It is possible to generate a similar workflow adapted for single-end data. To do this you simply have to swap the blue bricks dada2_plotQualityProfile PE, dada2_filter_trim PE and dada2_Err_Derep_makeSequenceTable PE with the bricks dada2_plotQualityProfile SE, dada2_filter_trim SE and dada2_Err_Derep_makeSequenceTable SE respectively. In addition, the 2 green reads_PE boxes must be replaced by a green reads_SE box.

### **assign taxonomy with a custom database (from other source than FROGS or self build database)**


By default, the workflow uses one of the databases provided by [frogs](http://genoweb.toulouse.inra.fr/frogs_databanks/assignation/readme.txt). However, it is also possible to use your own custom database. To do this, replace the blue dada2_assign_taxonomy box with the dada2_assign_taxonomy_customDB box. This new box in i1 and i2 will use the same inputs as dada2_assign_taxonomy and will require a fasta_file in i3 and a taxonomyDB in i4.

The fasta_file in i3 must have the format :


```
>ID1
GTCGGTAAAATTCGTGCCAGCCACCGCGGTTATACGAAAGACCCTAGTTGATAGCCACGGCGTAAAGGGTGGTTAAGGTACATAATAAATAAAGCTAAATAGCCTCTAAGCCGTCGAACGCATCCTGAGACCTCGAAGCTCAAAAACGAAAGTAGCTTTAAAATATCACACCTGACCCCACGAAAGCTAAGAAACAAACTGGGATTAGATACCCCACTATGC
>ID2
GTCGGTAAAATTCGTGCCAGCCACCGCGGTTATACGAAAGACCCTAGTTGATAGCCACGGCGTAAAGGGTGGTTATGGTAAACAACAAATAAAGCTAAAGAGCCTCTAAGTCGTCGCACACATTCCGAGAACTCGAAATCCAAATACGAAAGTAGCTTTAAAACAAAACACACCTGACTCCACGAAAGCTAAGAAACAAACTGGGATTAGATACCCCACTATGC
>ID3
GTCGGTAAAATTCGTGCCAGCCACCGCGGTTATACGAAAGACCCTAGTTGATAGCCACGGCGTAAAGGGTGGTTAAGGTAAATAATAAATAAAGCTAAAGAGCCTCTAAGTCGTCGCACGCATCCCGAGAGCTCGAAACCCAAACACGAAAGTAGCTTTAAAACAAATACACCTGACCCCACGAAAGCTAAGAAACAAACTGGGATTAGATACCCCACTATGC
>ID4
GTCGGTAAAATTCGTGCCAGCCACCGCGGTTATACGAAAGACCCTAGTTGATAGCCACGGCGTAAAGGGTGGTTAAGGTAACTAATAAATAAAGCTAAAGAGCCTCTAAGCCGTCGCACGCATTCCGAGAGCTCGAAGCCCAAACACGAAAGTAGCTTTAAAACAAAACACACCTGACCCCACGAAAGCTAAGAAACAAACTGGGATTAGATACCCCACTATGC
>ID5
GTCGGTAAAATTCGTGCCAGCCACCGCGGTTATACGAAAGACCCTAGTTGATAGCCACGGCGTAAAGGGTGGTTAAGGTAACTAATAAATAAAGCTAAAGAGCCTCTAAGCCGTCGCACGCATTCCGAGAGCTCGAAGCCCAAACACGAAAGTAGCTTTAAAATAAAGCACACCTGACCCCACGAAAGCTAAGAAACAAACTGGGATTAGATACCCCACTATGC
>ID6
GTCGGTAAAACTCGTGCCAGCCACCGCGGTTATACGAGAGACCCTAATTGATAGCTACGGCGTAAAGAGTGGTTTAGGATAAAAAACAAATAAAGCCAAAGATCCTCCTGGCTGTAGCACGCACTCCGAGGACACGAAGCCCCACCACGAAGGTAGCTTTACCAACATTTCCTGACCCCACGAAAGCTAAGAAACAAACTGGGATTAGATACCCCACTATGA
```

The taxonomyDB file must contain 2 columns, one with the ID present in the fasta file and one with the taxonomy. The taxonomy column has the format “Kingdom;Phylum;Class;Order;Family;Genus;Species”, all taxonomic ranks must be systematically present. It is possible to enter NA, undefined ... for unknown taxonomies.

|   |   |
| --- |---|
| ID1 |  Animalia;Chordata;Actynopterygii;Siluriformes;Pimelodidae;Brachyplatystoma;Brachyplatystoma_filamentosum |
| ID2 |  Animalia;Chordata;Actynopterygii;Siluriformes;NA;NA;NA |
| ID3 |  Animalia;Chordata;Actynopterygii;Siluriformes;Pimelodidae;Brachyplatystoma;Brachyplatystoma_sp |
| ID4 |  Animalia;Chordata;Actynopterygii;Siluriformes;Pimelodidae;Pseudoplatystoma;Pseudoplatystoma_tigrinum |
| ID5 |  Animalia;Chordata;Actynopterygii;Siluriformes;Pimelodidae;Pseudoplatystoma;Pseudoplatystoma_faciatum |
| ID6 | NA;NA;NA;NA;NA;NA;NA |


### **Remove ASV filtered step (lulu, negative control decontamination)**

ASV filtration step are not mandatory, it is possible to remove "lulu" and "negative control decontamination" boxes and to link the "dada2_Err_Derep_makeSequenceTable PE" box to the "dada2_assign_taxonomy" box.

## Possible enhancement

- Error when a sample has all it reads clean by trimming step.
- Verify or add swarm step
- Add other filter like in JAMP in negative control decontamination
- Possibility to build a Phyloseq object
- Add tool for taxonomy assignment (SINTAX, blast + filter)
- Add maxN parameter in trimming step.
- Think how to manage database in assign taxonomy for analysis on HPC.
- Use [mumu](https://github.com/frederic-mahe/mumu) instead of lulu (needs a conda package)

## Citation

This workflow was generated by the [MBB-workflow framework](https://gitlab.mbb.cnrs.fr/mbb/subwaw), if you use this workflow in your work thanks to write it in you Materials & Methods and to use the reference : "Benjamin Penaud, Rémy Dernat, Iago Bonnici and  Khalid Belkhir.  MBB-Framework : a set of tools to build and run reproducible and portable workflows. https://gitlab.mbb.cnrs.fr/mbb/subwaw . 2020. Accessed Day Month Year.".