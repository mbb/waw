{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_bams = raw_bams(config['results_dir'], config['sample_dir'], STOP_CASES)
config.update(raw_bams)
SAMPLES = raw_bams['samples']

# Tools inputs functions

def Picard_MarkDuplicates_inputs():
    inputs = dict()
    inputs["bam"] = raw_bams["bam"]
    return inputs

def gatk_IndelRealigner_inputs():
    inputs = dict()
    if (config["mark_duplicates"] == "Picard_MarkDuplicates"):
        inputs["bam"] = rules.Picard_MarkDuplicates.output.sorted_bam
    else:
        inputs["bam"] = raw_bams["bam"]
    return inputs

def gatk_haplotype_caller_inputs():
    inputs = dict()
    if (config["indel_realign"] == "gatk_IndelRealigner"):
        inputs["bams"] = expand(rules.gatk_IndelRealigner.output.sorted_bam,sample=SAMPLES)
    elif (config["mark_duplicates"] == "Picard_MarkDuplicates"):
        inputs["bams"] = expand(rules.Picard_MarkDuplicates.output.sorted_bam,sample=SAMPLES)
    else:
        inputs["bams"] = expand(raw_bams["bam"],sample=SAMPLES)
    return inputs

def bcftools_mpileup_and_call_inputs():
    return gatk_haplotype_caller_inputs()

def deep_variant_inputs():
    return gatk_haplotype_caller_inputs()

def freebayes_inputs():
    return gatk_haplotype_caller_inputs()

def octopus_inputs():
    return gatk_haplotype_caller_inputs()

def vcf_plot_dv_inputs():
    inputs = dict()
    vcfs = list()
    if (config["variant_calling_gatk"] == "gatk_haplotype_caller"):
        vcfs.append(rules.gatk_haplotype_caller.output.vcf)
    if (config["variant_calling_bcftools"] == "bcftools_mpileup"):
        vcfs.append(rules.bcftools_mpileup_and_call.output.vcf)
    if (config["variant_calling_deepvariant"] == "deep_variant"):
        vcfs.append(rules.deep_variant.output.vcf)
    if (config["variant_calling_freebayes"] == "freebayes"):
        vcfs.append(rules.freebayes.output.vcf)
    if (config["variant_calling_octopus"] == "octopus"):
        vcfs.append(rules.octopus.output.vcf) 
    if (config["compare_vcfs"] == "compare_vcfs_isec"):
        vcfs.append(rules.compare_vcfs_isec.output.common_vcf)

    inputs["vcfs"] = vcfs
    return inputs

def vcfparser_inputs():
    return vcf_plot_dv_inputs()

def compare_vcfs_isec_inputs():
    inputs = dict()
    vcfs = list()
    if (config["variant_calling_gatk"] == "gatk_haplotype_caller"):
        vcfs.append(rules.gatk_haplotype_caller.output.vcf)
    if (config["variant_calling_bcftools"] == "bcftools_mpileup"):
        vcfs.append(rules.bcftools_mpileup_and_call.output.vcf)
    if (config["variant_calling_deepvariant"] == "deep_variant"):
        vcfs.append(rules.deep_variant.output.vcf)
    if (config["variant_calling_freebayes"] == "freebayes"):
        vcfs.append(rules.freebayes.output.vcf)
    if (config["variant_calling_octopus"] == "octopus"):
        vcfs.append(rules.octopus.output.vcf)

    inputs["vcfs"] = vcfs
    return inputs

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "mark_duplicates"):
        if (config[step] == "Picard_MarkDuplicates"):
            outputs = expand(rules.Picard_MarkDuplicates.output,sample=SAMPLES)

    elif (step == "indel_realign"):
        if (config[step] == "gatk_IndelRealigner"):
            outputs = expand(rules.gatk_IndelRealigner.output,sample=SAMPLES)

    elif (step == "variant_calling_gatk"):
        if(config[step] == "gatk_haplotype_caller"):
            outputs = rules.gatk_haplotype_caller.output
    elif (step == "variant_calling_bcftools"):
        if(config[step] == "bcftools_mpileup"):
            outputs = rules.bcftools_mpileup_and_call.output
    elif (step == "variant_calling_deepvariant"):
        if(config[step] == "deep_variant"):
            outputs = rules.deep_variant.output
    elif (step == "variant_calling_freebayes"):
        if(config[step] == "freebayes"):
            outputs = rules.freebayes.output
    elif (step == "variant_calling_octopus"):
        if(config[step] == "octopus"):
            outputs = rules.octopus.output

    elif (step == "plot_vcfs"):
        if(config[step] == "vcf_plot_dv"):
            outputs = rules.vcf_plot_dv.output
        if(config[step] == "vcfparser"):
            outputs = rules.vcfparser.output

    elif (step == "compare_vcfs"):
        if(config[step] == "compare_vcfs_isec"):
            outputs = rules.compare_vcfs_isec.output

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}